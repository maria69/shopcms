/**
 * Created by ali on 6/26/17.
 */
var dashboard = 'dashboard909';

function getMediaTag(mediaObject,m,d,y,realPreviewStatus){
    if($.inArray(mediaObject.extension,["png","jpg","jpeg","gif"]) != -1) {
        console.log('/media/'+y+'/'+m+'/'+d+'/'+mediaObject.hash+'.'+mediaObject.extension);
        var mediaTags = '<img src="/media/'+y+'/'+m+'/'+d+'/'+mediaObject.hash+'.'+mediaObject.extension+'" class="img-responsive fullwidth" alt="'+mediaObject.name+'">';
    }else if($.inArray(mediaObject.extension,["mp4"]) != -1) {
        if(realPreviewStatus){
            var mediaTags = '<video controls> <source src="/media/'+y+'/'+m+'/'+d+'/'+mediaObject.hash+'.'+mediaObject.extension+'" type="video/mp4"> </video> ';
        }else{
            var mediaTags = '<a href="/media/'+y+'/'+m+'/'+d+'/'+mediaObject.hash+'.'+mediaObject.extension+'" download="/media/'+y+'/'+m+'/'+d+'/'+mediaObject.hash+'.'+mediaObject.extension+'"><img src="/assets/img/video.png" class="img-responsive fullwidth" alt="'+mediaObject.name+'"></a>';

        }
    }else if($.inArray(mediaObject.extension,["mp3"]) != -1) {
        if(realPreviewStatus){
            var mediaTags = '<audio controls> <source src="/media/'+y+'/'+m+'/'+d+'/'+mediaObject.hash+'.'+mediaObject.extension+'" type="audio/mpeg"> </audio> ';
        }else{
            var mediaTags = '<a href="/media/'+y+'/'+m+'/'+d+'/'+mediaObject.hash+'.'+mediaObject.extension+'" download="/media/'+y+'/'+m+'/'+d+'/'+mediaObject.hash+'.'+mediaObject.extension+'"><img src="/assets/img/music.png" class="img-responsive fullwidth" alt="'+mediaObject.name+'"></a>';
        }
    }else if($.inArray(mediaObject.extension,["pdf"]) != -1) {
        var mediaTags = '<a href="/media/'+y+'/'+m+'/'+d+'/'+mediaObject.hash+'.'+mediaObject.extension+'" download="/media/'+y+'/'+m+'/'+d+'/'+mediaObject.hash+'.'+mediaObject.extension+'"><img src="/assets/img/pdf.png" class="img-responsive fullwidth" alt="'+mediaObject.name+'"></a>';
    }else if($.inArray(mediaObject.extension,["doc","docx"]) != -1) {
        var mediaTags = '<a href="/media/'+y+'/'+m+'/'+d+'/'+mediaObject.hash+'.'+mediaObject.extension+'" download="/media/'+y+'/'+m+'/'+d+'/'+mediaObject.hash+'.'+mediaObject.extension+'"><img src="/assets/img/word.png" class="img-responsive fullwidth" alt="'+mediaObject.name+'"></a>';
    }

    return mediaTags;
}

$.fn.imagesLoaded = function () {

    // get all the images (excluding those with no src attribute)
    var $imgs = this.find('img[src!=""]');
    // if there's no images, just return an already resolved promise
    if (!$imgs.length) {return $.Deferred().resolve().promise();}

    // for each image, add a deferred object to the array which resolves when the image is loaded (or if loading fails)
    var dfds = [];
    $imgs.each(function(){

        var dfd = $.Deferred();
        dfds.push(dfd);
        var img = new Image();
        img.onload = function(){dfd.resolve();}
        img.onerror = function(){dfd.resolve();}
        img.src = this.src;

    });

    // return a master promise object which will resolve when all the deferred objects have resolved
    // IE - when all the images are loaded
    return $.when.apply($,dfds);

}

$('.panel').matchHeight();


$('.fullwidth').matchHeight();


function heightFixer(){
    $('#uploadRow .panel').matchHeight('remove').matchHeight();
    $('#uploadRow .fullwidth').matchHeight('remove').matchHeight();
}
function sleep(milliseconds) {
    var start = new Date().getTime();
    for (var i = 0; i < 1e7; i++) {
        if ((new Date().getTime() - start) > milliseconds){
            break;
        }
    }
}


$("#uploadRow").on('load','img',function(){

    heightFixer();

});

$(".delete").submit( function(e) {
    var form = this;
    e.preventDefault();
    swal({
            title: "آیا از حذف اطمینان دارید؟",
            text: "عملیات حذف غیر قابل بازگشت است!",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: 'بله',
            cancelButtonText: "خیر",
            closeOnConfirm: true,
            allowOutsideClick: false,
        },
        function() {
            form.submit();
        });
});






$("#uploadRow").on('click','.deleteImg',function(e) {
    var deleteImg = $(this);


    console.log(22);
    e.preventDefault();

    swal({
            title: "اخطار !",
            text: "آیا از حذف فایل "+deleteImg.attr("data-name")+" اطمینان دارید ؟",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: 'بله',
            cancelButtonText: "خیر",
            closeOnConfirm: true,
            allowOutsideClick: false,
        },
        function() {
            $.ajax({
                type:'DELETE',
                url: '/'+dashboard+'/media/'+deleteImg.attr("data-id"),
                async:true,
                cache:false,
                contentType: false,
                processData: false,
                error:function(returndata){

                    alert("error !");

                },
                success: function (returndata) {

                    //returndata = JSON.parse(returndata);

                    if(returndata.errorStatus == 0){
                        deleteImg.parent().parent().parent().parent().fadeOut(1);
                        setTimeout(function(){
                            heightFixer();
                        },2500);
                    }else{
                        swal("امکان پذیر نیست", "فایل مورد نظر در بخش دیگری استفاده شده و امکان حذف آن نیست !")
                    }

                }
            });
            return false;
        });


});



$("#addMoreFile").click(function(){

    $("#uploadInput .row .col-sm-9").append('<input type="file" name="uploads[]" id="image_file"  class="form-control" accept="image/*">');

});


$('#uploadRow').on('submit','.ajaxEdit',function (e){
    e.preventDefault();
    e.stopImmediatePropagation();
    currentEdit = $(this);
    editId = $(this).attr("data-id");
    $.ajax({
        type:'POST',
        url: '/'+dashboard+'/media/ajaxEdit/'+editId,
        data: new FormData($(this)[0]),
        async:true,
        cache:false,
        contentType: false,
        processData: false,
        error:function(returndata){

            alert("error");

        },
        success: function (returndata) {

            returndata = JSON.parse(returndata);
            var result = $('#result');
            if(returndata.hasOwnProperty("errorStatus")){
                if(returndata.errorStatus == 0){
                    $('.editImg[data-id="'+editId+'"]').parent().parent().prev().prev().find(".panel-title").text(returndata.name);

                    swal("انجام شد !", "فایل با موفقیت ویرایش شد", "success")

                    currentEdit.parent().parent().parent().modal('hide');

                }else{
                    swal("امکان پذیر نیست", "فایل درخواستی در سیستم موجود نیست")
                }


            }
        }
    });
    return false;
});





$('#searchMedia').on('submit',function (e){
    e.preventDefault();
    e.stopImmediatePropagation();

    if($("#searchMedia input[name='search']").val()!=""){
        $.ajax({
            type:'POST',
            url: '/'+dashboard+'/media/ajaxSearch',
            data: new FormData($(this)[0]),
            async:true,
            cache:false,
            contentType: false,
            processData: false,
            error:function(returndata){

                returndata = returndata.responseJSON;
                var result = $('#result');
                var resultText = "";
                $.each(returndata, function (i, n) {


                    $.each(n, function (i, n) {


                        resultText = n;


                    });

                });

                swal("خطا", resultText,'warning')

            },
            success: function (returndata) {


                var result = $('#result');
                if(returndata.hasOwnProperty("errorStatus")){


                    if(returndata.errorStatus == 0){



                        $("#uploadRow").html("");
                        $("#galleryPaginate").html("");



                        for(i=0;i<returndata.searchResult.length;i++){

                            var date = returndata.searchResult[i].created_at;
                            date = date.split(" ");
                            date = date[0].split("-");
                            var m = date[1];
                            var d = date[2];
                            var y = date[0];

                            var mediaTags = getMediaTag(returndata.searchResult[i],m,d,y,false);

                            if(prefix != dashboard+'/media'){

                                $("#uploadRow").append('<div class="col-md-3"> <div class="panel panel-default"> <div class="panel-heading"> <h3 class="panel-title">'+returndata.searchResult[i].name+'</h3> </div> <div class="panel-body"> '+mediaTags+' </div> <div class="panel-footer"> <div class="row" style="display: block; margin:0 auto;"> <button class="btn btn-primary btn-block choose"  data-date="'+returndata.searchResult[i].created_at+'" data-id="'+returndata.searchResult[i].id+'" data-name="'+returndata.searchResult[i].name+'" data-extension="'+returndata.searchResult[i].extension+'" data-hash="'+returndata.searchResult[i].hash+'"><span class="fa fa-photo"></span> انتخاب تصویر</button> </div> </div> </div> </div>  ');

                            }else{

                                $("#uploadRow").append('<div class="col-md-3"> <div class="panel panel-default"> <div class="panel-heading"> <h3 class="panel-title">'+returndata.searchResult[i].name+'</h3> </div> <div class="panel-body"> '+mediaTags+'</div> <div class="panel-footer"> <div class="row" style="display: block; margin:0 auto;"> <button class="btn btn-primary btn-xs editImg"  data-toggle="modal" data-target="#edit'+returndata.searchResult[i].id+'" data-id="'+returndata.searchResult[i].id+'"><span class="fa fa-pencil"></span></button> <button class="btn btn-danger btn-xs deleteImg" data-id="'+returndata.searchResult[i].id+'" data-name="'+returndata.searchResult[i].name+'"><span class="fa fa-trash-o"></span></button> </div> </div> </div> </div>     <!-- Modal --> <div class="modal bs-example-modal-lg fade" id="edit'+returndata.searchResult[i].id+'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"> <div class="modal-dialog" role="document"> <div class="modal-content"> <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> <h4 class="modal-title" id="myModalLabel">ویرایش فایل</h4> </div><form method="POST" action="'+dashboard+'/media/ajaxEdit/'+returndata.searchResult[i].id+'" accept-charset="UTF-8" class="ajaxEdit" data-id="'+returndata.searchResult[i].id+'"><div class="modal-body"> <div class="row"> <div class="col-md-12"><!-- last name--> <div class="form-group row"> <div class="col-lg-12 col-md-12 col-sm-12"> <div class="input-group"> <span class="input-group-addon"> <i class="fa fa-bars"></i> </span>  <input class="form-control" placeholder="نام تصویر" required="required" name="name" type="text" value="'+returndata.searchResult[i].name+'"> </div> </div> </div><!-- mail name--> </div> </div> <input type="hidden" value="'+returndata.searchResult[i].id+'" name="imageId"> </div> <div class="modal-footer"> <div class="btn-group"> <button type="button" class="btn btn-default" data-dismiss="modal">انصراف</button> <input type="submit" name="submit" class="btn btn-primary" value="ویرایش" id="edit"> </div> </div> </form> </div> </div> </div>');

                            }

                        }

                        $('.panel').matchHeight('remove').matchHeight();
                        $('.fullwidth').matchHeight('remove').matchHeight();


                    }else{
                        swal("نتیجه ای یافت نشد", "فایل در سیستم گالری موجود نیست",'warning')
                    }


                }

                heightFixer();
            }
        });
    }else{
        swal("کاراکتری وارد نشده", "لطفا چند کاراکتر برای جستجو در تصاویر وارد نمایید",'warning')
    }
    return false;
});




$("#galleryPaginate a").click(function(e){


    // e.preventDefault();

    // console.log($(this).attr("href"));


});



var statusShow = 1;


var currentGallery;

var pickerTypePage=null;

var galleryType = "none";

$(".galleryPickeraBtn").click(function(){


    pickerTypePage = $(this).attr("data-action");
    if(pickerTypePage!="thumb"){
        pickerTypePage=null;
    }

    currentGallery = $(this);

    galleryType = currentGallery.attr('data-type');
    if (typeof galleryType !== typeof undefined && galleryType !== false && galleryType == "mutiple") {
        galleryType = "mutiple";
    }else{
        galleryType = "none";
    }

    var picker = $("#galleryPicker");

    if(statusShow == 1){
        $.ajax({
            type:'GET',
            url: '/'+dashboard+'/media/picker/',
            async:true,
            cache:false,
            contentType: false,
            processData: false,
            success: function (returndata) {


                $("#uploadRow").html("");

                for(i=0;i<returndata.searchResult.length;i++){

                    var date = returndata.searchResult[i].created_at;
                    date = date.split(" ");
                    date = date[0].split("-");
                    var m = date[1];
                    var d = date[2];
                    var y = date[0];
                    var mediaTags = getMediaTag(returndata.searchResult[i],m,d,y,false);

                    $("#uploadRow").append('<div class="col-md-3"> <div class="panel panel-default"> <div class="panel-heading"> <h3 class="panel-title">'+returndata.searchResult[i].name+'</h3> </div> <div class="panel-body"> ' + mediaTags + '</div> ' + '<div class="panel-footer"> ' + '<div class="row" style="display: block; margin:0 auto;"> ' + '<button class="btn btn-primary btn-block choose" data-date="'+returndata.searchResult[i].created_at+'" data-hash="'+returndata.searchResult[i].hash+'" data-extension="'+returndata.searchResult[i].extension+'" data-name="'+returndata.searchResult[i].name+'" data-id="'+returndata.searchResult[i].id+'"><span class="fa fa-photo"></span> انتخاب</button> ' + '</div> </div> </div> </div>  ');
                    console.log(mediaTags);
                }



                picker.modal("show");
                heightFixer();

            }
        });

        statusShow++;
        var counter = 1;
        var fixHeightINterval = setInterval(function(){
            heightFixer();
            counter++;
            if(counter==3){
                clearInterval(fixHeightINterval);
            }
        },4000);
    }else{
        picker.modal("show");
    }

    heightFixer();

});


$('#ajaxUpload').submit(function (e){
    e.preventDefault();
    e.stopImmediatePropagation();
    var ajaxUploadAction = $(this).attr("action");
    var formData = new FormData($(this)[0]);
    var file = $('input[type=file]')[0].files[0];
    formData.append('upload_file',file);
    $('.progress-bar').css('width',"0%");
    $('.progress-bar').html("0%");
    $('.progress').show();
    $.ajax({
        xhr: function() {
            var xhr = new window.XMLHttpRequest();
            xhr.upload.addEventListener("progress", function(evt) {
                if (evt.lengthComputable) {
                    var percentComplete = evt.loaded / evt.total;
                    percentComplete = parseInt(percentComplete * 100);
                    $('.progress-bar').css('width',percentComplete+"%");
                    $('.progress-bar').html(percentComplete+"%");
                    if (percentComplete === 100) {



                    }
                }
            }, false);
            return xhr;
        },
        type:'POST',
        url: ajaxUploadAction,
        data: formData,
        async:true,
        cache:false,
        contentType: false,
        processData: false,
        error:function(returndata){
            returndata = returndata.responseJSON;
            var result = $('#result');
            var resultText = "";
            $.each(returndata, function (i, n) {

                $.each(n, function (i, n) {

                    resultText = '<div class="alert alert-danger">' + n + '</div>';

                });

            });

            result.html(resultText);
        },
        success: function (returndata) {
            returndata = JSON.parse(returndata);
            var result = $('#result');
            if(returndata.hasOwnProperty("errorStatus")){
                if(returndata.errorStatus == 0){

                    if(returndata.files.length > 0){
                        var resultText = '<div class="alert alert-success">فایل ها با موفقیت اپلود شدند</div>';
                        $.each(returndata.files, function(i, n){

                            var date = n.created_at;
                            date = date.split(" ");
                            date = date[0].split("-");
                            var m = date[1];
                            var d = date[2];
                            var y = date[0];

                            var mediaTags = getMediaTag({extension:n.extension,id:n.id,name:n.name,hash:n.hash},m,d,y,false);

                            resultText += '<div class="alert alert-info"><a target="_blank" href="/media/'+y+'/'+m+'/'+d+'/'+n.hash+'.'+n.extension+'">فایل شماره '+(i+1)+' : '+n.name+'</a></div>';

                            if(prefix != '/'+dashboard+'/media'){

                                $("#uploadRow").prepend('<div class="col-md-3"> <div class="panel panel-default"> <div class="panel-heading"> <h3 class="panel-title">'+n.name+'</h3> </div> <div class="panel-body"> '+mediaTags+' </div> <div class="panel-footer"> <div class="row" style="display: block; margin:0 auto;">  <button class="btn btn-primary btn-block choose" data-date="'+n.created_at+'" data-hash="'+n.hash+'" data-extension="'+n.extension+'" data-name="'+n.name+'" data-id="'+n.id+'" data-name="'+n.name+'"><span class="fa fa-photo"></span> انتخاب تصویر</button> </div> </div> </div> </div>  ');

                            }else{

                                $("#uploadRow").prepend('<div class="col-md-3"> <div class="panel panel-default"> <div class="panel-heading"> <h3 class="panel-title">'+n.name+'</h3> </div> <div class="panel-body"> '+mediaTags+' </div> <div class="panel-footer"> <div class="row" style="display: block; margin:0 auto;"> <button class="btn btn-primary btn-xs editImg"  data-toggle="modal" data-target="#edit'+n.id+'" data-id="'+n.id+'"><span class="fa fa-pencil"></span></button> <button class="btn btn-danger btn-xs deleteImg" data-id="'+n.id+'" data-name="'+n.name+'"><span class="fa fa-trash-o"></span></button> </div> </div> </div> </div>     <!-- Modal --> <div class="modal bs-example-modal-lg fade" id="edit'+n.id+'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"> <div class="modal-dialog" role="document"> <div class="modal-content"> <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> <h4 class="modal-title" id="myModalLabel">ویرایش فایل</h4> </div><form method="POST" action="http://endirim.dev/dashboard/ajaxEdit/'+n.id+'" accept-charset="UTF-8" class="ajaxEdit" data-id="'+n.id+'"><div class="modal-body"> <div class="row"> <div class="col-md-12"><!-- last name--> <div class="form-group row"> <div class="col-lg-12 col-md-12 col-sm-12"> <div class="input-group"> <span class="input-group-addon"> <i class="fa fa-bars"></i> </span>  <input class="form-control" placeholder="نام تصویر" required="required" name="name" type="text" value="'+n.name+'"> </div> </div> </div><!-- mail name--> </div> </div> <input type="hidden" value="'+n.id+'" name="imageId"> </div> <div class="modal-footer"> <div class="btn-group"> <button type="button" class="btn btn-default" data-dismiss="modal">انصراف</button> <input type="submit" name="submit" class="btn btn-primary" value="ویرایش" id="edit"> </div> </div> </form> </div> </div> </div>');

                            }



                        });


                        $('.panel').matchHeight('remove').matchHeight();
                        $('.fullwidth').matchHeight('remove').matchHeight();

                        result.html(resultText);
                    }else{
                        var resultText = '<div class="alert alert-danger"> فایلی برای اپلود انتخاب نشده است </div>';
                        result.html(resultText);
                    }

                }else{


                    result.html(returndata.errorText);

                }


            }
        }
    });
    return false;
});


var mutipleCount = 0;

var newItem = [];


$("#galleryPicker").on("click",".choose",function(){

    console.log("zero");
    var type = currentGallery.attr('data-type');
    if (typeof type !== typeof undefined && type !== false && type == "multiple") {

        console.log("one");
        if(mutipleCount == 0){

            console.log("one2");
            $(currentGallery).parent().next().find(".galleryInput").val($(this).attr("data-name"));
            $(currentGallery).parent().next().find(".imageHiddenInput").val($(this).attr("data-id"));

        }else{

            console.log("two2");
            $(currentGallery).parent().parent().next().append('<div class="input-group"> <span class="input-group-addon deleteMultipleImage"> <i class="fa fa-trash-o"></i> </span> <input class="form-control galleryInput" readonly="readonly" name="image" type="text"> <input class="imageHiddenInput" type="hidden" name="galleryImages[]" value="NULL"> </div>');

            $(currentGallery).parent().parent().next().find(".galleryInput").last().val($(this).attr("data-name"));
            $(currentGallery).parent().parent().next().find(".imageHiddenInput").last().val($(this).attr("data-id"));

        }


        mutipleCount++;

    }else{
        console.log("two");

        var name = $(this).attr("data-name");
        var id = $(this).attr("data-id");
        var extension = $(this).attr("data-extension");
        var date = $(this).attr("data-date");
        date = date.split(" ");
        date = date[0].split("-");
        var m = date[1];
        var d = date[2];
        var y = date[0];

        var mediaTags = getMediaTag({extension:extension,id:id,name:name,hash:$(this).attr("data-hash")},m,d,y,true);

        if(pickerTypePage=="thumb"){
            pickerTypePage=null;
            $(currentGallery).parent().next().find(".galleryInput").val(name);
            $(currentGallery).parent().next().find(".imageHiddenInput").val(id);
            $(currentGallery).parent().next().next().find(".preview").html(mediaTags);
            console.log($(currentGallery).parent().next().next());
            console.log("three");
            //$(currentGallery).parent().next().next().find(".preview").find("img").attr("src","/media/"+$(this).attr("data-hash")+"."+$(this).attr("data-extension"));
        }else{
            console.log("four");
            if(prefix == '/'+dashboard+'/departments' || prefix == '/'+dashboard+"/groups"){
                $(currentGallery).parent().next().find(".galleryInput").val(name);
                $(currentGallery).parent().next().find(".imageHiddenInput").val(id);
                $(currentGallery).parent().next().next().find(".preview").html(mediaTags);
                //$(currentGallery).parent().next().next().find(".preview").find("img").attr("src","/media/"+$(this).attr("data-hash")+"."+$(this).attr("data-extension"));
            }else{
                myValue = '<br>'+mediaTags;
                CKEDITOR.instances['ckeditor'].insertHtml(myValue)
            }
        }
    }



    $("#galleryPicker").modal('hide');
});

setTimeout(function(){
    heightFixer();
},6000);