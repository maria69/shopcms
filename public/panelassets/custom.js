/**
 * Created by ali bakhshi on 12/6/17.
 * ali-bakhshi.ir
 */
$(document).ready(function() {

    if($('.datatable').length > 0){
        $('.datatable').DataTable({
            "pageLength": 50,
            dom: 'Bfrtip',
            buttons: ['copy','csv','excel','pdf','print'],
            "language": {
                "sProcessing":    "در حال پردازش ...",
                "sLengthMenu":    "Mostrar _MENU_ registros",
                "sZeroRecords":   "نتیجه ای یافت نشد",
                "sEmptyTable":    "اطلاعاتی برای نمایش وجود ندارد",
                "sInfo":          "نمایش _START_ تا _END_ از _TOTAL_ مورد یافت شده",
                "sInfoEmpty":     "نمایش 0 تا 0 از 0 مورد یافت شده",
                "sInfoFiltered":  "(جستجو در بین _MAX_ رکورد)",
                "sInfoPostFix":   "",
                "sSearch":        "جستجو : ",
                "sUrl":           "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "در حال پردازش ...",
                "oPaginate": {
                    "sFirst":    "اولین",
                    "sLast":    "آخرین",
                    "sNext":    "بعد",
                    "sPrevious": "قبل"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            }
        });
    }



    if($('#ckeditor').length){
        CKEDITOR.replace( 'ckeditor', {contentsLangDirection : 'rtl',height:300,language:"fa",});
    }

    if($('#ckeditor2').length){
        CKEDITOR.replace( 'ckeditor2', {contentsLangDirection : 'rtl',height:300,language:"fa"});
    }

});

$(".delete").submit( function(e) {
    var form = this;
    e.preventDefault();

    swal({
            title: "آیا از حذف اطمینان دارید؟",
            text: "عملیات حذف غیر قابل بازگشت است!",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: 'بله',
            cancelButtonText: "خیر",
            closeOnConfirm: true,
            allowOutsideClick: false,
        },
        function() {
            form.submit();
            console.log(12);
        });
});


if($(".selectRtl").length > 0){
    $(".selectRtl").select2({
        dir: "rtl",
        placeholder:"انتخاب کنید ...",
        allowClear:!0
    });
}


function number_format (number, decimals, decPoint, thousandsSep) {
    number = (number + '').replace(/[^0-9+\-Ee.]/g, '')
    var n = !isFinite(+number) ? 0 : +number
    var prec = !isFinite(+decimals) ? 0 : Math.abs(decimals)
    var sep = (typeof thousandsSep === 'undefined') ? ',' : thousandsSep
    var dec = (typeof decPoint === 'undefined') ? '.' : decPoint
    var s = ''
    var toFixedFix = function (n, prec) {
        var k = Math.pow(10, prec)
        return '' + (Math.round(n * k) / k)
            .toFixed(prec)
    }
    // @todo: for IE parseFloat(0.55).toFixed(0) = 0;
    s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.')
    if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep)
    }
    if ((s[1] || '').length < prec) {
        s[1] = s[1] || ''
        s[1] += new Array(prec - s[1].length + 1).join('0')
    }
    return s.join(dec)
}