/**
 * Created by Ali Bakhshi on 6/28/17.
 * Ajax Script For Admin Panel
 * ali-bakhshi.ir
 */


/**
 * Toast Ajax Result
 */
toastr.options = {
    "closeButton": true,
    "debug": false,
    "positionClass": "toast-top-left",
    "onclick": null,
    "showDuration": "1000",
    "hideDuration": "1000",
    "timeOut": "20000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
}

function toastMessage(message,type){
    if(type=="error"){
        toastr.error(message);
    }else if(type=="success"){
        toastr.success(message);
    }else {
        toastr.warning(message);
    }
}
