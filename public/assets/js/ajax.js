$(".wishlist").click(function () {

    var pid = $(this).attr("data-product-id");

    $.ajax({
        url: routes.wishlist,
        method: 'post',
        data: {id: pid},
        success: function (data) {
            swal(
                '',
                data.msg,
                data.status
            );
        }
    });

});


$(".addtocart").click(function (e) {
    // alert();
    // console.log("1");
    e.preventDefault();


    var pid = $(this).attr("data-product-id");

    $.ajax({
        url: routes.addToCart,
        method: 'post',
        data: {id: pid},
        success: function (result) {
            console.log(result);
            // console.log(result);
            // console.log(result);


            $(".value").html(result['data']['final_price']);
            $(".count").html(result['data']['count']);

            $(".cart-menu").html("");


            var sum = 0;

            var resultpro = result.data.products;
            var out_put = "";

            for (var i in resultpro) {

                sum = sum + resultpro[i].final_price;

                out_put = out_put + '<li>\n' +
                    '<div class="whishlist-item">\n' +
                    '<div class="product-image">\n' +
                    '<a href="#" title="">\n' +
                    '<div class="image"><a href="#"><img\n' +
                    'src=' +  resultpro[i].address + '\n' +
                    'alt=""></a></div>\n' +
                    '</div>\n' +
                    '<div class="product-body">\n' +
                    '<div class="whishlist-name">\n' +
                    '<h3 class="name"><a href="/product/'+resultpro[i].product.id +'">' + resultpro[i].product.name + '</a>\n' +
                    '</h3>\n' +
                    '</div>\n' +
                    '<div class="whishlist-price">' + resultpro[i].unit_price + '</div>\n' +
                    ' <div class="whishlist-quantity"> تعداد: ' + resultpro[i].count + '</div>\n' +
                    '</div>\n' +
                    // '<a href="#" title="" class="remove">'+
                    // '<i class="icon icon-remove"></i>'+
                    // '</a>'+
                    ' <hr>\n' +
                    '</div>\n' +
                    '</div>\n' +
                    '</li>'

                // console.log(resultpro[i].final_price); //note i returns index
            }


            out_put = out_put + '<div class="clearfix cart-total">\n' +
                '                                <div class="pull-right"><span class="text">جمع کل :</span><span\n' +
                'class=\'price_total\'>' + result['data']['final_price'] + ' تومان  </span>\n' +
                '                                </div>\n' +
                '                                <div class="clearfix"></div>\n' +
                '                                <a href=" cart"\n' +
                '                                   class="btn btn-upper btn-primary btn-block m-t-20">تسویه حساب</a></div>';


            $(".cart-menu").html(out_put);
            $(".price_total").html(result['data']['final_price']);
            // for (var item in resultpro) {
            //
            //
            //
            //     console.log(resultpro.item);
            //     sum += result[ item ];
            // }


            // console.log($(".value").html(3));
            swal(
                '',
                result.msg,
                result.status
            );
        }
    });

});


$(".remove-from-cart").click(function (e) {

    e.preventDefault();

    var $this = $(this);

    var pid = $(this).attr("data-product-id");



    $.ajax({
        url: routes.deleteFromCart,
        method: 'post',
        data: {id: pid},
        success: function (data) {

            // console.log(data);
            if (data.status == "success") {


                $this.parent().parent().fadeOut(200, function () {
                    $(this).remove();


                });
            } else if (data.status == "error") {
                swal(
                    'خطا',
                    data.msg,
                    data.status
                );
            }



            $(".value").html(data.cart.final_price);
            $(".count").html(data.cart.count);
            $(".cart-menu").html("");


            var sum = 0;

            var resultpro = data.cart.products;

            //
            // console.log(data.cart.final_price);
            // console.log(resultpro);
            // console.log("resultpro");
            // console.log(resultpro['length']);

            var out_put = "";

            var set_cart = "";


            if (resultpro['length'] < 1)
            {
                console.log(resultpro['length']);


                set_cart = set_cart + '<div class="alert alert-danger  text-center cart-empty">\n' +
                    '                                <h4>سبد خرید خالی است</h4>\n' +
                    '                            </div>';
                $('.set-cart').html(set_cart);

            }

            if (resultpro['length'] >0)
                for (var i in resultpro) {

                    sum = sum + resultpro[i].unit_price;


                    out_put = out_put + '<li>\n' +
                        '<div class="cart-item product-summary">\n' +
                        '<div class="row">\n' +
                        '<div class="col-xs-4">\n' +
                        '<div class="image"><a href="#"><img\n' +
                        'src="/assets/images/cart.jpg"\n' +
                        'alt=""></a></div>\n' +
                        '</div>\n' +
                        ' <div class="col-xs-7">\n' +
                        '<h3 class="name"><a href="index.php?page-detail">' + resultpro[i].product.name + '</a>\n' +
                        '</h3>\n' +
                        ' <div class="price">' + resultpro[i].unit_price + '</div>\n' +
                        '</div>\n' +

                        '</div>\n' +
                        '</div>\n' +
                        '<div class="clearfix"></div>\n' +
                        ' <hr>\n' +
                        '</li>'

                    // console.log(resultpro[i].final_price); //note i returns index
                }


            out_put = out_put + '<div class="clearfix cart-total">\n' +
                '                                <div class="pull-right"><span class="text">جمع کل :</span><span\n' +
                'class=\'price_total\'>' +data.cart['final_price'] + ' تومان  </span>\n' +
                '                                </div>\n' +
                '                                <div class="clearfix"></div>\n' +
                '                                <a href=" cart"\n' +
                '                                   class="btn btn-upper btn-primary btn-block m-t-20">تسویه حساب</a></div>';


            $(".cart-menu").html(out_put);
            $(".price_total").html(data.cart['final_price']);


        }
    });

});


$(".counter_cart .arrow").click(function (e) {

    e.preventDefault();

    $("#loading").fadeIn(200);

    var $this = $(this);

    var pid = $(this).parent().attr("data-product-id");

    if ($this.hasClass("plus")) {
        var sign = "+";
    } else {
        var sign = "-";
    }

    $.ajax({
        url: '/cart/counter/' + sign,
        method: 'post',
        data: {id: pid},
        success: function (data) {
            if (data.status == "success") {
                // update whole basket

                if (data.hasOwnProperty("delete") && data.delete == 1) {
                    $this.parent().parent().parent().parent().parent().hide(200);
                } else {
                    $this.parent().next().val(data.count);
                    $this.parent().parent().parent().parent().next().next().next().find(".cart-grand-total-price-offer").text(number_format(data.final_price) + " تومان");
                }

            } else if (data.status == "error") {
                swal(
                    'خطا',
                    data.msg,
                    data.status
                );
            }
        },
        complete: function () {
            $("#loading").fadeOut(200);
        }
    });

});


$("#addAddress").submit(function (e) {

    e.preventDefault();

    $("#loading").fadeIn(200);

    $.ajax({
        url: $(this).attr("action"),
        method: 'post',
        data: $(this).serialize(),
        success: function (data) {
            swal(
                '',
                data.msg,
                data.status
            );

            $("#addressResult").append('<tr> <td><input type="radio" name="address" value="' + data.info.id + '"></td> <td class="cart-description item">' + data.info.city + ' </td> <td class="cart-product-name item">' + data.info.country + ' </td> <td class="cart-edit item">' + data.info.address + ' </td> <td class="cart-qty item">' + data.info.post_code + ' </td><td class="cart-qty item">' + data.info.phone_number + ' </td> </tr>');
        },
        complete: function () {
            $("#loading").fadeOut(200);
            $("#myModal").modal("hide");
        }
    });

});


$(".delete").submit(function (e) {

    e.preventDefault();

    $("#loading").fadeIn(200);

    var form = $(this);

    swal({
            title: "آیا از حذف اطمینان دارید؟",
            text: "عملیات حذف غیر قابل بازگشت است!",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: 'بله',
            cancelButtonText: "خیر",
            closeOnConfirm: true,
            allowOutsideClick: false,
        },
        function (isConfirm) {

            if (isConfirm) {
                $.ajax({
                    url: form.attr("action"),
                    method: 'post',
                    data: form.serialize(),
                    success: function (data) {

                        form.parent().parent().fadeOut(200, function () {
                            $(this).remove()
                        });

                    },
                    complete: function () {
                        $("#loading").fadeOut(200);
                    }
                });
            } else {
                $("#loading").fadeOut(200);
            }

        });

});


function number_format(number, decimals, dec_point, thousands_sep) {
    // Strip all characters but numerical ones.
    number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
    var n = !isFinite(+number) ? 0 : +number,
        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
        sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
        dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
        s = '',
        toFixedFix = function (n, prec) {
            var k = Math.pow(10, prec);
            return '' + Math.round(n * k) / k;
        };
    // Fix for IE parseFloat(0.55).toFixed(0) = 0;
    s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
    if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
    }
    if ((s[1] || '').length < prec) {
        s[1] = s[1] || '';
        s[1] += new Array(prec - s[1].length + 1).join('0');
    }
    return s.join(dec);
}


/*$("#searchForm").submit(function(e){
    e.preventDefault();
    $("#loading").fadeIn(200);

    $.ajax({
        url:routes.search,
        data:$(this).serialize(),
        success:function(data){

        },
        error:function(){
            swal(
                'خطا',
                "مشکلی در دریافت اطلاعات پیش آمده است",
                "error"
            );
        },
        complete:function(){
            $("#loading").fadeOut(200);
        }
    });

});*/

$(".next-step").click(function (e) {

    e.preventDefault();

    var step = $(this).attr("data-step");

    if (step == 2 || step == 1) {

        if (step == 2 && $(".shopping-cart-table #basket tbody tr").length > 0) {
            $("#collapse1").collapse("toggle");
            $("#collapse2").collapse("toggle");
        } else if (step == 2) {
            swal(
                'خطا',
                "ابتدا محصولی به سبد خرید اضافه نمایید",
                "error"
            );
        }

        if (step == 1) {
            $("#collapse1").collapse("toggle");
            $("#collapse2").collapse("toggle");
        }


    } else if (step == 3) {

        if ($("input[name='address']:checked").length == 1) {
            $("#collapse2").collapse("toggle");
            $("#collapse3").collapse("toggle");
        } else {
            swal(
                'خطا',
                "لطفا یک آدرس انتخاب نمایید",
                "error"
            );
        }

    } else if (step == 5) {
        $("#collapse2").collapse("toggle");
        $("#collapse3").collapse("toggle");
    }

});


$("#chooseType").change(function () {

    $("#loading").fadeIn(200);

    var action = $(this).attr("data-action");
    var typeId = $(this).val();

    $.ajax({
        url: action,
        method: 'post',
        data: {type: typeId},
        success: function (data) {
            var $newOptions = "";
            $(data).each(function (index, element) {
                $newOptions += '<option value="' + element.id + '">' + element.name + '</option>';
            });
            $(".chooseProducts").html($newOptions);
        },
        complete: function () {
            $("#loading").fadeOut(200);
        }
    });

});


$(".wrapper label").click(function (e) {

    $(this).siblings("label").css({color: "gray"});

    $(this).css({color: "orange"});

    $(this).nextAll("label").css({color: "orange"});

});


$(document).on("click", ".newminus .minus", function () {
    $(this).parent().remove();
});


$("#reviews_by_user #plus i").click(function () {
    $(this).parent().parent().append('<div class="form-group newminus"> <i style="color:red" class="fa fa-minus minus"></i> <input name="weaknesses[]" type="text" class="form-control txt" placeholder="قوت"> </div>');
});

$("#reviews_by_user #mines i").click(function () {
    $(this).parent().parent().append('<div class="form-group newminus"> <i style="color:red" class="fa fa-minus minus"></i> <input name="strengths[]" type="text" class="form-control txt" placeholder="ضعف"> </div>');
});


$(document).ready(function () {
    $('i.glyphicon-thumbs-up, i.glyphicon-thumbs-down').click(function () {

        $("#loading").fadeIn(200);

        var comment_id = $(this).parent().attr("data-id");

        if ($(this).hasClass("glyphicon-thumbs-up")) {
            var dir = "up";
        } else {
            var dir = "down";
        }

        var $this = $(this);

        $.ajax({
            url: '/like',
            method: 'post',
            data: {dir: dir, id: comment_id},
            success: function (data) {


                if (typeof data.status != undefined && data.status == "error") {
                    swal(
                        '',
                        data.msg,
                        data.status
                    );
                } else {
                    $this.siblings("#like1-bs3").text(data.like);
                    $this.siblings("#dislike1-bs3").text(data.unlike);
                }

            },
            complete: function () {
                $("#loading").fadeOut(200);
            }
        });


    });
});
