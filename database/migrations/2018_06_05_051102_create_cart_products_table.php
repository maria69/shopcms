<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCartProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cart_products', function (Blueprint $table) {
            $table->increments('id');


            $table->integer("product_id")->unsigned()->nullable()->index();
            $table->integer("cart_id")->unsigned()->nullable()->index();

            $table->integer("unit_price")->unsigned()->default(0);
            $table->integer("total_price")->unsigned()->default(0);
            $table->integer("offer_price")->unsigned()->default(0);
            $table->integer("final_price")->unsigned()->default(0);
            $table->tinyInteger("offer")->unsigned()->default(0);

            $table->integer("count")->unsigned()->default(0);


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cart_products');
    }
}
