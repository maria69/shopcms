<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stores', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('en_title');

            $table->integer('level_id')->unsigned();
            $table->foreign('level_id')->references('id')->on('levels');

            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');

            $table->integer('shopping_type_id')->unsigned();
            $table->foreign('shopping_type_id')->references('id')->on('shopping_types');

            $table->integer('shipping_type_id')->unsigned();
            $table->foreign('shipping_type_id')->references('id')->on('shipping_types');

            $table->integer('peyment_type_id')->unsigned();
            $table->foreign('peyment_type_id')->references('id')->on('peyment_types');

            $table->integer('city_id')->unsigned();
            $table->foreign('city_id')->references('id')->on('cities');

            $table->string('score_count')->unsigned();

            $table->integer('logo')->nullable();
            $table->foreign('logo')->references('id')->on('media');
            $table->softDeletes();

            $table->text('desc');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('stores', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });
    }
}
