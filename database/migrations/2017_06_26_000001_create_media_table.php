<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("media",function(Blueprint $table){

            $table->increments("id");
            $table->string("name");
            $table->string("hash",40);
            $table->string("extension",5);
            $table->string("size",20);
            $table->boolean("type")->default(0);
            $table->integer("usage",false,true)->default(0);
            $table->integer('admin_id')->unsigned()->nullable()->index();
            $table->foreign('admin_id')->references('id')->on('admins')->onDelete('set null');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("media",function (Blueprint $table){
            $table->dropForeign('admin_id');
        });
        Schema::drop("media");
    }
}