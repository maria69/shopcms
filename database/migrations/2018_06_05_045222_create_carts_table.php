<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('carts', function (Blueprint $table) {
            $table->increments('id');

            $table->integer("user_id")->index()->unsigned()->nullable();
            $table->string("cart_token")->index()->nullable();

            $table->integer("total_price")->unsigned()->default(0);
            $table->integer("offer_price")->unsigned()->default(0);
            $table->integer("final_price")->unsigned()->default(0);

            $table->integer("count")->unsigned()->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('carts');
    }
}
