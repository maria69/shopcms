<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reminder Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Passwords must be at least six characters and match the confirmation.',
    'user' => "ایمیل وارد شده در سیستم موجود نیست !",
    'token' => 'توکن بازنشانی رمز عبور اشتباه می باشد',
    'sent' => 'لینک بازیابی رمز عبور برای ایمیل شما ارسال شد',
    'reset' => 'رمز عبور شما بازنشانی شد',

];
