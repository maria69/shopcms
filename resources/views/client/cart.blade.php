@extends("client.layout")
@section("title") | سبد خرید @endsection
@section("content")

    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">آدرس جدید</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">

                            <div class="portlet box blue-hoki">
                                <div class="portlet-title">
                                    <div class="tools">
                                        <!--<a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                                        <a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title=""> </a>
                                        <a href="javascript:;" class="reload" data-original-title="" title=""> </a>
                                        <a href="javascript:;" class="remove" data-original-title="" title=""> </a>-->
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    @include("errors.list")
                                </div>
                                <div class="portlet-body form">
                                    <!-- BEGIN FORM-->
                                    {!! Form::open(["class"=>"form-horizontal","url"=>route("addresses.store"),"method"=>"post","id"=>"addAddress"]) !!}

                                    <div class="form-body">

                                        <div class="form-group form-md-line-input">
                                            {!! Form::label("country_id","کشور",["class"=>"col-md-3 control-label"]) !!}
                                            <div class="col-md-7">
                                                {!! Form::select('country_id',[1=>"ایران"] , 0,["class"=>"form-control selectRtl"]) !!}

                                            </div>
                                        </div>
                                        <hr>
                                        <div class="form-group form-md-line-input">
                                            {!! Form::label("city_id","شهر",["class"=>"col-md-3 control-label"]) !!}
                                            <div class="col-md-7">
                                                {!! Form::select('city_id',$cities, 0,["class"=>"form-control selectRtl"]) !!}

                                            </div>
                                        </div>
                                        <div class="form-group form-md-line-input">
                                            {!! Form::label("phone_number","شماره تماس",["class"=>"col-md-3 control-label"]) !!}
                                            <div class="col-md-7">
                                                {!! Form::text('phone_number',null,["class"=>"form-control "]) !!}

                                            </div>
                                        </div>
                                        <div class="form-group form-md-line-input">
                                            {!! Form::label("post_code","کد پستی",["class"=>"col-md-3 control-label"]) !!}
                                            <div class="col-md-7">
                                                {!! Form::text('post_code',null,["class"=>"form-control "]) !!}

                                            </div>
                                        </div>
                                        <hr>
                                        <div class="form-group form-md-line-input">
                                            {!! Form::label("address","آدرس",["class"=>"col-md-3 control-label"]) !!}
                                            <div class="col-md-8">
                                                {!! Form::textarea("address",null,['class'=>'form-control']) !!}

                                            </div>
                                        </div>
                                        <hr>

                                    </div>

                                    <div class="form-actions fluid">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                {!! Form::submit("ذخیره",["class"=>"btn green"]) !!}
                                            </div>
                                        </div>
                                    </div>
                                {!! Form::close() !!}
                                <!-- END FORM-->
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">بستن</button>
                </div>
            </div>

        </div>
    </div>

    <div class="breadcrumb">
        <div class="container">
            <div class="breadcrumb-inner">
                <ul class="list-inline list-unstyled">
                    <li><a href="/">خانه</a></li>
                    <li class='active'>سبد خرید</li>
                </ul>
            </div><!-- /.breadcrumb-inner -->
        </div><!-- /.container -->
    </div><!-- /.breadcrumb -->

    <div class="body-content outer-top-xs">
        <div class="container">

            <div class="checkout-box ">
                <div class="row">
                    <div class="col-md-12">
                        @include("errors.listClient")

                        @php $count=1; @endphp
                        @if($cart && $cart->products && $cart->products->count() > 0)
                            <div class="panel-group checkout-steps set-cart" id="accordion">
                                <!-- checkout-step-01  -->

                                <div class="panel panel-default checkout-step-01 ">

                                    <!-- panel-heading -->
                                    <div class="panel-heading">
                                        <h4 class="unicase-checkout-title">
                                            <a data-toggle="collapse" class="" data-parent="#accordion"
                                               href="#collapseOne">
                                                <span>{{$count}}</span>سبد خرید
                                            </a>
                                        </h4>
                                    </div>
                                    <!-- panel-heading -->

                                    <div id="collapse{{$count}}" class="panel-collapse collapse in">

                                        <!-- panel-body  -->
                                        <div class="panel-body">
                                            <div class="shopping-cart-table">
                                                <div class="table-responsive">
                                                    @if($cart->products->count() > 0)

                                                        <table class="ftable" id="basket">
                                                            <thead>
                                                            <tr>
                                                                <th class="cart-description item">تصویر</th>
                                                                <th class="cart-product-name item">نام</th>
                                                                <th class="cart-qty item">تعداد</th>
                                                                <th class="cart-sub-total item">قیمت واحد</th>
                                                                <th class="cart-total-offer last-item">درصد تخفیف</th>
                                                                <th class="cart-total-final last-item">قیمت نهایی</th>
                                                                <th class="cart-romove item">حذف</th>
                                                            </tr>
                                                            </thead><!-- /thead -->
                                                            <tfoot>
                                                            <tr>
                                                                <td colspan="7">
                                                                    <div class="shopping-cart-btn">
                                            <span class="">
                                                <a href="#"
                                                   class="btn btn-upper btn-primary pull-left outer-left-xs next-step"
                                                   data-step="2">مرحله بعد</a>
                                            </span>
                                                                    </div><!-- /.shopping-cart-btn -->
                                                                </td>
                                                            </tr>
                                                            </tfoot>
                                                            <tbody>

                                                            @foreach($cart->products as $product)


<!--                                                                --><?php //dd($product);?>
                                                                <tr>

                                                                    <td class="cart-image">
                                                                        <a class="entry-thumbnail" href="#">
                                                                            <img class="image-card-thumb" src="{{ thumb($product->product->media,5) }}"
                                                                                 alt="" >
                                                                        </a>
                                                                    </td>
                                                                    <td class="cart-product-name-info">
                                                                        <h4 class='cart-product-description'><a
                                                                                    href="/product/{{$product->product->id}}">{{$product->product->name}}</a>
                                                                        </h4>
                                                                        <div class="cart-product-info">
                                                                            {{--<span class="product-color">رنگ:<span>مشکی</span></span>--}}
                                                                        </div>
                                                                    </td>
                                                                    <td class="cart-product-quantity">
                                                                        <div class="cart-quantity">
                                                                            <div class="quant-input">
                                                                                <div class="arrows counter_cart"
                                                                                     data-product-id="{{$product->id}}">
                                                                                    <div class="arrow plus gradient">
                                                                                        <span class="ir"><i
                                                                                                    class="icon fa fa-sort-asc"></i></span>
                                                                                    </div>
                                                                                    <div class="arrow minus gradient">
                                                                                        <span class="ir"><i
                                                                                                    class="icon fa fa-sort-desc"></i></span>
                                                                                    </div>
                                                                                </div>
                                                                                <input type="text"
                                                                                       value="{{$product->count}}">
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                    <td class="cart-product-sub-total"><span
                                                                                class="cart-sub-total-price">{{number_format($product->product->price)}}
                                                                            تومان</span></td>

                                                                    {{--@php dd($product) ; @endphp--}}
                                                                    <td class="cart-product-grand-total"><span
                                                                                class="cart-grand-total-price-offer">{{$product->off ? $product->off->offer: 0}} </span>
                                                                    </td>
                                                                    <td class="cart-product-grand-final"><span
                                                                                class="cart-grand-total-price-offer">{{number_format($product->final_price)}}
                                                                            تومان</span></td>
                                                                    <td class="romove-item"><a href="#" title="حذف"
                                                                                               class="icon remove-from-cart"
                                                                                               data-product-id="{{$product->id}}"><i
                                                                                    class="fa fa-trash-o"></i></a></td>
                                                                </tr>

                                                            @endforeach


                                                            </tbody><!-- /tbody -->
                                                        </table><!-- /table -->

                                                    @else

                                                        <div class="alert alert-danger">
                                                            <p>محصولی در سبد خرید وجود ندارد</p>
                                                        </div>

                                                    @endif

                                                </div>
                                            </div><!-- /.shopping-cart-table -->
                                        </div>
                                        <!-- panel-body  -->

                                    </div><!-- row -->
                                </div>
                                <!-- checkout-step-01  -->
                                <!-- checkout-step-02  -->

                                @if(!Auth::check())

                                    @php $count++; @endphp

                                    <div class="panel panel-default checkout-step-02">
                                        <div class="panel-heading">
                                            <h4 class="unicase-checkout-title">
                                                <a data-toggle="collapse" class="collapsed"
                                                   data-parent="#accordion" href="#collapseTwo">
                                                    <span>{{$count}}</span>ورود یا عضویت
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapse{{$count}}" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <div class="sign-in-page">
                                                    <div class="row">
                                                        <!-- Sign-in -->
                                                        @include("errors.listClient")
                                                        <div class="col-md-6 col-sm-6 sign-in">
                                                            <h4 class="">ورود به حساب کاربری</h4>
                                                            {!! Form::open(["url"=>route('login'),"method"=>"post","id"=>"login"]) !!}
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <div class="col-md-4">
                                                                        <label class="info-title"
                                                                               for="exampleInputEmail1">موبایل/آدرس
                                                                            ایمیل<span>*</span></label>
                                                                    </div>
                                                                    <div class="col-md-8">
                                                                        {!! Form::text("email",null,["class"=>"form-control unicase-form-control text-input","autocomplete"=>"off","placeholder"=>"شماره موبایل و یا پست الکترونیک","id"=>"email"]) !!}
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <div class="col-md-4">
                                                                        <label class="info-title"
                                                                               for="exampleInputPassword1">رمز عبور
                                                                            <span>*</span></label>

                                                                    </div>
                                                                    <div class="col-md-8">
                                                                        {!! Form::input("password","password",null,["class"=>"form-control unicase-form-control text-input","autocomplete"=>"off","placeholder"=>"گذرواژه"]) !!}
                                                                    </div>
                                                                </div>

                                                            </div>

                                                            <input type="hidden" name="redirect" value="cart#two">

                                                            <div class="radio outer-xs">
                                                                <a href="#" class="forgot-password pull-right">رمز عبور
                                                                    خود را فراموش کرده اید؟</a>
                                                            </div>
                                                            {!! Form::submit("ورود",["class"=>"btn-upper btn btn-primary checkout-page-button"]) !!}
                                                            {!! Form::close() !!}
                                                        </div>
                                                        <!-- Sign-in -->

                                                        <!-- create a new account -->
                                                        <div class="col-md-6 col-sm-6 create-new-account">
                                                            <h4 class="checkout-subtitle">ثبت نام حساب کاربری جدید</h4>

                                                            {!! Form::open(["url"=>route('register'),"method"=>"post","id"=>"register"]) !!}
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <div class="col-md-3">
                                                                        <label class="info-title"
                                                                               for="exampleInputEmail2">آدرس ایمیل
                                                                            <span>*</span></label>
                                                                    </div>
                                                                    <div class="col-md-9">
                                                                        {!! Form::text("email",null,["class"=>"form-control unicase-form-control text-input","autocomplete"=>"off","placeholder"=>"پست الکترونیک","id"=>"email"]) !!}
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <div class="col-md-3">
                                                                        <label class="info-title"
                                                                               for="exampleInputEmail2">نام
                                                                            <span>*</span></label>
                                                                    </div>
                                                                    <div class="col-md-9">
                                                                        {!! Form::text("first_name",null,["class"=>"form-control unicase-form-control text-input","autocomplete"=>"off","placeholder"=>"نام","id"=>"first_name"]) !!}
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <div class="col-md-3">
                                                                        <label class="info-title"
                                                                               for="exampleInputEmail2">نام خانوادگی
                                                                            <span>*</span></label>
                                                                    </div>
                                                                    <div class="col-md-9">
                                                                        {!! Form::text("last_name",null,["class"=>"form-control unicase-form-control text-input","autocomplete"=>"off","placeholder"=>"نام خانوادگی","id"=>"last_name"]) !!}
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <div class="col-md-3">
                                                                        <label class="info-title"
                                                                               for="exampleInputEmail2">کد ملی
                                                                            <span>*</span></label>
                                                                    </div>
                                                                    <div class="col-md-9">
                                                                        {!! Form::text("code_melli",null,["class"=>"form-control unicase-form-control text-input","autocomplete"=>"off","placeholder"=>"کد ملی","id"=>"last_name"]) !!}
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <div class="col-md-3">
                                                                        <label class="info-title"
                                                                               for="exampleInputEmail2">شماره تماس
                                                                            <span>*</span></label>
                                                                    </div>
                                                                    <div class="col-md-9">
                                                                        {!! Form::text("phone",null,["class"=>"form-control unicase-form-control text-input","autocomplete"=>"off","placeholder"=>"شماره تماس","id"=>"phone"]) !!}
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <div class="col-md-3">
                                                                        <label class="info-title"
                                                                               for="exampleInputEmail2">شماره موبایل
                                                                            <span>*</span></label>
                                                                    </div>
                                                                    <div class="col-md-9">
                                                                        {!! Form::text("mobile",null,["class"=>"form-control unicase-form-control text-input","autocomplete"=>"off","placeholder"=>"شماره موبایل","id"=>"mobile"]) !!}
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <div class="col-md-3">
                                                                        <label class="info-title"
                                                                               for="exampleInputEmail2">رمز عبور
                                                                            <span>*</span></label>

                                                                    </div>
                                                                    <div class="col-md-9">
                                                                        {!! Form::input("password","password",null,["class"=>"form-control unicase-form-control text-input","autocomplete"=>"off","placeholder"=>"گذرواژه"]) !!}
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <div class="col-md-3">
                                                                        <label class="info-title"
                                                                               for="exampleInputEmail2">تایید رمز عبور
                                                                            <span>*</span></label>
                                                                    </div>
                                                                    <div class="col-md-9">
                                                                        {!! Form::input("password","password_confirmation",null,["class"=>"form-control unicase-form-control text-input","autocomplete"=>"off","placeholder"=>"تایید گذرواژه"]) !!}
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <input type="hidden" name="redirect" value="cart#two">

                                                            {!! Form::submit("ثبت نام",["class"=>"btn-upper btn btn-primary checkout-page-button"]) !!}
                                                            {!! Form::close() !!}


                                                        </div>
                                                        <!-- create a new account -->            </div><!-- /.row -->
                                                </div><!-- /.sigin-in-->
                                            </div>
                                        </div>
                                    </div>
                                    <!-- checkout-step-02  -->

                            @endif

                            @php $count++; @endphp

                            <!-- checkout-step-03  -->
                                {!! Form::open(["url"=>route('orders.store'),"method"=>"post","id"=>"orderCreate"]) !!}

                                <div class="panel panel-default checkout-step-03">
                                    <div class="panel-heading">
                                        <h4 class="unicase-checkout-title">
                                            <a data-toggle="collapse" class="collapsed"
                                               data-parent="#accordion" href="#collapseThree">
                                                <span>{{$count}}</span>لیست آدرس ها
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapse{{$count}}" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            @include("address")
                                        </div>
                                    </div>
                                </div>
                                <!-- checkout-step-03  -->

                            @php $count++; @endphp

                            <!-- checkout-step-04  -->
                                <div class="panel panel-default checkout-step-04">
                                    <div class="panel-heading">
                                        <h4 class="unicase-checkout-title">
                                            <a data-toggle="collapse" class="collapsed"
                                               data-parent="#accordion" href="#collapseFour">
                                                <span>{{$count}}</span>انتخاب نحوه پرداخت و ارسال
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapse{{$count}}" class="panel-collapse collapse">
                                        <br>
                                        <h4>انتخاب شیوه ارسال</h4>
                                        <hr>
                                        <div class='shippings panel-body'>
                                            <table class="table table-striped">
                                                <thead>
                                                <tr>
                                                    <th class="cart-description item">انتخاب</th>
                                                    <th class="cart-description item">عنوان</th>
                                                    <th class="cart-description item">توضیحات</th>
                                                    <th class="cart-product-name item">هزینه ارسال</th>
                                                </tr>
                                                </thead>

                                                <tbody id="shippment">

                                        @foreach($shippments as $shippment)
                                            <tr>
                                            <td><input type="radio" name="shipping_id" value="{{$shippment->id}}"></td>
                                            <td class="cart-description item">
                                                <img src="{{thumb($shippment->media,5) }}" width="50px" alt="{{$shippment->name}}"> {{$shippment->name}}
                                            </td>
                                            <td class="cart-edit item">
                                                {!! $shippment->description !!}
                                            </td>
                                                <td class="cart-product-name item">
                                                    {{number_format($shippment->price) }} تومان
                                                </td>
                                            </tr>
                                        @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        <br>
                                        <hr>

                                        <h4>انتخاب شیوه پرداخت</h4>
                                        <hr>
                                        <div class="panel-body">


                                            <table class="table table-striped">
                                                <thead>
                                                <tr>
                                                    <th class="cart-description item">انتخاب</th>
                                                    <th class="cart-description item">عنوان</th>
                                                    <th class="cart-description item">توضیحات</th>
                                                </tr>
                                                </thead>

                                                <tbody id="shippment">

                                                @foreach($payments as $payment)
                                                    <tr>
                                                        <td><input type="radio" name="payment_id" value="{{$payment->id}}"></td>
                                                        <td class="cart-description item">
                                                            <img src="{{thumb($payment->media,5) }}" width="50px" alt=""> {{$payment->name}}
                                                        </td>
                                                        <td class="cart-edit item">
                                                            {!! $payment->description !!}
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>

                                            <a href="#"
                                               class="btn btn-upper btn-primary pull-left outer-left-xs next-step pull-left"
                                               data-step="5">مرحله قبل</a>
                                            {!! Form::submit("ثبت نهایی",["class"=>"btn-upper btn btn-primary checkout-page-button"]) !!}

                                            {!! Form::close() !!}

                                        </div>
                                    </div>
                                </div>
                                <!-- checkout-step-04  -->


                            </div><!-- /.checkout-steps -->
                        @else
                            <div class="alert alert-danger text-center cart-empty">
                                <h4>سبد خرید خالی است</h4>
                            </div>
                        @endif


                    </div>
                </div><!-- /.row -->
            </div><!-- /.checkout-box -->

        <!-- ============================================== BRANDS CAROUSEL : END ============================================== -->
        </div><!-- /.container -->
    </div><!-- /.body-content -->


@endsection


@section("extra_script")

    <style>


        .payments {
            min-width: 320px;
        }

        /*
        * Buttons
        **********************************************************/
        .button {
            position: relative;
            height: 50px;
            padding: 0 0 0 50px;
            font-size: 14px;
            line-height: 48px;
            border-bottom: 1px solid #bbb;
            background: #fafafa;
            cursor: pointer;
        }

        .button:hover {
            background: #f5f5f5;
        }

        .button:after {
            content: '';
            position: absolute;
            top: 15px;
            left: 18px;
            display: block;
            width: 20px;
            height: 20px;
            border-radius: 50%;
            box-shadow: inset 0 0 0 1px #bbb, inset 0 0 0 7px #fff;
            background: #fff;
        }

        .button:hover:after {
            background: #bbb;
        }

        .button.active:after {
            background: #555;
        }

        /*
        * Breakpoint
        **********************************************************/
        @media all and (min-width: 500px) {

            .button {
                height: 60px;
                padding: 0 0 0 60px;
                font-size: 18px;
                line-height: 58px;
            }

            .button:after {
                top: 20px;
                left: 23px;
            }
        }

        /*
        * Breakpoint
        **********************************************************/
        @media all and (min-width: 700px) {

            .payments {
                max-width: 700px;
                margin: 0 auto;
                padding: 25px;
                overflow: hidden;
            }

            .button {
                float: left;
                width: 100px;
                height: 150px;
                margin-right: 10px;
                padding: 50px 0 0;
                font-size: 12px;
                line-height: 1;
                text-align: center;
                border: 0;
                border-radius: 3px;
                box-shadow: inset 0 0 0 1px #bbb;
            }

            .button:last-child {
                margin-right: 0;
            }

            .button:after {
                top: 15px;
                left: 40px;
            }

            .footer {
                border-top: 1px solid #bbb;
            }
        }
    </style>

    <style>
        [data-toggle="collapse"] {
            cursor: default;
        }
    </style>

    <script>

        $(function (e) {

            $('a[data-toggle="collapse"]').click(function (e) {
                e.stopPropagation();
            });


            var hash = document.URL.substr(document.URL.indexOf('#') + 1);


            if (hash == "two") {

                console.log(hash);

                $("#collapse1").collapse("toggle");
                $("#collapse2").collapse("toggle");

            }


            $('.button').on('click', function () {
                $('.button').removeClass('active');
                $(this).toggleClass('active');
            });


        });

    </script>

    {!! JsValidator::formRequest('App\Http\Requests\UserAddressRequest', '#addAddress') !!}

    {!! JsValidator::formRequest('App\Http\Requests\RegisterRequest', '#register') !!}

    {!! JsValidator::formRequest('App\Http\Requests\LoginRequest', '#login') !!}

@endsection

