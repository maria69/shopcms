<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="keywords" content="MediaCenter, Template, eCommerce">
    <meta name="robots" content="all">

    <title>
        فروشگاهی اینترنتی مهاجر
        @yield("title")


    </title>

    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">

    <!-- Customizable CSS -->
    <link rel="stylesheet" href="/assets/css/custom.css">
    <link rel="stylesheet" href="/assets/css/main.css">
    <link rel="stylesheet" href="/assets/css/blue.css">
    <link rel="stylesheet" href="/assets/css/owl.carousel.css">
    <link rel="stylesheet" href="/assets/css/owl.transitions.css">
    <link rel="stylesheet" href="/assets/css/animate.min.css">
    <link rel="stylesheet" href="/assets/css/rateit.css">
    <link rel="stylesheet" href="/assets/css/bootstrap-select.min.css">
    <!-- Icons/Glyphs -->
    <link rel="stylesheet" href="/assets/css/font-awesome.css">
    <link href="/assets/css/lightbox.css" rel="stylesheet">
    <link href="/panelassets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css"/>
    <link href="/panelassets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css"/>

    <link rel="stylesheet" href="/assets/css/menu.css">

    <!-- Icons/Glyphs -->
    <link rel="stylesheet" href="assets/css/font-awesome.css">

    <!-- Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,600,600italic,700,700italic,800'
          rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    @yield('head')
</head>
<body class="cnt-home">
<div id="loading">
    <div class="loader" id="loader-1"></div>
</div>
<style>
    #loading {
        position: fixed;
        left: 0;
        top: 0;
        background: rgba(0, 0, 0, 0.5);
        width: 100%;
        height: 100vh;
        z-index: 10000;
        display: none;
    }

    /* ALL LOADERS */

    .loader {
        width: 100px;
        height: 100px;
        border-radius: 100%;
        position: relative;
        margin: 0 auto;
        top: 44vh;
    }

    /* LOADER 1 */

    #loader-1:before, #loader-1:after {
        content: "";
        position: absolute;
        top: -10px;
        left: -10px;
        width: 100%;
        height: 100%;
        border-radius: 100%;
        border: 10px solid transparent;
        border-top-color: #3498db;
    }

    #loader-1:before {
        z-index: 100;
        animation: spin 1s infinite;
    }

    #loader-1:after {
        border: 10px solid #ccc;
    }

    @keyframes spin {
        0% {
            -webkit-transform: rotate(0deg);
            -ms-transform: rotate(0deg);
            -o-transform: rotate(0deg);
            transform: rotate(0deg);
        }

        100% {
            -webkit-transform: rotate(360deg);
            -ms-transform: rotate(360deg);
            -o-transform: rotate(360deg);
            transform: rotate(360deg);
        }
    }
</style>
<!-- ============================================== HEADER ============================================== -->
<header class="header-style-1">

    <!-- ============================================== TOP MENU ============================================== -->
    <div class="top-bar animate-dropdown">
        <div class="container">
            <div class="header-top-inner">
                <div class="cnt-account">
                    <ul class="list-unstyled">
                        {{--<li><a href="{{route('wish_list.index')}}"><i class="icon fa fa-heart"></i>لیست علاقه مندی--}}
                        {{--ها</a></li>--}}
                        <li><a href="{{route('cart')}}"><i class="icon fa fa-shopping-cart"></i>سبد خرید</a></li>
                        @guest
                            <li><a href="{{route('phoneLogin')}}"><i class="icon fa fa-lock"></i>ورود</a></li>
                            <li><a href="{{route('phoneLogin')}}"><i class="icon fa fa-lock"></i>ثبت نام</a></li>
                        @else
                            <li><a class="nav-link text-success btn btn-outline-success" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();document.getElementById('logout-form').submit();"><i
                                            class="icon fa fa-lock"></i>خروج</a></li>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                            <li><a href="{{route('userProfile')}}"><i class="icon fa fa-user"></i>حساب کاربری</a></li>
                            <li><a href="{{route('orders.index')}}"><i class="icon fa fa-user"></i>تاریخچه سفارشات</a>
                            </li>
                        @endguest
                    </ul>
                </div>
                <!-- /.cnt-account -->

                <!-- /.cnt-cart -->
                <div class="clearfix"></div>
            </div>
            <!-- /.header-top-inner -->
        </div>
        <!-- /.container -->
    </div>
    <!-- /.header-top -->
    <!-- ============================================== TOP MENU : END ============================================== -->
    <div class="main-header">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-2 logo-holder">
                    <!-- ============================================================= LOGO ============================================================= -->
                    <div class="logo"><a href="/"> <img src=".." alt="logo"> </a></div>
                    <!-- /.logo -->
                    <!-- ============================================================= LOGO : END ============================================================= -->
                </div>
                <!-- /.logo-holder -->


                <div class="col-xs-12 col-sm-12 col-md-7 top-search-holder">
                    <!-- /.contact-row -->
                    <!-- ============================================================= SEARCH AREA ============================================================= -->
                    <div class="search-area">
                        {!! Form::open(['url'=>route("search"),'method'=>'get','id'=>'searchForm']) !!}
                        <div class="control-group">
                            <input class="search-field" name="s"
                                   value="{{ Request::has("s") ? Request::input("s") : "" }}"
                                   placeholder="نام محصول را وارد نمایید ..."/>
                            <button class="search-button" href="#"></button>
                            <label class="label_filter">
                                <select class="categories-filter" name="type">
                                    @foreach($productTypeSearch as $k=>$v)

                                        <option value="{{$k}}" {{ Request::has("type") ? ( $k==Request::input("type") ? "selected" : "" ) : null }}>{{$v}}</option>

                                    @endforeach

                                </select>
                            </label>
                            {{--<ul class="categories-filter animate-dropdown">--}}
                            {{--<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown"--}}
                            {{--href="">دسته بندی ها <b class="caret"></b></a>--}}
                            {{--<ul class="dropdown-menu" role="menu">--}}

                            {{----}}
                            {{----}}
                            {{--@foreach($categorySearch as $category)--}}

                            {{--<li>{{$category->name}}</li>--}}

                            {{--@endforeach--}}

                            {{--</ul>--}}
                            {{--</li>--}}
                            {{--</ul>--}}
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <!-- /.search-area -->
                    <!-- ============================================================= SEARCH AREA : END ============================================================= -->
                </div>
                <!-- /.top-search-holder -->

                <div class="col-xs-12 col-sm-12 col-md-2 animate-dropdown top-cart-row">
                    <!-- ============================================================= SHOPPING CART DROPDOWN ============================================================= -->

                    <div class="dropdown dropdown-cart"><a href="{{route('cart')}}" class="dropdown-toggle lnk-cart"
                                                           data-toggle="dropdown">
                            <div class="items-cart-inner">
                                <div class="basket"><i class="glyphicon glyphicon-shopping-cart"></i></div>

                                <div class="basket-item-count"><span
                                            class="count"> @if( isset($cartTop)) {{$cartTop['count']}} @else
                                            0 @endif </span></div>

                                <div class="total-price-basket"><span class="lbl">سبد خرید</span>
                                    <span class="value">

                                        @if(isset($cartTop))     {{number_format($cartTop['totalPrice'])}} @else
                                            0 @endif
                                    </span>
                                    {{--<span class="value"> @if(isset($cartTop->final_price)) {{$cartTop->final_price}} @else 0 @endif </span>--}}
                                    <span class="total-price"> <span class="sign">تومان</span>

                                    </span>

                                </div>
                            </div>
                        </a>


                        <ul class="dropdown-menu cart-menu">

                            @if(isset($cartTop))
                                @foreach($cartTop['products'] as $product)

                                    <li>
                                        <div class="cart-item product-summary">
                                            <div class="row">
                                                <div class="col-xs-4">
                                                    <div class="image"><a
                                                                href="{{getUrl($product->product->id)}}"><img
                                                                    src="{{thumb($product->product->media,5)}}"
                                                                    alt="{{$product->name}}"></a>
                                                    </div>
                                                </div>
                                                <div class="col-xs-7">
                                                    <h3 class="name"><a href="/product/{{$product->product->id}}">{{$product->product->name}}</a>
                                                    </h3>
                                                    <div class="price">{{number_format($product->unit_price)}}تومان
                                                    </div>
                                                </div>
                                                {{--<div class="col-xs-1 action"><a href="#"><i class="fa fa-trash"></i></a>--}}
                                                {{--</div>--}}
                                            </div>
                                        </div>
                                        <!-- /.cart-item -->
                                        <div class="clearfix"></div>
                                        <hr>

                                        <!-- /.cart-total-->

                                    </li>

                                @endforeach
                            @endif


                            <div class="clearfix cart-total">
                                <div class="pull-right"><span class="text">جمع کل :</span><span
                                            class='price_total'>  {{number_format($cartTop['totalPrice'])}}تومان</span>
                                </div>
                                <div class="clearfix"></div>
                                <a href="{{"/cart"}}"
                                   class="btn btn-upper btn-primary btn-block m-t-20">تسویه حساب</a></div>
                        </ul>
                        <!-- /.dropdown-menu-->
                    </div>
                    <!-- /.dropdown-cart -->

                    <!-- ============================================================= SHOPPING CART DROPDOWN : END============================================================= -->
                </div>
                <!-- /.top-cart-row -->


            </div>
            <!-- /.row -->

        </div>
        <!-- /.container -->

    </div>
    <!-- /.main-header -->

    <!-- ============================================== NAVBAR ============================================== -->

    <div id='cssmenu'>
        <ul>


            <li><a href='/'><span class="home"> <i class="fa fa-home" aria-hidden="true"></i></span></a></li>
            @foreach($categoriesInNav as $item)
                @if($item->childrens->count() > 0)
                    <li class="active has-sub"><a href='/category/{{$item->id}}'><span>{{$item->name}}</span></a>
                        <ul>
                            @foreach($item->childrens as $child)
                                <li class='has-sub'><a href='/category/{{$child->id}}'><span>{{$child->name}}</span></a>
                                    @if($child->childrens->count() > 0)
                                        <ul>
                                            @foreach($child->childrens  as $childitem)
                                                <li>
                                                    <a href='/category/{{$childitem->id}}'><span>{{$childitem->name}}</span></a>
                                                </li>
                                                {{--<li class='last'><a href='#'><span>Sub Product</span></a></li>--}}
                                            @endforeach
                                        </ul>
                                    @endif
                                </li>
                            @endforeach
                        </ul>
                    </li>

                @else
                    <li><a href='/category/{{$item->id}}'><span>{{$item->name}}</span></a></li>
                @endif
            @endforeach

            <li><a href='/تماس با ما'><span>تماس با ما</span></a></li>

            {{--<li><a href='/'><span>خانه</span></a></li>--}}
        </ul>
    </div>


{{--<div class="header-nav animate-dropdown">--}}
{{--<div class="container">--}}
{{--<div class="yamm navbar navbar-default" role="navigation">--}}
{{--<div class="navbar-header">--}}
{{--<button data-target="#mc-horizontal-menu-collapse" data-toggle="collapse"--}}
{{--class="navbar-toggle collapsed" type="button">--}}
{{--<span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span--}}
{{--class="icon-bar"></span> <span class="icon-bar"></span></button>--}}
{{--</div>--}}


{{--<div class="nav-bg-class">--}}
{{--<div class="navbar-collapse collapse" id="mc-horizontal-menu-collapse">--}}
{{--<div class="nav-outer">--}}
{{--<ul class="nav navbar-nav">--}}


{{--@foreach($categoriesInNav as $item)--}}
{{--<li class="dropdown yamm mega-menu">--}}
{{--<a href="/category/{{$item->id}}"--}}
{{--data-hover="dropdown"--}}
{{--class="dropdown-toggle"--}}
{{--data-toggle="dropdown">{{$item->name}} </a>--}}
{{--@if($item->childrens->count() > 0)--}}


{{--@foreach($item->childrens as $child)--}}
{{--<ul class="dropdown-menu container">--}}
{{--<li>--}}
{{--<div class="yamm-content ">--}}
{{--<div class="row col-md-12">--}}


{{--<div class="col-xs-12 col-sm-12 col-md-12 col-menu">--}}
{{--<h2 class="title">{{$child->name}}</h2>--}}

{{--@if($child->childrens->count() > 0)--}}
{{--<ul class="links">--}}
{{--@foreach($item->childrens as $child)--}}
{{--<li class="col-md-4">--}}
{{--<a href="/category/{{$child->id}}">{{$child->name}}</a>--}}
{{--</li>--}}
{{--@endforeach--}}
{{--</ul>--}}
{{--@endif--}}
{{--</div>--}}
{{--<!-- /.col -->--}}

{{--<div class="col-xs-12 col-sm-6 col-md-4 col-menu banner-image">--}}
{{--<img class="img-responsive" src="{{thumb($child->media,5)}}" alt="">--}}
{{--</div>--}}
{{--<!-- /.yamm-content -->--}}
{{--</div>--}}
{{--</div>--}}
{{--</li>--}}
{{--</ul>--}}
{{--@endforeach--}}
{{--@endif--}}
{{--</li>--}}
{{--@endforeach--}}


{{--</ul>--}}
{{--<!-- /.navbar-nav -->--}}
{{--<div class="clearfix"></div>--}}
{{--</div>--}}
{{--<!-- /.nav-outer -->--}}
{{--</div>--}}
{{--<!-- /.navbar-collapse -->--}}

{{--</div>--}}
{{--<!-- /.nav-bg-class -->--}}
{{--</div>--}}
{{--<!-- /.navbar-default -->--}}
{{--</div>--}}
{{--<!-- /.container-class -->--}}

{{--</div>--}}
<!-- /.header-nav -->
    <!-- ============================================== NAVBAR : END ============================================== -->

</header>

<!-- ============================================== HEADER : END ============================================== -->


@yield("content")


<!-- ============================================================= FOOTER ============================================================= -->
<footer id="footer" class="footer color-bg">
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-3 text-right">
                    <div class="module-heading">
                        <h4 class="module-title">تماس با ما</h4>
                    </div>
                    <!-- /.module-heading -->

                    <div class="module-body">
                        <ul class="toggle-footer" style="">
                            <li class="media">
                                <div class="pull-right"><span class="icon fa-stack fa-lg"> <i
                                                class="fa fa-map-marker fa-stack-1x fa-inverse"></i> </span></div>
                                <div class="media-body">
                                    <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ</p>
                                </div>
                            </li>
                            <li class="media">
                                <div class="pull-right"><span class="icon fa-stack fa-lg"> <i
                                                class="fa fa-mobile fa-stack-1x fa-inverse"></i> </span></div>
                                <div class="media-body">
                                    <p>+(98) 111-1111<br>
                                        +(98) 000-0000</p>
                                </div>
                            </li>
                            <li class="media">
                                <div class="pull-right"><span class="icon fa-stack fa-lg"> <i
                                                class="fa fa-envelope fa-stack-1x fa-inverse"></i> </span></div>
                                <div class="media-body"><span><a href="#">info@daytel.com</a></span></div>
                            </li>
                        </ul>
                    </div>
                    <!-- /.module-body -->
                </div>
                <!-- /.col -->

                <div class="col-xs-12 col-sm-6 col-md-3 text-right">
                    <div class="module-heading">
                        <h4 class="module-title">خدمات مشتریان</h4>
                    </div>
                    <!-- /.module-heading -->

                    <div class="module-body">
                        <ul class='list-unstyled'>
                            <li class="first"><a href="#" title="Contact us">حساب کاربری</a></li>
                            <li><a href="#" title="About us">تاریخچه سفارشات</a></li>
                            <li><a href="#" title="faq">سوالات متدوال</a></li>
                            <li><a href="#" title="Popular Searches">تماس با ما</a></li>
                            <li class="last"><a href="#" title="Where is my order?">پشتیبانی</a></li>
                        </ul>
                    </div>
                    <!-- /.module-body -->
                </div>
                <!-- /.col -->

                <div class="col-xs-12 col-sm-6 col-md-3 text-right">
                    <div class="module-heading">
                        <h4 class="module-title">لینک های مفید</h4>
                    </div>
                    <!-- /.module-heading -->

                    <div class="module-body">
                        <ul class='list-unstyled'>
                            <li class="first"><a title="Your Account" href="#">درباره ما</a></li>
                            <li><a title="Information" href="#">خدمات مشتریان</a></li>
                            <li><a title="Addresses" href="#">برند ها</a></li>
                            <li><a title="Addresses" href="#">همکاری با ما</a></li>
                            <li class="last"><a title="Orders History" href="#">بلاگ</a></li>
                        </ul>
                    </div>
                    <!-- /.module-body -->
                </div>
                <!-- /.col -->

                <div class="col-xs-12 col-sm-6 col-md-3 text-right">
                    <div class="module-heading">
                        <h4 class="module-title">فروشگاه دی تل</h4>
                    </div>
                    <!-- /.module-heading -->

                    <div class="module-body">
                        <ul class='list-unstyled'>
                            <li class="first"><a href="#" title="About us">راهنمای ثبت سفارش</a></li>
                            <li><a href="#" title="Blog">وبلگ</a></li>
                            <li><a href="#" title="Company">آشنایی با دی تل</a></li>
                            <li><a href="#" title="Investor Relations">نمایندگی ها</a></li>
                            <li class=" last"><a href="" title="Suppliers">تماس با ما</a></li>
                        </ul>
                    </div>
                    <!-- /.module-body -->
                </div>
            </div>
        </div>
    </div>
    <div class="copyright-bar">
        <div class="container">
            <div class="col-xs-12 col-sm-6 no-padding social">
                <ul class="link">
                    <li class="fb pull-left"><a target="_blank" rel="nofollow" href="#" title="Facebook"></a></li>
                    <li class="tw pull-left"><a target="_blank" rel="nofollow" href="#" title="Twitter"></a></li>
                    <li class="googleplus pull-left"><a target="_blank" rel="nofollow" href="#" title="GooglePlus"></a>
                    </li>
                    <li class="rss pull-left"><a target="_blank" rel="nofollow" href="#" title="RSS"></a></li>
                    <li class="pintrest pull-left"><a target="_blank" rel="nofollow" href="#" title="PInterest"></a>
                    </li>
                    <li class="linkedin pull-left"><a target="_blank" rel="nofollow" href="#" title="Linkedin"></a></li>
                    <li class="youtube pull-left"><a target="_blank" rel="nofollow" href="#" title="Youtube"></a></li>
                </ul>
            </div>
            <div class="col-xs-12 col-sm-6 no-padding">
                <div class="clearfix payment-methods">
                    <ul>
                        <li><img src="/assets/images/payments/1.png" alt=""></li>
                        <li><img src="/assets/images/payments/2.png" alt=""></li>
                        <li><img src="/assets/images/payments/3.png" alt=""></li>
                        <li><img src="/assets/images/payments/4.png" alt=""></li>
                        <li><img src="/assets/images/payments/5.png" alt=""></li>
                    </ul>
                </div>
                <!-- /.payment-methods -->
            </div>
        </div>
    </div>
</footer>
<!-- ============================================================= FOOTER : END============================================================= -->

<!-- For demo purposes – can be removed on production -->

<!-- For demo purposes – can be removed on production : End -->

<!-- JavaScripts placed at the end of the document so the pages load faster -->
<link rel="stylesheet" href="/assets/css/sweetalert.css">
<script src="/assets/js/sweetalert.min.js"></script>
{{--@include("errors.sweet")--}}


<script src="/assets/js/jquery-1.11.1.min.js"></script>
<script src="/assets/js/bootstrap.min.js"></script>
<script src="/assets/js/bootstrap-hover-dropdown.min.js"></script>
<script src="/assets/js/owl.carousel.min.js"></script>
<script src="/assets/js/echo.min.js"></script>
<script src="/assets/js/jquery.easing-1.3.min.js"></script>
<script src="/assets/js/bootstrap-slider.min.js"></script>
<script src="/assets/js/jquery.rateit.min.js"></script>
<script type="text/javascript" src="/assets/js/lightbox.min.js"></script>
<script src="/assets/js/bootstrap-select.min.js"></script>
<script src="/assets/js/wow.min.js"></script>
<script src="/assets/js/scripts.js"></script>
<script src="/vendor/jsvalidation/js/jsvalidation.js"></script>
<script src="/panelassets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
@yield('extra_script')
<script type="text/javascript">

    var routes = {
        wishlist: '{{route("addWishList")}}',
        search: '{{route("search")}}',
        addAddress: '{{route("addWishList")}}',
        addToCart: '{{route("cart.addToCart")}}',
        deleteFromCart: '{{route("cart.deleteFromCart")}}',
    };

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });


</script>

<script src="/assets/js/ajax.js"></script>
</body>
</html>