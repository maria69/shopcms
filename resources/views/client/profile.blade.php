@extends("client.layout")
{{--@section("title") فروشگاه اینترنتی دی تل @endsection--}}
@section("content")

    <div class="body-content outer-top-xs" id="top-banner-and-menu">
        <div class="container">
            <div class="row">
                <!-- ========================================== SECTION – HERO ========================================= -->
                <div class="col-xs-12 col-sm-12 col-md-12 homebanner-holder">
                    <div class="body-content">
                        <div class="container">
                            <div class="checkout-box ">
                                <div class="row">
                                    <div class="col-md-12">
                                        @include("errors.listClient")

                                        <div class="panel-group checkout-steps" id="accordion">
                                            <!-- checkout-step-01  -->
                                            <div class="panel panel-default checkout-step-01">

                                                <!-- panel-heading -->
                                                <div class="panel-heading">
                                                    <h4 class="unicase-checkout-title">
                                                        <a data-toggle="collapse" class="" data-parent="#accordion"
                                                           href="#collapseOne">
                                                            <span>1</span>ویرایش حساب کاربری
                                                        </a>
                                                    </h4>
                                                </div>
                                                <!-- panel-heading -->

                                                <div id="collapseOne" class="panel-collapse collapse in">

                                                    <!-- panel-body  -->
                                                    <div class="panel-body">
                                                        <div class="row">

                                                            <div class="col-md-12 col-sm-12 create-new-account">
                                                                <h4 class="checkout-subtitle">ویرایش حساب کاربری
                                                                    جدید</h4>

                                                                {!! Form::open(["url"=>route('updateUserInfo'),"method"=>"put","id"=>"register"]) !!}
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-md-3">
                                                                            <label class="info-title"
                                                                                   for="exampleInputEmail2">آدرس ایمیل
                                                                                <span>*</span></label>
                                                                        </div>
                                                                        <div class="col-md-9">
                                                                            {!! Form::text("email",$user->email,["class"=>"form-control unicase-form-control text-input","autocomplete"=>"off","placeholder"=>"پست الکترونیک","id"=>"email"]) !!}
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-md-3">
                                                                            <label class="info-title"
                                                                                   for="exampleInputEmail2">نام
                                                                                <span>*</span></label>
                                                                        </div>
                                                                        <div class="col-md-9">
                                                                            {!! Form::text("first_name",$user->first_name,["class"=>"form-control unicase-form-control text-input","autocomplete"=>"off","placeholder"=>"نام","id"=>"first_name"]) !!}
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-md-3">
                                                                            <label class="info-title"
                                                                                   for="exampleInputEmail2">نام خانوادگی
                                                                                <span>*</span></label>
                                                                        </div>
                                                                        <div class="col-md-9">
                                                                            {!! Form::text("last_name",$user->last_name,["class"=>"form-control unicase-form-control text-input","autocomplete"=>"off","placeholder"=>"نام خانوادگی","id"=>"last_name"]) !!}
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-md-3">
                                                                            <label class="info-title"
                                                                                   for="exampleInputEmail2">کد ملی
                                                                                <span>*</span></label>
                                                                        </div>
                                                                        <div class="col-md-9">
                                                                            {!! Form::text("code_melli",$user->code_melli,["class"=>"form-control unicase-form-control text-input","autocomplete"=>"off","placeholder"=>"کد ملی","id"=>"last_name"]) !!}
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-md-3">
                                                                            <label class="info-title"
                                                                                   for="exampleInputEmail2">شماره تماس
                                                                                <span>*</span></label>
                                                                        </div>
                                                                        <div class="col-md-9">
                                                                            {!! Form::text("phone",$user->phone,["class"=>"form-control unicase-form-control text-input","autocomplete"=>"off","placeholder"=>"شماره تماس","id"=>"phone"]) !!}
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-md-3">
                                                                            <label class="info-title"
                                                                                   for="exampleInputEmail2">شماره موبایل
                                                                                <span>*</span></label>
                                                                        </div>
                                                                        <div class="col-md-9">
                                                                            {!! Form::text("mobile",$user->mobile,["class"=>"form-control unicase-form-control text-input","autocomplete"=>"off","placeholder"=>"شماره موبایل","id"=>"mobile"]) !!}
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-md-3">
                                                                            <label class="info-title"
                                                                                   for="exampleInputEmail2">رمز عبور
                                                                                <span>*</span></label>

                                                                        </div>
                                                                        <div class="col-md-9">
                                                                            {!! Form::input("password","password",null,["class"=>"form-control unicase-form-control text-input","autocomplete"=>"off","placeholder"=>""]) !!}
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-md-3">
                                                                            <label class="info-title"
                                                                                   for="exampleInputEmail2">تایید رمز
                                                                                عبور <span>*</span></label>
                                                                        </div>
                                                                        <div class="col-md-9">
                                                                            {!! Form::input("password","password_confirmation",null,["class"=>"form-control unicase-form-control text-input","autocomplete"=>"off","placeholder"=>"تایید گذرواژه"]) !!}
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                {!! Form::submit("ثبت نام",["class"=>"btn-upper btn btn-primary checkout-page-button"]) !!}
                                                                {!! Form::close() !!}


                                                            </div>

                                                        </div>
                                                    </div>
                                                    <!-- panel-body  -->

                                                </div><!-- row -->
                                            </div>
                                            <!-- checkout-step-01  -->
                                            <!-- checkout-step-02  -->
                                            <div class="panel panel-default checkout-step-02">
                                                <div class="panel-heading">
                                                    <h4 class="unicase-checkout-title">
                                                        <a data-toggle="collapse" class="collapsed"
                                                           data-parent="#accordion" href="#collapseTwo">
                                                            <span>2</span>لیست آدرس ها
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseTwo" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        @include("address")
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- checkout-step-02  -->

                                            <!-- checkout-step-03  -->
                                            <!-- checkout-step-03  -->

                                            <!-- checkout-step-04  -->
                                            <div class="panel panel-default checkout-step-03">
                                                <div class="panel-heading">
                                                    <h4 class="unicase-checkout-title">
                                                        <a data-toggle="collapse" class="collapsed"
                                                           data-parent="#accordion" href="#collapse3">
                                                            <span>3</span>لیست سفارشات
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapse3" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <div class="table-responsive">
                                                            <table class="table table-striped">
                                                                <thead>
                                                                <tr>
                                                                    <th class="cart-description item">کد سفارش</th>
                                                                    <th class="cart-description item">تاریخ ثبت سفارش</th>
                                                                    <th class="cart-product-name item">مبلغ سفارش</th>
                                                                    <th class="cart-edit item">وضعیت سفارش</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody id="addressResult">
                                                                @foreach($orders as $order)
                                                                    <tr>
                                                                        <td class="cart-description item">
                                                                            {{$order->trackingCode}}
                                                                        </td>
                                                                        <td>
                                                                            <span class='label label-primary'> {{ jDate::forge($order->created_at)->format('Y/m/d H:i:s') }}</span>
                                                                        </td>
                                                                        <td class="cart-edit item">
                                                                            {{number_format($order->finalPrice)}} تومان
                                                                        </td>
                                                                        <td class="cart-qty item">
                                                                            {!! orderStatus($order->status) !!}
                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                                </tbody><!-- /tbody -->
                                                            </table><!-- /table -->
                                                        </div>                                                    </div>
                                                </div>
                                            </div>
                                            <!-- checkout-step-04  -->
                                            <div class="panel panel-default checkout-step-04">
                                                <div class="panel-heading">
                                                    <h4 class="unicase-checkout-title">
                                                        <a data-toggle="collapse" class="collapsed"
                                                           data-parent="#accordion" href="#collapseFour">
                                                            <span>4</span>پیگیری سفارشات
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseFour" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                                                        eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                                                        enim ad minim veniam, quis nostrud exercitation ullamco laboris
                                                        nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
                                                        in reprehenderit in voluptate velit esse cillum dolore eu fugiat
                                                        nulla pariatur. Excepteur sint occaecat cupidatat non proident,
                                                        sunt in culpa qui officia deserunt mollit anim id est laborum.
                                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                                                        eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                                                        enim ad minim veniam, quis nostrud exercitation ullamco laboris
                                                        nisi ut aliquip ex ea commodo consequat.
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- checkout-step-04  -->


                                        </div><!-- /.checkout-steps -->
                                    </div>
                                </div><!-- /.row -->
                            </div><!-- /.checkout-box -->
                            <!-- ============================================== BRANDS CAROUSEL ============================================== -->
                            <div id="brands-carousel" class="logo-slider wow fadeInUp">

                                <div class="logo-slider-inner">
                                    <div id="brand-slider" class="owl-carousel brand-slider custom-carousel owl-theme">
                                        <div class="item m-t-15">
                                            <a href="#" class="image">
                                                <img data-echo="assets/images/brands/brand1.png"
                                                     src="assets/images/blank.gif" alt="">
                                            </a>
                                        </div><!--/.item-->

                                        <div class="item m-t-10">
                                            <a href="#" class="image">
                                                <img data-echo="assets/images/brands/brand2.png"
                                                     src="assets/images/blank.gif" alt="">
                                            </a>
                                        </div><!--/.item-->

                                        <div class="item">
                                            <a href="#" class="image">
                                                <img data-echo="assets/images/brands/brand3.png"
                                                     src="assets/images/blank.gif" alt="">
                                            </a>
                                        </div><!--/.item-->

                                        <div class="item">
                                            <a href="#" class="image">
                                                <img data-echo="assets/images/brands/brand4.png"
                                                     src="assets/images/blank.gif" alt="">
                                            </a>
                                        </div><!--/.item-->

                                        <div class="item">
                                            <a href="#" class="image">
                                                <img data-echo="assets/images/brands/brand5.png"
                                                     src="assets/images/blank.gif" alt="">
                                            </a>
                                        </div><!--/.item-->

                                        <div class="item">
                                            <a href="#" class="image">
                                                <img data-echo="assets/images/brands/brand6.png"
                                                     src="assets/images/blank.gif" alt="">
                                            </a>
                                        </div><!--/.item-->

                                        <div class="item">
                                            <a href="#" class="image">
                                                <img data-echo="assets/images/brands/brand2.png"
                                                     src="assets/images/blank.gif" alt="">
                                            </a>
                                        </div><!--/.item-->

                                        <div class="item">
                                            <a href="#" class="image">
                                                <img data-echo="assets/images/brands/brand4.png"
                                                     src="assets/images/blank.gif" alt="">
                                            </a>
                                        </div><!--/.item-->

                                        <div class="item">
                                            <a href="#" class="image">
                                                <img data-echo="assets/images/brands/brand1.png"
                                                     src="assets/images/blank.gif" alt="">
                                            </a>
                                        </div><!--/.item-->

                                        <div class="item">
                                            <a href="#" class="image">
                                                <img data-echo="assets/images/brands/brand5.png"
                                                     src="assets/images/blank.gif" alt="">
                                            </a>
                                        </div><!--/.item-->
                                    </div><!-- /.owl-carousel #logo-slider -->
                                </div><!-- /.logo-slider-inner -->

                            </div><!-- /.logo-slider -->
                            <!-- ============================================== BRANDS CAROUSEL : END ============================================== -->
                        </div><!-- /.container -->
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection
@section('extra_script')
    {!! JsValidator::formRequest('App\Http\Requests\UserAddressRequest', '#addAddress') !!}
    @endsection
