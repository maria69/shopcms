@extends("client.layout")
{{--@section("title") فروشگاه اینترنتی دی تل @endsection--}}
@section("content")

    <br>
    <div class="body-content">
        <div class="container">
            <div class="table-responsive">
                <table class="table table-striped" id="compaire">
                    <tbody>
                    <tr>
                        <td width="15%" class="feature">محصولات</td>
                        <td width="21.25%">
                            @if(!isset($products[0]))

                                <h5>افزودن</h5>
                                <hr>

                                {!! Form::open(['url'=>route("addCompaireToList"),'method'=>'post']) !!}


                                <div class="form-group">
                                    {!! Form::select('type',$types , $type,["class"=>"form-control selectRtl","id"=>"chooseType","data-action"=>route("getProductsByType")]) !!}
                                </div>

                                <hr>

                                <div class="form-group">
                                    {!! Form::select('c1',$allProducts , (isset($products[0]) ? key($products[0]) : null),["class"=>"form-control selectRtl chooseProducts"]) !!}
                                </div>

                                <button class="btn btn-primary pull-left">افزودن</button>


                                {!! Form::close() !!}

                            @else
                                <div class="productInfo">
                                    {!! Form::open(['url'=>route("removeFromCompaire"),'method'=>'post']) !!}
                                      <button class="close"><i class="fa fa-close"></i></button>
                                        <input name="cId" type="hidden" value="1">
                                    {!! Form::close() !!}
                                <img src="{{thumb($products[0]->media,5)}}" alt="">
                                <h4 class="productHeader">{{$products[0]->name}}</h4>
                                <h4 class="productHeader">{{$products[0]->en_name}}</h4>
                                </div>
                            @endif

                        </td>
                        <td width="21.25%">
                            @if(!isset($products[1]))
                                <h5>افزودن</h5>
                                <hr>
                                {!! Form::open(['url'=>route("addCompaireToList"),'method'=>'post']) !!}

                                <div class="form-group">
                                    {!! Form::select('type',$types , $type,["class"=>"form-control selectRtl","disabled"]) !!}
                                </div>

                                <hr>

                                <div class="form-group">
                                    {!! Form::select('c2',$allProducts, (isset($products[1]) ? key($products[1]) : null),["class"=>"form-control selectRtl chooseProducts",(isset($products[0])?"":"disabled")]) !!}
                                </div>

                                <button class="btn btn-primary pull-left" {{(isset($products[0])?"":"disabled")}}>افزودن</button>

                                {!! Form::close() !!}
                            @else

                                <div class="productInfo">
                                        {!! Form::open(['url'=>route("removeFromCompaire"),'method'=>'post']) !!}
                                          <button class="close"><i class="fa fa-close"></i></button>
                                            <input name="cId" type="hidden" value="2">
                                        {!! Form::close() !!}
                                    <img src="{{thumb($products[1]->media,5)}}" alt="">
                                    <h4 class="productHeader">{{$products[1]->name}}</h4>
                                    <h4 class="productHeader">{{$products[1]->en_name}}</h4>
                                </div>
                            @endif
                        </td>
                        <td width="21.25%">
                            @if(!isset($products[2]))
                            <h5>افزودن</h5>
                            <hr>

                                {!! Form::open(['url'=>route("addCompaireToList"),'method'=>'post']) !!}

                                    <div class="form-group">
                                        {!! Form::select('type',$types , $type,["class"=>"form-control selectRtl","disabled"]) !!}
                                    </div>

                                    <hr>

                                    <div class="form-group">
                                        {!! Form::select('c3',$allProducts , (isset($products[2]) ? key($products[2]) : null),["class"=>"form-control selectRtl chooseProducts",(isset($products[1])?"":"disabled")]) !!}
                                    </div>

                                    <button class="btn btn-primary pull-left" {{(isset($products[1])?"":"disabled")}}>افزودن</button>

                                {!! Form::close() !!}
                            @else
                                <div class="productInfo">
                                        {!! Form::open(['url'=>route("removeFromCompaire"),'method'=>'post']) !!}
                                          <button class="close"><i class="fa fa-close"></i></button>
                                            <input name="cId" type="hidden" value="3">
                                        {!! Form::close() !!}
                                    <img src="{{thumb($products[2]->media,5)}}" alt="">
                                    <h4 class="productHeader">{{$products[2]->name}}</h4>
                                    <h4 class="productHeader">{{$products[2]->en_name}}</h4>
                                </div>
                            @endif
                        </td>
                        <td width="22.5%">
                            @if(!isset($products[3]))
                                {!! Form::open(['url'=>route("addCompaireToList"),'method'=>'post']) !!}

                                <h5>افزودن</h5>
                                <hr>
                                <div class="form-group">
                                    {!! Form::select('type',$types , $type,["class"=>"form-control selectRtl","disabled"]) !!}
                                </div>

                                <hr>

                                <div class="form-group">
                                    {!! Form::select('c4',$allProducts , (isset($products[3]) ? key($products[3]) : null),["class"=>"form-control selectRtl chooseProducts",(isset($products[2])?"":"disabled")]) !!}
                                </div>

                                <button class="btn btn-primary pull-left" {{(isset($products[2])?"":"disabled")}}>افزودن</button>

                                {!! Form::close() !!}
                            @else
                                <div class="productInfo">
                                        {!! Form::open(['url'=>route("removeFromCompaire"),'method'=>'post']) !!}
                                            <button class="close"><i class="fa fa-close"></i></button>
                                            <input name="cId" type="hidden" value="4">
                                        {!! Form::close() !!}
                                    <img src="{{thumb($products[3]->media,5)}}" alt="">
                                    <h4 class="productHeader">{{$products[3]->name}}</h4>
                                    <h4 class="productHeader">{{$products[3]->en_name}}</h4>
                                </div>
                            @endif
                        </td>
                    </tr>
                    </tbody>
                </table>
                @if(Request::has(["c1"]) || Request::has(["c2"]) || Request::has(["c3"]) || Request::has(["c4"]))
                    <table class="table table-striped" id="compaireFeatures">
                        <tbody>
                        @foreach($featuresCategory as $category)

                            <tr>
                                <td class="featureHeader" colspan="5"> {{$category->name}} </td>
                                </td>


                            @foreach($category->features as $feature)
                                <tr>
                                    <td width="15%" class="feature"> {{$feature->name}} </td>
                                    <td width="21.25%">
                                        {{isset($featureValues[0]) ? (isset($featureValues[0][$feature->id])?$featureValues[0][$feature->id]:"") : ""}}
                                    </td>
                                    <td width="21.25%">
                                        {{isset($featureValues[1]) ? (isset($featureValues[1][$feature->id])?$featureValues[1][$feature->id]:"") : ""}}
                                    </td>
                                    <td width="21.25%">
                                        {{isset($featureValues[2]) ? (isset($featureValues[2][$feature->id])?$featureValues[2][$feature->id]:"") : ""}}
                                    </td>
                                    <td width="21.25%">
                                        {{isset($featureValues[3]) ? (isset($featureValues[3][$feature->id])?$featureValues[3][$feature->id]:"") : ""}}
                                    </td>
                                </tr>
                            @endforeach

                        @endforeach
                        </tbody>
                    </table>
                @endif
            </div>
        </div>
    </div>

@endsection

@section("extra_script")

    <script>
        if ($(".selectRtl").length > 0) {
            $(".selectRtl").select2({
                dir: "rtl",
                placeholder: "انتخاب کنید ...",
                allowClear: !0
            });
        }
    </script>

    <style>

        #compaire tbody tr td {
            width: 22.5% !important;
        }

        #compaire tbody tr td:first-child {
            width: 10%;
        }


        #compaireFeatures td {
            text-align: center;
            line-height: 1.8em;
        }

        .productHeader {
            font-size:1em;
            line-height: 1.5em;
        }

        .productInfo {
            position: relative;
            font: normal normal normal 14px/1 FontAwesome !important;
        }
        .productInfo .close {
            font: normal normal normal 14px/1 FontAwesome !important;
            color: #d92027;
            position: absolute;
            left: 0px;
            top: 0px;
            font-size: 1.5em !important;
            padding: 5px;
        }

    </style>

@endsection
