@extends("client.layout")
{{--@section("title") فروشگاه اینترنتی دی تل @endsection--}}
@section("content")
    <br>
    <div class="body-content">
        <div class="container">
            <div class="my-wishlist-page">
                <div class="row">
                    @include("errors.listClient")
                    <div class="col-md-12 my-wishlist">
                        <div class="">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th colspan="4" class="heading-title">لیست علاقه مندی ها</th>
                                </tr>
                                </thead>
                            </table>
                            @if( isset($wishlists) && count($wishlists) > 0 )
                            @foreach($wishlists as $wishlist)
                            <div class="row">
                                <div class="col-md-2">
                                    <img src="{!! thumb($wishlist->product->media,5) !!}" alt="{{$wishlist->product->name}}">
                                </div>
                                <div class="col-md-7">
                                    <div class="product-name"><a href="{{route('singleProduct',$wishlist->product->id)}}">{{$wishlist->product->name}}</a></div>
                                    <div class="rating">
                                        <i class="fa fa-star rate"></i>
                                        <i class="fa fa-star rate"></i>
                                        <i class="fa fa-star rate"></i>
                                        <i class="fa fa-star rate"></i>
                                        <i class="fa fa-star non-rate"></i>
                                        <span class="review">( {{$wishlist->product->comment_count}} نظر )</span>
                                    </div>
                                    <div class="price">
                                        600،000 تومان
                                        <span>{{$wishlist->product->price}} تومان</span>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <a href="#" class="btn-upper btn btn-primary addToCart">اضافه به سبد خرید</a>
                                </div>
                                <div class="col-md-1">
                                    {!! Form::open(['url'=>route('wish_list.destroy', $wishlist->id),'method'=>'delete','class'=>'delete']) !!}
                                    <button type="submit" class="btn red"> <i class="fa fa-times"></i></button> {!! Form::close() !!} </div>
                                </td>

                                {{--<a href="#" class=""></a>--}}
                                </div>
                            </div>
                            <hr>
                            @endforeach
                                @else
                                <h3 class="text-center">محصولی در لیست علاقمندی ها یافت نشد</h3>
                                @endif
                        </div>
                    </div>
                </div><!-- /.row -->
            </div><!-- /.sigin-in-->
            <!-- ============================================== BRANDS CAROUSEL ============================================== -->
            <div id="brands-carousel" class="logo-slider wow fadeInUp">

                <div class="logo-slider-inner">
                    <div id="brand-slider" class="owl-carousel brand-slider custom-carousel owl-theme">
                        <div class="item m-t-15">
                            <a href="#" class="image">
                                <img data-echo="assets/images/brands/brand1.png" src="assets/images/blank.gif" alt="">
                            </a>
                        </div><!--/.item-->

                        <div class="item m-t-10">
                            <a href="#" class="image">
                                <img data-echo="assets/images/brands/brand2.png" src="assets/images/blank.gif" alt="">
                            </a>
                        </div><!--/.item-->

                        <div class="item">
                            <a href="#" class="image">
                                <img data-echo="assets/images/brands/brand3.png" src="assets/images/blank.gif" alt="">
                            </a>
                        </div><!--/.item-->

                        <div class="item">
                            <a href="#" class="image">
                                <img data-echo="assets/images/brands/brand4.png" src="assets/images/blank.gif" alt="">
                            </a>
                        </div><!--/.item-->

                        <div class="item">
                            <a href="#" class="image">
                                <img data-echo="assets/images/brands/brand5.png" src="assets/images/blank.gif" alt="">
                            </a>
                        </div><!--/.item-->

                        <div class="item">
                            <a href="#" class="image">
                                <img data-echo="assets/images/brands/brand6.png" src="assets/images/blank.gif" alt="">
                            </a>
                        </div><!--/.item-->

                        <div class="item">
                            <a href="#" class="image">
                                <img data-echo="assets/images/brands/brand2.png" src="assets/images/blank.gif" alt="">
                            </a>
                        </div><!--/.item-->

                        <div class="item">
                            <a href="#" class="image">
                                <img data-echo="assets/images/brands/brand4.png" src="assets/images/blank.gif" alt="">
                            </a>
                        </div><!--/.item-->

                        <div class="item">
                            <a href="#" class="image">
                                <img data-echo="assets/images/brands/brand1.png" src="assets/images/blank.gif" alt="">
                            </a>
                        </div><!--/.item-->

                        <div class="item">
                            <a href="#" class="image">
                                <img data-echo="assets/images/brands/brand5.png" src="assets/images/blank.gif" alt="">
                            </a>
                        </div><!--/.item-->
                    </div><!-- /.owl-carousel #logo-slider -->
                </div><!-- /.logo-slider-inner -->

            </div><!-- /.logo-slider -->
            <!-- ============================================== BRANDS CAROUSEL : END ============================================== -->
        </div><!-- /.container -->
    </div>

@endsection