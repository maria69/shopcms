@extends("client.layout")
{{--@section("title") فروشگاه اینترنتی دی تل @endsection--}}
@section("content")



    <div class="breadcrumb">
        <div class="container">
            <div class="breadcrumb-inner">
                <ul class="list-inline list-unstyled">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Clothing</a></li>
                    <li class='active'>Floral Print Buttoned</li>
                </ul>
            </div><!-- /.breadcrumb-inner -->
        </div><!-- /.container -->
    </div><!-- /.breadcrumb -->
    <div class="body-content outer-top-xs">
        <div class='container'>
            <div class='row single-product'>
                {{--<div class='col-md-3 sidebar'>--}}
                {{--<div class="sidebar-module-container">--}}
                {{--<div class="home-banner outer-top-n">--}}
                {{--<img src="/assets/images/banners/LHS-banner.jpg" alt="Image">--}}
                {{--</div>--}}



                {{--<!-- ============================================== HOT DEALS ============================================== -->--}}
                {{--<div class="sidebar-widget hot-deals wow fadeInUp outer-top-vs">--}}
                {{--<h3 class="section-title">hot deals</h3>--}}
                {{--<div class="owl-carousel sidebar-carousel custom-carousel owl-theme outer-top-xs">--}}

                {{--<div class="item">--}}
                {{--<div class="products">--}}
                {{--<div class="hot-deal-wrapper">--}}
                {{--<div class="image">--}}
                {{--<img src="/assets/images/hot-deals/p5.jpg" alt="">--}}
                {{--</div>--}}
                {{--<div class="sale-offer-tag"><span>35%<br>off</span></div>--}}
                {{--<div class="timing-wrapper">--}}
                {{--<div class="box-wrapper">--}}
                {{--<div class="date box">--}}
                {{--<span class="key">120</span>--}}
                {{--<span class="value">Days</span>--}}
                {{--</div>--}}
                {{--</div>--}}

                {{--<div class="box-wrapper">--}}
                {{--<div class="hour box">--}}
                {{--<span class="key">20</span>--}}
                {{--<span class="value">HRS</span>--}}
                {{--</div>--}}
                {{--</div>--}}

                {{--<div class="box-wrapper">--}}
                {{--<div class="minutes box">--}}
                {{--<span class="key">36</span>--}}
                {{--<span class="value">MINS</span>--}}
                {{--</div>--}}
                {{--</div>--}}

                {{--<div class="box-wrapper hidden-md">--}}
                {{--<div class="seconds box">--}}
                {{--<span class="key">60</span>--}}
                {{--<span class="value">SEC</span>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div><!-- /.hot-deal-wrapper -->--}}

                {{--<div class="product-info text-left m-t-20">--}}
                {{--<h3 class="name"><a href="detail.html">{{$product->name}}<br><span>{{$product->en_name}}</span></a></h3>--}}
                {{--<div class="rating rateit-small"></div>--}}

                {{--<div class="product-price">--}}
                {{--<span class="price">--}}
                {{--$600.00--}}
                {{--</span>--}}

                {{--<span class="price-before-discount">$800.00</span>--}}

                {{--</div><!-- /.product-price -->--}}

                {{--</div><!-- /.product-info -->--}}

                {{--<div class="cart clearfix animate-effect">--}}
                {{--<div class="action">--}}

                {{--<div class="add-cart-button btn-group">--}}
                {{--<button class="btn btn-primary icon" data-toggle="dropdown" type="button">--}}
                {{--<i class="fa fa-shopping-cart"></i>--}}
                {{--</button>--}}
                {{--<button class="btn btn-primary cart-btn" type="button">Add to cart</button>--}}

                {{--</div>--}}

                {{--</div><!-- /.action -->--}}
                {{--</div><!-- /.cart -->--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--<div class="item">--}}
                {{--<div class="products">--}}
                {{--<div class="hot-deal-wrapper">--}}
                {{--<div class="image">--}}
                {{--<img src="/assets/images/products/p6.jpg" alt="">--}}
                {{--</div>--}}
                {{--<div class="sale-offer-tag"><span>35%<br>off</span></div>--}}
                {{--<div class="timing-wrapper">--}}
                {{--<div class="box-wrapper">--}}
                {{--<div class="date box">--}}
                {{--<span class="key">120</span>--}}
                {{--<span class="value">Days</span>--}}
                {{--</div>--}}
                {{--</div>--}}

                {{--<div class="box-wrapper">--}}
                {{--<div class="hour box">--}}
                {{--<span class="key">20</span>--}}
                {{--<span class="value">HRS</span>--}}
                {{--</div>--}}
                {{--</div>--}}

                {{--<div class="box-wrapper">--}}
                {{--<div class="minutes box">--}}
                {{--<span class="key">36</span>--}}
                {{--<span class="value">MINS</span>--}}
                {{--</div>--}}
                {{--</div>--}}

                {{--<div class="box-wrapper hidden-md">--}}
                {{--<div class="seconds box">--}}
                {{--<span class="key">60</span>--}}
                {{--<span class="value">SEC</span>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div><!-- /.hot-deal-wrapper -->--}}

                {{--<div class="product-info text-left m-t-20">--}}
                {{--<h3 class="name"><a href="detail.html">Floral Print Buttoned</a></h3>--}}
                {{--<div class="rating rateit-small"></div>--}}

                {{--<div class="product-price">--}}
                {{--<span class="price">--}}
                {{--$600.00--}}
                {{--</span>--}}

                {{--<span class="price-before-discount">$800.00</span>--}}

                {{--</div><!-- /.product-price -->--}}

                {{--</div><!-- /.product-info -->--}}

                {{--<div class="cart clearfix animate-effect">--}}
                {{--<div class="action">--}}

                {{--<div class="add-cart-button btn-group">--}}
                {{--<button class="btn btn-primary icon" data-toggle="dropdown" type="button">--}}
                {{--<i class="fa fa-shopping-cart"></i>--}}
                {{--</button>--}}
                {{--<button class="btn btn-primary cart-btn" type="button">Add to cart</button>--}}

                {{--</div>--}}

                {{--</div><!-- /.action -->--}}
                {{--</div><!-- /.cart -->--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--<div class="item">--}}
                {{--<div class="products">--}}
                {{--<div class="hot-deal-wrapper">--}}
                {{--<div class="image">--}}
                {{--<img src="/assets/images/products/p7.jpg" alt="">--}}
                {{--</div>--}}
                {{--<div class="sale-offer-tag"><span>35%<br>off</span></div>--}}
                {{--<div class="timing-wrapper">--}}
                {{--<div class="box-wrapper">--}}
                {{--<div class="date box">--}}
                {{--<span class="key">120</span>--}}
                {{--<span class="value">Days</span>--}}
                {{--</div>--}}
                {{--</div>--}}

                {{--<div class="box-wrapper">--}}
                {{--<div class="hour box">--}}
                {{--<span class="key">20</span>--}}
                {{--<span class="value">HRS</span>--}}
                {{--</div>--}}
                {{--</div>--}}

                {{--<div class="box-wrapper">--}}
                {{--<div class="minutes box">--}}
                {{--<span class="key">36</span>--}}
                {{--<span class="value">MINS</span>--}}
                {{--</div>--}}
                {{--</div>--}}

                {{--<div class="box-wrapper hidden-md">--}}
                {{--<div class="seconds box">--}}
                {{--<span class="key">60</span>--}}
                {{--<span class="value">SEC</span>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div><!-- /.hot-deal-wrapper -->--}}

                {{--<div class="product-info text-left m-t-20">--}}
                {{--<h3 class="name"><a href="detail.html">Floral Print Buttoned</a></h3>--}}
                {{--<div class="rating rateit-small"></div>--}}

                {{--<div class="product-price">--}}
                {{--<span class="price">--}}
                {{--$600.00--}}
                {{--</span>--}}

                {{--<span class="price-before-discount">$800.00</span>--}}

                {{--</div><!-- /.product-price -->--}}

                {{--</div><!-- /.product-info -->--}}

                {{--<div class="cart clearfix animate-effect">--}}
                {{--<div class="action">--}}

                {{--<div class="add-cart-button btn-group">--}}
                {{--<button class="btn btn-primary icon" data-toggle="dropdown" type="button">--}}
                {{--<i class="fa fa-shopping-cart"></i>--}}
                {{--</button>--}}
                {{--<button class="btn btn-primary cart-btn" type="button">Add to cart</button>--}}

                {{--</div>--}}

                {{--</div><!-- /.action -->--}}
                {{--</div><!-- /.cart -->--}}
                {{--</div>--}}
                {{--</div>--}}


                {{--</div><!-- /.sidebar-widget -->--}}
                {{--</div>--}}
                {{--<!-- ============================================== HOT DEALS: END ============================================== -->--}}

                {{--<!-- ============================================== NEWSLETTER ============================================== -->--}}
                {{--<div class="sidebar-widget newsletter wow fadeInUp outer-bottom-small outer-top-vs">--}}
                {{--<h3 class="section-title">Newsletters</h3>--}}
                {{--<div class="sidebar-widget-body outer-top-xs">--}}
                {{--<p>Sign Up for Our Newsletter!</p>--}}
                {{--<form>--}}
                {{--<div class="form-group">--}}
                {{--<label class="sr-only" for="exampleInputEmail1">Email address</label>--}}
                {{--<input type="email" class="form-control" id="exampleInputEmail1" placeholder="Subscribe to our newsletter">--}}
                {{--</div>--}}
                {{--<button class="btn btn-primary">Subscribe</button>--}}
                {{--</form>--}}
                {{--</div><!-- /.sidebar-widget-body -->--}}
                {{--</div><!-- /.sidebar-widget -->--}}
                {{--<!-- ============================================== NEWSLETTER: END ============================================== -->--}}

                {{--<!-- ============================================== Testimonials============================================== -->--}}
                {{--<div class="sidebar-widget  wow fadeInUp outer-top-vs ">--}}
                {{--<div id="advertisement" class="advertisement">--}}
                {{--<div class="item">--}}
                {{--<div class="avatar"><img src="/assets/images/testimonials/member1.png" alt="Image"></div>--}}
                {{--<div class="testimonials"><em>"</em> Vtae sodales aliq uam morbi non sem lacus port mollis. Nunc condime tum metus eud molest sed consectetuer.<em>"</em></div>--}}
                {{--<div class="clients_author">John Doe	<span>Abc Company</span>	</div><!-- /.container-fluid -->--}}
                {{--</div><!-- /.item -->--}}

                {{--<div class="item">--}}
                {{--<div class="avatar"><img src="/assets/images/testimonials/member3.png" alt="Image"></div>--}}
                {{--<div class="testimonials"><em>"</em>Vtae sodales aliq uam morbi non sem lacus port mollis. Nunc condime tum metus eud molest sed consectetuer.<em>"</em></div>--}}
                {{--<div class="clients_author">Stephen Doe	<span>Xperia Designs</span>	</div>--}}
                {{--</div><!-- /.item -->--}}

                {{--<div class="item">--}}
                {{--<div class="avatar"><img src="/assets/images/testimonials/member2.png" alt="Image"></div>--}}
                {{--<div class="testimonials"><em>"</em> Vtae sodales aliq uam morbi non sem lacus port mollis. Nunc condime tum metus eud molest sed consectetuer.<em>"</em></div>--}}
                {{--<div class="clients_author">Saraha Smith	<span>Datsun &amp; Co</span>	</div><!-- /.container-fluid -->--}}
                {{--</div><!-- /.item -->--}}

                {{--</div><!-- /.owl-carousel -->--}}
                {{--</div>--}}

                {{--<!-- ============================================== Testimonials: END ============================================== -->--}}



                {{--</div>--}}
                {{--</div>--}}
                <div class='col-md-12'>
                    <div class="detail-block">
                        <div class="row  wow fadeInUp">

                            <div class="col-xs-12 col-sm-6 col-md-5 gallery-holder">
                                <div class="product-item-holder size-big single-product-gallery small-gallery">

                                    <div id="owl-single-product">

                                        @php $count=0; @endphp

                                        @if($product->media)

                                            <div class="single-product-gallery-item" id="slide{{$count}}">
                                                <a data-lightbox="image-1" data-title="{{$product->title}}" href="{{thumb($product->media,5)}}">
                                                    <img class="img-responsive" alt="{{$product->title}}" src="{{thumb($product->media,5)}}" data-echo="{{thumb($product->media,5)}}" />
                                                </a>
                                            </div><!-- /.single-product-gallery-item -->

                                            @php $count++; @endphp

                                        @endif

                                        @foreach($product->medias as $media)

                                            <div class="single-product-gallery-item" id="slide{{$count}}">
                                                <a data-lightbox="image-1" data-title="{{$product->title}}" href="{{thumb($media,5)}}">
                                                    <img class="img-responsive" alt="{{$product->title}}" src="{{thumb($media,5)}}" data-echo="{{thumb($media,5)}}" />
                                                </a>
                                            </div><!-- /.single-product-gallery-item -->

                                            @php $count++; @endphp

                                        @endforeach


                                    </div><!-- /.single-product-slider -->


                                    <div class="single-product-gallery-thumbs gallery-thumbs">

                                        <div id="owl-single-product-thumbnails">

                                            @php $count=0; @endphp

                                            @if($product->media)

                                                <div class="item">
                                                    <a class="horizontal-thumb active" data-target="#owl-single-product" data-slide="{{$count}}" href="#slide{{$count}}">
                                                        <img class="img-responsive" width="85" alt="" src="{{thumb($product->media,5)}}" data-echo="{{thumb($product->media,5)}}" />
                                                    </a>
                                                </div>

                                                @php $count++; @endphp

                                            @endif

                                            @foreach($product->medias as $media)

                                                <div class="item">
                                                    <a class="horizontal-thumb" data-target="#owl-single-product" data-slide="{{$count}}" href="#slide{{$count}}">
                                                        <img class="img-responsive" width="85" alt="" src="{{thumb($media,5)}}" data-echo="{{thumb($media,5)}}" />
                                                    </a>
                                                </div>

                                                @php $count++; @endphp

                                            @endforeach


                                        </div><!-- /#owl-single-product-thumbnails -->



                                    </div><!-- /.gallery-thumbs -->

                                </div><!-- /.single-product-gallery -->
                            </div><!-- /.gallery-holder -->

                            <div class='col-sm-6 col-md-7 product-info-block'>
                                <div class="product-info">
                                    <h1 class="name">{{$product->name}}<br><span>{{$product->en_name}}</h1>

                                    <div class="rating-reviews m-t-20">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                @for($x=1;$x<=$star_int;$x++)
                                                    <span style="color:orange;font-size:1.3em;">&#9733;</span>
                                                @endfor
                                                {{--@if ($star_float > 0)--}}
                                                {{--<span style="color:orange;font-size:1.3em;">&#9734;</span>--}}
                                                {{--@php $x++; @endphp--}}
                                                {{--@endif--}}
                                                @while ($x<=5)
                                                    <span style="color:orange;font-size:1.3em;">&#9734;</span>
                                                    @php $x++; @endphp
                                                @endwhile

                                                &nbsp;
                                                <span class="lnk">({{$comments->count()}} دیدگاه)</span>
                                                {{--<div class="rating rateit-small"></div>--}}
                                            </div>
                                        </div><!-- /.row -->
                                    </div><!-- /.rating-reviews -->

                                    <div class="stock-container info-container m-t-10">
                                        <div class="row">
                                            <div class="col-sm-2">
                                                <div class="stock-box">
                                                    <span class="label">وضعیت انبار :</span>
                                                </div>
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="stock-box">
                                                    <span class="value">موجود</span>
                                                </div>
                                            </div>
                                        </div><!-- /.row -->
                                    </div><!-- /.stock-container -->

                                    <div class="description-container m-t-20">

                                        {{$product->short_description}}
                                    </div><!-- /.description-container -->

                                    <div class="price-container info-container m-t-20">
                                        <div class="row">


                                            <div class="col-sm-6">
                                                <div class="price-box">
                                                    <span class="price">{{number_format($product->price)}} تومان</span>
                                                    <span class="price-strike"> <!-- offer --> </span>
                                                </div>
                                            </div>

                                            {{--<div class="col-sm-6">--}}
                                            {{--<div class="favorite-button m-t-10 pull-left">--}}
                                            {{--<a class="btn btn-primary lnk wishlist" data-product-id="{{$product->id}}" data-toggle="tooltip" data-placement="right" title="افزودن به لیست خرید" href="#">--}}
                                            {{--<i class="fa fa-heart"></i> افزودن به علاقه مندی ها--}}
                                            {{--</a>--}}
                                            {{--{!! Form::open(['url'=>route("addCompaireToListFromProduct"),'method'=>'post']) !!}--}}
                                            {{--<input type="hidden" name="product_id" value="{{$product->id}}">--}}
                                            {{--<button class="btn btn-primary" data-toggle="tooltip" data-placement="right" title="مقایسه محصول" href="#">--}}
                                            {{--<i class="fa fa-signal"></i> مقایسه محصول--}}
                                            {{--</button>--}}
                                            {{--{!! Form::close() !!}--}}


                                            {{--</div>--}}
                                            {{--</div>--}}

                                        </div><!-- /.row -->
                                    </div><!-- /.price-container -->

                                    <div class="quantity-container info-container">
                                        <div class="row">

                                            <div class="col-sm-7">
                                                <a href="#" class="btn btn-primary addtocart" data-product-id="{{$product->id}}"><i class="fa fa-shopping-cart inner-right-vs"></i> افزودن به سبد خرید</a>
                                            </div>


                                        </div><!-- /.row -->
                                    </div><!-- /.quantity-container -->






                                </div><!-- /.product-info -->
                            </div><!-- /.col-sm-7 -->
                        </div><!-- /.row -->
                    </div>

                    <div class="product-tabs inner-bottom-xs  wow fadeInUp">
                        <div class="row">
                            <div class="col-sm-3">
                                <ul id="product-tabs" class="nav nav-tabs nav-tab-cell">
                                    <li @if(!Session::has("comment") && !$errors->any()) class="active" @endif><a data-toggle="tab" href="#description">نقد و بررسی</a></li>
                                    <li><a data-toggle="tab" href="#properties">مشخصات فنی</a></li>
                                    <li @if(Session::has("comment") || $errors->any()) class="active" @endif><a data-toggle="tab" href="#review">دیدگاه کاربران</a></li>
                                </ul><!-- /.nav-tabs #product-tabs -->
                            </div>
                            <div class="col-sm-9">

                                <div class="tab-content">

                                    <div id="description" class="tab-pane in  @if(!Session::has("comment") && !$errors->any()) active @endif">
                                        <div class="product-tab">
                                            <p class="text">

                                                {!! $product->description !!}
                                            </p>
                                        </div>
                                    </div><!-- /.tab-pane -->

                                    <div id="review" class="tab-pane @if(Session::has("comment") || $errors->any()) active @endif">
                                        {!! Form::open(["url"=>route('comment.store'),'method'=>"post","class"=>"cnt-form"]) !!}

                                        <input type="hidden" name="product_id" value="{{$product->id}}">

                                        <div class="product-tab">

                                            <div class="product-reviews">
                                                <h4 class="title">دیدگاه مشتریان</h4>




                                                <div class="reviews">

                                                    @include("errors.listClient")

                                                    @if($comments->count() > 0)

                                                        @foreach($comments as $comment)


                                                            @if(Auth::check())

                                                                @if(Auth::user()->id == $comment->user_id && $comment->status == 0)
                                                                    <div class="review">
                                                                        <div class="review-title"><span class="date"><i class="fa fa-calendar"></i> &nbsp; <span>{{time_elapsed_string(strtotime($comment->created_at))}}</span></span><span class="summary">{{$comment->user->first_name}} {{$comment->user->last_name}} (دیدگاه شما پس از تایید کارشناس سایت نمایش داده میشود) </span></div>

                                                                        <hr>

                                                                        <div class="row">

                                                                            <div class="col-md-6">
                                                                                <h4 class="text-danger">نقاط ضعف</h4>
                                                                                <ul class="weaknesses">
                                                                                    @foreach($comment->weaknesses as $weaknesses)
                                                                                        <li>{{$weaknesses->description}}</li>
                                                                                    @endforeach
                                                                                </ul>
                                                                            </div>
                                                                            <div class="col-md-6">
                                                                                <h4 class="text-success">نقاط قوت</h4>
                                                                                <ul class="strengths">
                                                                                    @foreach($comment->strengths as $strengths)
                                                                                        <li>{{$strengths->description}}</li>
                                                                                    @endforeach
                                                                                </ul>
                                                                            </div>

                                                                        </div>

                                                                        <hr>

                                                                        <div class="text">{{$comment->body}}</div>
                                                                        <span class="pull-left" data-id="{{$comment->id}}">
                                                                             <div id="dislike1-bs3">{{$comment->unlike}}</div><i id="like1" class="glyphicon glyphicon-thumbs-up"></i>
                                                                            <div id="like1-bs3">{{$comment->like}}</div> <i id="dislike1" class="glyphicon glyphicon-thumbs-down"></i>
                                                                        </span>
                                                                    </div>
                                                                @elseif($comment->status == 1)
                                                                    <div class="review">
                                                                        <div class="review-title"><span class="date"><i class="fa fa-calendar"></i> &nbsp; <span>{{time_elapsed_string(strtotime($comment->created_at))}}</span></span><span class="summary">{{$comment->user->first_name}} {{$comment->user->last_name}}</span></div>
                                                                        <hr>

                                                                        <div class="row">

                                                                            <div class="col-md-6">
                                                                                <h4 class="text-danger">نقاط ضعف</h4>
                                                                                <ul class="weaknesses">
                                                                                    @foreach($comment->weaknesses as $weaknesses)
                                                                                        <li>{{$weaknesses->description}}</li>
                                                                                    @endforeach
                                                                                </ul>
                                                                            </div>
                                                                            <div class="col-md-6">
                                                                                <h4 class="text-success">نقاط قوت</h4>
                                                                                <ul class="strengths">
                                                                                    @foreach($comment->strengths as $strengths)
                                                                                        <li>{{$strengths->description}}</li>
                                                                                    @endforeach
                                                                                </ul>
                                                                            </div>

                                                                        </div>

                                                                        <hr>
                                                                        <div class="text">{{$comment->body}}</div>
                                                                        <span class="pull-left" data-id="{{$comment->id}}">
                                                                             <div id="dislike1-bs3">{{$comment->unlike}}</div><i id="like1" class="glyphicon glyphicon-thumbs-up"></i>
                                                                            <div id="like1-bs3">{{$comment->like}}</div> <i id="dislike1" class="glyphicon glyphicon-thumbs-down"></i>
                                                                        </span>
                                                                    </div>
                                                                @endif


                                                            @else

                                                                @if($comment->status == 1)

                                                                    <div class="review">
                                                                        <div class="review-title"><span class="date"><i class="fa fa-calendar"></i> &nbsp; <span>{{time_elapsed_string(strtotime($comment->created_at))}}</span></span><span class="summary">{{$comment->user->first_name}} {{$comment->user->last_name}}</span></div>
                                                                        <hr>

                                                                        <div class="row">

                                                                            <div class="col-md-6">
                                                                                <h4 class="text-danger">نقاط ضعف</h4>
                                                                                <ul class="weaknesses">
                                                                                    @foreach($comment->weaknesses as $weaknesses)
                                                                                        <li>{{$weaknesses->description}}</li>
                                                                                    @endforeach
                                                                                </ul>
                                                                            </div>
                                                                            <div class="col-md-6">
                                                                                <h4 class="text-success">نقاط قوت</h4>
                                                                                <ul class="strengths">
                                                                                    @foreach($comment->strengths as $strengths)
                                                                                        <li>{{$strengths->description}}</li>
                                                                                    @endforeach
                                                                                </ul>
                                                                            </div>

                                                                        </div>

                                                                        <hr>
                                                                        <div class="text">{{$comment->body}}</div>
                                                                        <span class="pull-left" data-id="{{$comment->id}}">
                                                                             <div id="dislike1-bs3">{{$comment->unlike}}</div><i id="like1" class="glyphicon glyphicon-thumbs-up"></i>
                                                                            <div id="like1-bs3">{{$comment->like}}</div> <i id="dislike1" class="glyphicon glyphicon-thumbs-down"></i>
                                                                        </span>
                                                                    </div>

                                                                @endif

                                                            @endif



                                                        @endforeach

                                                    @endif

                                                </div><!-- /.reviews -->
                                            </div><!-- /.product-reviews -->


                                            @if(Auth::check())
                                                <div class="product-add-review">
                                                    {{--<h4 class="title"></h4>--}}
                                                    <div class="review-table">
                                                        <div class="table-responsive">
                                                            <table class="table">
                                                                <thead>
                                                                <tr>
                                                                    <th colspan="2" class="cell-label">&nbsp;دیدگاه خود را بنویسید </th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                @foreach($stars as $star)
                                                                    <tr id="wrapper{{$star->id}}" class="wrapper">
                                                                        <td width="30%" class="cell-label">{{ $star->name }}</td>

                                                                        <td width="70%" class="cell-label">  <input id="radio1" type="radio" name="stars{{$star->id}}" value="5"><!--
                                                                                      --><label for="radio{{$star->id}}">&#9733;</label><!--
                                                                                      --><input id="radio{{$star->id}}" type="radio" name="stars{{$star->id}}" value="4"><!--
                                                                                      --><label for="radio{{$star->id}}">&#9733;</label><!--
                                                                                      --><input id="radio{{$star->id}}" type="radio" name="stars{{$star->id}}" value="3"><!--
                                                                                      --><label for="radio{{$star->id}}">&#9733;</label><!--
                                                                                      --><input id="radio{{$star->id}}" type="radio" name="stars{{$star->id}}" value="2"><!--
                                                                                      --><label for="radio{{$star->id}}">&#9733;</label><!--
                                                                                      --><input id="radio{{$star->id}}" type="radio" name="stars{{$star->id}}" value="1"><!--
                                                                                      --><label for="radio{{$star->id}}">&#9733;</label></td>
                                                                    </tr>
                                                                @endforeach

                                                                </tbody>
                                                            </table><!-- /.table .table-bordered -->


                                                        </div><!-- /.table-responsive -->
                                                    </div><!-- /.review-table -->


                                                    <div class="review-form">
                                                        <div class="form-container">


                                                            <div class="row" id="reviews_by_user">

                                                                <div class="col-sm-6" id="plus">
                                                                    <h4>نقاط قوت <i style="color:green;cursor: pointer" class="fa fa-plus"></i> </h4>
                                                                    <div class="form-group">
                                                                        <input type="text" name="strengths[]" class="form-control txt" placeholder="قوت">
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-6" id="mines">
                                                                    <h4>نقاط ضعف <i style="color:green;cursor: pointer" class="fa fa-plus"></i> </h4>
                                                                    <div class="form-group">
                                                                        <input name="weaknesses[]" type="text" class="form-control txt" placeholder="ضعف">
                                                                    </div>
                                                                </div>

                                                            </div>


                                                            <div class="row">

                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label for="body">دیدگاه <span class="astk">*</span></label>
                                                                        <textarea name="body" class="form-control txt txt-review" id="body" rows="4" placeholder="دیدگاه">{{old("body")}}</textarea>
                                                                    </div><!-- /.form-group -->
                                                                </div>
                                                            </div><!-- /.row -->

                                                            <div class="action text-right">
                                                                <button class="btn btn-primary btn-upper">ثبت</button>
                                                            </div><!-- /.action -->


                                                        </div><!-- /.form-container -->
                                                    </div><!-- /.review-form -->


                                                </div><!-- /.product-add-review -->

                                            @else

                                                <div class="alert alert-danger">
                                                    <p>برای ارسال دیدگاه وارد شده یا ثبت نام نمایید</p>
                                                </div>

                                            @endif

                                        </div><!-- /.product-tab -->
                                        {!! Form::close() !!}
                                    </div><!-- /.tab-pane -->

                                    <div id="properties" class="tab-pane">
                                        <div class="product-tag">



                                            @if($featuresCategory)
                                                <table class="table table-striped" id="compaireFeatures">
                                                    <tbody>
                                                    @foreach($featuresCategory as $category)

                                                        <tr>
                                                            <td class="featureHeader" colspan="5"> {{$category->name}} </td>
                                                            </td>
                                                        </tr>

                                                        @foreach($category->features as $feature)
                                                            <tr>
                                                                <td width="50%" class="feature"> {{$feature->name}} </td>
                                                                <td width="50%">
                                                                    {{isset($featureValues) ? (isset($featureValues[$feature->id])?$featureValues[$feature->id]:"") : ""}}
                                                                </td>
                                                            </tr>
                                                        @endforeach

                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            @endif


                                        </div><!-- /.product-tab -->
                                    </div><!-- /.tab-pane -->

                                </div><!-- /.tab-content -->
                            </div><!-- /.col -->
                        </div><!-- /.row -->
                    </div><!-- /.product-tabs -->

                @if($similarProductsCat->count() > 0)
                    <!-- ============================================== UPSELL PRODUCTS ============================================== -->
                        <section class="section featured-product wow fadeInUp">
                            <h3 class="section-title">محصولات مشابه</h3>
                            <div class="owl-carousel home-owl-carousel upsell-product custom-carousel owl-theme outer-top-xs">

                                @foreach($similarProductsCat as $category)

                                    @foreach($category->products as $products)

                                        <div class="item item-carousel">
                                            <div class="products">

                                                <div class="product">
                                                    <div class="product-image">
                                                        <div class="image">
                                                            <a href="detail.html"><img  src="{{thumb($product->media,5)}}" alt=""></a>
                                                        </div><!-- /.image -->

                                                        {{--<div class="tag sale"><span>sale</span></div>--}}
                                                    </div><!-- /.product-image -->


                                                    <div class="product-info text-left">
                                                        <h3 class="name"><a href="/product/{{$product->id}}">{{$product->name}}</a></h3>
                                                        <div class="rating rateit-small"></div>
                                                        <div class="description"></div>

                                                        <div class="product-price">
				<span class="price">
					{{number_format($product->price)}} تومان			</span>
                                                            <span class="price-before-discount">$ 800</span>

                                                        </div><!-- /.product-price -->

                                                    </div><!-- /.product-info -->
                                                    <div class="cart clearfix animate-effect">
                                                        <div class="action">
                                                            <ul class="list-unstyled">
                                                                <li class="add-cart-button btn-group">
                                                                    <button class="btn btn-primary icon" data-toggle="dropdown" type="button">
                                                                        <i class="fa fa-shopping-cart"></i>
                                                                    </button>
                                                                    <button class="btn btn-primary cart-btn" type="button">افزودن به سبد خرید</button>

                                                                </li>

                                                                <li class="lnk wishlist">
                                                                    <a class="add-to-cart" href="detail.html" title="Wishlist">
                                                                        <i class="icon fa fa-heart"></i>
                                                                    </a>
                                                                </li>

                                                                <li class="lnk">
                                                                    <a class="add-to-cart" href="detail.html" title="Compare">
                                                                        <i class="fa fa-signal"></i>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div><!-- /.action -->
                                                    </div><!-- /.cart -->
                                                </div><!-- /.product -->

                                            </div><!-- /.products -->
                                        </div><!-- /.item -->

                                    @endforeach

                                @endforeach


                            </div><!-- /.home-owl-carousel -->
                        </section><!-- /.section -->
                        <!-- ============================================== UPSELL PRODUCTS : END ============================================== -->
                    @endif
                </div><!-- /.col -->
                <div class="clearfix"></div>
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.body-content -->




@endsection

@section("extra_script")

    <style>
        i.glyphicon { display: inline; font-size: 1.125em; cursor: pointer;
        }

        #like1-bs3,#dislike1-bs3 {
            display: inline !important;
        }
        i.glyphicon-thumbs-up { color: #1abc9c; }
        i.glyphicon-thumbs-down { color: #e74c3c; padding-left: 5px; }

        #dislike1-bs3 {
            padding-left: 5px;
        }

        #description img {
            max-width: 90%;
            display: block;
            margin:0 auto;
            height: auto !important;
        }
    </style>


    @if(Session::has("comment") || $errors->any())
        <script>


            $(document).ready(function(){
                $('html, body').animate({scrollTop:$(".reviews").offset().top},1000);
            });

        </script>
    @endif
@endsection