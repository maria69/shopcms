@extends("client.layout")
{{--@section("title") فروشگاه اینترنتی دی تل @endsection--}}
@section("content")
    <div class="body-content outer-top-xs">
        <div class='container'>
            <div class='row'>
                <div class='col-md-12 sidebar'>
                    <!-- ================================== TOP NAVIGATION ================================== -->
                    <div class=" panel panel-body">
                        <div class="head"> جزئیات سفارش</div>

                    </div>






                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th class="cart-description item">#</th>
                                <th class="cart-description item">نام محصول</th>
                                <th class="cart-description item">تعداد</th>
                                <th class="cart-product-name item">قیمت واحد</th>
                                <th class="cart-edit item">قیمت کل</th>
                                <th class="cart-edit item">تخفیف</th>
                                <th class="cart-edit item">قیمت نهایی</th>
                                <th class="cart-edit item">عملیات</th>
                            </tr>
                            </thead>
                            <tbody id="addressResult">
                            @php $i=1; @endphp



                            <label><span>{{$order->number}}</span></label>

                            @foreach($order->order_item as $item)





                                <tr>
                                    <td class="cart-description item">
                                        {{$i++}}
                                    </td>
                                    <td>
                                        <span class='label label-primary'>

                                        {{$item->product->name}}
                                        {{--{{ jDate::forge($order->created_at)->format('Y/m/d H:i:s') }}--}}
                                        </span>
                                    </td>
                                    <td class="label label-primary">

                                        {{$item->quantity}}

                                    </td>
                                    <td class="cart-qty item">
                                        {{$item->unitPrice}}
                                    </td>

                                    <td class="cart-qty item">
                                        {{$item->totalPrice}}
                                    </td>

                                    <td class="cart-qty item">
                                        {{$item->offer}}
                                    </td>
                                    <td class="cart-qty item">
                                        {{$item->finalPrice}}
                                    </td>
                                    <td class="cart-qty item">
                               عملیات
                                    </td>

                                </tr>
                            @endforeach

                            </tbody><!-- /tbody -->
                        </table><!-- /table -->
                    </div>

                </div>

            </div>
        </div>
    </div>

@endsection