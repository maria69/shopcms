@extends("client.layout")
{{--@section("title") فروشگاه اینترنتی دی تل @endsection--}}
@section("content")

    <div class="body-content outer-top-xs" id="top-banner-and-menu">
        <div class="container">
            <div class="row">
                <!-- ========================================== SECTION – HERO ========================================= -->
                <div class="col-xs-12 col-sm-12 col-md-12 homebanner-holder">
                    <div class="body-content">
                        <div class="container">
                            <div class="checkout-box ">
                                <div class="row">
                                    <div class="col-md-12">
                                        @include("errors.listClient")

                                        <div class="panel-group checkout-steps" id="accordion">
                                            <!-- checkout-step-01  -->
                                            <div class="panel panel-default checkout-step-01">

                                                <!-- panel-heading -->
                                                <div class="panel-heading">
                                                    <h4 class="unicase-checkout-title">
                                                        <a data-toggle="collapse" class="" data-parent="#accordion"
                                                           href="#collapseOne">
                                                            <span>1</span>پیگیری سفارشات
                                                        </a>
                                                    </h4>
                                                </div>
                                                <!-- panel-heading -->

                                                <div id="collapseOne" class="panel-collapse collapse in">

                                                    <!-- panel-body  -->
                                                    <div class="panel-body">
                                                        <div class="row">

                                                            <div class="col-md-12 col-sm-12 create-new-account">
                                                                <br>
                                                                {!! Form::open(["url"=>route('updateUserInfo'),"method"=>"put","id"=>"register"]) !!}
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-md-3">
                                                                            <label class="info-title"
                                                                                   for="exampleInputEmail2">شماره شفارش
                                                                                <span>*</span></label>
                                                                        </div>
                                                                        <div class="col-md-9">
                                                                            {!! Form::text("email",null,["class"=>"form-control unicase-form-control text-input","autocomplete"=>"off","placeholder"=>"شماره پیگیری سفارش","id"=>"email"]) !!}
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-md-3">
                                                                            <label class="info-title"
                                                                                   for="exampleInputEmail2">شماره موبایل
                                                                                <span>*</span></label>
                                                                        </div>
                                                                        <div class="col-md-9">
                                                                            {!! Form::text("email",null,["class"=>"form-control unicase-form-control text-input","autocomplete"=>"off","placeholder"=>"شماره موبایل","id"=>"email"]) !!}
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                {!! Form::submit("جست و جو",["class"=>"btn-upper btn btn-primary checkout-page-button"]) !!}
                                                                {!! Form::close() !!}


                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- panel-body  -->

                                            </div><!-- row -->
                                            <!-- checkout-step-01  -->
                                            <!-- checkout-step-02  -->
                                            <div class="panel panel-default checkout-step-02">
                                                <div class="panel-heading">
                                                    <h4 class="unicase-checkout-title">
                                                        <a data-toggle="collapse" class="collapsed"
                                                           data-parent="#accordion" href="#collapseTwo">
                                                            <span>2</span>جزئیات سفارش
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseTwo" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <div class="table-responsive">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- checkout-step-02  -->
                                        </div>
                                    </div>
                                </div>
                                <!-- ============================================== BRANDS CAROUSEL ============================================== -->
                                <div id="brands-carousel" class="logo-slider wow fadeInUp">

                                    <div class="logo-slider-inner">
                                        <div id="brand-slider"
                                             class="owl-carousel brand-slider custom-carousel owl-theme">
                                            <div class="item m-t-15">
                                                <a href="#" class="image">
                                                    <img data-echo="assets/images/brands/brand1.png"
                                                         src="assets/images/blank.gif" alt="">
                                                </a>
                                            </div><!--/.item-->

                                            <div class="item m-t-10">
                                                <a href="#" class="image">
                                                    <img data-echo="assets/images/brands/brand2.png"
                                                         src="assets/images/blank.gif" alt="">
                                                </a>
                                            </div><!--/.item-->

                                            <div class="item">
                                                <a href="#" class="image">
                                                    <img data-echo="assets/images/brands/brand3.png"
                                                         src="assets/images/blank.gif" alt="">
                                                </a>
                                            </div><!--/.item-->

                                            <div class="item">
                                                <a href="#" class="image">
                                                    <img data-echo="assets/images/brands/brand4.png"
                                                         src="assets/images/blank.gif" alt="">
                                                </a>
                                            </div><!--/.item-->

                                            <div class="item">
                                                <a href="#" class="image">
                                                    <img data-echo="assets/images/brands/brand5.png"
                                                         src="assets/images/blank.gif" alt="">
                                                </a>
                                            </div><!--/.item-->

                                            <div class="item">
                                                <a href="#" class="image">
                                                    <img data-echo="assets/images/brands/brand6.png"
                                                         src="assets/images/blank.gif" alt="">
                                                </a>
                                            </div><!--/.item-->

                                            <div class="item">
                                                <a href="#" class="image">
                                                    <img data-echo="assets/images/brands/brand2.png"
                                                         src="assets/images/blank.gif" alt="">
                                                </a>
                                            </div><!--/.item-->

                                            <div class="item">
                                                <a href="#" class="image">
                                                    <img data-echo="assets/images/brands/brand4.png"
                                                         src="assets/images/blank.gif" alt="">
                                                </a>
                                            </div><!--/.item-->

                                            <div class="item">
                                                <a href="#" class="image">
                                                    <img data-echo="assets/images/brands/brand1.png"
                                                         src="assets/images/blank.gif" alt="">
                                                </a>
                                            </div><!--/.item-->

                                            <div class="item">
                                                <a href="#" class="image">
                                                    <img data-echo="assets/images/brands/brand5.png"
                                                         src="assets/images/blank.gif" alt="">
                                                </a>
                                            </div><!--/.item-->
                                        </div><!-- /.owl-carousel #logo-slider -->
                                    </div><!-- /.logo-slider-inner -->

                                </div><!-- /.logo-slider -->
                                <!-- ============================================== BRANDS CAROUSEL : END ============================================== -->
                            </div><!-- /.container -->
                        </div>
                    </div>
                </div>
            </div>
        </div>


    {{--<script>--}}

    {{--$("#searchOrder").submit(function(){--}}
    {{--$.ajax({--}}
    {{--url:"/getOrder",--}}
    {{--data:$(this).serialise(),--}}
    {{--success:function(data){--}}

    {{--$("#collapseOne").slideUp(1000,function(){--}}
    {{--$(data).each(function(index,element){--}}
    {{--$("#collapseTwo").find("#tbody").append('<tr> <td>'+element.id+'</td> <td>'+element.fn+'</td> <td>'+element.ln+'</td> </tr>');--}}
    {{--});--}}
    {{--$("#collapse-2").slideDown(1000);--}}
    {{--});--}}


    {{--},error:function(){--}}
    {{--alert("khaaakkkkkk !");--}}
    {{--}--}}
    {{--});--}}
    {{--});--}}

    {{--</script>--}}

@endsection