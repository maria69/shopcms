@extends("client.layout")
{{--@section("title") فروشگاه اینترنتی دی تل @endsection--}}
@section("content")

    <div class="body-content outer-top-xs" id="top-banner-and-menu">
        <div class="container">
            <div class="row">
                <!-- ========================================== SECTION – HERO ========================================= -->
                <div class="col-xs-12 col-sm-12 col-md-12 homebanner-holder">
                    <div class="body-content">
                        <div class="container">
                            <div class="checkout-box ">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="panel-heading">
                                            <h4 class="unicase-checkout-title">
                                                سفارش شما تکمیل شد :)
                                            </h4>
                                            <p style="text-align: justify">سفارش شما با موفقیت ثبت گردید.</p>
                                            <p style="text-align: justify">شماره پیگیری
                                                سفارش{{$orderInfo->trackingCode}} </p>
                                            <p style="text-align: justify">{{$payment->description}}</p>
                                        </div>

                                    </div>
                                </div>
                            </div><!-- /.container -->
                        </div>
                    </div>
                </div>
            </div>
        </div>


    {{--<script>--}}

    {{--$("#searchOrder").submit(function(){--}}
    {{--$.ajax({--}}
    {{--url:"/getOrder",--}}
    {{--data:$(this).serialise(),--}}
    {{--success:function(data){--}}

    {{--$("#collapseOne").slideUp(1000,function(){--}}
    {{--$(data).each(function(index,element){--}}
    {{--$("#collapseTwo").find("#tbody").append('<tr> <td>'+element.id+'</td> <td>'+element.fn+'</td> <td>'+element.ln+'</td> </tr>');--}}
    {{--});--}}
    {{--$("#collapse-2").slideDown(1000);--}}
    {{--});--}}


    {{--},error:function(){--}}
    {{--alert("khaaakkkkkk !");--}}
    {{--}--}}
    {{--});--}}
    {{--});--}}

    {{--</script>--}}

@endsection