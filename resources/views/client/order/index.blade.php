@extends("client.layout")
{{--@section("title") فروشگاه اینترنتی دی تل @endsection--}}
@section("content")
    <div class="body-content outer-top-xs">
        <div class='container'>
            <div class='row'>
                <div class='col-md-12 sidebar'>
                    <!-- ================================== TOP NAVIGATION ================================== -->
                    <div class=" panel panel-body">
                        <div class="head"> تاریخچه سفارشات</div>

                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th class="cart-description item">#</th>
                                <th class="cart-description item">شماره سفارش</th>
                                <th class="cart-description item">تاریخ ثبت سفارش</th>
                                <th class="cart-product-name item">زمان تحویل سفارش</th>
                                <th class="cart-edit item">مبلغ قابل پرداخت</th>
                                <th class="cart-edit item">مبلغ کل</th>
                                <th class="cart-edit item">عملیات پرداخت</th>
                                <th class="cart-edit item">جزئیات</th>
                            </tr>
                            </thead>
                            <tbody id="addressResult">
                            @php $i=1; @endphp
                            @foreach($orders as $order)

                                <tr>
                                    <td class="cart-description item">
                                        {{$i++}}
                                    </td>
                                    <td>
                                        {{--<span class='label label-primary'>--}}

                                        {{$order->number}}
                                        {{--{{ jDate::forge($order->created_at)->format('Y/m/d H:i:s') }}--}}
                                        {{--</span>--}}
                                    </td>
                                    <td class="cart-edit item">
                                        {{--{{$order->create}}--}}

                                        @php  echo(jdate()->forge($order->created_at)->format('%A, %d %B %y')); @endphp
                                        {{--{{ jDate::forge($order->created_at)->format('Y/m/d H:i:s') }}--}}
                                    </td>
                                    <td class="cart-qty item">
                                        {{--{!! orderStatus($order->status) !!}--}}
                                        @php  echo(jdate()->forge($order->date_receive_order)->format('%A, %d %B %y')); @endphp

                                    </td>

                                    <td class="cart-qty item">
                                        {{--{!! orderStatus($order->status) !!}--}}
                                        @php  echo($order->finalPrice); @endphp

                                    </td>

                                    <td class="cart-qty item">
                                        {{--{!! orderStatus($order->status) !!}--}}
                                        @php  echo($order->finalPrice); @endphp

                                    </td>
                                    <td class="cart-qty item">
                                        {{--{!! orderStatus($order->status) !!}--}}
                                        {!! orderStatus($order->status) !!}

                                    </td>
                                    <td class="cart-qty item">
                                        <a href="{{route('orderItem',$order->id)}}">

                                            <i class="fa fa-chevron-left"></i>


                                        </a>

                                    </td>

                                </tr>
                            @endforeach

                            </tbody><!-- /tbody -->
                        </table><!-- /table -->
                    </div>

                </div>

            </div>
        </div>
    </div>

@endsection