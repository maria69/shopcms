@extends("client.layout")
{{--@section("title") فروشگاه اینترنتی دی تل @endsection--}}
@section("content")

    <div class="body-content outer-top-xs" id="top-banner-and-menu">
        <div class="container">
            <div class="row">
                <!-- ============================================== SIDEBAR ============================================== -->

                <!-- /.sidemenu-holder -->
                <!-- ============================================== SIDEBAR : END ============================================== -->

                <!-- ============================================== CONTENT ============================================== -->
                <div class="col-xs-12 col-sm-12 col-md-12 homebanner-holder">
                    <!-- ========================================== SECTION – HERO ========================================= -->

                    <div id="hero">
                        <div id="owl-main" class="owl-carousel owl-inner-nav owl-ui-sm">
                            @foreach($sliders as $slider)

                                <div class="item" style="background-image: url({!!  thumb($slider->media,5) !!});">
                                    <div class="container-fluid">
                                        <div class="caption bg-color vertical-center text-right">
                                            <div class="slider-header fadeInDown-1">{{$slider->title}}</div>
                                            <div class="big-text fadeInDown-1"> {{$slider->body}}</div>
                                            {{--<div class="excerpt fadeInDown-2 hidden-xs"><span>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم.</span>--}}
                                            {{--</div>--}}
                                            <div class="button-holder fadeInDown-3"><a href="{{$slider->link}}"
                                                                                       class="btn-lg btn btn-uppercase btn-primary shop-now-button">کلیگ
                                                    گنید</a></div>
                                        </div>
                                        <!-- /.caption -->
                                    </div>
                                    <!-- /.container-fluid -->
                                </div>
                            @endforeach
                        </div>
                        <!-- /.owl-carousel -->
                    </div>

                    <!-- ========================================= SECTION – HERO : END ========================================= -->

                    <!-- ============================================== INFO BOXES ============================================== -->
                    <div class="info-boxes wow fadeInUp">
                        <div class="info-boxes-inner">
                            <div class="row">
                                <div class="col-md-6 col-sm-4 col-lg-4">
                                    <div class="info-box">
                                        <img class="img-responsive" src="assets/images/banners/home-banner-01.jpg" alt="">
                                    </div>
                                </div>
                                <!-- .col -->

                                <div class="hidden-md col-sm-4 col-lg-4">
                                    <div class="info-box">
                                        <img class="img-responsive" src="assets/images/banners/home-banner-02.jpg" alt="">
                                    </div>
                                </div>
                                <!-- .col -->

                                <div class="col-md-6 col-sm-4 col-lg-4">
                                    <div class="info-box">
                                        <img class="img-responsive" src="assets/images/banners/home-banner-03.jpg" alt="">
                                    </div>
                                </div>
                                <!-- .col -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.info-boxes-inner -->

                    </div>
                    <!-- /.info-boxes -->
                    <!-- ============================================== INFO BOXES : END ============================================== -->
                    <!-- ============================================== SCROLL TABS ============================================== -->
                    <div class="row">

                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                            <div id="product-tabs-slider" class="scroll-tabs outer-top-vs wow fadeInUp">
                                <div class="more-info-tab clearfix ">
                                    <h3 class="new-product-title pull-right">دسته بندی ها</h3>
                                    <ul class="nav nav-tabs nav-tab-line pull-left" id="new-products-1">

                                        <li class="active"><a data-transition-type="backSlide" href="#all"
                                                              data-toggle="tab">همه</a></li>

                                        @foreach($categoriesAndProducts as $category)

                                            <li><a data-transition-type="backSlide" href="#cat{{$category->id}}"
                                                   data-toggle="tab">{{$category->name}}</a></li>

                                        @endforeach

                                    </ul>
                                    <!-- /.nav-tabs -->
                                </div>
                                <div class="tab-content outer-top-xs">

                                    <div class="tab-pane in active" id="all">
                                        <div class="product-slider">
                                            <div class="owl-carousel home-owl-carousel custom-carousel owl-theme" data-item="3">



                                                @foreach($lastProducts  as $product)


                                                    {{--@php dd($lastProducts); @endphp--}}
                                                    <div class="item item-carousel">
                                                        <div class="products">
                                                            <div class="product procat">
                                                                <div class="product-image ">
                                                                    <div class="image">
                                                                        <a
                                                                                href="{{getUrl($product->id)}}">
                                                                            <img src="{!! thumb($product->media,5) !!}" style="width: 90%;height: 125px;margin: 4%;">

                                                                        </a>
                                                                    </div>
                                                                    <!-- /.image -->

                                                                    <div class="tag new"><span>جدید</span></div>
                                                                </div>
                                                                <!-- /.product-image -->

                                                                <div class="product-info text-right ">
                                                                    <h3 class="name"><a href="/product/{{$product->id}}">{{$product->name}}</a>
                                                                    </h3>
                                                                    {{--<div class="rating rateit-small"></div>--}}
                                                                    {{--<div class="description"></div>--}}

                                                                    @php $off_val = $product->off ?  $product->off->offer : 0 ;@endphp
                                                                    <div class="product-price">


                                                                        <span class="price">   @php echo(number_format($product->price - $off_val)); @endphp  تومان </span>
                                                                        <span class="price-before-discount">@php echo(number_format($product->price )); @endphpتومان</span>
                                                                    </div>
                                                                    <!-- /.product-price -->

                                                                </div>
                                                                <!-- /.product-info -->
                                                                <div class="cart clearfix animate-effect">
                                                                    <div class="action">
                                                                        <ul class="list-unstyled">
                                                                            <li class="add-cart-button btn-group">
                                                                                <button data-toggle="tooltip"
                                                                                        class="btn btn-primary icon addtocart" data-product-id="{{$product->id}}"
                                                                                        type="button" title="افزودن به سبد خرید" ><i
                                                                                            class="fa fa-shopping-cart"></i>
                                                                                </button>
                                                                                <button class="btn btn-primary cart-btn "
                                                                                        type="button">افزودن به سبد خرید
                                                                                </button>
                                                                            </li>

                                                                        </ul>
                                                                    </div>
                                                                    <!-- /.action -->
                                                                </div>
                                                                <!-- /.cart -->
                                                            </div>
                                                            <!-- /.product -->

                                                        </div>
                                                        <!-- /.products -->
                                                    </div>

                                            @endforeach


                                            <!-- /.item -->
                                            </div>
                                            <!-- /.home-owl-carousel -->
                                        </div>
                                        <!-- /.product-slider -->
                                    </div>
                                    <!-- /.tab-pane -->


                                    @foreach($categoriesAndProducts as $category)

                                        <div class="tab-pane" id="cat{{$category->id}}" >
                                            <div class="product-slider">
                                                <div class="owl-carousel home-owl-carousel custom-carousel owl-theme"  data-item="3" >

                                                    @foreach($category->products as $product)
                                                        <div class="item item-carousel">
                                                            <div class="products">
                                                                <div class="product procat">
                                                                    <div class="product-image">
                                                                        <div class="image">    <a
                                                                                    href="{{getUrl($product->id)}}">
                                                                                <img src="{!! thumb($product->media,5) !!}" style="width: 90%;height: 125px;margin: 4%;">

                                                                            </a>

                                                                        </div>
                                                                        <!-- /.image

                                                                        <div class="tag sale"><span>پرفروش</span></div>-->
                                                                    </div>
                                                                    <!-- /.product-image -->

                                                                    <div class="product-info text-right">
                                                                        <h3 class="name"><a
                                                                                    href="{{getUrl($product->id)}}">{{$product->name}}</a>
                                                                        </h3>
                                                                        <div class="rating rateit-small"></div>
                                                                        <div class="description"></div>
                                                                        <div class="product-price"><span
                                                                                    class="price"> {{showPrice($product->price)}} </span>
                                                                            <span
                                                                                    class="price-before-discount">{{showPrice($product->price)}}</span>
                                                                        </div>
                                                                        <!-- /.product-price -->

                                                                    </div>
                                                                    <!-- /.product-info -->
                                                                    <div class="cart clearfix animate-effect">
                                                                        <div class="action">
                                                                            <ul class="list-unstyled">
                                                                                <li class="add-cart-button btn-group">
                                                                                    <button data-toggle="tooltip"
                                                                                            class="btn btn-primary icon addtocart" data-product-id="{{$product->id}}"
                                                                                            type="button" title="افزودن به سبد خرید" ><i
                                                                                                class="fa fa-shopping-cart"></i>
                                                                                    </button>
                                                                                    <button class="btn btn-primary cart-btn"
                                                                                            type="button">افزودن به سبد
                                                                                        خرید
                                                                                    </button>
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                        <!-- /.action -->
                                                                    </div>
                                                                    <!-- /.cart -->
                                                                </div>
                                                                <!-- /.product -->

                                                            </div>
                                                            <!-- /.products -->
                                                        </div>
                                                        <!-- /.item -->
                                                    @endforeach

                                                </div>
                                                <!-- /.home-owl-carousel -->
                                            </div>
                                            <!-- /.product-slider -->
                                        </div>
                                        <!-- /.tab-pane -->

                                    @endforeach


                                </div>
                                <!-- /.tab-content -->
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <div class="sidebar-widget hot-deals wow fadeInUp outer-bottom-xs animated" style="visibility: visible; animation-name: fadeInUp;">

                                <h3 class="section-title">پیشنهادات روزانه</h3>
                                {{--<div class="owl-carousel sidebar-carousel custom-carousel owl-theme outer-top-ss" style="opacity: 1; display: block;">--}}
                                <div id="product-tabs-slider" class="scroll-tabs outer-top-vs wow fadeInUp">
                                    <div class="tab-content outer-top-xs">

                                        <div class="tab-pane in active" id="all">
                                            <div class="product-slider">
                                                <div class="owl-carousel home-owl-carousel custom-carousel owl-theme" data-item="1">



                                                    @foreach($dailyPro as $pro)

                                                        <div class="item item-carousel">
                                                            <div class="item">
                                                                <div class="products">
                                                                    <div class="hot-deal-wrapper">
                                                                        <div class="image">
                                                                            <a
                                                                                    href="{{getUrl($pro->id)}}">
                                                                                <img src="{!! thumb($pro->media,5) !!}" style="width: 90%;height: 125px;margin: 4%;">

                                                                            </a>
                                                                        </div>
                                                                        <div class="sale-offer-tag"><span>{{offerType($pro->off->offer_type,$pro->off->offer)}}<br>off</span></div>
                                                                        {!! offerDay($pro->off->start_date,$pro->off->end_date) !!}

                                                                    </div><!-- /.hot-deal-wrapper -->

                                                                    <div class="product-info text-right m-t-20">
                                                                        <h3 class="name pronamedaily"><a href="{{getUrl($pro->id)}}">{{$pro->name}}</a>
                                                                        </h3>

                                                                        <div class="product-price">
								<span class="price">
									{{showPrice($pro->price)}}
								</span>
                                                                            <br/>

                                                                            <span class="price-before-discount">{{number_format($pro->price + offVal($pro->off->offer_type,$pro->off->offer,$pro->price)) . " تومان "}}</span>

                                                                        </div><!-- /.product-price -->

                                                                    </div><!-- /.product-info -->

                                                                    <div class="cart clearfix animate-effect">
                                                                        <div class="action">

                                                                            <div class="add-cart-button btn-group">
                                                                                <button data-toggle="tooltip"
                                                                                        class="btn btn-primary icon addtocart" data-product-id="{{$product->id}}"
                                                                                        type="button" title="افزودن به سبد خرید" ><i
                                                                                            class="fa fa-shopping-cart"></i>
                                                                                </button>

                                                                            </div>

                                                                        </div><!-- /.action -->
                                                                    </div><!-- /.cart -->
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                </div>
                                                <!-- /.home-owl-carousel -->
                                            </div>
                                            <!-- /.product-slider -->
                                        </div>
                                        <!-- /.tab-pane -->


                                        @foreach($categoriesAndProducts as $category)

                                            <div class="tab-pane" id="cat{{$category->id}}">
                                                <div class="product-slider">
                                                    <div class="owl-carousel home-owl-carousel custom-carousel owl-theme">

                                                        @foreach($category->products as $product)
                                                            <div class="item item-carousel">
                                                                <div class="products">
                                                                    <div class="product">
                                                                        <div class="product-image">
                                                                            <div class="image"><a
                                                                                        href="{{getUrl($product->id)}}"><img
                                                                                            src="{{thumb($product->media,5)}}"
                                                                                            alt="{{$product->name}}"></a>
                                                                            </div>
                                                                            <!-- /.image

                                                                            <div class="tag sale"><span>پرفروش</span></div>-->
                                                                        </div>
                                                                        <!-- /.product-image -->

                                                                        <div class="product-info text-right">
                                                                            <h3 class="name"><a
                                                                                        href="{{getUrl($product->id)}}">{{$product->name}}</a>
                                                                            </h3>
                                                                            <div class="rating rateit-small"></div>
                                                                            <div class="description"></div>
                                                                            <div class="product-price"><span
                                                                                        class="price"> {{showPrice($product->price)}} </span>
                                                                                <span
                                                                                        class="price-before-discount">{{showPrice($product->price)}}</span>
                                                                            </div>
                                                                            <!-- /.product-price -->

                                                                        </div>
                                                                        <!-- /.product-info -->
                                                                        <div class="cart clearfix animate-effect">
                                                                            <div class="action">
                                                                                <ul class="list-unstyled">
                                                                                    <li class="add-cart-button btn-group">
                                                                                        <button class="btn btn-primary icon"
                                                                                                data-toggle="dropdown"
                                                                                                type="button"><i
                                                                                                    class="fa fa-shopping-cart"></i>
                                                                                        </button>
                                                                                        <button class="btn btn-primary cart-btn"
                                                                                                type="button">افزودن به سبد
                                                                                            خرید
                                                                                        </button>
                                                                                    </li>
                                                                                    <li class="lnk wishlist"><a
                                                                                                class="add-to-cart"
                                                                                                href="{{getUrl($product->id)}}"
                                                                                                title="Wishlist"> <i
                                                                                                    class="icon fa fa-heart"></i>
                                                                                        </a></li>
                                                                                    <li class="lnk"><a class="add-to-cart"
                                                                                                       href="{{getUrl($product->id)}}"
                                                                                                       title="Compare"> <i
                                                                                                    class="fa fa-signal"
                                                                                                    aria-hidden="true"></i>
                                                                                        </a></li>
                                                                                </ul>
                                                                            </div>
                                                                            <!-- /.action -->
                                                                        </div>
                                                                        <!-- /.cart -->
                                                                    </div>
                                                                    <!-- /.product -->

                                                                </div>
                                                                <!-- /.products -->
                                                            </div>
                                                            <!-- /.item -->
                                                        @endforeach

                                                    </div>
                                                    <!-- /.home-owl-carousel -->
                                                </div>
                                                <!-- /.product-slider -->
                                            </div>
                                            <!-- /.tab-pane -->

                                        @endforeach


                                    </div>
                                </div>

                                {{--<div class="owl-wrapper-outer">--}}
                                {{--<div class="owl-wrapper">--}}
                                {{--<div class="owl-item" style="width: 223px;">--}}
                                {{--<div class="item">--}}
                                {{--<div class="products">--}}
                                {{--<div class="hot-deal-wrapper">--}}
                                {{--<div class="image">--}}
                                {{--<img src="assets/images/hot-deals/p25.jpg" alt="">--}}
                                {{--</div>--}}
                                {{--<div class="sale-offer-tag"><span>11111111111%<br>off</span></div>--}}
                                {{--<div class="timing-wrapper">--}}
                                {{--<div class="box-wrapper">--}}
                                {{--<div class="date box">--}}
                                {{--<span class="key">11111111111111</span>--}}
                                {{--<span class="value">DAYS</span>--}}
                                {{--</div>--}}
                                {{--</div>--}}

                                {{--<div class="box-wrapper">--}}
                                {{--<div class="hour box">--}}
                                {{--<span class="key">20</span>--}}
                                {{--<span class="value">HRS</span>--}}
                                {{--</div>--}}
                                {{--</div>--}}

                                {{--<div class="box-wrapper">--}}
                                {{--<div class="minutes box">--}}
                                {{--<span class="key">36</span>--}}
                                {{--<span class="value">MINS</span>--}}
                                {{--</div>--}}
                                {{--</div>--}}

                                {{--<div class="box-wrapper hidden-md">--}}
                                {{--<div class="seconds box">--}}
                                {{--<span class="key">60</span>--}}
                                {{--<span class="value">SEC</span>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--</div><!-- /.hot-deal-wrapper -->--}}

                                {{--<div class="product-info text-right m-t-20">--}}
                                {{--<h3 class="name"><a href="detail.html">لورم ایپسوم</a>--}}
                                {{--</h3>--}}
                                {{--<div class="rating rateit-small rateit">--}}
                                {{--<button id="rateit-reset-2" data-role="none"--}}
                                {{--class="rateit-reset" aria-label="reset rating"--}}
                                {{--aria-controls="rateit-range-2"--}}
                                {{--style="display: none;"></button>--}}
                                {{--<div id="rateit-range-2" class="rateit-range"--}}
                                {{--tabindex="0"--}}
                                {{--role="slider" aria-label="rating"--}}
                                {{--aria-owns="rateit-reset-2" aria-valuemin="0"--}}
                                {{--aria-valuemax="5" aria-valuenow="4"--}}
                                {{--aria-readonly="true"--}}
                                {{--style="width: 70px; height: 14px;">--}}
                                {{--<div class="rateit-selected"--}}
                                {{--style="height: 14px; width: 56px;"></div>--}}
                                {{--<div class="rateit-hover" style="height:14px"></div>--}}
                                {{--</div>--}}
                                {{--</div>--}}

                                {{--<div class="product-price">--}}
                                {{--<span class="price">--}}
                                {{--تومان600.00--}}
                                {{--</span>--}}

                                {{--<span class="price-before-discount">تومان800.00</span>--}}

                                {{--</div><!-- /.product-price -->--}}

                                {{--</div><!-- /.product-info -->--}}

                                {{--<div class="cart clearfix animate-effect">--}}
                                {{--<div class="action">--}}

                                {{--<div class="add-cart-button btn-group">--}}
                                {{--<button class="btn btn-primary icon"--}}
                                {{--data-toggle="dropdown" type="button">--}}
                                {{--<i class="fa fa-shopping-cart"></i>--}}
                                {{--</button>--}}
                                {{--<button class="btn btn-primary cart-btn"--}}
                                {{--type="button">--}}
                                {{--افزودن به سبد خرید--}}
                                {{--</button>--}}

                                {{--</div>--}}

                                {{--</div><!-- /.action -->--}}
                                {{--</div><!-- /.cart -->--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--</div>--}}

                                {{--<div class="owl-item" style="width: 223px;">--}}
                                {{--<div class="item">--}}
                                {{--<div class="products">--}}
                                {{--<div class="hot-deal-wrapper">--}}
                                {{--<div class="image">--}}
                                {{--<img src="assets/images/hot-deals/p10.jpg" alt="">--}}
                                {{--</div>--}}
                                {{--<div class="sale-offer-tag"><span>33333333333333333333%<br>off</span></div>--}}
                                {{--<div class="timing-wrapper">--}}
                                {{--<div class="box-wrapper">--}}
                                {{--<div class="date box">--}}
                                {{--<span class="key">333333333333333333</span>--}}
                                {{--<span class="value">Days</span>--}}
                                {{--</div>--}}
                                {{--</div>--}}

                                {{--<div class="box-wrapper">--}}
                                {{--<div class="hour box">--}}
                                {{--<span class="key">20</span>--}}
                                {{--<span class="value">HRS</span>--}}
                                {{--</div>--}}
                                {{--</div>--}}

                                {{--<div class="box-wrapper">--}}
                                {{--<div class="minutes box">--}}
                                {{--<span class="key">36</span>--}}
                                {{--<span class="value">MINS</span>--}}
                                {{--</div>--}}
                                {{--</div>--}}

                                {{--<div class="box-wrapper hidden-md">--}}
                                {{--<div class="seconds box">--}}
                                {{--<span class="key">60</span>--}}
                                {{--<span class="value">SEC</span>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--</div><!-- /.hot-deal-wrapper -->--}}

                                {{--<div class="product-info text-right m-t-20">--}}
                                {{--<h3 class="name"><a href="detail.html">لورم ایپسوم</a>--}}
                                {{--</h3>--}}
                                {{--<div class="rating rateit-small rateit">--}}
                                {{--<button id="rateit-reset-4" data-role="none"--}}
                                {{--class="rateit-reset" aria-label="reset rating"--}}
                                {{--aria-controls="rateit-range-4"--}}
                                {{--style="display: none;"></button>--}}
                                {{--<div id="rateit-range-4" class="rateit-range"--}}
                                {{--tabindex="0"--}}
                                {{--role="slider" aria-label="rating"--}}
                                {{--aria-owns="rateit-reset-4" aria-valuemin="0"--}}
                                {{--aria-valuemax="5" aria-valuenow="4"--}}
                                {{--aria-readonly="true"--}}
                                {{--style="width: 70px; height: 14px;">--}}
                                {{--<div class="rateit-selected"--}}
                                {{--style="height: 14px; width: 56px;"></div>--}}
                                {{--<div class="rateit-hover" style="height:14px"></div>--}}
                                {{--</div>--}}
                                {{--</div>--}}

                                {{--<div class="product-price">--}}
                                {{--<span class="price">--}}
                                {{--تومان600.00--}}
                                {{--</span>--}}

                                {{--<span class="price-before-discount">تومان800.00</span>--}}

                                {{--</div><!-- /.product-price -->--}}

                                {{--</div><!-- /.product-info -->--}}

                                {{--<div class="cart clearfix animate-effect">--}}
                                {{--<div class="action">--}}

                                {{--<div class="add-cart-button btn-group">--}}
                                {{--<button class="btn btn-primary icon"--}}
                                {{--data-toggle="dropdown" type="button">--}}
                                {{--<i class="fa fa-shopping-cart"></i>--}}
                                {{--</button>--}}
                                {{--<button class="btn btn-primary cart-btn"--}}
                                {{--type="button">--}}
                                {{--افزودن به سبد خرید--}}
                                {{--</button>--}}

                                {{--</div>--}}

                                {{--</div><!-- /.action -->--}}
                                {{--</div><!-- /.cart -->--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--</div>--}}


                                <div class="owl-controls clickable">
                                    <div class="owl-buttons">
                                        <div class="owl-prev" style="left: 0px"></div>
                                        <div class="owl-next" style="left: 25px"></div>
                                    </div>
                                </div>
                            </div><!-- /.sidebar-widget -->
                            {{--</div>--}}
                        </div>
                    </div>

                    <!-- /.scroll-tabs -->
                    <!-- ============================================== SCROLL TABS : END ============================================== -->
                    <!-- ============================================== WIDE PRODUCTS ============================================== -->
                    <div class="wide-banners wow fadeInUp outer-bottom-xs">
                        <div class="row">
                            <div class="col-md-6 col-sm-6">
                                <div class="wide-banner cnt-strip">
                                    <div class="image"><img class="img-responsive"
                                                            src="assets/images/banners/banner-top-1.png" alt=""></div>
                                </div>
                                <!-- /.wide-banner -->
                            </div>
                            <!-- /.col -->
                            <div class="col-md-6 col-sm-6">
                                <div class="wide-banner cnt-strip">
                                    <div class="image"><img class="img-responsive"
                                                            src="assets/images/banners/banner-top-2.png" alt=""></div>
                                </div>
                                <!-- /.wide-banner -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.wide-banners -->

                    <!-- ============================================== WIDE PRODUCTS : END ============================================== -->
                    <!-- ============================================== FEATURED PRODUCTS ============================================== -->
                    <section class="section featured-product wow fadeInUp">
                        <h3 class="section-title">محصولات ویژه</h3>
                        <div class="owl-carousel home-owl-carousel custom-carousel owl-theme outer-top-xs">

                            @foreach($featuredProducts as $product)

                                <div class="item item-carousel">

                                    <div class="products">
                                        <div class="product">
                                            <div class="product-image">
                                                <div class="image"><a href="{{getUrl($product->id)}}"><img
                                                                src="{{thumb($product->media,5)}}"
                                                                alt="{{$product->name}}"></a></div>
                                                <!-- /.image -->

                                                <!--<div class="tag hot"><span>جدید</span></div>-->
                                            </div>
                                            <!-- /.product-image -->

                                            <div class="product-info text-right">
                                                <h3 class="name"><a
                                                            href="{{getUrl($product->id)}}">{{$product->name}}</a></h3>
                                                <div class="rating rateit-small"></div>
                                                <div class="description"></div>
                                                <div class="product-price"><span
                                                            class="price"> {{showPrice($product->price)}} </span> <span
                                                            class="price-before-discount">{{showPrice($product->price)}}</span>
                                                </div>
                                                <!-- /.product-price -->

                                            </div>
                                            <!-- /.product-info -->
                                            <div class="cart clearfix animate-effect">
                                                <div class="action">
                                                    <ul class="list-unstyled">
                                                        <li class="add-cart-button btn-group">
                                                            <button class="btn btn-primary icon" data-toggle="dropdown"
                                                                    type="button"><i class="fa fa-shopping-cart"></i>
                                                            </button>
                                                            <button class="btn btn-primary cart-btn" type="button">
                                                                افزودن به سبد خرید
                                                            </button>
                                                        </li>
                                                        <li class="lnk wishlist"><a class="add-to-cart"
                                                                                    href="{{getUrl($product->id)}}"
                                                                                    title="Wishlist"> <i
                                                                        class="icon fa fa-heart"></i> </a></li>
                                                        <li class="lnk"><a class="add-to-cart"
                                                                           href="{{getUrl($product->id)}}"
                                                                           title="Compare"> <i class="fa fa-signal"
                                                                                               aria-hidden="true"></i>
                                                            </a></li>
                                                    </ul>
                                                </div>
                                                <!-- /.action -->
                                            </div>
                                            <!-- /.cart -->
                                        </div>
                                        <!-- /.product -->

                                    </div>
                                    <!-- /.products -->
                                </div>

                        @endforeach
                        <!-- /.item -->
                            <!-- /.item -->
                        </div>
                        <!-- /.home-owl-carousel -->
                    </section>
                    <!-- /.section -->
                    <!-- ============================================== FEATURED PRODUCTS : END ============================================== -->
                    <!-- ============================================== WIDE PRODUCTS ============================================== -->
                    <div class="wide-banners wow fadeInUp outer-bottom-xs">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="wide-banner cnt-strip">
                                    <div class="image"><img class="img-responsive"
                                                            src="assets/images/banners/slider-3.png" alt=""></div>
                                    <div class="strip strip-text">
                                        <div class="strip-inner">
                                            {{--<h2 class="text-center">محل درج بنر و شعار تبلبغاتی<br>--}}
                                            {{--<span class="shopping-needs">لورم ایپسوم متن</span></h2>--}}
                                        </div>
                                    </div>
                                {{--<div class="new-label">--}}
                                {{--<div class="text">جدید</div>--}}
                                {{--</div>--}}
                                <!-- /.new-label -->
                                </div>
                                <!-- /.wide-banner -->
                            </div>
                            <!-- /.col -->

                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.wide-banners -->
                    <!-- ============================================== WIDE PRODUCTS : END ============================================== -->


                    <!-- ============================================== FEATURED PRODUCTS ============================================== -->
                    <section class="section wow fadeInUp new-arriavls">
                        <h3 class="section-title">جدید ترین محصولات</h3>


                        <div class="owl-carousel home-owl-carousel custom-carousel owl-theme outer-top-xs newpro">


                            @foreach($lastProducts as $product)

                                <div class="item item-carousel">
                                    <div class="products">
                                        <div class="product">
                                            <div class="product-image">
                                                <div class="image">

                                                    <a href="{{getUrl($product->id)}}  alt="{{$product->name}}>
                                                        @if(isset($product->media))<img src="{!! thumb($product->media,5) !!}" style="width: 90%;height: 125px;margin: 4%;">@else {!! thumb($product->media,2) !!} @endif

                                                    </a>
                                                </div>
                                                <!-- /.image -->

                                                <div class="tag new"><span>جدید</span></div>
                                            </div>
                                            <!-- /.product-image -->

                                            <div class="product-info text-right">
                                                <h3 class="name"><a
                                                            href="{{getUrl($product->id)}}">{{$product->name}}</a></h3>
                                                <div class="rating rateit-small"></div>
                                                <div class="description"></div>
                                                <div class="product-price"><span
                                                            class="price"> {{showPrice($product->price)}} </span> <span
                                                            class="price-before-discount"> {{showPrice($product->price)}} </span>
                                                </div>
                                                <!-- /.product-price -->

                                            </div>
                                            <!-- /.product-info -->
                                            <div class="cart clearfix animate-effect">
                                                <div class="action">
                                                    <ul class="list-unstyled">
                                                        <li class="add-cart-button btn-group">
                                                            <button class="btn btn-primary icon" data-toggle="dropdown"
                                                                    type="button"><i class="fa fa-shopping-cart"></i>
                                                            </button>
                                                            <button class="btn btn-primary cart-btn" type="button">
                                                                افزودن به سبد خرید
                                                            </button>
                                                        </li>
                                                        {{--<li class="lnk wishlist"><a class="add-to-cart"--}}
                                                        {{--href="{{getUrl($product->id)}}"--}}
                                                        {{--title="Wishlist"> <i--}}
                                                        {{--class="icon fa fa-heart"></i> </a></li>--}}
                                                        {{--<li class="lnk"><a class="add-to-cart"--}}
                                                        {{--href="{{getUrl($product->id)}}"--}}
                                                        {{--title="Compare"> <i class="fa fa-signal"--}}
                                                        {{--aria-hidden="true"></i>--}}
                                                        {{--</a></li>--}}
                                                    </ul>
                                                </div>
                                                <!-- /.action -->
                                            </div>
                                            <!-- /.cart -->
                                        </div>
                                        <!-- /.product -->

                                    </div>
                                    <!-- /.products -->
                                </div>
                                <!-- /.item -->

                            @endforeach


                        </div>
                        <!-- /.home-owl-carousel -->
                    </section>
                    <!-- /.section -->
                    <!-- ============================================== FEATURED PRODUCTS : END ============================================== -->


                    <!-- ============================================== BEST SELLER ============================================== -->

                    <div class="best-deal wow fadeInUp outer-bottom-xs">
                        <h3 class="section-title">پرفروش ترین ها</h3>
                        <div class="sidebar-widget-body outer-top-xs">
                            <div class="owl-carousel best-seller custom-carousel owl-theme outer-top-xs">
                                @foreach($lastProducts as $product)
                                    <div class="item">
                                        <div class="products best-product">
                                            <div class="product">
                                                <div class="product-micro">
                                                    <div class="row product-micro-row">
                                                        <div class="col col-xs-5">
                                                            <div class="product-image">
                                                                <div class="image"><a href="{{getUrl($product->id)}}">
                                                                        <img src="{{thumb($product->media,5)}}" alt="{{$product->name}}"> </a></div>
                                                                <!-- /.image -->

                                                            </div>
                                                            <!-- /.product-image -->
                                                        </div>
                                                        <!-- /.col -->
                                                        <div class="col2 col-xs-7">
                                                            <div class="product-info">
                                                                <h3 class="name"><a
                                                                            href="{{getUrl($product->id)}}">{{$product->name}}</a>
                                                                </h3>
                                                                <div class="rating rateit-small"></div>
                                                                <div class="product-price"><span
                                                                            class="price"> {{showPrice($product->price)}} </span>
                                                                </div>
                                                                <!-- /.product-price -->

                                                            </div>
                                                        </div>
                                                        <!-- /.col -->
                                                    </div>
                                                    <!-- /.product-micro-row -->
                                                </div>
                                                <!-- /.product-micro -->

                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <!-- /.sidebar-widget-body -->
                    </div>
                    <!-- /.sidebar-widget -->
                    <!-- ============================================== BEST SELLER : END ============================================== -->


                    <!-- ============================================== BLOG SLIDER ============================================== -->
                    <section class="section latest-blog outer-bottom-vs wow fadeInUp">
                        <h3 class="section-title">آخرین مطالب وبلاگ</h3>
                        <div class="blog-slider-container outer-top-xs">
                            <div class="owl-carousel blog-slider custom-carousel">
                                <div class="item">
                                    <div class="blog-post">
                                        <div class="blog-post-image">
                                            <div class="image"><a href="blog.html"><img
                                                            src="assets/images/blog-post/post1.jpg" alt=""></a></div>
                                        </div>
                                        <!-- /.blog-post-image -->

                                        <div class="blog-post-info text-right">
                                            <h3 class="name"><a href="#">لورم ایپسوم متن ساختگی با تولید سادگی
                                                    نامفهوم</a>
                                            </h3>
                                            <span class="info">لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم </span>
                                            <p class="text">لورم ایپسوم متن ساختگی با تولید سادگی نامفهوملورم ایپسوم متن
                                                ساختگی با تولید سادگی نامفهوم</p>
                                            <a href="#" class="lnk btn btn-primary">ادامه مطلب</a></div>
                                        <!-- /.blog-post-info -->

                                    </div>
                                    <!-- /.blog-post -->
                                </div>
                                <!-- /.item -->

                                <div class="item">
                                    <div class="blog-post">
                                        <div class="blog-post-image">
                                            <div class="image"><a href="blog.html"><img
                                                            src="assets/images/blog-post/post2.jpg" alt=""></a></div>
                                        </div>
                                        <!-- /.blog-post-image -->

                                        <div class="blog-post-info text-right">
                                            <h3 class="name"><a href="#">لورم ایپسوم متن ساختگی با تولید سادگی
                                                    نامفهوم</a>
                                            </h3>
                                            <span class="info">لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم </span>
                                            <p class="text">لورم ایپسوم متن ساختگی با تولید سادگی نامفهوملورم ایپسوم متن
                                                ساختگی با تولید سادگی نامفهوم</p>
                                            <a href="#" class="lnk btn btn-primary">ادامه مطلب</a></div>
                                        <!-- /.blog-post-info -->

                                    </div>
                                    <!-- /.blog-post -->
                                </div>
                                <!-- /.item -->

                                <!-- /.item -->

                                <div class="item">
                                    <div class="blog-post">
                                        <div class="blog-post-image">
                                            <div class="image"><a href="blog.html"><img
                                                            src="assets/images/blog-post/post1.jpg" alt=""></a></div>
                                        </div>
                                        <!-- /.blog-post-image -->

                                        <div class="blog-post-info text-right">
                                            <h3 class="name"><a href="#">لورم ایپسوم متن ساختگی با تولید سادگی
                                                    نامفهوم</a>
                                            </h3>
                                            <span class="info">لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم </span>
                                            <p class="text">لورم ایپسوم متن ساختگی با تولید سادگی نامفهوملورم ایپسوم متن
                                                ساختگی با تولید سادگی نامفهوم</p>
                                            <a href="#" class="lnk btn btn-primary">ادامه مطلب</a></div>
                                        <!-- /.blog-post-info -->

                                    </div>
                                    <!-- /.blog-post -->
                                </div>
                                <!-- /.item -->

                                <div class="item">
                                    <div class="blog-post">
                                        <div class="blog-post-image">
                                            <div class="image"><a href="blog.html"><img
                                                            src="assets/images/blog-post/post2.jpg" alt=""></a></div>
                                        </div>
                                        <!-- /.blog-post-image -->

                                        <div class="blog-post-info text-right">
                                            <h3 class="name"><a href="#">لورم ایپسوم متن ساختگی با تولید سادگی
                                                    نامفهوم</a>
                                            </h3>
                                            <span class="info">لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم </span>
                                            <p class="text">لورم ایپسوم متن ساختگی با تولید سادگی نامفهوملورم ایپسوم متن
                                                ساختگی با تولید سادگی نامفهوم</p>
                                            <a href="#" class="lnk btn btn-primary">ادامه مطلب</a></div>
                                        <!-- /.blog-post-info -->

                                    </div>
                                    <!-- /.blog-post -->
                                </div>
                                <!-- /.item -->

                                <div class="item">
                                    <div class="blog-post">
                                        <div class="blog-post-image">
                                            <div class="image"><a href="blog.html"><img
                                                            src="assets/images/blog-post/post1.jpg" alt=""></a></div>
                                        </div>
                                        <!-- /.blog-post-image -->

                                        <div class="blog-post-info text-right">
                                            <h3 class="name"><a href="#">لورم ایپسوم متن ساختگی با تولید سادگی
                                                    نامفهوم</a>
                                            </h3>
                                            <span class="info">لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم </span>
                                            <p class="text">لورم ایپسوم متن ساختگی با تولید سادگی نامفهوملورم ایپسوم متن
                                                ساختگی با تولید سادگی نامفهوم</p>
                                            <a href="#" class="lnk btn btn-primary">ادامه مطلب</a></div>
                                        <!-- /.blog-post-info -->

                                    </div>
                                    <!-- /.blog-post -->
                                </div>
                                <!-- /.item -->

                            </div>
                            <!-- /.owl-carousel -->
                        </div>
                        <!-- /.blog-slider-container -->
                    </section>
                    <!-- /.section -->
                    <!-- ============================================== BLOG SLIDER : END ============================================== -->


                </div>
                <!-- /.homebanner-holder -->
                <!-- ============================================== CONTENT : END ============================================== -->
            </div>
            <!-- /.row -->
            <!-- ============================================== BRANDS CAROUSEL ============================================== -->
            <section class="section wow fadeInUp new-arriavls">
                <h3 class="section-title">برند ها</h3>


                <div class="owl-carousel home-owl-carousel custom-carousel owl-theme outer-top-xs">

                    @foreach($brands as $brand)
                        <div class="item m-t-10 brands-div" style="margin-bottom: 10px"><a  style="padding-left: 10% !important;"  href="#" class="image brands-a"> <img class="brands-image" data-echo="{{thumb($brand->media,5)}}" src="{{thumb($brand->media,5)}}" alt="{{$brand->name}}"> </a>
                        </div>
                    @endforeach
                    @foreach($brands as $brand)
                        <div class="item m-t-10 brands-div" style="margin-bottom: 10px"><a  style="padding-left: 10% !important;"  href="#" class="image brands-a"> <img class="brands-image" data-echo="{{thumb($brand->media,5)}}" src="{{thumb($brand->media,5)}}" alt="{{$brand->name}}"> </a>
                        </div>
                    @endforeach

                </div>
                <!-- /.home-owl-carousel -->
            </section>

        {{--<div id="brands-carousel" class="logo-slider wow fadeInUp">--}}
        {{--<div class="logo-slider-inner">--}}
        {{--<div id="brand-slider" class="owl-carousel brand-slider custom-carousel owl-theme">--}}
        {{--@foreach($brands as $brand)--}}
        {{--<div class="item m-t-10 brands-div"><a  style="padding-left: 10% !important;"  href="#" class="image brands-a"> <img class="brands-image"--}}
        {{--data-echo="{{thumb($brand->media,5)}}"--}}
        {{--src="{{thumb($brand->media,5)}}" alt="{{$brand->name}}"> </a>--}}
        {{--</div>--}}
        {{--@endforeach--}}

        {{--</div>--}}
        {{--<!-- /.owl-carousel #logo-slider -->--}}
        {{--</div>--}}
        {{--<!-- /.logo-slider-inner -->--}}

        {{--</div>--}}
        <!-- /.logo-slider -->
            <!-- ============================================== BRANDS CAROUSEL : END ============================================== -->
        </div>
        <!-- /.container -->
    </div>
    <!-- /#top-banner-and-menu -->


@endsection