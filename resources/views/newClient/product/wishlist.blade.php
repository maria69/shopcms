@extends("newClient.layout")
{{--@section("title") فروشگاه اینترنتی دی تل @endsection--}}
@section("content")
    <div id="main">

        <div class="main-header background background-image-heading-product"
             style="background-image:url('img/backgrounds/heading-lookbook.jpg');">
            <div class="container">
                <h1>لیست علاقه مندی ها</h1>
            </div>
        </div>
        <div id="breadcrumb">
            <div class="container">
                <ol class="breadcrumb">
                    <li><a href="#">خانه</a></li>
                    <li class="active"><span>لیست علاقه مندی ها</span></li>
                </ol>
            </div>
        </div><!-- breadcrumb -->
        <section id="cart">
            <div class="container">
                <p class="my_cart">
                    لیست علاقه مندی ها
                </p>
                <form class="cart-form" action="/cart" method="post" novalidate="">
                    <div class="table-responsive">
                        <table class="table cart-table">
                            <thead>
                            <tr>
                                <th></th>
                                <th class="product-name">عنوان محصول</th>
                                <th>قیمت</th>
                                <th>وضعیت موجودی</th>
                                <th>جزئیات محصول</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if( isset($wishlists) && count($wishlists) > 0 )
                                @foreach($wishlists as $wishlist)
                                    <tr>
                                        <td class="product-remove">
                                            {!! Form::open(['url'=>route('wish_list.destroy', $wishlist->id),'method'=>'delete','class'=>'delete']) !!}
                                            <button type="submit" class="btn red"> <i class="fa fa-times-circle"></i></button>
                                            {!! Form::close() !!} </div>
                                        </td>
                                        <td class="product-name">
                                            <a href="#">
                                                <img src="{!! thumb($wishlist->product->media,5) !!}" alt="{{$wishlist->product->name}}">
                                            </a>
                                            <a href="{{route('singleProduct',$wishlist->product->id)}}">{{$wishlist->product->name}}</a>
                                        </td>
                                        <td class="product-price">
                                            <p class="price"><span class="money">{{number_format($wishlist->product->price)}} تومان</span></p>
                                        </td>
                                        <td class="product-stock-status">
                                            <span class="wishlist-InStock">InStock</span>
                                        </td>
                                        <td class="product-subtotal">
                                            <div class="action">
                                                <a href="{{getUrl($wishlist->product->id)}}" class="btn-upper btn btn-primary addToCart">جزئیات محصول</a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </form>
            </div>
        </section>
    </div>
@endsection