@extends("newClient.layout")
@section("title") فروشگاه چرم و صنایع دستی@endsection
@section("content")

    <div>
        <div class="container">
            <div class="free-shiping center space-20">
                <div class="col-md-4 col-sm-4">
                    <span class="fa fa-paper-plane-o"></span>
                    <p>ارسال رایگان <br> برای سفارشات بالای 200 هزار تومان</p>
                </div>
                <div class="col-md-4 col-sm-4">
                    <span class="fa fa-refresh"></span>
                    <p>تضمین بازگشت وجه <br>
                    در صورت عدم رضایت از محصول</p>
                </div>
                <div class="col-md-4 col-sm-4">
                    <span class="fa fa-comments-o"></span>
                    <p>پشتیبانی آنلاین
                        <br>
                    ۲۴ ساعته هر روز هفته</p>
                </div>
            </div>
            <!-- End container -->
        </div>
    </div>
    <section>
        <div class="main-slider-wrapper space-40">
            <div class="container">
                <div class="main-slider owl-carousel owl-carousel-inset">
                    @foreach($sliders as $slider)
                        <div class="main-slider-item">
                        <div class="main-slider-image">
                            <img src="{!!  thumb($slider->media,5) !!}" alt="">
                        </div>
                        <div class="main-slider-text">
                            <div class="fp-table">
                                <div class="fp-table-cell left">
                                    <div class="container">
                                        <div class="col-md-7 col-md-offset-5 col-sm-offset-5">
                                            <h5 class="color-black size-20">{{$slider->title}}</h5>
                                            <h2 class="color-home margin-bottom-40 size-55">{{$slider->body}}</h2>
                                            <div class="button">
                                                <a href="{{$slider->link}}" class="btn btn-lg btn-black btn-outline">نمایش</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            <!-- End container -->
        </div>
    </section>
    <script>
        $(function() {  aweMainSlider(); });
    </script>
    <div>
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <div class="awe-media margin-bottom-20">
                        <div class="awe-media-image hover-images">
                            <a href="#">
                                <img alt="" src="./img/samples/banners/home/1.jpg">
                            </a>
                        </div>
                    </div><!-- /.awe-media -->
                </div>

                <div class="col-md-6 col-sm-6">
                    <div class="awe-media margin-bottom-20">
                        <div class="awe-media-image hover-images">
                            <a href="#">
                                <img alt="" src="./img/samples/banners/home/1.jpg">
                            </a>
                        </div><!-- /.awe-media -->
                    </div>
                </div>
            </div><!-- /.row -->

        </div><!-- /.container -->
    </div>
    <section>
        <div class="container">

            <div class="home-products padding-vertical-60">
                <div class="row">
                    <div class="col-md-3 col-sm-4 space-30">
                        <div class="awe-media home-cate-media">
                            <div class="awe-media-header">
                                <div class="awe-media-image">
                                    <img src="/img/samples/collections/index-1/clothing.jpg" alt="">
                                </div><!-- /.awe-media-image -->

                                <div class="awe-media-overlay overlay-dark-50 fullpage">
                                    <div class="content">

                                        <div class="fp-table text-left">
                                            <div class="fp-table-cell">

                                                <h2 class="upper">Clothing</h2>
                                                <p class="margin-bottom-50">At vero eos et accusamus et iusto odio dignissimosmus voluptatum deleniti</p>
                                                <a href="#" class="btn btn-sm btn-outline btn-white">View All</a>

                                            </div>
                                        </div>

                                    </div>
                                </div><!-- /.awe-media-overlay -->

                            </div><!-- /.awe-media-header -->
                        </div><!-- /.awe-media -->
                    </div>

                    <div class="col-md-9 col-sm-8">
                        <div class="products owl-carousel" data-items="3">

                            @foreach($lastProducts  as $product)

                                <div class="product product-grid">
                                <div class="product-media">
                                    <div class="product-thumbnail">
                                        <a href="{{getUrl($product->slug)}}" title="">
                                            @if(isset($product->media))
                                                <img src="{!! thumb($product->media,5) !!}" alt="" class="current">
                                        @else
                                            {!! thumb($product->media,5) !!}
                                        @endif
                                            <!-- <img src="./img/samples/products/index/clothing/1.jpg" alt=""> -->
                                        </a>
                                    </div><!-- /.product-thumbnail -->


                                    <div class="product-hover">
                                        <div class="product-actions">
                                            <a href="#" class="awe-button product-add-cart addtocart" data-toggle="tooltip" data-product-id="{{$product->slug}}" title="Add to cart">
                                                <i class="icon icon-shopping-bag"></i>
                                            </a>

                                            <a href="#" class="awe-button product-quick-wishlist" data-toggle="tooltip" title="Add to wishlist">
                                                <i class="icon icon-star"></i>
                                            </a>
                                            <a href="#" class="awe-button product-quick-view" data-toggle="tooltip" title="Quickview">
                                                <i class="icon icon-eye"></i>
                                            </a>
                                        </div>
                                    </div><!-- /.product-hover -->
                                </div><!-- /.product-media -->
                                <div class="product-body">
                                    <h2 class="product-name">
                                        <a href="{{getUrl($product->slug)}}" title="{{$product->name}}">{{$product->name}}</a>
                                    </h2><!-- /.product-product -->

                                    <div class="rating small">
                                        <span class="star"></span>
                                        <span class="star"></span>
                                        <span class="star"></span>
                                        <span class="star"></span>
                                        <span class="star disable"></span>
                                    </div>
                                    <div class="section-post-price">
                                        @php $off_val = $product->off ?  $product->off->offer : 0 ;@endphp
                                        @if($off_val > 0)
                                            <span class="amount"> @php echo(number_format($product->price - $off_val)); @endphp  تومان
                                            @endif
                                        <br>
                                           @if($off_val > 0) <del class="smallPrice">تومان  {{ number_format($product->price)}}</del>@else <span>تومان  {{ number_format($product->price)}}</span>@endif
                                        </span>
                                    </div><!-- /.product-price -->
                                </div><!-- /.product-body -->
                            </div><!-- /.product -->
                            @endforeach
                        </div><!-- ./products -->
                    </div>
                </div><!-- /.row -->
                <div class="divider"></div>
                <div class="section-brands">
                    <div class="brands-carousel owl-carousel" id="brands-carousel">
                        <div class="center">
                            <img src="./img/samples/brands/brand-1.png" alt="">
                        </div><!-- /.center -->
                        <div class="center">
                            <img src="./img/samples/brands/brand-1.png" alt="">
                        </div><!-- /.center -->
                        <div class="center">
                            <img src="./img/samples/brands/brand-1.png" alt="">
                        </div><!-- /.center -->
                        <div class="center">
                            <img src="./img/samples/brands/brand-1.png" alt="">
                        </div><!-- /.center -->
                        <div class="center">
                            <img src="./img/samples/brands/brand-1.png" alt="">
                        </div><!-- /.center -->
                        <div class="center">
                            <img src="./img/samples/brands/brand-1.png" alt="">
                        </div><!-- /.center -->
                        <div class="center">
                            <img src="./img/samples/brands/brand-1.png" alt="">
                        </div><!-- /.center -->
                        <div class="center">
                            <img src="./img/samples/brands/brand-1.png" alt="">
                        </div><!-- /.center -->
                    </div><!-- /.brands-carousel -->
                </div><!-- /.section-brands -->
                <div class="divider"></div>
                <div class="row">
                    <div class="col-md-3 col-sm-4 col-md-push-9 col-sm-push-8 space-30">
                        <div class="awe-media home-cate-media">
                            <div class="awe-media-header">
                                <div class="awe-media-image">
                                    <img src="./img/samples/collections/index-1/shoes.jpg" alt="">
                                </div><!-- /.awe-media-image -->
                                <div class="awe-media-overlay overlay-dark-50 fullpage">
                                    <div class="content">
                                        <div class="fp-table text-left">
                                            <div class="fp-table-cell">
                                                <h2 class="upper">Shoes</h2>
                                                <p class="margin-bottom-50">At vero eos et accusamus et iusto odio dignissimosmus voluptatum deleniti corrupti quos dolores</p>
                                                <a href="#" class="btn btn-sm btn-outline btn-white">View All</a>
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- /.awe-media-overlay -->
                            </div><!-- /.awe-media-header -->
                        </div><!-- /.awe-media -->
                    </div>
                    <div class="col-md-9 col-sm-8 col-md-pull-3 col-sm-pull-4">
                        <div class="products owl-carousel" data-items="3">
                            @foreach($lastProducts  as $product)
                                <div class="product product-grid">
                                    <div class="product-media">
                                        <div class="product-thumbnail">
                                            <a href="{{getUrl($product->slug)}}" title="">
                                                @if(isset($product->media))
                                                <img src="{!! thumb($product->media,5) !!}" alt="" class="current">
                                                @else
                                                {!! thumb($product->media,5) !!}
                                                @endif
                                                <!-- <img src="./img/samples/products/index/clothing/1.jpg" alt=""> -->
                                            </a>
                                        </div><!-- /.product-thumbnail -->
                                        <div class="product-hover">
                                            <div class="product-actions">
                                                <a href="#" class="awe-button product-add-cart addtocart" data-toggle="tooltip" data-product-id="{{$product->slug}}" title="Add to cart">
                                                    <i class="icon icon-shopping-bag"></i>
                                                </a>
                                                <a href="#" class="awe-button product-quick-wishlist" data-toggle="tooltip" title="Add to wishlist">
                                                    <i class="icon icon-star"></i>
                                                </a>
                                                <a href="#" class="awe-button product-quick-view" data-toggle="tooltip" title="Quickview">
                                                    <i class="icon icon-eye"></i>
                                                </a>
                                            </div>
                                        </div><!-- /.product-hover -->
                                    </div><!-- /.product-media -->
                                    <div class="product-body">
                                        <h2 class="product-name">
                                            <a href="{{getUrl($product->slug)}}" title="{{$product->name}}">{{$product->name}}</a>
                                        </h2><!-- /.product-product -->
                                        <div class="rating small">
                                            <span class="star"></span>
                                            <span class="star"></span>
                                            <span class="star"></span>
                                            <span class="star"></span>
                                            <span class="star disable"></span>
                                        </div>
                                        <div class="section-post-price">
                                            @php $off_val = $product->off ?  $product->off->offer : 0 ;@endphp
                                            @if($off_val > 0)
                                                <span class="amount"> @php echo(number_format($product->price - $off_val)); @endphp  تومان
                                            @endif
                                        <br>
                                           @if($off_val > 0) <del class="smallPrice">تومان  {{ number_format($product->price)}}</del>@else <span>تومان  {{ number_format($product->price)}}</span>@endif
                                        </span>
                                        </div>
                                        <!-- /.product-price -->
                                    </div><!-- /.product-body -->
                                </div><!-- /.product -->
                            @endforeach
                        </div><!-- ./products -->
                    </div>
                </div><!-- /.row -->
            </div><!-- /.home-products -->
        </div><!-- /.container -->
    </section><!-- /section -->

    <section class="background background-color-gray">
        <div class="container">
            <div class="padding-vertical-70">

                <div class="row">
                    <div class="col-md-6 col-sm-5">
                        <div class="text-center">
                            <img src="img/samples/products/2.jpg" alt="">
                        </div>
                    </div>

                    <div class="col-md-5 col-md-offset-1 col-sm-7">
                        <div class="saleoff-actions">
                            <div class="time" data-countdown="countdown" data-date="10-20-2016-10-20-30"></div>
                            <span class="saleoff-price">
                            <span class="amount">$50.00</span>
                            <del class="amount">$120</del>
                        </span><!-- /.product-price -->

                            <h4>Blue/Black Single Speed Bike</h4>
                            <p>Fusce ante augue, eleifend vel lorem sit amet, laoreet congue velit. Proin dui libero, fermentum a facilisis non, elementum sit amet nisi.</p>

                            <button class="btn btn-lg btn-primary" >Shop now</button>
                        </div><!-- /.product-list-actions -->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="container">
            <div class="padding-vertical-60">
                <div class="section-header">
                    <h2>محصولات پر فروش</h2>
                </div>

                <div class="section-content">
                    <div class="home-section-featured slider-items3 owl-carousel">
                        @foreach ($lastProducts->chunk(3) as $chunk)
{{--                            {{dd($chunk)}}--}}
                            <div class="items">
                                @foreach ($chunk as $product)
                                    <div class="section-post">
                                        <div class="section-post-media">
                                            <a href="{{getUrl($product->slug)}}" title="{{$product->name}}">
                                                @if(isset($product->media))
                                                    <img src="{!! thumb($product->media,5) !!}" alt="" class="current">
                                                @else
                                                    {!! thumb($product->media,5) !!}
                                                @endif
                                            </a>
                                        </div>

                                        <div class="section-post-body">
                                            <h4 class="section-post-title">
                                                <a href="{{getUrl($product->slug)}}" title="{{$product->name}}">{{str_limit($product->name, $limit = 28, $end = ' ...') }}</a>
                                            </h4>
{{--                                            <div class="rating small">--}}
{{--                                                <span class="star"></span>--}}
{{--                                                <span class="star"></span>--}}
{{--                                                <span class="star"></span>--}}
{{--                                                <span class="star"></span>--}}
{{--                                                <span class="star disable"></span>--}}
{{--                                            </div>--}}

                                            <div class="clearfix"></div>

                                            <div class="section-post-price">
                                                @php $off_val = $product->off ?  $product->off->offer : 0 ;@endphp
                                                @if($off_val > 0)
                                                    <span class="amount"> @php echo(number_format($product->price - $off_val)); @endphp  تومان
                                            @endif
                                        <br>
                                           @if($off_val > 0) <del class="smallPrice">تومان  {{ number_format($product->price)}}</del>@else <span>تومان  {{ number_format($product->price)}}</span>@endif
                                        </span>
                                            </div>
                                        </div>
                                    </div>

                                @endforeach
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="container">
        <div class="divider"></div>
    </div>
    <section>
        <div class="section-header center size-30">
            <span class="icons icons-instag"></span>
            <h2>Instagram Widget</h2>
        </div>
        <div class="widget">
            <ul class="instagram-widget">
                <li><a href="#" title="Instagram"><img src="img/samples/instagram/1.jpg" alt="Instagram"></a></li>
                <li><a href="#" title="Instagram"><img src="img/samples/instagram/1.jpg" alt="Instagram"></a></li>
                <li><a href="#" title="Instagram"><img src="img/samples/instagram/1.jpg" alt="Instagram"></a></li>
                <li><a href="#" title="Instagram"><img src="img/samples/instagram/1.jpg" alt="Instagram"></a></li>
                <li><a href="#" title="Instagram"><img src="img/samples/instagram/1.jpg" alt="Instagram"></a></li>
                <li><a href="#" title="Instagram"><img src="img/samples/instagram/1.jpg" alt="Instagram"></a></li>
                <li><a href="#" title="Instagram"><img src="img/samples/instagram/1.jpg" alt="Instagram"></a></li>
                <li><a href="#" title="Instagram"><img src="img/samples/instagram/1.jpg" alt="Instagram"></a></li>
            </ul>
        </div>
    </section>
    <section>
        <div class="container">
            <div class="subscible-wrapper subscible-wrapper-v2 subscible-inline">
                <div class="row">
                    <div class="col-md-6">
                        <div class="subscribe-comment">
                            <h3 class="subscribe-title left">Sign Up For The Newsletter</h3>
                            <p>Subscribe for latest stories and promotions</p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <form class="subscible-form" action="index.html" method="GET">
                            <div class="form-group">
                                <label for="subscribe-email" class="sr-only">Email</label>
                                <input type="email" id="subscribe-email" class="form-control" placeholder="Enter your email address">
                            </div>
                            <div class="form-submit">
                                <button type="submit" class="btn btn-newsletter"><span class="fa fa-envelope-o"></span></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div><!-- /.subscible-wrapper -->
        </div><!-- /.container -->
    </section>
@endsection