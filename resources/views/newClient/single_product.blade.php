@extends("newClient.layout")
@section("title"){{$product->name}} @endsection
@section("content")
    <div id="main">
        <div class="main-header background background-image-heading-product">
            <div class="container">
                <h1>{{$product->name}}</h1>
            </div>
        </div>
        <div id="breadcrumb">
            <div class="container">
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li class="active"><span>Product</span></li>
                </ol>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="product-slider-wrapper thumbs-bottom">

                        <div class="swiper-container product-slider-main">
                            <div class="swiper-wrapper">
                                @foreach($product->medias as $media)
                                    <div class="swiper-slide">
                                        <div class="easyzoom easyzoom--overlay">
                                            <a href="{{thumb($media,5)}}" title="{{$product->name}}">
                                                <img src="{{thumb($media,5)}}" alt="{{$product->name}}">
                                            </a>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <div class="swiper-button-prev"><i class="fa fa-chevron-left"></i></div>
                            <div class="swiper-button-next"><i class="fa fa-chevron-right"></i></div>
                        </div><!-- /.swiper-container -->
                        <div class="swiper-container product-slider-thumbs">
                            <div class="swiper-wrapper">
                                @foreach($product->medias as $media)
                                    <div class="swiper-slide">
                                        <img src="{{thumb($media,5)}}" alt="">
                                    </div>
                                @endforeach
                            </div>
                        </div><!-- /.swiper-container -->
                    </div><!-- /.product-slider-wrapper -->
                </div>
                <div class="col-md-6">
                    <nav class="pnav">
                        <div class="pull-left">

                            <a href="#" class="btn btn-sm btn-arrow btn-default">
                                <i class="fa fa-chevron-right"></i>
                            </a>
                            <a href="#" class="btn btn-sm btn-arrow btn-default">
                                <i class="fa fa-chevron-left"></i>
                            </a>

                        </div>

                        <a href="#" class="back-to-pcate">
                            <i class="fa fa-chevron-left"></i>
                            <span>Back to Portfolio</span>
                        </a>
                    </nav><!-- /header -->

                    <div class="product-details-wrapper">
                        <h2 class="product-name">
                            <a href="{{getUrl($product->id)}}" title=" {{$product->name}}"> {{$product->name}}</a>
                        </h2><!-- /.product-name -->

                        <div class="product-status">
                            <span class="in-stock">موجود</span>
                            <span>-</span>
                            <small>{{$product->en_name}}</small>
                        </div><!-- /.product-status -->

                        <div class="product-stars">
                        <span class="rating">
                           @for($x=1;$x<=$star_int;$x++)
                                <span class="star"></span>
                            @endfor
                            {{--@if ($star_float > 0)--}}
                            {{--<span style="color:orange;font-size:1.3em;">&#9734;</span>--}}
                            {{--@php $x++; @endphp--}}
                            {{--@endif--}}
                            @while ($x<=5)
                                   <span class=""></span>
                                @php $x++; @endphp
                            @endwhile
                        </span>
                        </div><!-- /.product-stars -->

                        <div class="product-description">
                            {!! $product->short_description !!}
                        </div><!-- /.product-description -->

                        <div class="product-features">
                            <h3>Special Features:</h3>

                            <ul>
                                <li>1914 translation by H. Rackham</li>
                                <li>The standard Lorem Ipsum passage, used since the 1500s</li>
                                <li>Section 1.10.33 of "de Finibus Bonorum et Malorum</li>
                            </ul>
                        </div><!-- /.product-features -->

                        <div class="product-actions-wrapper">
                            <form action="#" method="POST">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="p_color">Color</label>
                                            <select name="p_color" id="p_color" class="form-control">
                                                <option value="">Blue</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="p_size">Size</label>
                                            <select name="p_size" id="p_size" class="form-control">
                                                <option value="">XL</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="p_qty">Qty</label>
                                            <select name="p_qty" id="p_qty" class="form-control">
                                                <option value="">1</option>
                                                <option value="">2</option>
                                                <option value="">3</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </form><!-- /.form -->

                            <div class="product-list-actions">
                            <span class="product-price">
                                <span class="amount">{{number_format($product->price)}} تومان </span>
                            </span><!-- /.product-price -->
                                <a href="#" class="btn btn-lg btn-primary addtocart" data-product-id="{{$product->id}}"><i class="fa fa-shopping-cart inner-right-vs"></i> افزودن به سبد خرید</a>

                            </div><!-- /.product-list-actions -->
                        </div><!-- /.product-actions-wrapper -->

                        <div class="product-meta">
                        <span class="product-category">
                            <span>Category:</span>
                            <a href="#" title="">Outerwear</a>
                        </span>

                            <span>-</span>

                            <span class="product-tags">
                            <span>Tags:</span>
                            <a href="#" title="">Jacket</a>
                        </span>
                        </div><!-- /.product-meta -->
                    </div><!-- /.product-details-wrapper -->
                </div>
            </div>

            <div class="product-socials">
                <ul class="list-socials">

                    <li><a href="#" data-toggle="tooltip" title="Twitter"><i class="icon icon-twitter"></i></a></li>
                    <li><a href="#" data-toggle="tooltip" title="Facebook"><i class="icon icon-facebook"></i></a></li>
                    <li><a href="#" data-toggle="tooltip" title="Dot-Dot"><i class="icon icon-dot-dot"></i></a></li>
                    <li><a href="#" data-toggle="tooltip" title="Google+"><i class="icon icon-google-plus"></i></a></li>
                    <li><a href="#" data-toggle="tooltip" title="Pinterest"><i class="icon icon-pinterest"></i></a></li>

                </ul>
            </div><!-- /.product-socials -->

            <div class="product-details-left">
                <div role="tabpanel" class="product-details">
                    <nav>
                        <ul class="nav" role="tablist">
                            <li role="presentation" class="active">
                                <a href="#product-description" data-toggle="tab">توضیحات محصول</a>
                            </li>
                            <li role="presentation">
                                <a href="#product-infomation" data-toggle="tab">اطلعات تکمیلی</a>
                            </li>
                            <li role="presentation">
                                <a href="#product-review" data-toggle="tab">نظرات <span>{{$comments->count()}}</span></a>
                            </li>
                        </ul><!-- /.nav -->
                    </nav><!-- /nav -->

                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="product-description">
                            {!! $product->description !!}
                        </div><!-- /.tab-pane -->

                        <div role="tabpanel" class="tab-pane" id="product-infomation">
                            <ul>
                                <li>
                                    <span>Weight</span>
                                    <span class="value">8.6kg</span>
                                </li>

                                <li>
                                    <span>Color</span>
                                    <span class="value">Yellow, Brown</span>
                                </li>

                                <li>
                                    <span>Size</span>
                                    <span class="value">S, M, L, XL, XXL</span>
                                </li>

                                <li>
                                    <span>Material</span>
                                    <span class="value">Nylon, Coton</span>
                                </li>
                            </ul>
                        </div><!-- /.tab-pane -->

                        <div role="tabpanel" class="tab-pane" id="product-review">
                            <h3>نظرات<span>{{$comments->count()}}</span></h3>

                            @if($comments->count() > 0)

                                @foreach($comments as $comment)


                                    @if(Auth::check())

                                        @if(Auth::user()->id == $comment->user_id && $comment->status == 0)
                                            <div class="review">
                                                <div class="review-title"><span class="date"><i class="fa fa-calendar"></i> &nbsp; <span>{{time_elapsed_string(strtotime($comment->created_at))}}</span></span><span class="summary">{{$comment->user->first_name}} {{$comment->user->last_name}} (دیدگاه شما پس از تایید کارشناس سایت نمایش داده میشود) </span></div>

                                                <hr>

                                                <div class="row">

                                                    <div class="col-md-6">
                                                        <h4 class="text-danger">نقاط ضعف</h4>
                                                        <ul class="weaknesses">
                                                            @foreach($comment->weaknesses as $weaknesses)
                                                                <li>{{$weaknesses->description}}</li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <h4 class="text-success">نقاط قوت</h4>
                                                        <ul class="strengths">
                                                            @foreach($comment->strengths as $strengths)
                                                                <li>{{$strengths->description}}</li>
                                                            @endforeach
                                                        </ul>
                                                    </div>

                                                </div>

                                                <hr>

                                                <div class="text">{{$comment->body}}</div>
                                                <span class="pull-left" data-id="{{$comment->id}}">
                                                                             <div id="dislike1-bs3">{{$comment->unlike}}</div><i id="like1" class="glyphicon glyphicon-thumbs-up"></i>
                                                                            <div id="like1-bs3">{{$comment->like}}</div> <i id="dislike1" class="glyphicon glyphicon-thumbs-down"></i>
                                                                        </span>
                                            </div>
                                        @elseif($comment->status == 1)
                                            <div class="review">
                                                <div class="review-title"><span class="date"><i class="fa fa-calendar"></i> &nbsp; <span>{{time_elapsed_string(strtotime($comment->created_at))}}</span></span><span class="summary">{{$comment->user->first_name}} {{$comment->user->last_name}}</span></div>
                                                <hr>

                                                <div class="row">

                                                    <div class="col-md-6">
                                                        <h4 class="text-danger">نقاط ضعف</h4>
                                                        <ul class="weaknesses">
                                                            @foreach($comment->weaknesses as $weaknesses)
                                                                <li>{{$weaknesses->description}}</li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <h4 class="text-success">نقاط قوت</h4>
                                                        <ul class="strengths">
                                                            @foreach($comment->strengths as $strengths)
                                                                <li>{{$strengths->description}}</li>
                                                            @endforeach
                                                        </ul>
                                                    </div>

                                                </div>

                                                <hr>
                                                <div class="text">{{$comment->body}}</div>
                                                <span class="pull-left" data-id="{{$comment->id}}">
                                                                             <div id="dislike1-bs3">{{$comment->unlike}}</div><i id="like1" class="glyphicon glyphicon-thumbs-up"></i>
                                                                            <div id="like1-bs3">{{$comment->like}}</div> <i id="dislike1" class="glyphicon glyphicon-thumbs-down"></i>
                                                                        </span>
                                            </div>
                                        @endif


                                    @else

                                        @if($comment->status == 1)

                                            <div class="review">
                                                <div class="review-title"><span class="date"><i class="fa fa-calendar"></i> &nbsp; <span>{{time_elapsed_string(strtotime($comment->created_at))}}</span></span><span class="summary">{{$comment->user->first_name}} {{$comment->user->last_name}}</span></div>
                                                <hr>

                                                <div class="row">

                                                    <div class="col-md-6">
                                                        <h4 class="text-danger">نقاط ضعف</h4>
                                                        <ul class="weaknesses">
                                                            @foreach($comment->weaknesses as $weaknesses)
                                                                <li>{{$weaknesses->description}}</li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <h4 class="text-success">نقاط قوت</h4>
                                                        <ul class="strengths">
                                                            @foreach($comment->strengths as $strengths)
                                                                <li>{{$strengths->description}}</li>
                                                            @endforeach
                                                        </ul>
                                                    </div>

                                                </div>

                                                <hr>
                                                <div class="text">{{$comment->body}}</div>
                                                <span class="pull-left" data-id="{{$comment->id}}">
                                                                             <div id="dislike1-bs3">{{$comment->unlike}}</div><i id="like1" class="glyphicon glyphicon-thumbs-up"></i>
                                                                            <div id="like1-bs3">{{$comment->like}}</div> <i id="dislike1" class="glyphicon glyphicon-thumbs-down"></i>
                                                                        </span>
                                            </div>

                                        @endif

                                    @endif



                                @endforeach

                            @endif

                            <div class="product-add-review">
                                {{--<h4 class="title"></h4>--}}
                                <div class="review-table">
                                    <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <th colspan="2" class="cell-label">&nbsp;دیدگاه خود را بنویسید </th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($stars as $star)
                                                <tr id="wrapper{{$star->id}}" class="wrapper">
                                                    <td width="30%" class="cell-label">{{ $star->name }}</td>

                                                    <td width="70%" class="cell-label">  <input id="radio1" type="radio" name="stars{{$star->id}}" value="5"><!--
                                                                                      --><label for="radio{{$star->id}}">&#9733;</label><!--
                                                                                      --><input id="radio{{$star->id}}" type="radio" name="stars{{$star->id}}" value="4"><!--
                                                                                      --><label for="radio{{$star->id}}">&#9733;</label><!--
                                                                                      --><input id="radio{{$star->id}}" type="radio" name="stars{{$star->id}}" value="3"><!--
                                                                                      --><label for="radio{{$star->id}}">&#9733;</label><!--
                                                                                      --><input id="radio{{$star->id}}" type="radio" name="stars{{$star->id}}" value="2"><!--
                                                                                      --><label for="radio{{$star->id}}">&#9733;</label><!--
                                                                                      --><input id="radio{{$star->id}}" type="radio" name="stars{{$star->id}}" value="1"><!--
                                                                                      --><label for="radio{{$star->id}}">&#9733;</label></td>
                                                </tr>
                                            @endforeach

                                            </tbody>
                                        </table><!-- /.table .table-bordered -->


                                    </div><!-- /.table-responsive -->
                                </div><!-- /.review-table -->


                                <div class="review-form">
                                    <div class="form-container">


                                        <div class="row" id="reviews_by_user">

                                            <div class="col-sm-6" id="plus">
                                                <h4>نقاط قوت <i style="color:green;cursor: pointer" class="fa fa-plus"></i> </h4>
                                                <div class="form-group">
                                                    <input type="text" name="strengths[]" class="form-control txt" placeholder="قوت">
                                                </div>
                                            </div>

                                            <div class="col-sm-6" id="mines">
                                                <h4>نقاط ضعف <i style="color:green;cursor: pointer" class="fa fa-plus"></i> </h4>
                                                <div class="form-group">
                                                    <input name="weaknesses[]" type="text" class="form-control txt" placeholder="ضعف">
                                                </div>
                                            </div>

                                        </div>


                                        <div class="row">

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="body">دیدگاه <span class="astk">*</span></label>
                                                    <textarea name="body" class="form-control txt txt-review" id="body" rows="4" placeholder="دیدگاه">{{old("body")}}</textarea>
                                                </div><!-- /.form-group -->
                                            </div>
                                        </div><!-- /.row -->

                                        <div class="action text-right">
                                            <button class="btn btn-primary btn-upper">ثبت</button>
                                        </div><!-- /.action -->


                                    </div><!-- /.form-container -->
                                </div><!-- /.review-form -->


                            </div><!-- /.product-add-review -->

                        </div><!-- /.tab-pane -->
                    </div><!-- /.tab-pane -->
                </div><!-- /.tab-content -->
            </div><!-- /.product-details-left -->

            <div class="relared-products">
                <div class="relared-products-header margin-bottom-50">
                    <h3 class="upper">Related Products</h3>
                </div>

                <div class="products owl-carousel" data-items="4">


                    <div class="product product-grid">
                        <div class="product-media">
                            <div class="product-thumbnail">
                                <a href="product-fullwidth.html" title="">
                                    <img src="./img/samples/products/grid/1.jpg" alt="" class="current">
                                    <!-- <img src="./img/samples/products/index/clothing/2.jpg" alt=""> -->
                                </a>
                            </div><!-- /.product-thumbnail -->


                            <div class="product-hover">
                                <div class="product-actions">
                                    <a href="#" class="awe-button product-add-cart" data-toggle="tooltip"
                                       title="Add to cart">
                                        <i class="icon icon-shopping-bag"></i>
                                    </a>

                                    <a href="#" class="awe-button product-quick-wishlist" data-toggle="tooltip"
                                       title="Add to wishlist">
                                        <i class="icon icon-star"></i>
                                    </a>

                                    <a href="#" class="awe-button product-quick-view" data-toggle="tooltip"
                                       title="Quickview">
                                        <i class="icon icon-eye"></i>
                                    </a>
                                </div>
                            </div><!-- /.product-hover -->


                            <span class="product-label hot">
                            <span>hot</span>
                        </span>

                        </div><!-- /.product-media -->

                        <div class="product-body">
                            <h2 class="product-name">
                                <a href="#" title="Gin Lane Greenport Cotton Shirt">Gin Lane Greenport Cotton Shirt</a>
                            </h2><!-- /.product-product -->

                            <div class="product-category">
                                <span>Jackets</span>
                            </div><!-- /.product-category -->

                            <div class="product-price">

                                <span class="amount">$246</span>

                            </div><!-- /.product-price -->
                        </div><!-- /.product-body -->
                    </div><!-- /.product -->


                    <div class="product product-grid">
                        <div class="product-media">
                            <div class="product-thumbnail">
                                <a href="product-fullwidth.html" title="">
                                    <img src="./img/samples/products/grid/1.jpg" alt="" class="current">
                                    <!-- <img src="./img/samples/products/index/clothing/2.jpg" alt=""> -->
                                </a>
                            </div><!-- /.product-thumbnail -->


                            <div class="product-hover">
                                <div class="product-actions">
                                    <a href="#" class="awe-button product-add-cart" data-toggle="tooltip"
                                       title="Add to cart">
                                        <i class="icon icon-shopping-bag"></i>
                                    </a>

                                    <a href="#" class="awe-button product-quick-wishlist" data-toggle="tooltip"
                                       title="Add to wishlist">
                                        <i class="icon icon-star"></i>
                                    </a>

                                    <a href="#" class="awe-button product-quick-view" data-toggle="tooltip"
                                       title="Quickview">
                                        <i class="icon icon-eye"></i>
                                    </a>
                                </div>
                            </div><!-- /.product-hover -->


                        </div><!-- /.product-media -->

                        <div class="product-body">
                            <h2 class="product-name">
                                <a href="#" title="Gin Lane Greenport Cotton Shirt">Gin Lane Greenport Cotton Shirt</a>
                            </h2><!-- /.product-product -->

                            <div class="product-category">
                                <span>Short</span>
                            </div><!-- /.product-category -->

                            <div class="product-price">

                                <span class="amount">$60</span>

                            </div><!-- /.product-price -->
                        </div><!-- /.product-body -->
                    </div><!-- /.product -->


                    <div class="product product-grid">
                        <div class="product-media">
                            <div class="product-thumbnail">
                                <a href="product-fullwidth.html" title="">
                                    <img src="./img/samples/products/grid/1.jpg" alt="" class="current">
                                    <!-- <img src="./img/samples/products/index/clothing/2.jpg" alt=""> -->
                                </a>
                            </div><!-- /.product-thumbnail -->


                            <div class="product-hover">
                                <div class="product-actions">
                                    <a href="#" class="awe-button product-add-cart" data-toggle="tooltip"
                                       title="Add to cart">
                                        <i class="icon icon-shopping-bag"></i>
                                    </a>

                                    <a href="#" class="awe-button product-quick-wishlist" data-toggle="tooltip"
                                       title="Add to wishlist">
                                        <i class="icon icon-star"></i>
                                    </a>

                                    <a href="#" class="awe-button product-quick-view" data-toggle="tooltip"
                                       title="Quickview">
                                        <i class="icon icon-eye"></i>
                                    </a>
                                </div>
                            </div><!-- /.product-hover -->


                            <span class="product-label sale">
                            <span>sale</span>
                        </span>

                        </div><!-- /.product-media -->

                        <div class="product-body">
                            <h2 class="product-name">
                                <a href="#" title="Gin Lane Greenport Cotton Shirt">Gin Lane Greenport Cotton Shirt</a>
                            </h2><!-- /.product-product -->

                            <div class="product-category">
                                <span>Jackets</span>
                            </div><!-- /.product-category -->

                            <div class="product-price">

                                <span class="amount">$120</span>
                                <del class="amount">$320</del>

                            </div><!-- /.product-price -->
                        </div><!-- /.product-body -->
                    </div><!-- /.product -->


                    <div class="product product-grid">
                        <div class="product-media">
                            <div class="product-thumbnail">
                                <a href="product-fullwidth.html" title="">
                                    <img src="./img/samples/products/grid/1.jpg" alt="" class="current">
                                    <!-- <img src="./img/samples/products/index/clothing/2.jpg" alt=""> -->
                                </a>
                            </div><!-- /.product-thumbnail -->


                            <div class="product-hover">
                                <div class="product-actions">
                                    <a href="#" class="awe-button product-add-cart" data-toggle="tooltip"
                                       title="Add to cart">
                                        <i class="icon icon-shopping-bag"></i>
                                    </a>

                                    <a href="#" class="awe-button product-quick-wishlist" data-toggle="tooltip"
                                       title="Add to wishlist">
                                        <i class="icon icon-star"></i>
                                    </a>

                                    <a href="#" class="awe-button product-quick-view" data-toggle="tooltip"
                                       title="Quickview">
                                        <i class="icon icon-eye"></i>
                                    </a>
                                </div>
                            </div><!-- /.product-hover -->


                            <span class="product-label hot">
                            <span>hot</span>
                        </span>

                        </div><!-- /.product-media -->

                        <div class="product-body">
                            <h2 class="product-name">
                                <a href="#" title="Gin Lane Greenport Cotton Shirt">Gin Lane Greenport Cotton Shirt</a>
                            </h2><!-- /.product-product -->

                            <div class="product-category">
                                <span>Shocks</span>
                            </div><!-- /.product-category -->

                            <div class="product-price">

                                <span class="amount">$12</span>

                            </div><!-- /.product-price -->
                        </div><!-- /.product-body -->
                    </div><!-- /.product -->


                    <div class="product product-grid">
                        <div class="product-media">
                            <div class="product-thumbnail">
                                <a href="product-fullwidth.html" title="">
                                    <img src="./img/samples/products/grid/1.jpg" alt="" class="current">
                                    <!-- <img src="./img/samples/products/index/clothing/2.jpg" alt=""> -->
                                </a>
                            </div><!-- /.product-thumbnail -->


                            <div class="product-hover">
                                <div class="product-actions">
                                    <a href="#" class="awe-button product-add-cart" data-toggle="tooltip"
                                       title="Add to cart">
                                        <i class="icon icon-shopping-bag"></i>
                                    </a>

                                    <a href="#" class="awe-button product-quick-wishlist" data-toggle="tooltip"
                                       title="Add to wishlist">
                                        <i class="icon icon-star"></i>
                                    </a>

                                    <a href="#" class="awe-button product-quick-view" data-toggle="tooltip"
                                       title="Quickview">
                                        <i class="icon icon-eye"></i>
                                    </a>
                                </div>
                            </div><!-- /.product-hover -->


                            <span class="product-label new">
                            <span>new</span>
                        </span>

                        </div><!-- /.product-media -->

                        <div class="product-body">
                            <h2 class="product-name">
                                <a href="#" title="Gin Lane Greenport Cotton Shirt">Gin Lane Greenport Cotton Shirt</a>
                            </h2><!-- /.product-product -->

                            <div class="product-category">
                                <span>Jackets</span>
                            </div><!-- /.product-category -->

                            <div class="product-price">

                                <span class="amount">$145</span>

                            </div><!-- /.product-price -->
                        </div><!-- /.product-body -->
                    </div><!-- /.product -->


                    <div class="product product-grid">
                        <div class="product-media">
                            <div class="product-thumbnail">
                                <a href="product-fullwidth.html" title="">
                                    <img src="./img/samples/products/grid/1.jpg" alt="" class="current">
                                    <!-- <img src="./img/samples/products/index/clothing/2.jpg" alt=""> -->
                                </a>
                            </div><!-- /.product-thumbnail -->


                            <div class="product-hover">
                                <div class="product-actions">
                                    <a href="#" class="awe-button product-add-cart" data-toggle="tooltip"
                                       title="Add to cart">
                                        <i class="icon icon-shopping-bag"></i>
                                    </a>

                                    <a href="#" class="awe-button product-quick-wishlist" data-toggle="tooltip"
                                       title="Add to wishlist">
                                        <i class="icon icon-star"></i>
                                    </a>

                                    <a href="#" class="awe-button product-quick-view" data-toggle="tooltip"
                                       title="Quickview">
                                        <i class="icon icon-eye"></i>
                                    </a>
                                </div>
                            </div><!-- /.product-hover -->


                        </div><!-- /.product-media -->

                        <div class="product-body">
                            <h2 class="product-name">
                                <a href="#" title="Gin Lane Greenport Cotton Shirt">Gin Lane Greenport Cotton Shirt</a>
                            </h2><!-- /.product-product -->

                            <div class="product-category">
                                <span>Shirts</span>
                            </div><!-- /.product-category -->

                            <div class="product-price">

                                <span class="amount">$50</span>

                            </div><!-- /.product-price -->
                        </div><!-- /.product-body -->
                    </div><!-- /.product -->


                    <div class="product product-grid">
                        <div class="product-media">
                            <div class="product-thumbnail">
                                <a href="product-fullwidth.html" title="">
                                    <img src="./img/samples/products/grid/1.jpg" alt="" class="current">
                                    <!-- <img src="./img/samples/products/index/clothing/2.jpg" alt=""> -->
                                </a>
                            </div><!-- /.product-thumbnail -->


                            <div class="product-hover">
                                <div class="product-actions">
                                    <a href="#" class="awe-button product-add-cart" data-toggle="tooltip"
                                       title="Add to cart">
                                        <i class="icon icon-shopping-bag"></i>
                                    </a>

                                    <a href="#" class="awe-button product-quick-wishlist" data-toggle="tooltip"
                                       title="Add to wishlist">
                                        <i class="icon icon-star"></i>
                                    </a>

                                    <a href="#" class="awe-button product-quick-view" data-toggle="tooltip"
                                       title="Quickview">
                                        <i class="icon icon-eye"></i>
                                    </a>
                                </div>
                            </div><!-- /.product-hover -->


                        </div><!-- /.product-media -->

                        <div class="product-body">
                            <h2 class="product-name">
                                <a href="#" title="Gin Lane Greenport Cotton Shirt">Gin Lane Greenport Cotton Shirt</a>
                            </h2><!-- /.product-product -->

                            <div class="product-category">
                                <span>Shirts</span>
                            </div><!-- /.product-category -->

                            <div class="product-price">

                                <span class="amount">$125</span>

                            </div><!-- /.product-price -->
                        </div><!-- /.product-body -->
                    </div><!-- /.product -->


                    <div class="product product-grid">
                        <div class="product-media">
                            <div class="product-thumbnail">
                                <a href="product-fullwidth.html" title="">
                                    <img src="./img/samples/products/grid/1.jpg" alt="" class="current">
                                    <!-- <img src="./img/samples/products/index/clothing/2.jpg" alt=""> -->
                                </a>
                            </div><!-- /.product-thumbnail -->


                            <div class="product-hover">
                                <div class="product-actions">
                                    <a href="#" class="awe-button product-add-cart" data-toggle="tooltip"
                                       title="Add to cart">
                                        <i class="icon icon-shopping-bag"></i>
                                    </a>

                                    <a href="#" class="awe-button product-quick-wishlist" data-toggle="tooltip"
                                       title="Add to wishlist">
                                        <i class="icon icon-star"></i>
                                    </a>

                                    <a href="#" class="awe-button product-quick-view" data-toggle="tooltip"
                                       title="Quickview">
                                        <i class="icon icon-eye"></i>
                                    </a>
                                </div>
                            </div><!-- /.product-hover -->


                        </div><!-- /.product-media -->

                        <div class="product-body">
                            <h2 class="product-name">
                                <a href="#" title="Gin Lane Greenport Cotton Shirt">Gin Lane Greenport Cotton Shirt</a>
                            </h2><!-- /.product-product -->

                            <div class="product-category">
                                <span>Jackets</span>
                            </div><!-- /.product-category -->

                            <div class="product-price">

                                <span class="amount">$360</span>

                            </div><!-- /.product-price -->
                        </div><!-- /.product-body -->
                    </div><!-- /.product -->


                    <div class="product product-grid">
                        <div class="product-media">
                            <div class="product-thumbnail">
                                <a href="product-fullwidth.html" title="">
                                    <img src="./img/samples/products/grid/1.jpg" alt="" class="current">
                                    <!-- <img src="./img/samples/products/index/clothing/2.jpg" alt=""> -->
                                </a>
                            </div><!-- /.product-thumbnail -->


                            <div class="product-hover">
                                <div class="product-actions">
                                    <a href="#" class="awe-button product-add-cart" data-toggle="tooltip"
                                       title="Add to cart">
                                        <i class="icon icon-shopping-bag"></i>
                                    </a>

                                    <a href="#" class="awe-button product-quick-wishlist" data-toggle="tooltip"
                                       title="Add to wishlist">
                                        <i class="icon icon-star"></i>
                                    </a>

                                    <a href="#" class="awe-button product-quick-view" data-toggle="tooltip"
                                       title="Quickview">
                                        <i class="icon icon-eye"></i>
                                    </a>
                                </div>
                            </div><!-- /.product-hover -->


                        </div><!-- /.product-media -->

                        <div class="product-body">
                            <h2 class="product-name">
                                <a href="#" title="Gin Lane Greenport Cotton Shirt">Gin Lane Greenport Cotton Shirt</a>
                            </h2><!-- /.product-product -->

                            <div class="product-category">
                                <span>Shirts</span>
                            </div><!-- /.product-category -->

                            <div class="product-price">

                                <span class="amount">$125</span>

                            </div><!-- /.product-price -->
                        </div><!-- /.product-body -->
                    </div><!-- /.product -->


                </div>
            </div><!-- /.relared-products -->
        </div>

        <script>
            $(function () {
                aweProductRender(true);
            });
        </script>

    </div>
@endsection