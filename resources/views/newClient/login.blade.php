@extends("newClient.layout")
@section("title") ثبت نام / ورود به سایت @endsection
@section("content")
    <div id="main">
        <div class="main-header background background-image-heading-product">
            <div class="container">
                <h1>ورود / ثبت نام </h1>
            </div>
        </div>
        <div id="breadcrumb">
            <div class="container">
                <ol class="breadcrumb">
                    <li><a href="/">خانه</a></li>
                    <li class="active"><span>ورود / ثبت نام </span></li>
                </ol>

            </div>
        </div>
        <div class="body-content">
            <div class="container">

                <div class="sign-in-page">
                    <div class="row">
                        <!-- Sign-in -->
                        @include("errors.listClient")
                        <div class="col-md-6 col-sm-6 sign-in">
                            <h4 class="">ورود به حساب کاربری</h4>
                            <hr>
                            {!! Form::open(["url"=>route('login'),"method"=>"post","id"=>"login"]) !!}
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label class="info-title" for="exampleInputEmail1">موبایل/آدرس
                                            ایمیل<span>*</span></label>
                                    </div>
                                    <div class="col-md-8">
                                        {!! Form::text("email",null,["class"=>"form-control unicase-form-control text-input","autocomplete"=>"off","placeholder"=>"شماره موبایل و یا پست الکترونیک","id"=>"email"]) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label class="info-title" for="exampleInputPassword1">رمز عبور
                                            <span>*</span></label>
                                    </div>
                                    <div class="col-md-8">
                                        {!! Form::input("password","password",null,["class"=>"form-control unicase-form-control text-input","autocomplete"=>"off","placeholder"=>"گذرواژه"]) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="radio outer-xs">
                                <a href="#" class="btn-upper btn btn-primary ">رمز عبور خود را فراموش کرده اید؟</a>
                                {!! Form::submit("ورود",["class"=>"btn-upper btn btn-primary checkout-page-button"]) !!}
                            </div>
                            {!! Form::close() !!}
                        </div>
                        <div class="col-md-6 col-sm-6 create-new-account">
                            <h4 class="checkout-subtitle">ثبت نام حساب کاربری جدید</h4>
                            <hr>
                            {!! Form::open(["url"=>route('register'),"method"=>"post","id"=>"register"]) !!}
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label class="info-title" for="exampleInputEmail2">آدرس ایمیل
                                            <span>*</span></label>
                                    </div>
                                    <div class="col-md-9">
                                        {!! Form::text("email",null,["class"=>"form-control unicase-form-control text-input","autocomplete"=>"off","placeholder"=>"پست الکترونیک","id"=>"email"]) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label class="info-title" for="exampleInputEmail2">نام <span>*</span></label>
                                    </div>
                                    <div class="col-md-9">
                                        {!! Form::text("first_name",null,["class"=>"form-control unicase-form-control text-input","autocomplete"=>"off","placeholder"=>"نام","id"=>"first_name"]) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label class="info-title" for="exampleInputEmail2">نام خانوادگی
                                            <span>*</span></label>
                                    </div>
                                    <div class="col-md-9">
                                        {!! Form::text("last_name",null,["class"=>"form-control unicase-form-control text-input","autocomplete"=>"off","placeholder"=>"نام خانوادگی","id"=>"last_name"]) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label class="info-title" for="exampleInputEmail2">کد ملی
                                            <span>*</span></label>
                                    </div>
                                    <div class="col-md-9">
                                        {!! Form::text("code_melli",null,["class"=>"form-control unicase-form-control text-input","autocomplete"=>"off","placeholder"=>"کد ملی","id"=>"last_name"]) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label class="info-title" for="exampleInputEmail2">شماره تماس
                                            <span>*</span></label>
                                    </div>
                                    <div class="col-md-9">
                                        {!! Form::text("phone",null,["class"=>"form-control unicase-form-control text-input","autocomplete"=>"off","placeholder"=>"شماره تماس","id"=>"phone"]) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label class="info-title" for="exampleInputEmail2">شماره موبایل
                                            <span>*</span></label>
                                    </div>
                                    <div class="col-md-9">
                                        {!! Form::text("mobile",null,["class"=>"form-control unicase-form-control text-input","autocomplete"=>"off","placeholder"=>"شماره موبایل","id"=>"mobile"]) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label class="info-title" for="exampleInputEmail2">رمز عبور
                                            <span>*</span></label>
                                    </div>
                                    <div class="col-md-9">
                                        {!! Form::input("password","password",null,["class"=>"form-control unicase-form-control text-input","autocomplete"=>"off","placeholder"=>"گذرواژه"]) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label class="info-title" for="exampleInputEmail2">تایید رمز عبور
                                            <span>*</span></label>
                                    </div>
                                    <div class="col-md-9">
                                        {!! Form::input("password","password_confirmation",null,["class"=>"form-control unicase-form-control text-input","autocomplete"=>"off","placeholder"=>"تایید گذرواژه"]) !!}
                                    </div>
                                </div>
                            </div>
                            {!! Form::submit("ثبت نام",["class"=>"btn-upper btn btn-primary checkout-page-button"]) !!}
                            {!! Form::close() !!}
                        </div>
                    </div><!-- /.row -->
                </div><!-- /.sigin-in-->
            </div><!-- /.container -->
        </div><!-- /.body-content -->
    </div>
@endsection
@section("extra_script")
    {!! JsValidator::formRequest('App\Http\Requests\RegisterRequest', '#register') !!}
    {!! JsValidator::formRequest('App\Http\Requests\LoginRequest', '#login') !!}
@endsection