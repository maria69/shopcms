<!DOCTYPE html>
<html class="no-js" lang="{{ app()->getLocale() }}">
<head>

    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>
        @yield("title")
    </title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700|Roboto:300,400,400i,700,700i"
          rel="stylesheet">
    <link rel="shortcut icon" href="img/favicon.png"/>

    <!-- build:css css/bootstrap.css -->
    <link rel="stylesheet" href="/newAssets/css/bootstrap.css">
    <!-- endbuild -->

    <!-- build:css css/plugins.css -->
    <link rel="stylesheet" href="/newAssets/css/awe-icon.css">
    <link rel="stylesheet" href="/newAssets/css/font-awesome.css">
    <link rel="stylesheet" href="/newAssets/css/magnific-popup.css">
    <link rel="stylesheet" href="/newAssets/css/owl.carousel.css">
    <link rel="stylesheet" href="/newAssets/css/awemenu.css">
    <link rel="stylesheet" href="/newAssets/css/swiper.css">
    <link rel="stylesheet" href="/newAssets/css/easyzoom.css">
    <link rel="stylesheet" href="/newAssets/css/nanoscroller.css">
    <link rel="stylesheet" href="/newAssets/css/jquery.mb.YTPlayer.min.css">
    <link rel="stylesheet" href="/newAssets/css/jquery.flipster.min.css">
    <!-- endbuild -->

    <!-- build:css css/styles.css -->
    <link rel="stylesheet" href="/newAssets/css/awe-background.css">
    <link rel="stylesheet" href="/newAssets/css/main.css">
    <link rel="stylesheet" href="/newAssets/css/docs.css">
    <!-- endbuild -->

    <!-- build:js js/vendor.js -->
    <script src="/newAssets/js/vendor/modernizr-2.8.3.min.js"></script>
    <script src="/newAssets/js/vendor/jquery-1.11.3.min.js"></script>
    <!-- endbuild -->

    <script>window.SHOW_LOADING = false;</script>
    @yield('head')

</head>
<body>
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->


<!-- // LOADING -->
<div class="awe-page-loading">
    <div class="awe-loading-wrapper">
        <div class="awe-loading-icon">
            <img src="img/logo-dark.png" alt="images">
        </div>

        <div class="progress">
            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0"
                 aria-valuemax="100"></div>
        </div>
    </div>
</div>
<!-- // END LOADING -->


<div id="wrapper" class="main-wrapper ">

    <div class="menubar-search-form menubar-search-form-v3" id="menubar-search-form">
        <form action="index.html" method="GET">
            <input type="text" name="s" class="form-control" placeholder="Search and hit enter">
            <div class="menubar-search-buttons">
                <button type="submit" class="icons"><span class="icons-search"></span></button>

            </div>

        </form>
        <button type="button" class="btn btn-sm btn-white" id="close-search-form">
            <span class="icon icon-remove"></span>
        </button>
    </div><!-- /.menubar-search-form -->
    <header id="header" class="awe-menubar-header header-style-1 header-v2">
        <div class="header-top">
            <div class="container">
                <div class="header-top-relative">
                    <nav class="navtop">
                        <div class="awe-logo">
                            <a href="index.html" title=""><img src="/img/logo-home2.png" alt=""></a>
                        </div><!-- /.awe-logo -->


                        <ul class="navbar-icons">

                            <li class="menubar-cart">
                                <a href="#" title="" class="awemenu-icon menu-shopping-cart item-number">
                                    <div class="icon icon-cart icon-cart2"></div>
                                    <span class="number count">
                                            @if( isset($cartTop)) {{$cartTop['count']}} @else
                                            0 @endif</span>
                                </a>

                                <ul class="submenu megamenu">
                                    <li>
                                        <div class="container-fluid">

                                            <ul class="whishlist cart-menu ">


                                                @if(isset($cartTop))
                                                    @foreach($cartTop['products'] as $product)

                                                        <li>
                                                            <div class="whishlist-item">
                                                                <div class="product-image">
                                                                    <a href="{{getUrl($product->product->id)}}" title="">
                                                                        <img src="{{thumb($product->product->media,5)}}"
                                                                             alt="{{$product->name}}">
                                                                    </a>
                                                                </div>

                                                                <div class="product-body">
                                                                    <div class="whishlist-name">
                                                                        <a href="{{getUrl($product->product->id)}}"
                                                                               title="">{{$product->product->name}}</h3></a><h3>

                                                                    </div>

                                                                    <div class="whishlist-price">
                                                                        <span>قیمت: </span>
                                                                        <strong>{{number_format($product->unit_price)}}
                                                                            تومان </strong>
                                                                    </div>

                                                                    <div class="whishlist-quantity">
                                                                        <span>تعداد:</span>
                                                                        <span>{{$product->count}}</span>
                                                                    </div>
                                                                </div>
                                                            </div>

{{--                                                            <a href="#" title="" class="remove">--}}
{{--                                                                <i class="icon icon-remove"></i>--}}
{{--                                                            </a>--}}
                                                        </li>
                                                    @endforeach
                                                @endif

                                            </ul>

                                            <div class="menu-cart-total">
                                                <span>جمع کل: </span>
                                                <span class="price value" >

                                                        @if(isset($cartTop))    {{number_format($cartTop['totalPrice'])}}
                                                    @else
                                                        0
                                                    @endif
                                                    </span>
                                            </div>

                                            <div class="cart-action">
                                                <a href="{{route('cart')}}" title=""
                                                   class="btn btn-lg btn-dark btn-outline btn-block">نمایش سبد خرید
                                                    <span
                                                            style="color: red">

                                                        </span></a>
                                                <a href="checkout.html" title=""
                                                   class="btn btn-lg btn-primary btn-block">تسویه حساب</a>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                        </ul>

                        <ul class="navbar-icons nav-left">
                            <li class="menubar-account">
                                <a href="#" title="" class="awemenu-icon">
                                    <i class="icons-user-circle"></i>
                                    <span class="awe-hidden-text">Account</span>
                                </a>

                                <ul class="submenu megamenu">
                                    <li>
                                        <div class="container-fluid">
                                            <div class="header-account">
                                                @guest
                                                    <div class="header-account-avatar">
                                                        <a href="{{route('phoneLogin')}}"><i
                                                                    class="icon fa fa-lock"></i>ورود</a>
                                                        <a href="{{route('phoneLogin')}}"><i
                                                                    class="icon fa fa-lock"></i>ثبت نام</a>
                                                    </div>
                                                @else
                                                    <div class="header-account-avatar">
                                                        <a href="#" title="">
                                                            <img src="./img/samples/avatars/customers/1.jpg" alt=""
                                                                 class="img-circle">
                                                        </a>
                                                    </div>
                                                    <div class="header-account">
                                                        <div class="header-account-username">
                                                            <h4>
                                                                <a href="#">{{auth()->user()->first_name.' '.auth()->user()->last_name}}</a>
                                                            </h4>
                                                        </div>

                                                        <ul>
                                                            <li><a href="{{route('userProfile')}}">حساب کاربری</a></li>
                                                            <li><a href="{{route('orders.index')}}">تاریخچه سفارشات</a>
                                                            </li>
                                                            <li><a href="{{ route('logout') }}"
                                                                   onclick="event.preventDefault();document.getElementById('logout-form').submit();">خروج</a>
                                                            </li>
                                                            <form id="logout-form" action="{{ route('logout') }}"
                                                                  method="POST" style="display: none;">
                                                                {{ csrf_field() }}
                                                            </form>
                                                        </ul>
                                                    </div>
                                                @endguest
                                            </div>
                                    </li>
                                </ul>
                            </li>

                            <li class="menubar-wishlist">
                                <a href="{{route('wish_list.index')}}" title="" class="awemenu-icon">
                                    <i class="icons-star"></i>
                                    <span class="awe-hidden-text">Wishlist</span>
                                </a>

                                {{--                                <ul class="submenu megamenu">--}}
                                {{--                                    <li>--}}
                                {{--                                        <div class="container-fluid">--}}
                                {{--                                            <ul class="whishlist">--}}

                                {{--                                                <li>--}}
                                {{--                                                    <div class="whishlist-item">--}}
                                {{--                                                        <div class="product-image">--}}
                                {{--                                                            <a href="#" title="">--}}
                                {{--                                                                <img src="./img/samples/products/cart/1.jpg" alt="">--}}
                                {{--                                                            </a>--}}
                                {{--                                                        </div>--}}

                                {{--                                                        <div class="product-body">--}}
                                {{--                                                            <div class="whishlist-name">--}}
                                {{--                                                                <h3><a href="#" title="">Gin Lane Greenport Cotton--}}
                                {{--                                                                        Shirt</a></h3>--}}
                                {{--                                                            </div>--}}

                                {{--                                                            <div class="whishlist-price">--}}
                                {{--                                                                <span>Price:</span>--}}
                                {{--                                                                <strong>$120</strong>--}}
                                {{--                                                            </div>--}}
                                {{--                                                        </div>--}}
                                {{--                                                    </div>--}}

                                {{--                                                    <a href="#" title="" class="remove">--}}
                                {{--                                                        <i class="icon icon-remove"></i>--}}
                                {{--                                                    </a>--}}
                                {{--                                                </li>--}}

                                {{--                                                <li>--}}
                                {{--                                                    <div class="whishlist-item">--}}
                                {{--                                                        <div class="product-image">--}}
                                {{--                                                            <a href="#" title="">--}}
                                {{--                                                                <img src="./img/samples/products/cart/1.jpg" alt="">--}}
                                {{--                                                            </a>--}}
                                {{--                                                        </div>--}}

                                {{--                                                        <div class="product-body">--}}
                                {{--                                                            <div class="whishlist-name">--}}
                                {{--                                                                <h3><a href="#" title="">Gin Lane Greenport Cotton--}}
                                {{--                                                                        Shirt</a></h3>--}}
                                {{--                                                            </div>--}}

                                {{--                                                            <div class="whishlist-price">--}}
                                {{--                                                                <span>Price:</span>--}}
                                {{--                                                                <strong>$120</strong>--}}
                                {{--                                                            </div>--}}
                                {{--                                                        </div>--}}
                                {{--                                                    </div>--}}

                                {{--                                                    <a href="#" title="" class="remove">--}}
                                {{--                                                        <i class="icon icon-remove"></i>--}}
                                {{--                                                    </a>--}}
                                {{--                                                </li>--}}

                                {{--                                                <li>--}}
                                {{--                                                    <div class="whishlist-item">--}}
                                {{--                                                        <div class="product-image">--}}
                                {{--                                                            <a href="#" title="">--}}
                                {{--                                                                <img src="./img/samples/products/cart/1.jpg" alt="">--}}
                                {{--                                                            </a>--}}
                                {{--                                                        </div>--}}

                                {{--                                                        <div class="product-body">--}}
                                {{--                                                            <div class="whishlist-name">--}}
                                {{--                                                                <h3><a href="#" title="">Gin Lane Greenport Cotton--}}
                                {{--                                                                        Shirt</a></h3>--}}
                                {{--                                                            </div>--}}

                                {{--                                                            <div class="whishlist-price">--}}
                                {{--                                                                <span>Price:</span>--}}
                                {{--                                                                <strong>$120</strong>--}}
                                {{--                                                            </div>--}}
                                {{--                                                        </div>--}}
                                {{--                                                    </div>--}}

                                {{--                                                    <a href="#" title="" class="remove">--}}
                                {{--                                                        <i class="icon icon-remove"></i>--}}
                                {{--                                                    </a>--}}
                                {{--                                                </li>--}}

                                {{--                                            </ul>--}}

                                {{--                                            <hr>--}}

                                {{--                                            <div class="whishlist-action">--}}
                                {{--                                                <a href="#" title="" class="btn btn-dark btn-lg btn-outline btn-block">View--}}
                                {{--                                                    Wishlist</a>--}}
                                {{--                                            </div>--}}
                                {{--                                        </div>--}}
                                {{--                                    </li>--}}
                                {{--                                </ul>--}}
                            </li>
                        </ul>

                        <ul class="navbar-search">
                            <li>
                                <a href="#" title="" class="awemenu-icon awe-menubar-search" id="open-search-form">
                                    <span class="sr-only">Search</span>
                                    <span class="icons-search"></span>
                                </a>
                            </li>
                        </ul>

                    </nav>
                </div>
            </div>
        </div>

        <nav class="awemenu-nav" data-responsive-width="1200">
            <div class="container">
                <div class="awemenu-container center">
                    <div class="awe-logo logo-mobile">
                        <a href="index.html" title=""><img src="./img/logo.png" alt=""></a>
                    </div><!-- /.awe-logo -->


                    <ul class="awemenu awemenu-left">
                        @foreach($categoriesInNav as $item)
                            @if($item->childrens->count() > 0)

                                <li class="awemenu-item">
                                    <a href="/category/{{$item->slug}}" title="">
                                        <span>{{$item->name}}</span>
                                    </a>
                                    <ul class="awemenu-submenu awemenu-megamenu" data-width="650px" data-animation="fadeup">
                                        <li class="awemenu-megamenu-item">
                                            <div class="container-fluid">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <ul class="list-unstyled">
                                                            @foreach($item->childrens as $child)
                                                                <li class="awemenu-item">
                                                                    <a href="/category/{{$item->slug}}" title=""><span>{{$item->name}}</span></a>
                                                                    @if($child->childrens->count() > 0)
                                                                        <ul class="subItem">
                                                                            @foreach($child->childrens  as $childitem)
                                                                                <li class="liSubItem">
                                                                                    <a href='/category/{{$childitem->id}}'><span>{{$childitem->name}}</span></a>
                                                                                </li>
                                                                            @endforeach
                                                                        </ul>
                                                                    @endif
                                                                </li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </li>
                            @else
                                <li class="awemenu-item"><a href='/category/{{$item->slug}}'><span>{{$item->name}}</span></a>
                                </li>

                            @endif
                        @endforeach
                        <li class="awemenu-item">
                            <a href="#" title="News">News</a>
                        </li>
                        <li class="awemenu-item">
                            <a href="/تماس با ما" title="Contact US">تماس با ما</a>
                        </li>
                    </ul><!-- /.awemenu -->
                </div>
            </div><!-- /.container -->
        </nav><!-- /.awe-menubar -->
    </header><!-- /.awe-menubar-header -->
    @yield("content")

    <footer class="footer">
        <div class="footer-wrapper">
            <div class="footer-widgets">


                <div class="container">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="row">
                                <div class="col-md-12 col-sm-6">

                                    <div class="widget">
                                        <a class="logo-footer" href="#" title="logo-footer"><img
                                                    src="img/logo-footer.png"
                                                    alt="logo-footer"></a>

                                        <div class="widget-content">
                                            <p>Continually synthesize global metrics do proin utilize whereas
                                                leading-edge
                                                platforms...</p>

                                        </div>
                                    </div>
                                    <div class="widget">
                                        <ul class="info">
                                            <li class="address">121 King Street, Melbourne 3000 Australia</li>
                                            <li class="phone">+61 3 8376 6284</li>
                                            <li class="email">hello@uruana.com</li>
                                        </ul>
                                    </div>
                                    <div class="widget">
                                        <ul class="list-socials size-20">
                                            <li><a title="" href="#"><i class="icon fa fa-twitter"></i></a></li>
                                            <li><a title="" href="#"><i class="icon fa fa-facebook"></i></a></li>
                                            <li><a title="" href="#"><i class="icon fa fa-dribbble"></i></a></li>
                                            <li><a title="" href="#"><i class="icon fa fa-instagram"></i></a></li>
                                            <li><a title="" href="#"><i class="icon fa fa-behance"></i></a></li>
                                        </ul>
                                    </div>

                                </div>

                            </div>
                        </div>

                        <div class="col-md-2 col-sm-6">

                            <div class="widget">
                                <h3 class="widget-title">Shopping</h3>
                                <ul>
                                    <li><a href="#" title="">Your Cart</a></li>
                                    <li><a href="#" title="">Your Orders</a></li>
                                    <li><a href="#" title="">Compared Items</a></li>
                                    <li><a href="#" title="">Wishlist Items</a></li>
                                    <li><a href="#" title="">Shipping Detail</a></li>
                                </ul>
                            </div><!-- /.widget -->

                        </div>

                        <div class="col-md-2 col-sm-6">

                            <div class="widget">
                                <h3 class="widget-title">MORE LINK</h3>

                                <ul>
                                    <li><a href="#" title="">Blog</a></li>
                                    <li><a href="#" title="">Gift Center</a></li>
                                    <li><a href="#" title="">Buying Guides</a></li>
                                    <li><a href="#" title="">New Arrivals</a></li>
                                    <li><a href="#" title="">Clearance</a></li>
                                </ul>
                            </div><!-- /.widget -->

                        </div>

                        <div class="col-md-4">

                            <div class="widget">
                                <h3 class="widget-title">Form the blog</h3>

                                <div class="widget-content">
                                    <ul class="form-blog">
                                        <li>
                                            <p class="date"><span>26</span> August</p>
                                            <p class="title">Collaboratively utilize granular bandwidth before
                                                leading</p>
                                            <p class="comments">03 comments</p>
                                        </li>
                                        <li>
                                            <p class="date"><span>26</span> August</p>
                                            <p class="title">Collaboratively utilize granular bandwidth before
                                                leading</p>
                                            <p class="comments">03 comments</p>
                                        </li>
                                    </ul>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>


            </div><!-- /.footer-widgets -->
            <div class="container">
                <div class="footer-copyright">
                    <div class="copyright">
                        <p>Copyright &copy; 2015 uruana - All Rights Reserved.</p>
                    </div>

                    <div class="footer-nav">
                        <nav>
                            <ul>
                                <li><a href="#" title="payment"><img src="img/footer-pay.png" alt="payment"></a></li>
                            </ul>
                        </nav>

                        <nav>
                            <ul>
                                <li class="dropdown dropup">
                                    <div class="language-select">
                                        <span class="select-title">Language:</span>

                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                            <span class="color">ENG</span>
                                            <span class="icon icon-arrow-down"></span>
                                        </a>

                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="#">English</a></li>
                                            <li><a href="#">Vietnamese</a></li>
                                        </ul>
                                    </div>
                                </li>

                                <li class="dropdown dropup">
                                    <div class="price-select">
                                        <span class="select-title">Price:</span>

                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                            <span class="color">USD</span>
                                            <span class="icon icon-arrow-down"></span>
                                        </a>

                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="#">USD</a></li>
                                            <li><a href="#">VND</a></li>
                                        </ul>
                                    </div>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div><!-- /.footer-copyright -->
            </div><!-- End container -->
        </div><!-- /.footer-wrapper -->
    </footer><!-- /footer -->

</div><!-- /#wrapper -->


<script type="text/javascript">

    var routes = {
        wishlist: '{{route("addWishList")}}',
        search: '{{route("search")}}',
        addAddress: '{{route("addWishList")}}',
        addToCart: '{{route("cart.addToCart")}}',
        deleteFromCart: '{{route("cart.deleteFromCart")}}',
    };

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });


</script>

<script src="/assets/js/ajax.js"></script>

<script src="http://maps.google.com/maps/api/js?sensor=true"></script>

<script src="/newAssets/js/vendor/jquery-ui.min.js"></script>
<script>$.widget.bridge('uitooltip', $.ui.tooltip);</script>

<!-- build:js js/plugins.js -->
<script src="/newAssets/js/plugins/bootstrap.min.js"></script>

<script src="/newAssets/js/plugins/awemenu.min.js"></script>
<script src="/newAssets/js/plugins/headroom.min.js"></script>

<script src="/newAssets/js/plugins/hideshowpassword.min.js"></script>
<script src="/newAssets/js/plugins/jquery.parallax-1.1.3.min.js"></script>
<script src="/newAssets/js/plugins/jquery.magnific-popup.min.js"></script>
<script src="/newAssets/js/plugins/jquery.nanoscroller.min.js"></script>

<script src="/newAssets/js/plugins/swiper.min.js"></script>
<script src="/newAssets/js/plugins/owl.carousel.min.js"></script>
<script src="/newAssets/js/plugins/jquery.countdown.min.js"></script>
<script src="/newAssets/js/plugins/easyzoom.js"></script>

<script src="/newAssets/js/plugins/masonry.pkgd.min.js"></script>
<script src="/newAssets/js/plugins/isotope.pkgd.min.js"></script>
<script src="/newAssets/js/plugins/imagesloaded.pkgd.min.js"></script>
<script src="/newAssets/js/plugins/jquery.mb.YTPlayer.min.js"></script>
<script src="/newAssets/js/plugins/jquery.flipster.min.js"></script>

<script src="/newAssets/js/plugins/gmaps.min.js"></script>
<!-- endbuild -->

<!-- build:js js/main.js -->
<script src="/newAssets/js/awe/awe-carousel-branch.js"></script>
<script src="/newAssets/js/awe/awe-carousel-blog.js"></script>
<script src="/newAssets/js/awe/awe-carousel-products.js"></script>
<script src="/newAssets/js/awe/awe-carousel-testimonial.js"></script>
<script src="/newAssets/js/awe/awe-carousel-featured.js"></script>
<script src="/newAssets/js/awe/awe-carousel-model.js"></script>
<script src="/newAssets/js/awe/awe-carousel-branch-2.js"></script>
<script src="/newAssets/js/awe/awe-carousel-flipster.js"></script>

<script src="/newAssets/js/awe-uruana.js"></script>
<script src="/newAssets/js/engo-plugins.js"></script>
<script src="/newAssets/js/main.js"></script>
<!-- endbuild -->

<!-- build:js js/docs.js -->
<script src="/newAssets/js/plugins/list.min.js"></script>
<script src="/newAssets/js/docs.js"></script>
<!-- endbuild -->

</body>
</html>
