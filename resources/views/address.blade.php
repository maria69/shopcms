<style>
    .btn-mysize {
        font-size: 1em !important;
    }
</style>
<div class="table-responsive">
    <table class="table table-striped">
        <thead>
        <tr>
            <th class="cart-description item">انتخاب</th>
            <th class="cart-description item">شهر</th>
            <th class="cart-product-name item">کشور</th>
            <th class="cart-edit item">آدرس</th>
            <th class="cart-qty item">کد پستی</th>
            <th class="cart-sub-total item">شماره تماس</th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <td colspan="7">
            </td>
        </tr>
        </tfoot>
        <tbody id="addressResult">
        @foreach($userAddresses as $userAddress)
            <tr>

                <td><input type="radio" name="address" value="{{$userAddress->id}}"></td>

                <td class="cart-description item">
                    {{$userAddress->city->name}}
                </td>
                <td class="cart-product-name item">
                    {{$userAddress->country->name}}
                </td>
                <td class="cart-edit item">
                    {{$userAddress->address}}
                </td>
                <td class="cart-qty item">
                    {{$userAddress->post_code}}
                </td>
                <td class="cart-sub-total item">
                    {{$userAddress->phone_number}}
                </td>
                <td>



                        {{--{!! Form::open(['url'=>route('addresses.destroy', $userAddress->id),'method'=>'delete','class'=>'delete','style'=>'display:flex']) !!}--}}

                                    {{--<button type="submit" class="btn btn-danger"> <i class="fa fa-trash"></i> </button>--}}
                                    {{--<a href="{{route('addresses.edit', $userAddress->id)}}" class="btn btn-success"> <i class="fa fa-pencil"></i></a>--}}


                        {{--{!! Form::close() !!}--}}




                </td>
            </tr>
        @endforeach
        </tbody><!-- /tbody -->
    </table><!-- /table -->
    <button type="button" class="btn btn-info pull-right" data-toggle="modal" data-target="#myModal">افزودن آدرس</button>
    <a href="#" class="btn btn-upper btn-primary pull-left outer-left-xs next-step pull-left" data-step="1">مرحله قبل</a>
    <a href="#" class="btn btn-upper btn-primary pull-left outer-left-xs next-step pull-left" data-step="3">مرحله بعد</a>
</div>

<!-- add address modal-->
