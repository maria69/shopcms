@if($errors->any())
    @foreach($errors->all() as $error)
        <div class="alert alert-danger">
            <strong>خطا : </strong> {{ $error }}
        </div>
    @endforeach
@endif

@if(Session::has('ok'))
    <div class="alert alert-success">
        <strong></strong> {{ Session::get('ok') }}
    </div>
@endif

@if(Session::has('error'))
    <div class="alert alert-danger">
        <strong></strong> {{ Session::get('error') }}
    </div>
@endif