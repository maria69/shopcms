@extends("errors.layout")
@section("title") 403 @endsection
@section("subTitle") شما به این صفحه دسترسی ندارید @endsection
@section("description") امکان نمایش محتوای صفحه برای حساب کاربری شما وجود ندارد @endsection
@section("subDescription")شما میتوانید با کلیک بر روی دکمه های زیر به وب سایت بازگردید @endsection
@section("button") صفحه اصلی فروشگاه دی تل @endsection
@section("pageTitle") صفحه مورد نظر پیدا نشد | خطای 404 @endsection