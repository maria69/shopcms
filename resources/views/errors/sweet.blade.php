@if(Session::has('ok'))
    <script>
        swal(
            '',
            '{{Session::get("ok")}}',
            'success'
        )

    </script>
@endif

@if(Session::has('error'))
    <script>
        swal(
            'خطا',
            '{{Session::get("error")}}',
            'error'
        )
    </script>
@endif