{{--@extends("errors.layout")--}}
{{--@section("title") 404 @endsection--}}
{{--@section("subTitle") صفحه مورد نظر پیدا نشد @endsection--}}
{{--@section("description") این صفحه ممکن است حذف شده باشد یا اصلا وجود نداشته باشد @endsection--}}
{{--@section("subDescription") شما میتوانید با کلیک بر روی دکمه های زیر به وب سایت بازگردید @endsection--}}
{{--@section("button") صفحه اصلی فروشگاه دی تل @endsection--}}
{{--@section("pageTitle") صفحه مورد نظر پیدا نشد | خطای 404 @endsection--}}
@extends("client.layout")
{{--@section("title") فروشگاه اینترنتی دی تل @endsection--}}
@section('head')
    <link rel="stylesheet" href="/css/404/style.css">
@endsection
@section("content")




    <div class="circles">
        <p>404<br>
            <small>صفحه مورد نظر شما یافت نشد</small>
        </p>
        <span class="circle big"></span>
        <span class="circle med"></span>
        <span class="circle small"></span>
    </div>





@endsection


