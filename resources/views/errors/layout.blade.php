<!DOCTYPE html>
<html lang="fa">
<head>
    <meta charset="utf-8" />
    <title> @yield("pageTitle") </title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <link href="/panelassets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/panelassets/global/css/components-md.min.css" rel="stylesheet" id="style_components" type="text/css" />
    <link href="/panelassets/pages/css/error.min.css" rel="stylesheet" type="text/css" />
    <link href="/css/rtl.css" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="favicon.ico.jpg" />
    <link rel="stylesheet" href="/assets/css/fonts.css">
    <style> body{font-family: iranyekan;}  body, h1, h2, h3, h4, h5, h6 , p {  font-family: iranyekan;  } .page-404-3 .error-404 { text-align: center; padding-right: 40px; }.btn-outline {font-size: 1.2em !important;} .page-404-3 h1 { line-height:30px !important; font-size:2.7em !important; }</style></head>

<!-- END HEAD -->

<body class=" page-404-3">
<div class="page-inner">
    <img src="/panelassets/pages/media/pages/earth.jpg" class="img-responsive" alt="earth"> </div>
<div class="container error-404">
    <h1> @yield("title") </h1>
    <h2> @yield("subTitle") </h2>
    <img src="http://192.168.1.5:8081/img/logo.png" class="img-responsive center-block" width="200px" alt="">
    <p> @yield("description") </p>
    <p> @yield("subDescription") </p>
    <p>
        <a href="/" class="btn red btn-outline"> @yield("button") </a>
    </p>
</div>
</body>
</html>