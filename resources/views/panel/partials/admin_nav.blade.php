<?php
$current_module = request()->segment(count(request()->segments()));
$current_module_before = request()->segment(count(request()->segments())-1);
$current_module_before_2 = request()->segment(count(request()->segments())-2);


//dd($current_module_before);
?>
@php
    //$admin = \App\Model\Admin::with('roles')->find(Auth::guard("admins")->user()->id);
    //$roles=$admin->roles()->pluck('id','prefix')->toArray();
    $uri=trim(Request::route()->uri());
    $dashboard = 'dashboard909';
@endphp


{{--<ul class="sidebar-menu" data-widget="tree">--}}
    {{--<li class="header">منوی کاربری</li>--}}
    {{--<!-- Optionally, you can add icons to the links -->--}}
    {{--<li class="@if($current_module == "admin") active @endif"><a href="{{url('/admin')}}"><i class="fa fa-dashboard"></i> <span>داشبورد</span></a></li>--}}
    {{--<li class="treeview @if($current_module == "user" || $current_module == "banusers") active @endif">--}}
        {{--<a href="#"><i class="fa fa-users"></i> <span>مدیریت کاربران</span>--}}

            {{--<span class="pull-left-container">--}}
                {{--<i class="fa fa-angle-right pull-left"></i>--}}
              {{--</span>--}}
        {{--</a>--}}
        {{--<ul class="treeview-menu">--}}
            {{--<li><a href="{{url('admin/user')}}"><i class="fa fa-circle-o"></i>لیست کاربران</a></li>--}}
            {{--<li><a href="{{url('admin/banusers')}}"><i class="fa fa-circle-o"></i>کاربران مسدود شده</a></li>--}}
        {{--</ul>--}}
    {{--</li>--}}
    {{--<li class="treeview @if($current_module == "product" || $current_module == "propending" || $current_module == "prodelete" || $current_module == "proreports") active @endif">--}}
        {{--<a href="#"><i class="fa fa-newspaper-o"></i> <span>مدیریت آگهی ها</span>--}}
          {{----}}
            {{--<span class="pull-left-container">--}}
                {{--<i class="fa fa-angle-right pull-left"></i>--}}
              {{--</span>--}}
        {{--</a>--}}
        {{--<ul class="treeview-menu">--}}
            {{--<li><a href="{{url('admin/product')}}"><i class="fa fa-circle-o"></i>لیست آگهی ها</a></li>--}}
            {{--<li><a href="{{url('admin/propending')}}"><i class="fa fa-circle-o"></i>آگهی های درحال انتظار</a></li>--}}
            {{--<li><a href="{{url('admin/prodelete')}}"><i class="fa fa-circle-o"></i>آگهی های حذف شده</a></li>--}}
            {{--<li><a href="{{url('admin/proreports')}}"><i class="fa fa-circle-o"></i>گزارشات مردمی</a></li>--}}
        {{--</ul>--}}
    {{--</li>--}}
    {{--<li class="@if($current_module == "category") active @endif"><a href="{{url('admin/category')}}"><i class="fa fa-th-list"></i> <span>دسته بندی ها</span></a></li>--}}
    {{--<li class="@if($current_module == "message") active @endif"><a href="{{url('admin/message')}}"><i class="fa fa-envelope"></i> <span>پیام های کاربران</span>--}}
            {{----}}
        {{--</a></li>--}}

    {{--<li class="treeview @if($current_module == "sms" || $current_module == "groupsms") active @endif">--}}
        {{--<a href="#"><i class="fa fa-paper-plane"></i> <span>پنل پیامکی</span>--}}
            {{--<span class="pull-left-container">--}}
                {{--<i class="fa fa-angle-right pull-left"></i>--}}
              {{--</span>--}}
        {{--</a>--}}
        {{--<ul class="treeview-menu">--}}
            {{--<li><a href="{{url('admin/sms')}}"><i class="fa fa-circle-o"></i>پیامک های تکی</a></li>--}}
            {{--<li><a href="{{url('admin/groupsms')}}"><i class="fa fa-circle-o"></i>پیامک های گروهی</a></li>--}}
        {{--</ul>--}}
    {{--</li>--}}

    {{--<li class="treeview @if($current_module == "setting" || $current_module == "setting-pages") active @endif">--}}
        {{--<a href="#"><i class="fa fa-cog"></i> <span>تنظیمات</span>--}}
            {{--<span class="pull-left-container">--}}
                  {{--<i class="fa fa-angle-right pull-left"></i>--}}
                {{--</span>--}}
        {{--</a>--}}
        {{--<ul class="treeview-menu">--}}
            {{--<li><a href="{{url('admin/setting-pages')}}"><i class="fa fa-circle-o"></i>تنظیمات صفحات</a></li>--}}
            {{--<li><a href="{{url('admin/setting')}}"><i class="fa fa-circle-o"></i>تنظیمات کل سایت</a></li>--}}
        {{--</ul>--}}
    {{--</li>--}}
    {{--<li class="@if($current_module == "log") active @endif"><a href="{{url('admin/log')}}"><i class="fa fa-bar-chart"></i> <span>گزارشات</span></a></li>--}}
{{--</ul>--}}

<ul class="page-sidebar-menu  page-header-fixed page-sidebar-menu-light " data-keep-expanded="false"
    data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
    <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
    <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
    <li class="sidebar-toggler-wrapper hide">
        <div class="sidebar-toggler">
            <span></span>
        </div>
    </li>


    <li class="nav-item start @if($uri == $dashboard){{ "active" }}@endif ">
        <a href="/{{$dashboard}}" class="nav-link nav-toggle">
            <i class="fa fa-home"></i>
            <span class="title">پیشخوان مدیریت</span>
            @if($uri == $dashboard)<span class="selected"></span>@endif
        </a>
    </li>




    +

    <li class="nav-item start @if($uri == $dashboard.'/media'){{ "active" }}@endif ">
        <a href="{{route('media.index')}}" class="nav-link nav-toggle">
            <i class="fa fa-picture-o"></i>
            <span class="title">گالری</span>
            @if($uri == $dashboard.'/media')<span class="selected"></span>@endif

        </a>
    </li>


    <li class="nav-item start
@if($current_module == "productOptionCategories" || $current_module == "productOptions" || $current_module == "products" || $current_module == "product_types"
    || $current_module == "features" || $current_module == "feature_categories" || $current_module_before == "productOptionCategories" || $current_module_before == "productOptions" ||  $current_module_before == "products"
    ||  $current_module_before == "product_types" || $current_module_before =="feature_categories" ||  $current_module_before_2 == "productOptionCategories" || $current_module_before_2 == "productOptions" ||  $current_module_before_2 == "products"
    ||  $current_module_before_2 == "product_types" || $current_module_before_2 =="feature_categories"
  ) active  @endif
            " >



        <a href="javascript:;" class="nav-link nav-toggle">
            <span class="arrow pull-left"></span>
            <i class="fa fa-shopping-cart"></i>
            <span class="title">فروشگاه ها</span>
            @if(stripos($uri, $dashboard.'/stores') !== false || stripos($uri, $dashboard.'/stores') !== false)
                <span class="selected"></span>
            @endif

        </a>
        <ul class="sub-menu">
            <li class="nav-item start @if($current_module == "productOptionCategories" || $current_module_before == "productOptionCategories"  || $current_module_before_2 == "productOptionCategories") subactive @endif ">
                <a href="{{route('productOptionCategories.index')}}" class="nav-link ">
                    <i class="fa fa-file-text-o"></i>
                    <span class="title">لیست فروشگاه ها</span>
                </a>
            </li>
            <li class="nav-item start @if( $current_module == "productOptions" || $current_module_before == "productOptions" || $current_module_before_2 == "productOptions") subactive @endif "  >
                <a href="{{route('productOptions.index')}}" class="nav-link ">
                    <i class="fa fa-file-text-o"></i>
                    <span class="title">آپشن محصولات</span>
                </a>
            </li>

            {{--<li class="nav-item start">--}}
            {{--<a href="{{route('products.create')}}" class="nav-link ">--}}
            {{--<i class="fa fa-files-o"></i>--}}
            {{--<span class="title">افزودن محصول</span>--}}
            {{--</a>--}}
            {{--</li>--}}
            <li class="nav-item start @if( $current_module == "product_types" || $current_module_before == "product_types" || $current_module_before_2 == "product_types") subactive @endif ">
                <a href="{{route('product_types.index')}}" class="nav-link ">
                    <i class="fa fa-file-text-o"></i>
                    <span class="title">انواع محصول</span>
                </a>
            </li>
            {{--<li class="nav-item start">--}}
            {{--<a href="{{route('product_types.create')}}" class="nav-link ">--}}
            {{--<i class="fa fa-files-o"></i>--}}
            {{--<span class="title">افزودن نوع محصول</span>--}}
            {{--</a>--}}
            {{--</li>--}}

            <li class="nav-item start  @if( $current_module == "feature_categories" || $current_module_before == "feature_categories" || $current_module_before_2 == "feature_categories") subactive @endif  ">
                <a href="{{route('feature_categories.index')}}" class="nav-link ">
                    <i class="fa fa-file-text-o"></i>
                    <span class="title">دسته بندی ویژگی ها</span>
                </a>
            </li>
            <li class="nav-item start  @if( $current_module == "features" || $current_module_before == "features" || $current_module_before_2 == "features") subactive @endif  ">
                <a href="{{route('features.index')}}" class="nav-link ">
                    <i class="fa fa-file-text-o"></i>
                    <span class="title">ویژگی های محصول</span>
                </a>
            </li>

            <li class="nav-item start   @if( $current_module == "products" || $current_module_before == "products" || $current_module_before_2 == "products") subactive @endif  ">
                <a href="{{route('products.index')}}" class="nav-link ">
                    <i class="fa fa-file-text-o"></i>
                    <span class="title">لیست محصولات</span>
                </a>
            </li>
            {{--<li class="nav-item start">--}}
            {{--<a href="{{route('features.create')}}" class="nav-link ">--}}
            {{--<i class="fa fa-files-o"></i>--}}
            {{--<span class="title">افزودن ویژگی محصول</span>--}}
            {{--</a>--}}
            {{--</li>--}}

            <li class="nav-item start ">
                <a href="{{route('comments.index')}}" class="nav-link ">
                    <i class="fa fa-users"></i>
                    <span class="title">دیدگاه مشتریان</span>
                </a>
            </li>
        </ul>
    </li>



    <li class="nav-item start
@if($current_module == "productOptionCategories" || $current_module == "productOptions" || $current_module == "products" || $current_module == "product_types"
    || $current_module == "features" || $current_module == "feature_categories" || $current_module_before == "productOptionCategories" || $current_module_before == "productOptions" ||  $current_module_before == "products"
    ||  $current_module_before == "product_types" || $current_module_before =="feature_categories" ||  $current_module_before_2 == "productOptionCategories" || $current_module_before_2 == "productOptions" ||  $current_module_before_2 == "products"
    ||  $current_module_before_2 == "product_types" || $current_module_before_2 =="feature_categories"
  ) active  @endif
  " >



        <a href="javascript:;" class="nav-link nav-toggle">
            <span class="arrow pull-left"></span>
            <i class="fa fa-shopping-cart"></i>
            <span class="title">محصولات</span>
            @if(stripos($uri, $dashboard.'/products') !== false || stripos($uri, $dashboard.'/product_types') !== false)
                <span class="selected"></span>
            @endif

        </a>
        <ul class="sub-menu">
            <li class="nav-item start @if($current_module == "productOptionCategories" || $current_module_before == "productOptionCategories"  || $current_module_before_2 == "productOptionCategories") subactive @endif ">
                <a href="{{route('productOptionCategories.index')}}" class="nav-link ">
                    <i class="fa fa-file-text-o"></i>
                    <span class="title">دسته بندی آپشن </span>
                </a>
            </li>
            <li class="nav-item start @if( $current_module == "productOptions" || $current_module_before == "productOptions" || $current_module_before_2 == "productOptions") subactive @endif "  >
                <a href="{{route('productOptions.index')}}" class="nav-link ">
                    <i class="fa fa-file-text-o"></i>
                    <span class="title">آپشن محصولات</span>
                </a>
            </li>

            {{--<li class="nav-item start">--}}
                {{--<a href="{{route('products.create')}}" class="nav-link ">--}}
                    {{--<i class="fa fa-files-o"></i>--}}
                    {{--<span class="title">افزودن محصول</span>--}}
                {{--</a>--}}
            {{--</li>--}}
            <li class="nav-item start @if( $current_module == "product_types" || $current_module_before == "product_types" || $current_module_before_2 == "product_types") subactive @endif ">
                <a href="{{route('product_types.index')}}" class="nav-link ">
                    <i class="fa fa-file-text-o"></i>
                    <span class="title">انواع محصول</span>
                </a>
            </li>
            {{--<li class="nav-item start">--}}
                {{--<a href="{{route('product_types.create')}}" class="nav-link ">--}}
                    {{--<i class="fa fa-files-o"></i>--}}
                    {{--<span class="title">افزودن نوع محصول</span>--}}
                {{--</a>--}}
            {{--</li>--}}

            <li class="nav-item start  @if( $current_module == "feature_categories" || $current_module_before == "feature_categories" || $current_module_before_2 == "feature_categories") subactive @endif  ">
                <a href="{{route('feature_categories.index')}}" class="nav-link ">
                    <i class="fa fa-file-text-o"></i>
                    <span class="title">دسته بندی ویژگی ها</span>
                </a>
            </li>
            <li class="nav-item start  @if( $current_module == "features" || $current_module_before == "features" || $current_module_before_2 == "features") subactive @endif  ">
                <a href="{{route('features.index')}}" class="nav-link ">
                    <i class="fa fa-file-text-o"></i>
                    <span class="title">ویژگی های محصول</span>
                </a>
            </li>

            <li class="nav-item start   @if( $current_module == "products" || $current_module_before == "products" || $current_module_before_2 == "products") subactive @endif  ">
                <a href="{{route('products.index')}}" class="nav-link ">
                    <i class="fa fa-file-text-o"></i>
                    <span class="title">لیست محصولات</span>
                </a>
            </li>
            {{--<li class="nav-item start">--}}
                {{--<a href="{{route('features.create')}}" class="nav-link ">--}}
                    {{--<i class="fa fa-files-o"></i>--}}
                    {{--<span class="title">افزودن ویژگی محصول</span>--}}
                {{--</a>--}}
            {{--</li>--}}

            <li class="nav-item start ">
                <a href="{{route('comments.index')}}" class="nav-link ">
                    <i class="fa fa-users"></i>
                    <span class="title">دیدگاه مشتریان</span>
                </a>
            </li>
        </ul>
    </li>


    <li class="nav-item start @if($uri == route('shippings.index')){{ "active" }}@endif ">
        <a href="{{route('shippings.index')}}" class="nav-link nav-toggle">
            <i class="fa fa-picture-o"></i>
            <span class="title">شیوه های ارسال</span>
            @if($uri == route('shippings.index'))<span class="selected"></span>@endif

        </a>
    </li>


    <li class="nav-item start @if($uri == route('media.index')){{ "active" }}@endif ">
        <a href="{{route('media.index')}}" class="nav-link nav-toggle">
            <i class="fa fa-picture-o"></i>
            <span class="title">پرونده های چندرسانه ای</span>
            @if($uri == route('media.index'))<span class="selected"></span>@endif

        </a>
    </li>



    <li class="nav-item start @if($uri == $dashboard.'/brands'){{ "active" }}@endif ">
        <a href="javascript:;" class="nav-link nav-toggle">
            <span class="arrow pull-left"></span>
            <i class="fa fa-bandcamp"></i>
            <span class="title">برند</span>
            @if($uri == $dashboard.'/brands')
                <span class="selected"></span>
            @endif

        </a>
        <ul class="sub-menu">
            <li class="nav-item start ">
                <a href="{{route('brands.index')}}" class="nav-link ">
                    <i class="fa fa-file-text-o"></i>
                    <span class="title">لیست برند ها</span>
                </a>
            </li>
            <li class="nav-item start">
                <a href="{{route('brands.create')}}" class="nav-link ">
                    <i class="fa fa-files-o"></i>
                    <span class="title">افزودن برند</span>
                </a>
            </li>
        </ul>
    </li>
    <li class="nav-item start @if($uri == $dashboard.'/companies'){{ "active" }}@endif ">
        <a href="javascript:;" class="nav-link nav-toggle">
            <span class="arrow pull-left"></span>
            <i class="fa fa-bandcamp"></i>
            <span class="title">کمپانی ها</span>
            @if($uri == $dashboard.'/companies')
                <span class="selected"></span>
            @endif

        </a>
        <ul class="sub-menu">
            <li class="nav-item start ">
                <a href="{{route('companies.index')}}" class="nav-link ">
                    <i class="fa fa-file-text-o"></i>
                    <span class="title">لیست کمپانی ها</span>
                </a>
            </li>
            <li class="nav-item start">
                <a href="{{route('companies.create')}}" class="nav-link ">
                    <i class="fa fa-files-o"></i>
                    <span class="title">افزودن کمپانی جدید</span>
                </a>
            </li>
        </ul>
    </li>


    <li class="nav-item start @if($uri == $dashboard.'/brands'){{ "active" }}@endif ">
        <a href="javascript:;" class="nav-link nav-toggle">
            <span class="arrow pull-left"></span>
            <i class="fa fa-align-justify"></i>
            <span class="title">دسته بندی</span>
            @if($uri == $dashboard.'/brands')
                <span class="selected"></span>
            @endif

        </a>
        <ul class="sub-menu">
            <li class="nav-item start ">
                <a href="{{route('categories.index')}}" class="nav-link ">
                    <i class="fa fa-file-text-o"></i>
                    <span class="title">لیست دسته بندی</span>
                </a>
            </li>
            <li class="nav-item start">
                <a href="{{route('categories.create')}}" class="nav-link ">
                    <i class="fa fa-files-o"></i>
                    <span class="title">افزودن دسته بندی</span>
                </a>
            </li>
        </ul>
    </li>


    <li class="nav-item start @if($uri == $dashboard.'/brands'){{ "active" }}@endif ">
        <a href="javascript:;" class="nav-link nav-toggle">
            <span class="arrow pull-left"></span>
            <i class="fa fa-users"></i>
            <span class="title">کاربران</span>
            @if($uri == $dashboard.'/users')
                <span class="selected"></span>
            @endif

        </a>
        <ul class="sub-menu">
            <li class="nav-item start ">
                <a href="{{route('users.index')}}" class="nav-link ">
                    <i class="fa fa-file-text-o"></i>
                    <span class="title">لیست کاربران</span>
                </a>
            </li>
            <li class="nav-item start">
                <a href="{{route('users.create')}}" class="nav-link ">
                    <i class="fa fa-files-o"></i>
                    <span class="title">افزودن کاربر</span>
                </a>
            </li>
        </ul>
    </li>


    <li class="nav-item start @if($uri == $dashboard.'/admins'){{ "active" }}@endif ">
        <a href="javascript:;" class="nav-link nav-toggle">
            <span class="arrow pull-left"></span>
            <i class="fa fa-users"></i>
            <span class="title">کارمندان</span>
            @if($uri == $dashboard.'/admins')
                <span class="selected"></span>
            @endif

        </a>
        <ul class="sub-menu">
            <li class="nav-item start ">
                <a href="{{route('admins.index')}}" class="nav-link ">
                    <i class="fa fa-file-text-o"></i>
                    <span class="title">لیست کارمندان</span>
                </a>
            </li>
            <li class="nav-item start">
                <a href="{{route('admins.create')}}" class="nav-link ">
                    <i class="fa fa-files-o"></i>
                    <span class="title">افزودن کارمند</span>
                </a>
            </li>
        </ul>
    </li>

    <li class="nav-item start @if($uri == route('order.index')){{ "active" }}@endif ">
        <a href="{{route('order.index')}}" class="nav-link nav-toggle">
            <i class="fa fa-picture-o"></i>
            <span class="title">لیست سفارشات</span>
            @if($uri == route('order.index'))<span class="selected"></span>@endif

        </a>
    </li>


    {{--<li class="nav-item start @if($uri == $dashboard.'/permissions' || $uri == $dashboard.'/roles'){{ "active" }}@endif ">--}}
        {{--<a href="javascript:;" class="nav-link nav-toggle">--}}
            {{--<span class="arrow pull-left"></span>--}}
            {{--<i class="fa fa-users"></i>--}}
            {{--<span class="title">نقش ها و سطوح دسترسی</span>--}}
            {{--@if($uri == $dashboard.'/admins')--}}
                {{--<span class="selected"></span>--}}
            {{--@endif--}}

        {{--</a>--}}
        {{--<ul class="sub-menu">--}}
            {{--<li class="nav-item start ">--}}
                {{--<a href="/{{$dashboard}}/permissions" class="nav-link ">--}}
                    {{--<i class="fa fa-file-text-o"></i>--}}
                    {{--<span class="title">سطوح دسترسی</span>--}}
                {{--</a>--}}
            {{--</li>--}}
            {{--<li class="nav-item start">--}}
                {{--<a href="/{{$dashboard}}/roles" class="nav-link ">--}}
                    {{--<i class="fa fa-files-o"></i>--}}
                    {{--<span class="title">نقش های سیستمی</span>--}}
                {{--</a>--}}
            {{--</li>--}}
        {{--</ul>--}}
    {{--</li>--}}


    {{--<li class="nav-item start @if($uri == $dashboard.'/sms' || $uri == $dashboard.'/email'){{ "active" }}@endif ">--}}
        {{--<a href="javascript:;" class="nav-link nav-toggle">--}}
            {{--<span class="arrow pull-left"></span>--}}
            {{--<i class="fa fa-users"></i>--}}
            {{--<span class="title">اطلاع رسانی</span>--}}
            {{--@if($uri == $dashboard.'/sms' || $uri == $dashboard.'/email')--}}
                {{--<span class="selected"></span>--}}
            {{--@endif--}}

        {{--</a>--}}
        {{--<ul class="sub-menu">--}}
            {{--<li class="nav-item start ">--}}
                {{--<a href="/{{$dashboard}}/sms" class="nav-link ">--}}
                    {{--<i class="fa fa-file-text-o"></i>--}}
                    {{--<span class="title">پیامک</span>--}}
                {{--</a>--}}
            {{--</li>--}}
            {{--<li class="nav-item start">--}}
                {{--<a href="/{{$dashboard}}/email" class="nav-link ">--}}
                    {{--<i class="fa fa-files-o"></i>--}}
                    {{--<span class="title">پست الکترونیک</span>--}}
                {{--</a>--}}
            {{--</li>--}}
        {{--</ul>--}}
    {{--</li>--}}


    <li class="nav-item start @if($uri == $dashboard.'/offers'){{ "active" }}@endif ">
        <a href="javascript:;" class="nav-link nav-toggle">
            <span class="arrow pull-left"></span>
            <i class="fa fa-usd"></i>
            <span class="title">تخفیف</span>
            @if($uri == $dashboard.'/offers')
                <span class="selected"></span>
            @endif

        </a>
        <ul class="sub-menu">
            <li class="nav-item start ">
                <a href="/{{$dashboard}}/offers" class="nav-link ">
                    <i class="fa fa-file-text-o"></i>
                    <span class="title">لیست تخفیفات</span>
                </a>
            </li>
            <li class="nav-item start">
                <a href="/{{$dashboard}}/offers/create" class="nav-link ">
                    <i class="fa fa-files-o"></i>
                    <span class="title">افزودن تخفیف</span>
                </a>
            </li>
        </ul>
    </li>


    <li class="nav-item start @if($uri == $dashboard.'/settings'){{ "active" }}@endif ">
        <a href="javascript:;" class="nav-link nav-toggle">
            <span class="arrow pull-left"></span>
            <i class="fa fa-gear"></i>
            <span class="title">تنظیمات</span>
            @if($uri == $dashboard.'/settings')
                <span class="selected"></span>
            @endif

        </a>
        <ul class="sub-menu">
            <li class="nav-item start ">
                <a href="/{{$dashboard}}/settings/options" class="nav-link ">
                    <i class="fa fa-file-text-o"></i>
                    <span class="title">تنظیمات عمومی</span>
                </a>
            </li>
            {{--<li class="nav-item start ">--}}
                {{--<a href="/{{$dashboard}}/settings/navs" class="nav-link ">--}}
                    {{--<i class="fa fa-file-text-o"></i>--}}
                    {{--<span class="title">منو های سایت</span>--}}
                {{--</a>--}}
            {{--</li>--}}
            {{--<li class="nav-item start ">--}}
                {{--<a href="/{{$dashboard}}/settings/gateways" class="nav-link ">--}}
                    {{--<i class="fa fa-file-text-o"></i>--}}
                    {{--<span class="title">شبکه های اجتماعی</span>--}}
                {{--</a>--}}
            {{--</li>--}}

            <li class="nav-item start ">
                <a href="/{{$dashboard}}/settings/gateways" class="nav-link ">
                    <i class="fa fa-file-text-o"></i>
                    <span class="title">درگاه پرداخت</span>
                </a>
            </li>
            <li class="nav-item start ">
                <a href="/{{$dashboard}}/settings/cities" class="nav-link ">
                    <i class="fa fa-file-text-o"></i>
                    <span class="title">شهر</span>
                </a>
            </li>
            <li class="nav-item start">
                <a href="/{{$dashboard}}/settings/states" class="nav-link ">
                    <i class="fa fa-files-o"></i>
                    <span class="title">استان</span>
                </a>
            </li>
        </ul>
    </li>


    {{--<li class="nav-item start @if($uri == $dashboard.'/reports'){{ "active" }}@endif ">--}}
        {{--<a href="javascript:;" class="nav-link nav-toggle">--}}
            {{--<span class="arrow pull-left"></span>--}}
            {{--<i class="fa fa-gear"></i>--}}
            {{--<span class="title">گزارش گیری</span>--}}
            {{--@if($uri == $dashboard.'/reports')--}}
                {{--<span class="selected"></span>--}}
            {{--@endif--}}

        {{--</a>--}}
        {{--<ul class="sub-menu">--}}

            {{--<li class="nav-item start ">--}}
                {{--<a href="/{{$dashboard}}/reports/sales" class="nav-link ">--}}
                    {{--<i class="fa fa-file-text-o"></i>--}}
                    {{--<span class="title">فروش</span>--}}
                {{--</a>--}}
            {{--</li>--}}

            {{--<li class="nav-item start ">--}}
                {{--<a href="/{{$dashboard}}/reports/payments" class="nav-link ">--}}
                    {{--<i class="fa fa-file-text-o"></i>--}}
                    {{--<span class="title">تراکنش اینترنتی</span>--}}
                {{--</a>--}}
            {{--</li>--}}

        {{--</ul>--}}
    {{--</li>--}}


    <li class="nav-item start @if($uri == $dashboard.'/payments'){{ "active" }}@endif ">
        <a href="/{{$dashboard}}/payments" class="nav-link nav-toggle">
            <i class="fa fa-usd"></i>
            <span class="title">تراکنش های اینترنتی</span>
            @if($uri == $dashboard.'/payments')<span class="selected"></span>@endif

        </a>
    </li>


    <li class="nav-item start @if($uri == $dashboard.'/sliders'){{ "active" }}@endif ">
        <a href="/{{$dashboard}}/sliders" class="nav-link nav-toggle">
            <i class="fa fa-picture-o"></i>
            <span class="title">اسلایدر</span>
            @if($uri == $dashboard.'/sliders')<span class="selected"></span>@endif

        </a>
    </li>


    {{--<li class="nav-item start @if($uri == $dashboard.'/banners'){{ "active" }}@endif ">--}}
        {{--<a href="/{{$dashboard}}/banners" class="nav-link nav-toggle">--}}
            {{--<i class="fa fa-picture-o"></i>--}}
            {{--<span class="title">بنر های تبلیغاتی</span>--}}
            {{--@if($uri == $dashboard.'/banners')<span class="selected"></span>@endif--}}

        {{--</a>--}}
    {{--</li>--}}


</ul>
<!-- END SIDEBAR MENU -->