@php $dash.="-"; @endphp

@foreach($categories as $category)

    <option value="{{$category->id}}" {{((!is_null($currentCategories) && in_array($category->id,$currentCategories)) || (!is_null($olds) && in_array($category->id,$olds)))?"selected":""}}>{{$dash." ".$category->name}}</option>

    @if(isset($category->childrens) && $category->childrens->count() > 0)

        @include("panel.partials.getSubCats",['categories'=>$category->childrens,'currentCategories'=>$currentCategories,'olds'=>$olds])

    @endif

@endforeach