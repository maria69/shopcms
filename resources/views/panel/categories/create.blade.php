@extends('panel.layout')

@section('title')
    افزودن دسته بندی محصولات
@endsection
@section('content')


    <div class="row">
        <div class="col-md-12">

            <div class="portlet box blue-hoki">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-building"></i>افزودن دسته بندی محصولات
                    </div>
                    <div class="tools">
                        <!--<a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                        <a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title=""> </a>
                        <a href="javascript:;" class="reload" data-original-title="" title=""> </a>
                        <a href="javascript:;" class="remove" data-original-title="" title=""> </a>-->
                    </div>
                </div>
                <div class="portlet-body">
                    @include("errors.list")
                </div>
                <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    {!! Form::open(["class"=>"form-horizontal","url"=>route('categories.store'),"method"=>"post"]) !!}

                    <div class="form-body">

                        <div class="form-group form-md-line-input">
                            {!! Form::label("name","نام دسته نبدی",["class"=>"col-md-2 control-label"]) !!}
                            <div class="col-md-4">
                                {!! Form::input("text","name",null,["class"=>"form-control","placeholder"=>"نام دسته بندی"]) !!}
                                <div class="form-control-focus"></div>
                            </div>
                        </div>


                        <div class="form-group form-md-line-input">
                            <div class="col-md-8 col-md-offset-2">


                                <div class="row">
                                    <div class="col-md-3">

                                        <button type="button" data-action="thumb" class="galleryPickeraBtn btn btn-info"
                                                style="width:100%">انتخاب فایل چند رسانه ای برای دسته بندی
                                        </button>

                                    </div>

                                    <div class="col-md-3">

                                        {!! Form::text("image_name",null,['class'=>'form-control galleryInput','readonly'=>'readonly']) !!}
                                        <input class="imageHiddenInput" type="hidden" name="media_id" value="">

                                    </div>

                                    <div class="col-md-3">
                                        <figure class="preview" style="max-width:100%">

                                        </figure>
                                    </div>
                                </div>


                            </div>
                        </div>

                        <div class="form-group form-md-line-input">
                            {!! Form::label("department_id","دسته بندی پدر",["class"=>"col-md-2 control-label"]) !!}
                            <div class="col-md-4">
                                {!! Form::select('parent_id',$categories , 0,["class"=>"form-control selectRtl"]) !!}

                            </div>
                        </div>

                        <div class="form-group form-md-line-input">
                            {!! Form::label("status","وضعیت انتشار",["class"=>"col-md-2 control-label"]) !!}
                            <div class="col-md-4">
                                <div class="md-checkbox">
                                    <input id="status" value="1" class="md-check" name="status" type="checkbox">
                                    <label for="status">
                                        <span></span>
                                        <span class="check"></span>
                                        <span class="box"></span> وضعیت انتشار </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group form-md-line-input">
                            {!! Form::label("content","توضیحات",["class"=>"col-md-2 control-label"]) !!}
                            <div class="col-md-8">
                                {!! Form::textarea("description",null,['class'=>'form-group','id'=>'ckeditor']) !!}
                                <div class="form-control-focus"></div>
                            </div>
                        </div>
                        <div class="form-group form-md-line-input">
                            {!! Form::label("content","توضیحات متا",["class"=>"col-md-2 control-label"]) !!}
                            <div class="col-md-8">
                                {!! Form::textarea("meta",null,['class'=>'form-group','id'=>'ckeditor']) !!}
                                <div class="form-control-focus"></div>
                            </div>
                        </div>
                        <div class="form-group form-md-line-input">
                            {!! Form::label("name","کلمات کلیدی",["class"=>"col-md-2 control-label"]) !!}
                            <div class="col-md-4">
                                {!! Form::input("text","keywords",null,["class"=>"form-control","placeholder"=>"کلمات کلیدی"]) !!}
                                <div class="form-control-focus"></div>
                            </div>
                        </div>

                        <hr>

                    </div>
                    <div class="form-actions fluid">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                {!! Form::submit("ذخیره",["class"=>"btn green"]) !!}
                                <a href="/dashboard/admin/articles/">
                                    <button type="button" class="btn default">بازگشت</button>
                                </a>
                            </div>
                        </div>
                    </div>
                {!! Form::close() !!}
                <!-- END FORM-->
                </div>
            </div>


        </div>
    </div>


@endsection