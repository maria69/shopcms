@extends('panel.layout')

@section('content')

    <div class="row">
        <div class="col-md-12">

            <div class="portlet box blue-hoki">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-building"></i>لیست دسته بندی ها
                    </div>
                    <div class="pull-left">
                        <a class="btn green" href="{{route('categories.create')}}"> <i class="fa fa-plus"></i> افزودن
                            دسته بندی جدید
                        </a>
                    </div>
                </div>
                <div class="portlet-body">
                    @include("errors.list")
                </div>
                <div class="portlet-body">
                    <!-- BEGIN TABLE-->
                    <table class="table table-striped table-bordered table-hover datatable" id="sample_1">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>نام دسته بندی</th>
                            <th>دسته بندی والد</th>
                            <th>وضعیت</th>
                            <th>تاریخ</th>
                            <th>عملیات</th>
                        </tr>
                        </thead>


                        <tbody>
                        @foreach($categories as $category)

                            <tr>
                                <td>{{ $category->id }}</td>
                                <td>{{ $category->name }}</td>
                                <td>{{ isset($category->parent->name) ? $category->parent->name : "دسته بندی والد است" }}</td>
                                <td>{!! status($category->status) !!}</td>
                                <td><span class='label label-primary'>ویرایش {{ jDate::forge($category->updated_at)->format('Y/m/d H:i:s') }}</span> <span class='label label-primary'>ساخت : {{ jDate::forge($category->created_at)->format('Y/m/d H:i:s') }}</span></td>
                                <td><div class="btn-group btn-group-lg" role="group"><a href="{{route('categories.edit', $category->id)}}" class="btn green">ویرایش</a></div><div class="btn-group btn-group-lg" role="group">

                                    {!! Form::open(['url'=>route('categories.destroy', $category->id),'method'=>'delete','class'=>'delete']) !!}
                                     <button type="submit" class="btn red"> حذف</button> {!! Form::close() !!} </div></td>
                            </tr>
                        @endforeach
                        </tbody>

                    </table>
                    <!-- END TABLE-->
                </div>
            </div>


        </div>
    </div>


@endsection