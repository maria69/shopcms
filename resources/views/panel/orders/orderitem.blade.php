@extends('panel.layout')

@section('content')




    <div class="row col-md-6">
        <label class="control-label col-sm-12 col-md-4" for="number">شماره سفارش :</label>

        <div class="col-sm-12 col-md-6">
            <input type="text" readonly class="col-sm-12 col-md-12 factor_input" name="number" value="{{$orders->number }}">
        </div>
    </div>
    <br/>
    <br/>
    <div class="boxer" >


        <div class="row col-md-12">


            <div class="form-group col-md-6">
                <label class="control-label col-sm-12 col-md-4" for="name">نام گیرنده:</label>

                <div class="col-sm-12 col-md-8">
                    <input type="text" readonly class="col-sm-12 col-md-12 factor_input" name="name" value="{{$orders->user->first_name . "  " . $orders->user->last_name}}">
                </div>
                {{--<input type="text" readonly class="btn-primary col-md-3" value="{{$orders->user->first_name . "  " . $orders->user->last_name}}">--}}
            </div>
            <div class="form-group col-md-6">
                <label class="control-label col-sm-12 col-md-4" for="phone">شماره تماس:</label>

                <div class="col-sm-12 col-md-8">
                    <input type="text" readonly class="col-sm-12 col-md-12 factor_input" name="phone" value="{{$orders->user->phone }}">
                </div>
                {{--<input type="text" readonly class="btn-primary col-md-3" value="{{$orders->user->first_name . "  " . $orders->user->last_name}}">--}}
            </div>
        </div>
        <div class="row col-md-12">


            <div class="form-group col-md-6">
                <label class="control-label col-sm-12 col-md-4" for="payment">نحوه پرداخت:</label>

                <div class="col-sm-12 col-md-8">
                    <input type="text" readonly class="col-sm-12 col-md-12 factor_input" name="payment" value="{{$orders->payment_info->name }}">
                </div>
                {{--<input type="text" readonly class="btn-primary col-md-3" value="{{$orders->user->first_name . "  " . $orders->user->last_name}}">--}}
            </div>
            <div class="form-group col-md-6">
                <label class="control-label col-sm-12 col-md-4" for="shipping">شیوه ارسال:</label>

                <div class="col-sm-12 col-md-8">
                    <input type="text" readonly class="col-sm-12 col-md-12 factor_input" name="shipping" value="{{$orders->shipping_info->name }}">
                </div>
                {{--<input type="text" readonly class="btn-primary col-md-3" value="{{$orders->user->first_name . "  " . $orders->user->last_name}}">--}}
            </div>
        </div>

        <div class="row col-md-12">


            <div class="form-group col-md-6">
                <label class="control-label col-sm-12 col-md-4" for="address">آدرس:</label>

                <div class="col-sm-12 col-md-8">
                    <input type="text" readonly class="col-sm-12 col-md-12 factor_input" name="address" value="{{$orders->order_info->address }}">
                </div>
                {{--<input type="text" readonly class="btn-primary col-md-3" value="{{$orders->user->first_name . "  " . $orders->user->last_name}}">--}}
            </div>
            <div class="form-group col-md-6">
                <label class="control-label col-sm-12 col-md-4" for="status">وضعیت سفارش:</label>


{!! orderStatus($orders->status) !!}

                {{--<input type="text" readonly class="btn-primary col-md-3" value="{{$orders->user->first_name . "  " . $orders->user->last_name}}">--}}
            </div>
        </div>

        <div class="row col-md-12">


            <div class="form-group col-md-12">
                <label class="control-label col-sm-4 col-md-2" for="price">قیمت کل:</label>

                <div class="col-sm-12 col-md-6">
                    <input type="text" readonly class="col-sm-12 col-md-12 factor_input" name="price" value="{{$orders->finalPrice }}">
                </div>
                {{--<input type="text" readonly class="btn-primary col-md-3" value="{{$orders->user->first_name . "  " . $orders->user->last_name}}">--}}
            </div>

        </div>
    </div>

    <div class="portlet-body">
        <div class="row">
            <div class="col-md-12">
                {{--<table class="table table-bordered " id="orderitems">--}}
                    {{--<thead>--}}
                    {{--<th>ردیف</th>--}}
                    {{--<th>نام محصول</th>--}}
                    {{--<th>دسته ی محصول</th>--}}
                    {{--<th>کد تخفیف</th>--}}
                    {{--<th>مبلغ تخفیف</th>--}}
                    {{--<th>تعداد</th>--}}
                    {{--<th>قیمت محصول</th>--}}
                    {{--<th>وضعیت سفارش</th>--}}
                    {{--<th>عملیات</th>--}}


                    {{--</thead>--}}


                {{--</table>--}}

                <table id="dtBasicExample" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th class="th-sm">ردیف</th>
                        <th class="th-sm">نام محصول</th>

                        <th class="th-sm">تعداد</th>
                        <th class="th-sm"> قیمت محصول</th>

                        <th class="th-sm">کد تخفیف</th>
                        <th class="th-sm">قیمت نهایی</th>
                        <th class="th-sm">وضعیت سفارش محصول</th>

                    </tr>
                    </thead>
                    <tbody>


                    @foreach($orders->order_item as $order)
                        <tr>

                            <td>{{$order->id}}</td>
                            {{--<td>{{$order->product->serialnumber}}</td>--}}
                            <td>{{$order->product->name}}</td>
                            <td>{{$order->quantity}}</td>
                            <td>{{$order->unitPrice}}</td>
                            <td>{{$order->offer}}</td>
                            <td>{{$order->finalPrice}}</td>
                            <td>{!! orderStatus($order->status) !!}</td>
                        </tr>

                  @endforeach
                    </tbody>

                </table>
            </div>
        </div>

    </div>

@endsection
