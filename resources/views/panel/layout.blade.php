<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    {{-- Encrypted CSRF token for Laravel, in order for Ajax requests to work --}}
    <meta name="csrf-token" content="{{ csrf_token() }}"/>

    <title>
        مدیریت فروشگاه | @yield("title")
    </title>

    @yield('before_styles')
    @stack('before_styles')

    {{--    @php $user = Auth::guard("admins")->user(); @endphp--}}

    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    <link href="/assets/css/bootstrap-colorpicker.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/calendar-blue.css">
    <link rel="stylesheet" type="text/css" href="/font-awesome/css/font-awesome.min.css">
    <link href="/panelassets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="/panelassets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="/panelassets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet"
          type="text/css"/>
    <script src="/js/jalali.js"></script>
    <script src="/js/calendar.js"></script>
    <script src="/js/calendar-setup.js"></script>
    <script src="/js/calendar-fa.js"></script>
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="/panelassets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="/panelassets/global/plugins/morris/morris.css" rel="stylesheet" type="text/css"/>
    <link href="/panelassets/global/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css"/>
    <link href="/panelassets/global/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css"/>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="/panelassets/global/css/components-md.min.css" rel="stylesheet" id="style_components" type="text/css"/>
    <link href="/panelassets/global/css/plugins-md.min.css" rel="stylesheet" type="text/css"/>
    <!-- END THEME GLOBAL STYLES -->
    <!-- BEGIN THEME LAYOUT STYLES -->
    <link href="/panelassets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css"/>
    <link href="/panelassets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css"
          id="style_color"/>
    <link href="/panelassets/global/plugins/bootstrap-toastr/toastr.min.css" rel="stylesheet" type="text/css"/>
    <link href="/panelassets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css"/>
    <link href="/panelassets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="/panelassets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="/panelassets/css/datatables.min.css"/>

    <link rel="stylesheet" type="text/css" href="/panelassets/global/plugins/datatables/datatables.min.css"/>


    <link type="text/css" rel="stylesheet" href="/panelassets/table.css"/>
    <link href="/panelassets/bootstrap-rtl.min.css" rel="stylesheet" type="text/css"/>
    <link href="/panelassets/style.css" rel="stylesheet" type="text/css"/>

    <!-- END THEME LAYOUT STYLES -->
    <link rel="shortcut icon" href="favicon.ico.jpg"/>
    <link rel="stylesheet" href="/css/persianDatepicker-default.css"/>
    <script src="/js/jquery.min.js"></script>
    <script src="/js/persianDatepicker.min.js"></script>
@yield('after_styles')
@stack('after_styles')

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="/panelassets/js/html5shiv.min.js"></script>
    <script src="/panelassets/js/respond.min.js"></script>
    <![endif]-->
</head>

<body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid page-content-white page-md">
<div id="loading">
    <div class="loader" id="loader-1"></div>
</div>
<style>
    #loading {
        position: fixed;
        left: 0;
        top: 0;
        background: rgba(0, 0, 0, 0.5);
        width: 100%;
        height: 100vh;
        z-index: 10000;
        display: none;
    }

    /* ALL LOADERS */

    .loader {
        width: 100px;
        height: 100px;
        border-radius: 100%;
        position: relative;
        margin: 0 auto;
        top: 44vh;
    }

    /* LOADER 1 */

    #loader-1:before, #loader-1:after {
        content: "";
        position: absolute;
        top: -10px;
        left: -10px;
        width: 100%;
        height: 100%;
        border-radius: 100%;
        border: 10px solid transparent;
        border-top-color: #3498db;
    }

    #loader-1:before {
        z-index: 100;
        animation: spin 1s infinite;
    }

    #loader-1:after {
        border: 10px solid #ccc;
    }

    @keyframes spin {
        0% {
            -webkit-transform: rotate(0deg);
            -ms-transform: rotate(0deg);
            -o-transform: rotate(0deg);
            transform: rotate(0deg);
        }

        100% {
            -webkit-transform: rotate(360deg);
            -ms-transform: rotate(360deg);
            -o-transform: rotate(360deg);
            transform: rotate(360deg);
        }
    }
</style>
<div class="page-wrapper">
    <!-- BEGIN HEADER -->
    <div class="page-header navbar navbar-fixed-top">
        <!-- BEGIN HEADER INNER -->
        <div class="page-header-inner ">
            <!-- BEGIN LOGO -->
            <div class="page-logo">
                <div class="menu-toggler sidebar-toggler pull-right">
                    <span></span>
                </div>
                <h1 class="pull-right" id="mft-title">مدیریت فروشگاه</h1>
            </div>
            <!-- END LOGO -->
            <!-- BEGIN RESPONSIVE MENU TOGGLER -->
            <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse"
               data-target=".navbar-collapse">
                <span></span>
            </a>
            <!-- END RESPONSIVE MENU TOGGLER -->
            <!-- BEGIN TOP NAVIGATION MENU -->
            <div class="top-menu">
                <ul class="nav navbar-nav pull-left">
                    @php

                        if(Auth::guard("admins")->check()){
                        $seller = 1;
                          $user = Auth::guard("admins")->user();
                        }
                       else{
                       $seller = 0;
                          $user = Auth::guard("superAdmin")->user();
                       }

                    @endphp

                    {{--                    @php $user = Auth::guard("superAdmin")->user() @endphp--}}
                    @if(isset($user))
                        <li class="dropdown dropdown-user">
                            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                               data-close-others="true">
                                <img alt="" class="img-circle" src="/avatars/{{ $user->avatar }}"/>
                                <span class="username username-hide-on-mobile"> @if($seller == 1) فروشنده محترم@else
                                    مدیریت محترم @endif  <span class="adminstyle"
                                                                    style="font-weight: bold;color: red;">{{ $user->first_name." ".$user->last_name }} </span>خوش آمدید </span>
                                <i class="fa fa-angle-down"></i>

                            </a>
                            <ul class="dropdown-menu dropdown-menu-default">
                                <li>

                                    <a href="{{route("profile")}}">
                                        <i class="fa fa-user"></i> ویرایش پروفایل </a>
                                </li>

                                <li>
                                    <a href="{{route("password")}}">
                                        <i class="fa fa-key"></i> تغیییر رمز عبور </a>
                                </li>

                                <li>
                                    <a href="{{route("avatar")}}">
                                        <i class="fa fa-picture-o"></i> تصویر حساب کاربری </a>
                                </li>

                                <li>
                                    <a href="{{route("adminLogout")}}">
                                        <i class="fa fa-sign-out"></i> خروج </a>
                                </li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
            <!-- END TOP NAVIGATION MENU -->
        </div>
        <!-- END HEADER INNER -->
    </div>
    <!-- END HEADER -->
    <!-- BEGIN HEADER & CONTENT DIVIDER -->
    <div class="clearfix"></div>
    <!-- END HEADER & CONTENT DIVIDER -->


    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN SIDEBAR -->
        <div class="page-sidebar-wrapper">
            <div class="page-sidebar navbar-collapse collapse">


            @if($seller == 0)
                @include("panel.partials.admin_nav")
            @else
                @include("panel.partials.seller_nav")
            @endif
            <!-- END SIDEBAR MENU -->
            </div>
            <!-- END SIDEBAR -->
        </div>
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <div class="page-content">
                <!-- BEGIN PAGE HEADER-->

                <!-- BEGIN PAGE BAR -->
                <div class="page-bar">
                    <ul class="page-breadcrumb pull-right">
                        <li>
                            <a href="{{route('dashboard')}}">پیشخوان</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>صفحه اصلی</span>
                        </li>
                    </ul>
                </div>
                <!-- END PAGE BAR -->
                <!-- BEGIN PAGE TITLE-->
                <h1 class="page-title"> پنل مدیریت
                    <small>خوش آمدید</small>
                </h1>


                @yield('content')


            </div>

        </div>

    @yield('before_scripts')
    @stack('before_scripts')

    <!-- BEGIN FOOTER -->
        <div class="page-footer">
            <div class="page-footer-inner"> Developed By
                <a target="_blank" href="http://www.ali-bakhshi.ir">Maghsoodi</a> && <a target="_blank"
                                                                                        href="#">Niazi</a>
            </div>
            <div class="scroll-to-top">
                <i class="fa fa-arrow-circle-up"></i>
            </div>
        </div>
        <!-- END FOOTER -->
    </div>
</div>
@include("panel.common.galleryPicker")
<!--[if lt IE 9]>
<script src="/panelassets/global/plugins/respond.min.js"></script>
<script src="/panelassets/global/plugins/excanvas.min.js"></script>
<script src="/panelassets/global/plugins/ie8.fix.min.js"></script>
<![endif]-->
<!-- BEGIN CORE PLUGINS -->
{{--<script src="/panelassets/global/plugins/jquery.min.js" type="text/javascript"></script>   --}}
<script src="/panelassets/js/jquery.min.js"></script>


<script src="/panelassets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="/vendor/jsvalidation/js/jsvalidation.js"></script>
<script src="/panelassets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
<script src="/panelassets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js"
        type="text/javascript"></script>
<script src="/panelassets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="/panelassets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js"
        type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<script src="/js/xep.js" type="text/javascript"></script>
<script src="/panelassets/global/plugins/morris/morris.min.js" type="text/javascript"></script>
<script src="/panelassets/global/plugins/morris/raphael-min.js" type="text/javascript"></script>
<script src="/panelassets/global/scripts/app.min.js" type="text/javascript"></script>
<script src="/panelassets/pages/scripts/dashboard.min.js" type="text/javascript"></script>
<script src="/panelassets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
<script src="/panelassets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script>
<script src="/panelassets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
<script src="/panelassets/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>
<script src="/panelassets/global/plugins/bootstrap-toastr/toastr.min.js" type="text/javascript"></script>
<script src="/panelassets/global/scripts/app.min.js" type="text/javascript"></script>
<link rel="stylesheet"
      href="/panelassets/css/bootstrap-select.min.css">
<script src="/panelassets/js/bootstrap-select.min.js"></script>
<script src="/panelassets/js/i18n/defaults-*.min.js"></script>
<script src="/panelassets/pages/scripts/ui-toastr.min.js" type="text/javascript"></script>
@include("errors.toast")
<link rel="stylesheet" href="/assets/css/sweetalert.css">
<script src="/assets/js/sweetalert.min.js"></script>
@include("errors.sweet")
<script src="/panelassets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<script src="/ckeditor/ckeditor.js"></script>
<!-- <script type="text/javascript"
         src="https://cdn.datatables.net/tabletools/2.2.4/js/dataTables.tableTools.min.js"></script>
 <script type="text/javascript" src="https://cdn.datatables.net/tabletools/2.2.2/swf/copy_csv_xls_pdf.swf"></script>-->
{{--<script type="text/javascript" src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>--}}
<script type="text/javascript" src="/panelassets/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="/panelassets/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="/panelassets/js/2.5.0/jszip.min.js"></script>
<script type="text/javascript" src="/panelassets/js/build/pdfmake.min.js"></script>
<script type="text/javascript" src="/panelassets/js/build/vfs_fonts.js"></script>
<script type="text/javascript" src="/panelassets/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="/panelassets/js/buttons.print.min.js"></script>
<script src="/panelassets/js/jquery-ui.js"></script>
<script src="/panelassets/js/jquery.mjs.nestedSortable.js"></script>
<script type="text/javascript" src="/panelassets/js/datatables.min.js"></script>
{{--<script src="/panelassets/global/plugins/datatables/datatables.min.js"/>--}}


<script src="/panelassets/custom.js"></script>
<script>
    var csrfToken = '{{ csrf_token() }}';
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': csrfToken
        }
    });
</script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery.matchHeight/0.7.0/jquery.matchHeight-min.js"></script>
<script src="/panelassets/media.js"></script>
@if(Auth::guard("admins")->check())
    @php
        $uri=trim(Request::route()->uri());
        $uri = explode("/",$uri);
        $uri = implode("/",array_slice($uri, 0,3));
    @endphp
    <script>var prefix = '{{ $uri }}';</script>
@endif

<script src="/panelassets/ajaxCall.js"></script>
<link rel="stylesheet" href="/panelassets/dashboard_style.css">
<script src="/panelassets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js"
        type="text/javascript"></script>
<!--<script src="/panelassets/pages/scripts/form-input-mask.min.js" type="text/javascript"></script>-->
<script src="/panelassets/global/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
<script src="/panelassets/global/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>
<script src="/panelassets/global/scripts/app.min.js" type="text/javascript"></script>
{{--<script src="//code.jquery.com/jquery-3.2.1.js"></script>--}}
<script src="/assets/js/bootstrap-colorpicker.js"></script>
<script>
    $(function () {
        $('#mycp').colorpicker({format: "hex"});
    });
    $(document).ready(function () {
        $(".factor_input").click(function () {
            alert();
        });
        $('#dataAjax').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": "{{ url('myData') }}",
                "dataType": "json",
                "type": "POST",
                "data": {_token: "{{csrf_token()}}"}
            },
            "columns": [
                {"data": "id"},
                {"data": "number"},
                {"data": "username"},
                // {"data": "count"},
                {"data": "payment"},
                {"data": "shipping"},
                {"data": "offer"},
                {"data": "price"},
                {"data": "date"},
                // {"data": "address"},
                {"data": "options"}
            ]
        });
    });
    // $(document).ready(function() {
    //     var showChar = 100;
    //     var ellipsestext = "...";
    //     var moretext = "more";
    //     var lesstext = "less";
    //
    //     // alert();
    //     $('.factor_input').each(function() {
    //         var content = $(this).html();
    //         // alert();
    //
    //         if(content.length > showChar) {
    //
    //             var c = content.substr(0, showChar);
    //             var h = content.substr(showChar-1, content.length - showChar);
    //
    //             var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';
    //
    //             $(this).html(html);
    //         }
    //
    //     });
    //
    //     $(".morelink").click(function(){
    //         if($(this).hasClass("less")) {
    //             $(this).removeClass("less");
    //             $(this).html(moretext);
    //         } else {
    //             $(this).addClass("less");
    //             $(this).html(lesstext);
    //         }
    //         $(this).parent().prev().toggle();
    //         $(this).prev().toggle();
    //         return false;
    //     });
    // });

</script>


<script>
    {{--$(document).ready(function () {--}}
    {{--$('#orderitems').DataTable({--}}
    {{--"processing": true,--}}
    {{--"serverSide": true,--}}
    {{--"ajax":{--}}
    {{--"url": "{{ url('orderItem') }}",--}}
    {{--"dataType": "json",--}}
    {{--"type": "POST",--}}
    {{--"data":{ _token: "{{csrf_token()}}"}--}}
    {{--},--}}
    {{--"columns": [--}}
    {{--{ "data": "id" },--}}
    {{--{ "data": "number" },--}}
    {{--{ "data": "username" },--}}
    {{--{ "data": "count" },--}}
    {{--{ "data": "payment" },--}}
    {{--{ "data": "shipping" },--}}
    {{--{ "data": "offer" },--}}
    {{--{ "data": "price" },--}}
    {{--{ "data": "date" },--}}
    {{--{ "data": "address" },--}}
    {{--{ "data": "options" }--}}


    {{--]--}}

    {{--});--}}
    {{--});--}}

    $('#dtBasicExample').DataTable();
    $('.dataTables_length').addClass('bs-select');


    $(".mdcheck").on('change', function () {
        // alert();


        if ($(this).is(':checked')) {
            $(this).attr('value', '1');
        } else {
            $(this).attr('value', '0');
        }
        console.log($(this).val());

        // console.log( $(this).attr('value').val());
        $('#statusValue').val($(this).val());
    });
</script>

@yield('extra_script')
</body>
</html>