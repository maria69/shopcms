@extends('panel.layout')
@section("title") ساخت برند @endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue-hoki">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-building"></i>افزودن کد تخفیف
                    </div>
                    <div class="tools">
                        <!--<a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                        <a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title=""> </a>
                        <a href="javascript:;" class="reload" data-original-title="" title=""> </a>
                        <a href="javascript:;" class="remove" data-original-title="" title=""> </a>-->
                    </div>
                </div>
                <div class="portlet-body">
                    @include("errors.list")
                </div>
                <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    {!! Form::open(["class"=>"form-horizontal","url"=>route("offers.store"),"method"=>"post"]) !!}
                    <div class="form-body">
                        <div class="form-group form-md-line-input">
                            {!! Form::label("name","عنوان کد تخفیف",["class"=>"col-md-2 control-label"]) !!}
                            <div class="col-md-4">
                                {!! Form::input("text","name",null,["class"=>"form-control","placeholder"=>"عنوان کد تخفیف"]) !!}
                                <div class="form-control-focus"></div>
                            </div>
                        </div>
                        <div class="form-group form-md-line-input">
                            {!! Form::label("name","کد تخفیف",["class"=>"col-md-2 control-label"]) !!}
                            <div class="col-md-4">
                                {!! Form::input("text","code",null,["class"=>"form-control","placeholder"=>"کد تخفیف"]) !!}
                                <div class="form-control-focus"></div>
                            </div>
                        </div>
                        <div class="form-group form-md-line-input">
                            {!! Form::label("name","نوع تخفیف",["class"=>"col-md-2 control-label"]) !!}
                            <div class="col-md-4">
                                <select name="offer_type" id="" class="form-control">
                                    <option value="1">مبلغ</option>
                                    <option value="2">درصدی</option>
                                </select>
                                <div class="form-control-focus"></div>
                            </div>
                        </div>
                        <div class="form-group form-md-line-input">
                            {!! Form::label("name","مقدار تخفیف",["class"=>"col-md-2 control-label"]) !!}
                            <div class="col-md-4">
                                {!! Form::input("text","offer",null,["class"=>"form-control","placeholder"=>"مقدار تخفیف"]) !!}
                                <div class="form-control-focus"></div>
                            </div>
                        </div>
                        <div class="form-group form-md-line-input">
                            {!! Form::label("start_date","شروع تخفیف",["class"=>"col-md-2 control-label"]) !!}
                            <div class="col-md-4">
                                {!! Form::input("text","start_date",null,["id"=>"start_date","class"=>"form-control","placeholder"=>"شروع تخفیف"]) !!}
                                <div class="form-control-focus"></div>
                            </div>
                        </div>
                        <div class="form-group form-md-line-input">
                            {!! Form::label("end_date","اتمام تخفیف",["class"=>"col-md-2 control-label"]) !!}
                            <div class="col-md-4">
                                {!! Form::input("text","end_date",null,["id"=>"end_date","class"=>"form-control","placeholder"=>"اتمام تخفیف"]) !!}
                                <div class="form-control-focus"></div>
                            </div>
                        </div>
{{--                        <div class="form-group form-md-line-input">--}}
{{--                            {!! Form::label("name","تاریخ شروع تخفیف",["class"=>"col-md-2 control-label"]) !!}--}}
{{--                            <div class="col-md-4">--}}
{{--                                <div class="col-md-4"><select class="form-control" name="start_day" id="">--}}
{{--                                        <option value="1">1</option>--}}
{{--                                        <option value="2">2</option>--}}
{{--                                        <option value="3">3</option>--}}
{{--                                        <option value="4">4</option>--}}
{{--                                        <option value="5">5</option>--}}
{{--                                        <option value="6">6</option>--}}
{{--                                        <option value="7">7</option>--}}
{{--                                        <option value="8">8</option>--}}
{{--                                        <option value="9">9</option>--}}
{{--                                        <option value="10">10</option>--}}
{{--                                        <option value="11">11</option>--}}
{{--                                        <option value="12">12</option>--}}
{{--                                        <option value="13">13</option>--}}
{{--                                        <option value="14">14</option>--}}
{{--                                        <option value="15">15</option>--}}
{{--                                        <option value="16">16</option>--}}
{{--                                        <option value="17">17</option>--}}
{{--                                        <option value="18">18</option>--}}
{{--                                        <option value="19">19</option>--}}
{{--                                        <option value="20">20</option>--}}
{{--                                        <option value="21">21</option>--}}
{{--                                        <option value="22">22</option>--}}
{{--                                        <option value="23">23</option>--}}
{{--                                        <option value="24">24</option>--}}
{{--                                        <option value="25">25</option>--}}
{{--                                        <option value="26">26</option>--}}
{{--                                        <option value="27">27</option>--}}
{{--                                        <option value="28">28</option>--}}
{{--                                        <option value="29">29</option>--}}
{{--                                        <option value="30">30</option>--}}
{{--                                        <option value="31">31</option>--}}
{{--                                    </select></div>--}}

{{--                                <div class="col-md-4"><select class="form-control" name="start_month" id="">--}}
{{--                                        <option value="1">1</option>--}}
{{--                                        <option value="2">2</option>--}}
{{--                                        <option value="3">3</option>--}}
{{--                                        <option value="4">4</option>--}}
{{--                                        <option value="5">5</option>--}}
{{--                                        <option value="6">6</option>--}}
{{--                                        <option value="7">7</option>--}}
{{--                                        <option value="8">8</option>--}}
{{--                                        <option value="9">9</option>--}}
{{--                                        <option value="10">10</option>--}}
{{--                                        <option value="11">11</option>--}}
{{--                                        <option value="12">12</option>--}}
{{--                                    </select></div>--}}
{{--                                <div class="col-md-4"><select class="form-control" name="start_year" id="">--}}
{{--                                        <option value="1396">1396</option>--}}
{{--                                        <option value="1397">1397</option>--}}
{{--                                        <option value="1398">1398</option>--}}
{{--                                        <option value="1399">1399</option>--}}
{{--                                        <option value="1400">1400</option>--}}
{{--                                        <option value="1401">1401</option>--}}
{{--                                        <option value="1402">1402</option>--}}
{{--                                        <option value="1403">1403</option>--}}
{{--                                        <option value="1404">1404</option>--}}
{{--                                        <option value="1405">1405</option>--}}
{{--                                    </select></div>--}}
{{--                                <div class="form-control-focus"></div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="form-group form-md-line-input">--}}
{{--                            {!! Form::label("name","تاریخ پایان تخفیف",["class"=>"col-md-2 control-label"]) !!}--}}
{{--                            <div class="col-md-4">--}}
{{--                                <div class="col-md-4"><select class="form-control" name="end_day" id="">--}}
{{--                                        <option value="1">1</option>--}}
{{--                                        <option value="2">2</option>--}}
{{--                                        <option value="3">3</option>--}}
{{--                                        <option value="4">4</option>--}}
{{--                                        <option value="5">5</option>--}}
{{--                                        <option value="6">6</option>--}}
{{--                                        <option value="7">7</option>--}}
{{--                                        <option value="8">8</option>--}}
{{--                                        <option value="9">9</option>--}}
{{--                                        <option value="10">10</option>--}}
{{--                                        <option value="11">11</option>--}}
{{--                                        <option value="12">12</option>--}}
{{--                                        <option value="13">13</option>--}}
{{--                                        <option value="14">14</option>--}}
{{--                                        <option value="15">15</option>--}}
{{--                                        <option value="16">16</option>--}}
{{--                                        <option value="17">17</option>--}}
{{--                                        <option value="18">18</option>--}}
{{--                                        <option value="19">19</option>--}}
{{--                                        <option value="20">20</option>--}}
{{--                                        <option value="21">21</option>--}}
{{--                                        <option value="22">22</option>--}}
{{--                                        <option value="23">23</option>--}}
{{--                                        <option value="24">24</option>--}}
{{--                                        <option value="25">25</option>--}}
{{--                                        <option value="26">26</option>--}}
{{--                                        <option value="27">27</option>--}}
{{--                                        <option value="28">28</option>--}}
{{--                                        <option value="29">29</option>--}}
{{--                                        <option value="30">30</option>--}}
{{--                                        <option value="31">31</option>--}}
{{--                                    </select></div>--}}

{{--                                <div class="col-md-4"><select class="form-control" name="end_month" id="">--}}
{{--                                        <option value="1">1</option>--}}
{{--                                        <option value="2">2</option>--}}
{{--                                        <option value="3">3</option>--}}
{{--                                        <option value="4">4</option>--}}
{{--                                        <option value="5">5</option>--}}
{{--                                        <option value="6">6</option>--}}
{{--                                        <option value="7">7</option>--}}
{{--                                        <option value="8">8</option>--}}
{{--                                        <option value="9">9</option>--}}
{{--                                        <option value="10">10</option>--}}
{{--                                        <option value="11">11</option>--}}
{{--                                        <option value="12">12</option>--}}
{{--                                    </select></div>--}}

{{--                                <div class="col-md-4"><select class="form-control" name="end_year" id="">--}}
{{--                                        <option value="1396">1396</option>--}}
{{--                                        <option value="1397">1397</option>--}}
{{--                                        <option value="1398">1398</option>--}}
{{--                                        <option value="1399">1399</option>--}}
{{--                                        <option value="1400">1400</option>--}}
{{--                                        <option value="1401">1401</option>--}}
{{--                                        <option value="1402">1402</option>--}}
{{--                                        <option value="1403">1403</option>--}}
{{--                                        <option value="1404">1404</option>--}}
{{--                                        <option value="1405">1405</option>--}}
{{--                                    </select></div>--}}
                                <div class="form-control-focus"></div>
                            </div>
                        </div>
                        <hr>
                    </div>
                    <div class="form-actions fluid">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                {!! Form::submit("ذخیره",["class"=>"btn green"]) !!}
                                <a href="{{route("brands.index")}}">
                                    <button type="button" class="btn default">بازگشت</button>
                                </a>
                            </div>
                        </div>
                    </div>
                {!! Form::close() !!}
                <!-- END FORM-->
                </div>
            </div>
        </div>
    </div>
@endsection
@section('extra_script')
    <script>
        Calendar.setup({
            inputField: 'start_date',
            button: 'date_btn',
            ifFormat: '%Y-%m-%d',
            dateType: 'jalali'
        });
        Calendar.setup({
            inputField: 'end_date',
            button: 'date_btn',
            ifFormat: '%Y-%m-%d',
            dateType: 'jalali'
        });
    </script>
@endsection