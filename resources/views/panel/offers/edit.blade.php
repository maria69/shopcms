@extends('panel.layout')
@section("title") ویرایش کد تخفیف @endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue-hoki">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-building"></i>ویرایش کد تخفیف
                    </div>
                    <div class="tools">
                        <!--<a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                        <a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title=""> </a>
                        <a href="javascript:;" class="reload" data-original-title="" title=""> </a>
                        <a href="javascript:;" class="remove" data-original-title="" title=""> </a>-->
                    </div>
                </div>
                <div class="portlet-body">
                    @include("errors.list")
                </div>
                <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    {!! Form::open(["class"=>"form-horizontal","url"=>route("offers.update",$offer->id),"method"=>"put"]) !!}
                    <div class="form-body">
                        <div class="form-group form-md-line-input">
                            {!! Form::label("name","عنوان کد تخفیف",["class"=>"col-md-2 control-label"]) !!}
                            <div class="col-md-4">
                                {!! Form::input("text","name",$offer->name,["class"=>"form-control","placeholder"=>"عنوان کد تخفیف"]) !!}
                                <div class="form-control-focus"></div>
                            </div>
                        </div>
                        <div class="form-group form-md-line-input">
                            {!! Form::label("name","کد تخفیف",["class"=>"col-md-2 control-label"]) !!}
                            <div class="col-md-4">
                                {!! Form::input("text","code",$offer->code,["class"=>"form-control","placeholder"=>"کد تخفیف"]) !!}
                                <div class="form-control-focus"></div>
                            </div>
                        </div>
                        <div class="form-group form-md-line-input">
                            {!! Form::label("name","نوع تخفیف",["class"=>"col-md-2 control-label"]) !!}
                            <div class="col-md-4">
                                <select name="offer_type" id="" class="form-control">
                                    <option value="1">مبلغ</option>
                                    <option value="2">درصدی</option>
                                </select>
                                <div class="form-control-focus"></div>
                            </div>
                        </div>
                        <div class="form-group form-md-line-input">
                            {!! Form::label("name","مقدار تخفیف",["class"=>"col-md-2 control-label"]) !!}
                            <div class="col-md-4">
                                {!! Form::input("text","offer",$offer->offer,["class"=>"form-control","placeholder"=>"مقدار تخفیف"]) !!}
                                <div class="form-control-focus"></div>
                            </div>
                        </div>
                        <div class="form-group form-md-line-input">
                            {!! Form::label("start_date","شروع تخفیف",["class"=>"col-md-2 control-label"]) !!}
                            <div class="col-md-4">
                                {!! Form::input("text","start_date",jdate()->forge($offer->start_date)->format('Y-m-d'),["id"=>"start_date","class"=>"form-control","placeholder"=>"شروع تخفیف"]) !!}
                                <div class="form-control-focus"></div>
                            </div>
                        </div>
                        <div class="form-group form-md-line-input">
                            {!! Form::label("end_date","اتمام تخفیف",["class"=>"col-md-2 control-label"]) !!}
                            <div class="col-md-4">
                                {!! Form::input("text","end_date",jdate()->forge($offer->end_date)->format('Y-m-d'),["id"=>"end_date","class"=>"form-control","placeholder"=>"اتمام تخفیف"]) !!}
                                <div class="form-control-focus"></div>
                            </div>
                        </div>
                        <div class="form-control-focus"></div>
                    </div>
                </div>
                <hr>
            </div>
            <div class="form-actions fluid">
                <div class="row">
                    <div class="col-md-offset-3 col-md-9">
                        {!! Form::submit("ذخیره",["class"=>"btn green"]) !!}
                        <a href="{{route("brands.index")}}">
                            <button type="button" class="btn default">بازگشت</button>
                        </a>
                    </div>
                </div>
            </div>
        {!! Form::close() !!}
        <!-- END FORM-->
        </div>
    </div>
    </div>
    </div>
@endsection
@section('extra_script')
    <script>
        Calendar.setup({
            inputField: 'start_date',
            button: 'date_btn',
            ifFormat: '%Y-%m-%d',
            dateType: 'jalali'
        });
        Calendar.setup({
            inputField: 'end_date',
            button: 'date_btn',
            ifFormat: '%Y-%m-%d',
            dateType: 'jalali'
        });
    </script>
@endsection