@extends('panel.layout')

@section('content')

    <div class="row">
        <div class="col-md-12">

            <div class="portlet box blue-hoki">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-building"></i>لیست تخفیف ها
                    </div>
                    <div class="pull-left">
                        <a class="btn green" href="{{route('offers.create')}}"> <i class="fa fa-plus"></i>ایجاد تخفیف ها</a>
                    </div>
                </div>
                <div class="portlet-body">
                    @include("errors.list")
                </div>
                <div class="portlet-body">
                    <!-- BEGIN TABLE-->
                    <table class="table table-striped table-bordered table-hover datatable" id="sample_1">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>کد تخفیف</th>
                            <th>مقدار تخفیف</th>
                            <th>نوع تخفیف</th>
                            <th>درصد تخفیف</th>
                            <th>تاریخ شروع تخفیف</th>
                            <th>تاریخ پایان تخفیف</th>
                            <th>وضعیت</th>
                            <th>عملیات</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($offers as $offer )
                            <tr>
                                <td>{{ $offer->id }}</td>
                                <td>{{ $offer->code }}</td>
                                <td>{{ $offer->offer}}</td>
                                <td>{!! offStatus($offer->offer_type) !!} </td>
                                <td>{{ $offer->code }}</td>
                                <td><span class='label label-primary'>{{ jDate::forge($offer->start_date)->format('Y/m/d H:i:s') }}</span></td>
                                <td><span class='label label-primary'>{{ jDate::forge($offer->end_date)->format('Y/m/d H:i:s') }}</span></td>
                                <td><span class='label label-primary'>{{ $offer->status}}</span></td>
                                <td><div class="btn-group btn-group-lg" role="group"><a href="{{route('offers.edit', $offer->id)}}" class="btn green">ویرایش</a></div><div class="btn-group btn-group-lg" role="group">

                                        {!! Form::open(['url'=>route('offers.destroy', $offer->id),'method'=>'delete','class'=>'delete']) !!}
                                        <button type="submit" class="btn red"> حذف</button> {!! Form::close() !!} </div></td>
                            </tr>

                        @endforeach
                        </tbody>

                    </table>
                    <!-- END TABLE-->
                </div>
            </div>


        </div>
    </div>

@endsection