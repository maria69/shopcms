@extends('panel.layout')

@section('content')


    <div class="row">
        <div class="col-md-12">

            <div class="portlet box blue-hoki">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-building"></i>ویرایش مدیر - {{$admin->first_name." ".$admin->last_name}} </div>
                    <div class="tools">
                        <!--<a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                        <a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title=""> </a>
                        <a href="javascript:;" class="reload" data-original-title="" title=""> </a>
                        <a href="javascript:;" class="remove" data-original-title="" title=""> </a>-->
                    </div>
                </div>
                <div class="portlet-body">
                    @include("errors.list")
                </div>
                <div class="portlet-body form">
                    {!! Form::open(["class"=>"form-horizontal","url"=>route("admins.update",$admin->id),"method"=>"put"]) !!}

                    <div class="form-body">

                        <div class="form-group form-md-line-input">
                            {!! Form::label("first_name","نام",["class"=>"col-md-2 control-label"]) !!}
                            <div class="col-md-4">
                                {!! Form::input("text","first_name",$admin->first_name,["class"=>"form-control","placeholder"=>"نام"]) !!}
                                <div class="form-control-focus"> </div>
                            </div>
                        </div>


                        <div class="form-group form-md-line-input">
                            {!! Form::label("last_name","نام خانوادگی",["class"=>"col-md-2 control-label"]) !!}
                            <div class="col-md-4">
                                {!! Form::input("text","last_name",$admin->last_name,["class"=>"form-control","placeholder"=>"نام خانوادگی"]) !!}
                                <div class="form-control-focus"> </div>
                            </div>
                        </div>


                        <div class="form-group form-md-line-input">
                            {!! Form::label("email","ایمیل",["class"=>"col-md-2 control-label"]) !!}
                            <div class="col-md-4">
                                {!! Form::input("text","email",$admin->email,["class"=>"form-control","placeholder"=>"ایمیل"]) !!}
                                <div class="form-control-focus"> </div>
                            </div>
                        </div>



                        <div class="form-group form-md-line-input">
                            {!! Form::label("mobile","موبایل",["class"=>"col-md-2 control-label"]) !!}
                            <div class="col-md-4">
                                {!! Form::input("text","mobile",$admin->mobile,["class"=>"form-control","placeholder"=>"موبایل"]) !!}
                                <div class="form-control-focus"> </div>
                            </div>
                        </div>



                        <div class="form-group form-md-line-input">
                            {!! Form::label("code_melli","کد ملی",["class"=>"col-md-2 control-label"]) !!}
                            <div class="col-md-4">
                                {!! Form::input("text","code_melli",$admin->code_melli,["class"=>"form-control","placeholder"=>"کد ملی"]) !!}
                                <div class="form-control-focus"> </div>
                            </div>
                        </div>


                        <div class="form-group form-md-line-input">
                            {!! Form::label("status","وضعیت مدیر",["class"=>"col-md-2 control-label"]) !!}
                            <div class="col-md-4">
                                <div class="md-checkbox">
                                    <input id="status" value="1" class="md-check" name="status" type="checkbox" @if($admin->status == 1) checked @endif>
                                    <label for="status">
                                        <span></span>
                                        <span class="check"></span>
                                        <span class="box"></span> فعال </label>
                                </div>
                            </div>
                        </div>


                        <div class="alert alert-info">
                            <p>اگر نمیخواهید رمز عبور تغییر کند فیلد های رمزعبور را خالی بگذارید</p>
                        </div>


                        <div class="form-group form-md-line-input">
                            {!! Form::label("password","گذرواژه",["class"=>"col-md-2 control-label"]) !!}
                            <div class="col-md-4">
                                {!! Form::input("password","password",null,["class"=>"form-control","placeholder"=>"گذرواژه"]) !!}
                                <div class="form-control-focus"> </div>
                            </div>
                        </div>



                        <div class="form-group form-md-line-input">
                            {!! Form::label("password_confirmation","تکرار گذرواژه",["class"=>"col-md-2 control-label"]) !!}
                            <div class="col-md-4">
                                {!! Form::input("password","password_confirmation",null,["class"=>"form-control","placeholder"=>"تکرار گذرواژه"]) !!}
                                <div class="form-control-focus"> </div>
                            </div>
                        </div>



                    </div>
                    <div class="form-actions fluid">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                {!! Form::submit("ویرایش",["class"=>"btn green"]) !!}
                                <a href="{{route("admins.index")}}"><button type="button" class="btn default">بازگشت</button></a>
                            </div>
                        </div>
                    </div>
                {!! Form::close() !!}
                <!-- END FORM-->
                </div>
            </div>


        </div>
    </div>


@endsection