@extends('panel.layout')

@section('content')

    <div class="row">
        <div class="col-md-12">

            <div class="portlet box blue-hoki">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-building"></i>لیست مدیران </div>
                    {{--<div class="pull-left">--}}
                        {{--<a class="btn green" href="/administrator/jobs/create"> <i class="fa fa-plus"></i> افزودن شغل </a>--}}
                    {{--</div>--}}
                </div>
                <div class="portlet-body">
                    @include("errors.list")
                </div>
                <div class="portlet-body">
                    <!-- BEGIN TABLE-->
                    <table class="table table-striped table-bordered table-hover datatable" id="sample_1">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>نام و نام خانوادگی</th>
                            <th>موبایل</th>
                            <th>کد ملی</th>
                            <th>سطح دسترسی کاربر</th>
                            <th>تاریخ ساخت</th>
                            <th>ویرایش</th>
                            <th>حذف</th>
                        </tr>
                        </thead>


                        <tbody>
                        @foreach($admins as $row)

                            <tr>
                                <td>{{ $row->id }}</td>
                                <td>{{ $row->first_name." ".$row->last_name }}</td>
                                <td>{{ $row->mobile }}</td>
                                <td>{{ $row->code_melli }}</td>
                                <td></td>
                                <td>{{ jDate::forge($row->created_at)->format('Y/m/d H:i:s') }}</td>
                                <td><a href="{{route("admins.edit",$row->id)}}" class="btn green">ویرایش</a> </td>
                                <td> {!! Form::open(['url'=>route("admins.destroy",$row->id),'method'=>'delete','class'=>'delete']) !!}    <button type="submit" class="btn red"> Delete </button>   {!! Form::close() !!} </td>
                            </tr>

                        @endforeach
                        </tbody>

                    </table>
                    <!-- END TABLE-->
                </div>
            </div>


        </div>
    </div>


@endsection