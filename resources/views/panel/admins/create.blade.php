@extends('panel.layout')

@section('content')


    <div class="row">
        <div class="col-md-12">

            <div class="portlet box blue-hoki">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-building"></i>افزودن مدیر
                    </div>
                    <div class="tools">
                        <!--<a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                        <a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title=""> </a>
                        <a href="javascript:;" class="reload" data-original-title="" title=""> </a>
                        <a href="javascript:;" class="remove" data-original-title="" title=""> </a>-->
                    </div>
                </div>
                <div class="portlet-body">
                    @include("errors.list")
                </div>
                <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    {!! Form::open(["class"=>"form-horizontal","url"=>route("admins.store"),"method"=>"post"]) !!}

                    <div class="form-body">

                        <div class="form-group form-md-line-input">
                            {!! Form::label("first_name","نام",["class"=>"col-md-2 control-label"]) !!}
                            <div class="col-md-4">
                                {!! Form::input("text","first_name",null,["class"=>"form-control","placeholder"=>"نام"]) !!}
                                <div class="form-control-focus"></div>
                            </div>
                        </div>


                        <div class="form-group form-md-line-input">
                            {!! Form::label("last_name","نام خانوادگی",["class"=>"col-md-2 control-label"]) !!}
                            <div class="col-md-4">
                                {!! Form::input("text","last_name",null,["class"=>"form-control","placeholder"=>"نام خانوادگی"]) !!}
                                <div class="form-control-focus"></div>
                            </div>
                        </div>


                        <div class="form-group form-md-line-input">
                            {!! Form::label("email","ایمیل",["class"=>"col-md-2 control-label"]) !!}
                            <div class="col-md-4">
                                {!! Form::input("text","email",null,["class"=>"form-control","placeholder"=>"ایمیل"]) !!}
                                <div class="form-control-focus"></div>
                            </div>
                        </div>


                        <div class="form-group form-md-line-input">
                            {!! Form::label("mobile","موبایل",["class"=>"col-md-2 control-label"]) !!}
                            <div class="col-md-4">
                                {!! Form::input("text","mobile",null,["class"=>"form-control","placeholder"=>"موبایل"]) !!}
                                <div class="form-control-focus"></div>
                            </div>
                        </div>


                        <div class="form-group form-md-line-input">
                            {!! Form::label("code_melli","کد ملی",["class"=>"col-md-2 control-label"]) !!}
                            <div class="col-md-4">
                                {!! Form::input("text","code_melli",null,["class"=>"form-control","placeholder"=>"کد ملی"]) !!}
                                <div class="form-control-focus"></div>
                            </div>
                        </div>


                        <div class="form-group form-md-line-input">
                            {!! Form::label("status","وضعیت مدیر",["class"=>"col-md-2 control-label"]) !!}
                            <div class="col-md-4">
                                <div class="md-checkbox">
                                    <input id="status" value="1" class="md-check" name="status" type="checkbox">
                                    <label for="status">
                                        <span></span>
                                        <span class="check"></span>
                                        <span class="box"></span> فعال </label>
                                </div>
                            </div>
                        </div>


                        <div class="form-group form-md-line-input">
                            {!! Form::label("password","گذرواژه",["class"=>"col-md-2 control-label"]) !!}
                            <div class="col-md-4">
                                {!! Form::input("password","password",null,["class"=>"form-control","placeholder"=>"گذرواژه"]) !!}
                                <div class="form-control-focus"></div>
                            </div>
                        </div>


                        <div class="form-group form-md-line-input">
                            {!! Form::label("password_confirmation","تکرار گذرواژه",["class"=>"col-md-2 control-label"]) !!}
                            <div class="col-md-4">
                                {!! Form::input("password","password_confirmation",null,["class"=>"form-control","placeholder"=>"تکرار گذرواژه"]) !!}
                                <div class="form-control-focus"></div>
                            </div>
                        </div>


                    </div>
                    <div class="form-group form-md-line-input">
                        <div class="col-md-2">
                            <button class="btn btn-primary btn-block" id="addper" type="button">دسترسی ها</button>
                        </div>
                        <div class="col-md-10">
                            <figure id="optionsResult">

                                <div class="row roleList">

                                    <div class="col-md-6">
                                        <div class="form-group form-md-line-input">
                                            {!! Form::label("option1[]","دسترسی به بخش ",["class"=>"col-md-2 control-label","style"=>"text-align:right"]) !!}
                                            <div class="col-md-7">
                                                {!! Form::select('roles[]',$roles, old("option1.".""),["class"=>"form-control selectRtl"]) !!}

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group form-md-line-input">
                                            {!! Form::label("option2[]","مجوز های بخش",["class"=>"col-md-2 control-label","style"=>"text-align:right"]) !!}
                                            <div class="col-md-7">
                                                {!! Form::select('permissions[0][]',$permissions, old("option2.".""),["class"=>"form-control selectRtl","multiple"=>"multiple"]) !!}
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                </div>
                            </figure>
                        </div>
                    </div>
                    <hr>
                    <div class="form-actions fluid">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                {!! Form::submit("ذخیره",["class"=>"btn green"]) !!}
                                <a href="{{route("admins.index")}}">
                                    <button type="button" class="btn default">بازگشت</button>
                                </a>
                            </div>
                        </div>
                    </div>
                {!! Form::close() !!}
                <!-- END FORM-->
                </div>
            </div>


        </div>
    </div>


@endsection


@section('extra_script')
    {{--<script>--}}
    {{--$(document).ready(function () {--}}
    {{--$("#addper").click(function () {--}}
    {{--var war = $("#warranties").html();--}}
    {{--$("#per").append(per);--}}
    {{--$('#warranties').select2();--}}
    {{--});--}}
    {{--});--}}
    {{--</script>--}}

    <script>
        var count = 1;

        $("#addper").click(function () {


            var optionsList = ' <hr> <div class="row roleList"> <div class="col-md-6"> <div class="form-group form-md-line-input"> <label for="roles[]" class="col-md-2 control-label" style="text-align:right">دسترسی به بخش</label> <div class="col-md-7">';

            optionsList += '<select class="form-control selectRtl" id="option1[]" name="roles[]">';


            @foreach($roles as $k=>$v)

                optionsList += '<option value="{{$k}}">{{$v}}</option>';

            @endforeach


                optionsList += '</select> ';

            optionsList += '</div> </div>  </div> <div class="col-md-6"> <div class="form-group form-md-line-input"> <label for="permissions[' + count + '][]" class="col-md-2 control-label" style="text-align:right">مجوز های بخش</label> <div class="col-md-7"> <select multiple="multiple" class="form-control selectRtl" id="option2[]" name="permissions[' + count + '][]">';


            @foreach($permissions as $k=>$v)

                optionsList += '<option value="{{$k}}">{{$v}}</option>';

            @endforeach

                optionsList += '</select>\n</div> </div> </div> <hr> </div>';

            count++;
            $('#optionsResult .selectRtl').select2('destroy');

            $("#optionsResult").append(optionsList);

            $('#optionsResult .selectRtl').select2({
                dir: "rtl",
                placeholder: "انتخاب کنید ...",
                allowClear: !0,
            });

        });

    </script>
@endsection
