@extends('panel.layout')

@section('content')

    <div class="row">
        <div class="col-md-12">

            <div class="portlet box blue-hoki">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-building"></i>لیست ویژگی های محصولات
                    </div>
                    <div class="pull-left">
                        <a class="btn green" href="{{route('features.create')}}"> <i class="fa fa-plus"></i> افزودن
                            ویژگی جدید
                        </a>
                    </div>
                </div>
                <div class="portlet-body">
                    @include("errors.list")
                </div>
                <div class="portlet-body">
                    <!-- BEGIN TABLE-->
                    <table class="table table-striped table-bordered table-hover datatable" id="sample_1">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>نام ویژگی</th>
                            <th>نوع ویژگی</th>
                            <th>دسته بندی ویژگی</th>
                            <th>تاریخ</th>
                            <th>عملیات</th>
                        </tr>
                        </thead>


                        <tbody>
                        @foreach($features as $feature)

                            <tr>
                                <td>{{ $feature->id }}</td>
                                <td>{{ $feature->name }}   </td>
                                <td> <span class='label label-success'>{{ $feature->productType->name }}</span>  </td>
                                <td> <span class='label label-success'>{{ (isset($feature->featureCategory)   ? $feature->featureCategory->name : "دسته بندی نشده است!")  }}</span>  </td>
                                <td><span class='label label-primary'>ویرایش {{ jDate::forge($feature->updated_at)->format('Y/m/d H:i:s') }}</span> <span class='label label-primary'>ساخت : {{ jDate::forge($feature->created_at)->format('Y/m/d H:i:s') }}</span></td>
                                <td><div class="btn-group btn-group-lg" role="group"><a href="{{route('features.edit', $feature->id)}}" class="btn green">ویرایش</a></div><div class="btn-group btn-group-lg" role="group">

                                    {!! Form::open(['url'=>route('features.destroy', $feature->id),'method'=>'delete','class'=>'delete']) !!}
                                     <button type="submit" class="btn red"> حذف</button> {!! Form::close() !!} </div></td>
                            </tr>
                        @endforeach
                        </tbody>

                    </table>
                    <!-- END TABLE-->
                </div>
            </div>


        </div>
    </div>


@endsection