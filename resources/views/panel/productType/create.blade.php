@extends('panel.layout')
@section('title')
    افزودن نوع دسته بندی محصولات
@endsection
@section('content')


    <div class="row">
        <div class="col-md-12">

            <div class="portlet box blue-hoki">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-building"></i>افزودن نوع محصولات
                    </div>
                    <div class="tools">
                        <!--<a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                        <a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title=""> </a>
                        <a href="javascript:;" class="reload" data-original-title="" title=""> </a>
                        <a href="javascript:;" class="remove" data-original-title="" title=""> </a>-->
                    </div>
                </div>
                <div class="portlet-body">
                    @include("errors.list")
                </div>
                <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    {!! Form::open(["class"=>"form-horizontal","url"=>route('product_types.store'),"method"=>"post"]) !!}

                    <div class="form-body">

                        <div class="form-group form-md-line-input">
                            {!! Form::label("name","نوع محصول",["class"=>"col-md-2 control-label"]) !!}
                            <div class="col-md-4">
                                {!! Form::input("text","name",null,["class"=>"form-control","placeholder"=>"نام نوع محصول"]) !!}
                                <div class="form-control-focus"></div>
                            </div>
                        </div>
                        <hr>

                    </div>




                    <div class="form-group form-md-line-input">
                        {!! Form::label("status","وضعیت انتشار",["class"=>"col-md-2 control-label"]) !!}
                        <div class="col-md-4">
                            <div class="md-checkbox">

                                <input type="hidden" name="status" id="statusValue" value="0">
                                <input   id="status" value="1" class="mdcheck" type="checkbox"  >
                                <label for="status">
                                    <span></span>
                                    <span class="check"></span>
                                    <span class="box"></span> وضعیت انتشار </label>

                            </div>
                        </div>
                    </div>

                    <div class="form-actions fluid">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                {!! Form::submit("ذخیره",["class"=>"btn green"]) !!}
                                <a href="{{ route('product_types.index') }}">
                                    <button type="button" class="btn default">بازگشت</button>
                                </a>
                            </div>
                        </div>
                    </div>
                {!! Form::close() !!}
                <!-- END FORM-->
                </div>
            </div>


        </div>
    </div>


@endsection

@section('extra_script')

<script>

</script>
@endsection