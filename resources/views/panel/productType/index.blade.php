@extends('panel.layout')

@section('content')

    <div class="row">
        <div class="col-md-12">

            <div class="portlet box blue-hoki">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-building"></i>لیست نوع محصولات
                    </div>
                    <div class="pull-left">
                        <a class="btn green" href="{{route('product_types.create')}}"> <i class="fa fa-plus"></i> افزودن نوع دسته بندی جدید
                        </a>
                    </div>
                </div>
                <div class="portlet-body">
                    @include("errors.list")
                </div>
                <div class="portlet-body">
                    <!-- BEGIN TABLE-->
                    <table class="table table-striped table-bordered table-hover datatable" id="sample_1">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>نام نوع محصول</th>
                            <th>وضعیت</th>
                            <th>تاریخ </th>
                            <th>عملیات</th>
                        </tr>
                        </thead>


                        <tbody>
                        @foreach($productTypes as $productType)

                            <tr>
                                <td>{{ $productType->id }}</td>
                                <td>{{ $productType->name }}</td>
                                <td>{!! status($productType->status) !!}</td>
                                <td><span class='label label-primary'>ویرایش {{ jDate::forge($productType->updated_at)->format('Y/m/d H:i:s') }}</span> <span class='label label-primary'>ساخت : {{ jDate::forge($productType->created_at)->format('Y/m/d H:i:s') }}</span></td>
                                <td><div class="btn-group btn-group-lg" role="group"><a href="{{route('product_types.edit', $productType->id)}}" class="btn green">ویرایش</a> </div><div class="btn-group btn-group-lg" role="group">{!! Form::open(['url'=>route('product_types.destroy', $productType->id),'method'=>'delete','class'=>'delete']) !!}
                                        <button type="submit" class="btn red"> حذف</button> {!! Form::close() !!}</div> </td>
                            </tr>

                        @endforeach
                        </tbody>

                    </table>
                    <!-- END TABLE-->
                </div>
            </div>


        </div>
    </div>


@endsection