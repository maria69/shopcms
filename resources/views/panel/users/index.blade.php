@extends('panel.layout')

@section('content')

    <div class="row">
        <div class="col-md-12">

            <div class="portlet box blue-hoki">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-building"></i>کاربران
                    </div>
                    <div class="pull-left">
                        <a class="btn green" href="{{route('users.create')}}"> <i class="fa fa-plus"></i> ایجاد کاربر
                        </a>
                    </div>
                </div>
                <div class="portlet-body">
                    @include("errors.list")
                </div>
                <div class="portlet-body">
                    <!-- BEGIN TABLE-->
                    <table class="table table-striped table-bordered table-hover datatable" id="sample_1">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>نام و نام خانوادگی</th>
                            <th>شماره تماس</th>
                            <th>شماره موبایل</th>
                            <th>ایمیل</th>
                            <th>کد ملی</th>
                            <th>وضعیت کاربر</th>
                            <th>لیست سفارشات</th>
                            <th>لیست آدرس ها</th>
                            <th>عملیات</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)

                            <tr>
                                <td>{{ $user->id }}</td>
                                <td>{{ $user->first_name . $user->last_name}}</td>
                                <td>{{ $user->phone }}</td>
                                <td>{{ $user->mobile }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->code_melli }}</td>
                                <td>{!!  status($user->status) !!} </td>
                                <td> - </td>
                                <td> - </td>

                                {{--                                <td>{{ jDate::forge($row->created_at)->format('Y/m/d H:i:s') }}</td>--}}
                                <td>
                                    <div class="btn-group btn-group-lg" role="group"><a
                                                href="{{route('users.edit', $user->id)}}" class="btn green">ویرایش</a>
                                    </div>
                                    <div class="btn-group btn-group-lg"
                                         role="group">{!! Form::open(['url'=>route('users.destroy', $user->id),'method'=>'delete','class'=>'delete']) !!}
                                        <button type="submit" class="btn red"> حذف</button> {!! Form::close() !!} </div>
                                </td>
                            </tr>

                        @endforeach
                        </tbody>

                    </table>
                    <!-- END TABLE-->
                </div>
            </div>


        </div>
    </div>


@endsection