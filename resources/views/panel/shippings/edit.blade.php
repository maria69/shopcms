@extends('panel.layout')
@section("title") ویرایش شیوه ارسال @endsection
@section('content')


    <div class="row">
        <div class="col-md-12">

            <div class="portlet box blue-hoki">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-building"></i>ویرایش شیوه ارسال </div>
                    <div class="tools">
                        <!--<a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                        <a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title=""> </a>
                        <a href="javascript:;" class="reload" data-original-title="" title=""> </a>
                        <a href="javascript:;" class="remove" data-original-title="" title=""> </a>-->
                    </div>
                </div>
                <div class="portlet-body">
                    @include("errors.list")
                </div>
                <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    {!! Form::open(["class"=>"form-horizontal","url"=>route("shippings.update",$shipping->id),"method"=>"patch"]) !!}

                    <div class="form-body">

                        <div class="form-group form-md-line-input">
                            {!! Form::label("title","عنوان",["class"=>"col-md-2 control-label"]) !!}
                            <div class="col-md-4">
                                {!! Form::input("text","title",$shipping->title,["class"=>"form-control","placeholder"=>"عنوان شیوه حل و نقل"]) !!}
                                <div class="form-control-focus"> </div>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-4">
                                <button type="button" data-action="thumb" class="galleryPickeraBtn btn btn-info" style="width:100%">انتخاب تصویر برند</button>
                            </div>
                            <div class="col-md-4">
                                @if(!is_null($shipping->media))

                                    {!! Form::text("image_name",$shipping->media->name,['class'=>'form-control galleryInput','readonly'=>'readonly']) !!}
                                    <input class="imageHiddenInput" type="hidden" name="media_id" value="{{$shipping->media->id}}">
                                @else
                                    {!! Form::text("image_name",null,['class'=>'form-control galleryInput','readonly'=>'readonly']) !!}
                                    <input class="imageHiddenInput" type="hidden" name="media_id">
                                @endif
                            </div>
                            <div class="col-md-2">
                                <figure class="preview" style="max-width:100%">
                                    {!! thumb($shipping->media) !!}
                                </figure>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group form-md-line-input">
                            {!! Form::label("desc","توضیحات",["class"=>"col-md-2 control-label"]) !!}
                            <div class="col-md-8">
                                {!! Form::textarea("desc",$shipping->desc,['class'=>'form-group','id'=>'ckeditor']) !!}
                                <div class="form-control-focus"> </div>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group form-md-line-input">
                            {!! Form::label("status","وضعیت ",["class"=>"col-md-2 control-label"]) !!}
                            <div class="col-md-4">
                                <div class="md-checkbox">
                                    <input id="status" value="1" class="md-check" {{$shipping->status == 1 ? 'checked="checked"' : ""}} name="status" type="checkbox">
                                    <label for="status">
                                        <span></span>
                                        <span class="check"></span>
                                        <span class="box"></span>  وضعیت  </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions fluid">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                {!! Form::submit("ذخیره",["class"=>"btn green"]) !!}
                                <a href="{{route("shippings.index")}}"><button type="button" class="btn default">بازگشت</button></a>
                            </div>
                        </div>
                    </div>
                {!! Form::close() !!}
                <!-- END FORM-->
                </div>
            </div>


        </div>
    </div>


@endsection