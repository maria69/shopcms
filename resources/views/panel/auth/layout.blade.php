<!DOCTYPE html>
<html lang="fa">
<head>
    <meta charset="utf-8">
    <title> @yield("title") </title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />

    <link href="/panelassets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />+
    <link href="/panelassets/bootstrap-rtl.min.css" rel="stylesheet" type="text/css" />
    <link href="/panelassets/global/css/components-md.min.css" rel="stylesheet" id="style_components" type="text/css" />
    <link href="/panelassets/global/css/plugins-md.min.css" rel="stylesheet" type="text/css" />
    <link href="/panelassets/pages/css/login-2.min.css" rel="stylesheet" type="text/css" />
    <link href="/css/rtl.css" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="favicon.jpg" />
    <!-- END HEAD -->
</head>
<body class="login">
<!-- BEGIN LOGO -->
<div class="logo">
    {{--<img src="/img/logo.png"/>--}}
</div>
<!-- END LOGO -->


@yield('authForm')


<div class="copyright hide"> <?php echo date("Y"); ?> © MFT</div>
<!-- END LOGIN -->
<!--[if lt IE 9]>
<script src="/panelassets/global/plugins/respond.min.js"></script>
<script src="/panelassets/global/plugins/ie8.fix.min.js"></script>
<![endif]-->
<script src="/panelassets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="/panelassets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="/vendor/jsvalidation/js/jsvalidation.js"></script>
<style>.form-group.form-md-line-input { margin: 0 0 0px; }  .has-success .md-checkbox label > .box, .has-success .md-checkbox label > .check, .has-success.md-checkbox label > .box, .has-success.md-checkbox label > .check {  border-color: #edf4f8;  }.md-radio label > .check {background: #fff;}.md-radio label > .box {border: 2px solid #333;}.md-radio {margin-left: 25px;}</style>

</body>
</html>