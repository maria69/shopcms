
@extends("panel.auth.layout")

@section("title") ورود به حساب کاربری @endsection

@section("authForm")


<div class="content">
@include("errors.listClient")
    <!-- BEGIN LOGIN FORM -->
    {!! Form::open(["url"=>route("adminLogin"),"method"=>"post","id"=>"login"]) !!}
    <div class="form-title">
        <span class="form-title">مدیریت</span>
        <span class="form-subtitle">خوش امدید</span>
    </div>
    <div class="alert alert-danger display-hide">
        <button class="close" data-close="alert"></button>
        <span> ..</span>
    </div>
    <div class="form-group">
        <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
        <label class="control-label visible-ie8 visible-ie9">پست الکترونیک</label>
        {!! Form::text("email",null,["class"=>"form-control form-control-solid placeholder-no-fix","placeholder"=>"پست الکترونیک","id"=>"email"]) !!}
    </div>
    <div class="form-group">
        <label class="control-label visible-ie8 visible-ie9">گذرواژه</label>
        {!! Form::input("password","password",null,["class"=>"form-control form-control-solid placeholder-no-fix","autocomplete"=>"off","placeholder"=>"گذرواژه"]) !!}
    </div>

    <div class="form-actions">
        {!! Form::submit("ورود",["class"=>"btn red btn-block uppercase"]) !!}
    </div>


    <div class="form-actions">
        <a href="/auth/forgot"><button type="button" id="back-btn" class="btn btn-default pull-left">بازگشت به وب سایت</button></a>
        <a href="/auth/register" class="btn-primary btn">فراموشی رمز عبور</a>
    </div>
    {!! Form::close() !!}
<!-- END LOGIN FORM -->

<!-- END REGISTRATION FORM -->
</div>
@endsection