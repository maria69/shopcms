@extends('panel.layout')
@section("title") پنل مدیریت @endsection
@section('content')



    <div class="row">


        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="dashboard-stat2 ">
                <div class="display">
                    <div class="number">
                        <h3 class="font-green-sharp">
                            <span data-counter="counterup" data-value="{{$productCount}}">0</span>
                            <small class="font-green-sharp"> محصولات </small>
                        </h3>
                        <small>تعداد محصولات</small>
                    </div>
                    <div class="icon">
                        <i class="fa fa-shopping-cart"></i>
                    </div>
                </div>

            </div>
        </div>



        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="dashboard-stat2 ">
                <div class="display">
                    <div class="number">
                        <h3 class="font-blue-sharp">
                            <span data-counter="counterup" data-value="{{$categoryCount}}">0</span>
                            <small class="font-blue-sharp"> دسته بندی ها </small>
                        </h3>
                        <small>تعداد دسته بندی ها</small>
                    </div>
                    <div class="icon">
                        <i class="fa fa-sitemap"></i>
                    </div>
                </div>

            </div>
        </div>





        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="dashboard-stat2 ">
                <div class="display">
                    <div class="number">
                        <h3 class="font-purple-sharp">
                            <span data-counter="counterup" data-value="50">0</span>
                            <small class="font-purple-sharp"> کاربر </small>
                        </h3>
                        <small>تعداد کاربران</small>
                    </div>
                    <div class="icon">
                        <i class="fa fa-users"></i>
                    </div>
                </div>

            </div>
        </div>


        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="dashboard-stat2 ">
                <div class="display">
                    <div class="number">
                        <h3 class="font-red-sharp">
                            <span data-counter="counterup" data-value="2000">0</span>
                            <small class="font-red-sharp"> فاکتور </small>
                        </h3>
                        <small>تعدا فاکتور های فروش</small>
                    </div>
                    <div class="icon">
                        <i class="fa fa-pencil-square-o"></i>
                    </div>
                </div>

            </div>
        </div>



        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="dashboard-stat2 ">
                <div class="display">
                    <div class="number">
                        <h3 class="font-green-sharp">
                            <span data-counter="counterup" data-value="58460">0</span>
                            <small class="font-green-sharp"> تراکنش بانکی </small>
                        </h3>
                        <small>تعداد تراکنش درگاه پرداخت اینترنتی</small>
                    </div>
                    <div class="icon">
                        <i class="fa fa-usd"></i>
                    </div>
                </div>

            </div>
        </div>



        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="dashboard-stat2 ">
                <div class="display">
                    <div class="number">
                        <h3 class="font-blue-sharp">
                            <span data-counter="counterup" data-value="20">0</span>
                            <small class="font-blue-sharp"> دسته بندی ها </small>
                        </h3>
                        <small>تعداد دسته بندی ها</small>
                    </div>
                    <div class="icon">
                        <i class="fa fa-sitemap"></i>
                    </div>
                </div>

            </div>
        </div>



    </div>








                <div class="row">





                    <div class="col-md-12">

                        <div class="portlet box blue-hoki">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-bar-chart"></i>آمار بازدید امسال وب سایت </div>
                                <div class="pull-left">
                                    <a class="btn green" href="/dashboard/admin/statistics"> <i class="fa fa-list"></i> نمایش امار کامل </a>
                                </div>

                            </div>

                            <div class="portlet-body">

                                <div id="areachart" style="height:500px;"></div>

                            </div>
                        </div>


                    </div>


                    <script>

                        // var jsonStatistic = JSON.parse('');
                        //
                        // var chartData = [];
                        //
                        // var months = {1:"فروردین",2:"اردیبهشت",3:"خرداد",4:"تیر",5:"مرداد",6:"شهریور",7:"مهر",8:"آبان",9:"آذر",10:"دی",11:"بهمن",12:"اسفند"};
                        //
                        // for (i in jsonStatistic){
                        //     chartData.push({y:months[i],a:jsonStatistic[i]});
                        // }
                        //
                        // console.log(jsonStatistic);
                    </script>

                </div>



@endsection