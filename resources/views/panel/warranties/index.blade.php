@extends('panel.layout')

@section('content')

    <div class="row">
        <div class="col-md-12">

            <div class="portlet box blue-hoki">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-building"></i>لیست گارانتی ها
                    </div>
                    <div class="pull-left">
                        <a class="btn green" href="{{route('warranties.create')}}"> <i class="fa fa-plus"></i> افزودن
                            گارانتی جدید
                        </a>
                    </div>
                </div>
                <div class="portlet-body">
                    @include("errors.list")
                </div>
                <div class="portlet-body">
                    <!-- BEGIN TABLE-->
                    <table class="table table-striped table-bordered table-hover datatable" id="sample_1">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>کمپانی</th>
                            <th>مدت گارانتی</th>
                            <th>وضعیت</th>
                            <th>تاریخ</th>
                            <th>عملیات</th>
                        </tr>
                        </thead>


                        <tbody>
                        @foreach($warranties as $warranty)

                            <tr>
                                <td>{{ $warranty->id }}</td>
                                <td>{{ $warranty->company->name }}</td>
                                <td>{{ $warranty->expire_at}} ماه</td>
                                <td>{!! status($warranty->status) !!}</td>
                                <td><span class='label label-primary'>ویرایش {{ jDate::forge($warranty->updated_at)->format('Y/m/d H:i:s') }}</span> <span class='label label-primary'>ساخت : {{ jDate::forge($warranty->created_at)->format('Y/m/d H:i:s') }}</span></td>
                                <td><div class="btn-group btn-group-lg" role="group"><a href="{{route('warranties.edit', $warranty->id)}}" class="btn green">ویرایش</a></div><div class="btn-group btn-group-lg" role="group">
                                    {!! Form::open(['url'=>route('warranties.destroy', $warranty->id),'method'=>'delete','class'=>'delete']) !!}
                                     <button type="submit" class="btn red"> حذف</button> {!! Form::close() !!} </div></td>
                            </tr>
                        @endforeach
                        </tbody>

                    </table>
                    <!-- END TABLE-->
                </div>
            </div>


        </div>
    </div>


@endsection