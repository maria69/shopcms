@extends('panel.layout')

@section('title')
    افزودن دسته بندی محصولات
@endsection
@section('content')


    <div class="row">
        <div class="col-md-12">

            <div class="portlet box blue-hoki">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-building"></i>به روز رسانی گارانتی
                    </div>
                    <div class="tools">
                        <!--<a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                        <a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title=""> </a>
                        <a href="javascript:;" class="reload" data-original-title="" title=""> </a>
                        <a href="javascript:;" class="remove" data-original-title="" title=""> </a>-->
                    </div>
                </div>
                <div class="portlet-body">
                    @include("errors.list")
                </div>
                <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    {!! Form::open(["class"=>"form-horizontal","url"=>route('categories.update',$Warranty->id),"method"=>"patch"]) !!}

                    <div class="form-body">

                        <div class="form-group form-md-line-input">
                            {!! Form::label("name","مدت زمان گارانتی",["class"=>"col-md-2 control-label"]) !!}
                            <div class="col-md-4">
                                {!! Form::input("text","name",$Warranty->expire_at,["class"=>"form-control","placeholder"=>"مدت زمان گارانتی"]) !!}
                                <div class="form-control-focus"></div>
                            </div>
                        </div>




                        <div class="form-group form-md-line-input">
                            {!! Form::label("parent_id","دسته بندی پدر",["class"=>"col-md-2 control-label"]) !!}
                            <div class="col-md-4">
                                {!! Form::select('parent_id',$warranties , $warranty->company,["class"=>"form-control selectRtl"]) !!}

                            </div>
                        </div>
                        <hr>

                    </div>
                    <div class="form-actions fluid">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                {!! Form::submit("ذخیره",["class"=>"btn green"]) !!}
                                <a href="{{route('warranties.index')}}">
                                    <button type="button" class="btn default">بازگشت</button>
                                </a>
                            </div>
                        </div>
                    </div>
                {!! Form::close() !!}
                <!-- END FORM-->
                </div>
            </div>


        </div>
    </div>


@endsection



