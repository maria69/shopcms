@extends('panel.layout')
@section("title") ساخت آپشن @endsection
@section('content')


    <div class="row">
        <div class="col-md-12">

            <div class="portlet box blue-hoki">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-building"></i>افزودن آپشن </div>
                    <div class="tools">
                        <!--<a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                        <a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title=""> </a>
                        <a href="javascript:;" class="reload" data-original-title="" title=""> </a>
                        <a href="javascript:;" class="remove" data-original-title="" title=""> </a>-->
                    </div>
                </div>
                <div class="portlet-body">
                    @include("errors.list")
                </div>
                <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    {!! Form::open(["class"=>"form-horizontal","url"=>route("productOptions.store"),"method"=>"post"]) !!}

                    <div class="form-body">
                        <div class="form-group form-md-line-input">
                            {!! Form::label("product_options_category_id","انتخاب دسته بندی",["class"=>"col-md-2 control-label"]) !!}
                            <div class="col-md-4">
                                {!! Form::select('product_option_category_id',$categories , 0,["class"=>"form-control selectRtl","id"=>"typeOption"]) !!}
                            </div>
                        </div>
                        <hr>
                        <div class="form-group form-md-line-input">
                            {!! Form::label("name","عنوان آپشن",["class"=>"col-md-2 control-label"]) !!}
                            <div class="col-md-4">
                                {!! Form::input("text","name",null,["class"=>"form-control","placeholder"=>"عنوان آپشن"]) !!}
                                <div class="form-control-focus"> </div>
                            </div>
                        </div>
                        <hr>

                        <div class="form-group form-md-line-input" id="type1">
                            {!! Form::label("name","انتخاب رنگ",["class"=>"col-md-2 control-label"]) !!}
                            <div class="col-md-4">

                                {!! Form::input("text","value","",["class"=>"form-control","placeholder"=>"انتخاب رنگ",'id'=>'mycp']) !!}
                                <div class="form-control-focus"> </div>
                            </div>
                        </div>
                        <div class="form-group form-md-line-input" id="type3">
                            {!! Form::label("name","محتوا",["class"=>"col-md-2 control-label"]) !!}
                            <div class="col-md-4">

                                {!! Form::input("text","value","",["class"=>"form-control","placeholder"=>"محتوا"]) !!}
                                <div class="form-control-focus"> </div>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group form-md-line-input" id="type2">
                            <div class="col-md-8 col-md-offset-2">


                                <div class="row">
                                    <div class="col-md-3">

                                        <button type="button" data-action="thumb" class="galleryPickeraBtn btn btn-info"
                                                style="width:100%">انتخاب فایل چند رسانه ای برای دسته بندی
                                        </button>

                                    </div>

                                    <div class="col-md-3">

                                        {!! Form::text("image_name",null,['class'=>'form-control galleryInput','readonly'=>'readonly']) !!}
                                        <input class="imageHiddenInput" type="hidden" name="media_id" value="">

                                    </div>

                                    <div class="col-md-3">
                                        <figure class="preview" style="max-width:100%">

                                        </figure>
                                    </div>
                                </div>


                            </div>
                        </div>


                        <hr>

                    </div>


                    <div class="form-actions fluid">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                {!! Form::submit("ذخیره",["class"=>"btn green"]) !!}
                                <a href="{{route("productOptions.index")}}"><button type="button" class="btn default">بازگشت</button></a>
                            </div>
                        </div>
                    </div>
                {!! Form::close() !!}
                <!-- END FORM-->
                </div>
            </div>


        </div>
    </div>


@endsection


@section("extra_script")


    <script>

        $("#type1,#type3").hide();

        $("#typeOption").change(function(e){

            var pid = $(this).val();

            $.ajax({
                url     : '/dashboard909/getTypeFromOption',
                method  : 'post',
                data    : {id:pid},
                success : function(data){
                    $("#type1,#type2,#type3").hide(100,function(){
                        $("#type"+data.type).show(100);
                    });
                }
            });

        });

    </script>


@endsection