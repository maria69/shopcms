@extends('panel.layout')
@section("title") ویرایش آپشن @endsection
@section('content')


    <div class="row">
        <div class="col-md-12">

            <div class="portlet box blue-hoki">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-building"></i>ویرایش آپشن
                    </div>
                    <div class="tools">
                        <!--<a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                        <a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title=""> </a>
                        <a href="javascript:;" class="reload" data-original-title="" title=""> </a>
                        <a href="javascript:;" class="remove" data-original-title="" title=""> </a>-->
                    </div>
                </div>
                <div class="portlet-body">
                    @include("errors.list")
                </div>
                <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    {!! Form::open(["class"=>"form-horizontal","url"=>route("productOptions.update",$ProductOption->id),"method"=>"patch"]) !!}
                    <div class="form-body">
                        <div class="form-group form-md-line-input">
                            {!! Form::label("product_options_category_id","عنوان دسته بندی",["class"=>"col-md-2 control-label"]) !!}
                            <div class="col-md-4">
                                {{--{!! Form::select('product_option_categories',$ProductOptionCategories,[$ProOpCat->id],["class"=>"col-md-2 control-label"]) !!}--}}

                                {!! Form::select("product_options_category_id",$ProductOptionCategories,[$ProOpCat->id],["class"=>"form-control "]) !!}


                                {{--{!! Form::label("product_options_category_id",$ProductOption->category->name,["class"=>"col-md-2 control-label"]) !!}--}}
                                {{--{!! Form::input("hidden","product_options_category_id",$ProductOption->category->id,["class"=>"form-control","placeholder"=>$ProductOption->category->name])  !!}--}}
                            </div>
                        </div>
                        <hr>
                        <div class="form-group form-md-line-input">
                            {!! Form::label("name","نام آپشن",["class"=>"col-md-2 control-label"]) !!}
                            <div class="col-md-4">
                                {!! Form::input("text","name",$ProductOption->name,["class"=>"form-control","placeholder"=>"نام آپشن"]) !!}
                                <div class="form-control-focus"></div>
                            </div>
                        </div>
                        @if($ProductOption->category->type == 1)
                        <hr>
                        <div class="form-group form-md-line-input">
                            {!! Form::label("name","انتخاب رنگ",["class"=>"col-md-2 control-label"]) !!}
                            <div class="col-md-4">
                                {!! Form::input("text","value","$ProductOption->value",["class"=>"form-control","placeholder"=>"انتخاب رنگ",'id'=>'mycp']) !!}
                                <div class="form-control-focus"></div>
                            </div>
                        </div>
                        @elseif($ProductOption->category->type == 3)
                        <hr>
                        <div class="form-group form-md-line-input">
                            {!! Form::label("name","محتوا",["class"=>"col-md-2 control-label"]) !!}
                            <div class="col-md-4">

                                {!! Form::input("text","value","",["class"=>"form-control","placeholder"=>"محتوا"]) !!}
                                <div class="form-control-focus"></div>
                            </div>
                        </div>
                        @elseif($ProductOption->category->type == 2)
                        <hr>
                        <div class="row">
                            <div class="col-md-4">

                                <button type="button" data-action="thumb" class="galleryPickeraBtn btn btn-info"
                                        style="width:100%">انتخاب تصویر آپشن
                                </button>

                            </div>

                            <div class="col-md-4">

                                @if(!is_null($ProductOption->media))

                                    {!! Form::text("image_name",$ProductOption->media->name,['class'=>'form-control galleryInput','readonly'=>'readonly']) !!}
                                    <input class="imageHiddenInput" type="hidden" name="media_id"
                                           value="{{$ProductOption->media->id}}">

                                @else

                                    {!! Form::text("image_name",null,['class'=>'form-control galleryInput','readonly'=>'readonly']) !!}
                                    <input class="imageHiddenInput" type="hidden" name="media_id">

                                @endif
                            </div>
                            <div class="col-md-2">
                                <figure class="preview" style="max-width:100%">

                                    {!! thumb($ProductOption->media) !!}

                                </figure>
                            </div>
                        </div>
                        @endif
                        <hr>
                    </div>
                    <div class="form-actions fluid">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                {!! Form::submit("ذخیره",["class"=>"btn green"]) !!}
                                <a href="{{route("productOptions.index")}}">
                                    <button type="button" class="btn default">بازگشت</button>
                                </a>
                            </div>
                        </div>
                    </div>
                {!! Form::close() !!}
                <!-- END FORM-->
                </div>
            </div>


        </div>
    </div>


@endsection