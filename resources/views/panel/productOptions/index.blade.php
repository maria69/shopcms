@extends('panel.layout')

@section('content')

    <div class="row">
        <div class="col-md-12">

            <div class="portlet box blue-hoki">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-building"></i>لیست آپشن ها
                    </div>
                    <div class="pull-left">
                        <a class="btn green" href="{{route('productOptions.create')}}"> <i class="fa fa-plus"></i>افزودن آپشن </a>
                    </div>
                </div>
                <div class="portlet-body">
                    @include("errors.list")
                </div>
                <div class="portlet-body">
                    <!-- BEGIN TABLE-->
                    <table class="table table-striped table-bordered table-hover datatable" id="sample_1">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>عنوان آپشن</th>
                            <th>نوع</th>
                            <th>دسته بندی</th>
                            <th>تصویر - مقدار</th>
                            <th>تاریخ</th>
                            <th>عملیات</th>
                        </tr>
                        </thead>
                        <tbody>


<!--                        --><?php //dd($productOptions);?>
                        @foreach($productOptions as $option)

                            <tr>
                                <td>{{ $option->id }}</td>
                                <td>{{ $option->name }}</td>
                                <td>{!! prdOptionType($option->productoptioncategory->type ) !!} </td>
                                <td>{{ $option->productoptioncategory->name}}</td>
                                <td> @if(isset($option->media)){!!  thumb($option->media,1) !!} @else @if($option->productoptioncategory->type == 1) <span style="padding: 0px 10px; margin-top: 40px !important;border-radius: 50%; background-color: {{$option->value}}"></span> @else {{$option->value}} @endif @endif  </td>
                                <td><span class='label label-primary'>ویرایش {{ jDate::forge($option->updated_at)->format('Y/m/d H:i:s') }}</span> <span class='label label-primary'>ساخت : {{ jDate::forge($option->created_at)->format('Y/m/d H:i:s') }}</span></td>
                                <td><div class="btn-group btn-group-lg" role="group"><a href="{{route('productOptions.edit', $option->id)}}" class="btn green">ویرایش</a></div><div class="btn-group btn-group-lg" role="group">
                                        {!! Form::open(['url'=>route('productOptions.destroy', $option->id),'method'=>'delete','class'=>'delete']) !!}
                                        <button type="submit" class="btn red"> حذف</button> {!! Form::close() !!} </div></td>
                            </tr>

                        @endforeach
                        </tbody>

                    </table>
                    <!-- END TABLE-->
                </div>
            </div>


        </div>
    </div>


@endsection