@extends('panel.layout')

@section('content')

    <div class="row">
        <div class="col-md-12">

            <div class="portlet box blue-hoki">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-building"></i>شیوه های پرداخت
                    </div>
                    <div class="pull-left">
                        <a class="btn green" href="{{route('payments.create')}}"> <i class="fa fa-plus"></i>ایجاد شیوه
                            پرداخت</a>
                    </div>
                </div>
                <div class="portlet-body">
                    @include("errors.list")
                </div>
                <div class="portlet-body">
                    <!-- BEGIN TABLE-->
                    <table class="table table-striped table-bordered table-hover datatable" id="sample_1">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>عنوان</th>
                            <th>تصویر</th>
                            <th>تاریخ</th>
                            <th>توضیحات</th>
                            <th>عملیات</th>
                        </tr>
                        </thead>


                        <tbody>
                        @foreach($payments as $payment)

                            <tr>
                                <td>{{ $payment->id }}</td>
                                <td>{{ $payment->title }}</td>
                                <td> {!!  thumb($payment->media,1) !!}  </td>
                                <td>
                                <span class='label label-primary'>ویرایش {{ jDate::forge($payment->updated_at)->format('Y/m/d H:i:s') }}</span>
                                <span class='label label-primary'>ساخت : {{ jDate::forge($payment->created_at)->format('Y/m/d H:i:s') }}</span>
                                </td>
                                <td> {!!  $payment->description!!}</td>
                                <td>
                                    <div class="btn-group btn-group-lg" role="group"><a
                                                href="{{route('payments.edit', $payment->id)}}" class="btn green">ویرایش</a>
                                    </div>
                                    <div class="btn-group btn-group-lg" role="group">

                                        {!! Form::open(['url'=>route('payments.destroy', $payment->id),'method'=>'delete','class'=>'delete']) !!}
                                        <button type="submit" class="btn red"> حذف</button> {!! Form::close() !!} </div>
                                </td>
                            </tr>

                        @endforeach
                        </tbody>

                    </table>
                    <!-- END TABLE-->
                </div>
            </div>


        </div>
    </div>


@endsection