@extends('panel.layout')
@section("title") ساخت شیوه پرداخت @endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue-hoki">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-building"></i>افزودن شیوه پرداخت
                    </div>
                    <div class="tools">
                        <!--<a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                        <a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title=""> </a>
                        <a href="javascript:;" class="reload" data-original-title="" title=""> </a>
                        <a href="javascript:;" class="remove" data-original-title="" title=""> </a>-->
                    </div>
                </div>
                <div class="portlet-body">
                    @include("errors.list")
                </div>
                <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    {!! Form::open(["class"=>"form-horizontal","url"=>route("payments.store"),"method"=>"post"]) !!}

                    <div class="form-body">

                        <div class="form-group form-md-line-input">
                            {!! Form::label("title","عنوان",["class"=>"col-md-2 control-label"]) !!}
                            <div class="col-md-4">
                                {!! Form::input("text","title",null,["class"=>"form-control","placeholder"=>"عنوان"]) !!}
                                <div class="form-control-focus"></div>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            {!! Form::label("","آیکن شیوه پرداخت",["class"=>"col-md-2 control-label"]) !!}
                            <div class="col-md-8 ">
                                <div class="row">

                                    <div class="col-md-4">

                                        <button type="button" data-action="thumb" class="galleryPickeraBtn btn btn-info"
                                                style="width:100%">انتخاب تصویر
                                        </button>

                                    </div>

                                    <div class="col-md-4">

                                        {!! Form::text("image_name",null,['class'=>'form-control galleryInput','readonly'=>'readonly']) !!}
                                        <input class="imageHiddenInput" type="hidden" name="media_id" value="NULL">

                                    </div>

                                    <div class="col-md-2">
                                        <figure class="preview" style="max-width:100%">
                                            <img src="" class="img-responsive thumb">
                                        </figure>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <hr>
                        <div class="form-group form-md-line-input">
                            {!! Form::label("desc","توضیحات",["class"=>"col-md-2 control-label"]) !!}
                            <div class="col-md-8">
                                {!! Form::textarea("desc",null,['class'=>'form-group','id'=>'ckeditor']) !!}
                                <div class="form-control-focus"></div>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group form-md-line-input">
                            {!! Form::label("status","وضعیت نمایش ",["class"=>"col-md-2 control-label"]) !!}
                            <div class="col-md-4">
                                <div class="md-checkbox">
                                    <input id="status" value="1" class="md-check" name="status" type="checkbox">
                                    <label for="status">
                                        <span></span>
                                        <span class="check"></span>
                                        <span class="box"></span> وضعیت نمایش </label>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="form-actions fluid">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                {!! Form::submit("ذخیره",["class"=>"btn green"]) !!}
                                <a href="{{route("payments.index")}}">
                                    <button type="button" class="btn default">بازگشت</button>
                                </a>
                            </div>
                        </div>
                    </div>
                {!! Form::close() !!}
                <!-- END FORM-->
                </div>
            </div>


        </div>
    </div>


@endsection