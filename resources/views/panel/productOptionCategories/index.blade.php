@extends('panel.layout')

@section('content')

    <div class="row">
        <div class="col-md-12">

            <div class="portlet box blue-hoki">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-building"></i>لیست دسته بندی های آپشن های محصول
                    </div>
                    <div class="pull-left">
                        <a class="btn green" href="{{route('productOptionCategories.create')}}"> <i class="fa fa-plus"></i>افزودن  </a>
                    </div>
                </div>
                <div class="portlet-body">
                    @include("errors.list")
                </div>
                <div class="portlet-body">
                    <!-- BEGIN TABLE-->
                    <table class="table table-striped table-bordered table-hover datatable" id="sample_1">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>عنوان دسته بندی</th>
                            <th>نوع دسته بندی</th>
                            <th>تاریخ</th>
                            <th>عملیات</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($ProductOptionCategories as $category)

                            <tr>
                                <td>{{ $category->id }}</td>
                                <td>{{ $category->name }}</td>
                                <td> {!!  prdOptionType($category->type) !!}  </td>
                                <td><span class='label label-primary'>ویرایش {{ jDate::forge($category->updated_at)->format('Y/m/d H:i:s') }}</span> <span class='label label-primary'>ساخت : {{ jDate::forge($category->created_at)->format('Y/m/d H:i:s') }}</span></td>
                                <td><div class="btn-group btn-group-lg" role="group"><a href="{{route('productOptionCategories.edit', $category->id)}}" class="btn green">ویرایش</a></div><div class="btn-group btn-group-lg" role="group">

                                        {!! Form::open(['url'=>route('productOptionCategories.destroy', $category->id),'method'=>'delete','class'=>'delete']) !!}
                                        <button type="submit" class="btn red"> حذف</button> {!! Form::close() !!} </div></td>
                            </tr>

                        @endforeach
                        </tbody>

                    </table>
                    <!-- END TABLE-->
                </div>
            </div>


        </div>
    </div>


@endsection