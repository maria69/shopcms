@extends('panel.layout')
@section("title") ساخت شرکت @endsection
@section('content')


    <div class="row">
        <div class="col-md-12">

            <div class="portlet box blue-hoki">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-building"></i>ویرایش شرکت </div>
                    <div class="tools">
                        <!--<a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                        <a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title=""> </a>
                        <a href="javascript:;" class="reload" data-original-title="" title=""> </a>
                        <a href="javascript:;" class="remove" data-original-title="" title=""> </a>-->
                    </div>
                </div>
                <div class="portlet-body">
                    @include("errors.list")
                </div>
                <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    {!! Form::open(["class"=>"form-horizontal","url"=>route("companies.update",$company->id),"method"=>"put"]) !!}

                    <div class="form-body">

                        <div class="form-group form-md-line-input">
                            {!! Form::label("name","نام شرکت",["class"=>"col-md-2 control-label"]) !!}
                            <div class="col-md-4">
                                {!! Form::input("text","name",$company->name,["class"=>"form-control","placeholder"=>"نام شرکت"]) !!}
                                <div class="form-control-focus"> </div>
                            </div>
                        </div>
                        <hr>


                    </div>


                    <div class="form-actions fluid">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                {!! Form::submit("ویرایش",["class"=>"btn green"]) !!}
                                <a href="{{route("companies.index")}}"><button type="button" class="btn default">بازگشت</button></a>
                            </div>
                        </div>
                    </div>
                {!! Form::close() !!}
                <!-- END FORM-->
                </div>
            </div>


        </div>
    </div>


@endsection