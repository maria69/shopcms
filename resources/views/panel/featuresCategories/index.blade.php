@extends('panel.layout')

@section('content')

    <div class="row">
        <div class="col-md-12">

            <div class="portlet box blue-hoki">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-building"></i>لیست برند ها
                    </div>
                    <div class="pull-left">
                        <a class="btn green" href="{{route('feature_categories.create')}}"> <i class="fa fa-plus"></i>افزودن دسته نبدی </a>
                    </div>
                </div>
                <div class="portlet-body">
                    @include("errors.list")
                </div>
                <div class="portlet-body">
                    <!-- BEGIN TABLE-->
                    <table class="table table-striped table-bordered table-hover datatable" id="sample_1">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>نام دسته نبدی ویژگی</th>
                            <th>تاریخ</th>
                            <th>عملیات</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($fCategories as $fCategory)

                            <tr>
                                <td>{{ $fCategory->id }}</td>
                                <td>{{ $fCategory->name }}</td>
                                <td><span class='label label-primary'>ویرایش {{ jDate::forge($fCategory->updated_at)->format('Y/m/d H:i:s') }}</span> <span class='label label-primary'>ساخت : {{ jDate::forge($fCategory->created_at)->format('Y/m/d H:i:s') }}</span></td>
                                <td><div class="btn-group btn-group-lg" role="group"><a href="{{route('feature_categories.edit', $fCategory->id)}}" class="btn green">ویرایش</a></div><div class="btn-group btn-group-lg" role="group">

                                        {!! Form::open(['url'=>route('feature_categories.destroy', $fCategory->id),'method'=>'delete','class'=>'delete']) !!}
                                        <button type="submit" class="btn red"> حذف</button> {!! Form::close() !!} </div></td>
                            </tr>

                        @endforeach
                        </tbody>

                    </table>
                    <!-- END TABLE-->
                </div>
            </div>


        </div>
    </div>


@endsection