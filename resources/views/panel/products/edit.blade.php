@extends('panel.layout')



@section('content')

    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#productInfo">اطلاعات محصول</a></li>
        <li><a data-toggle="tab" href="#productPropertiesInfo">خصوصیات محصول</a></li>
    </ul>
    {!! Form::open(["class"=>"form-horizontal","url"=>route("products.update",$product->id),"method"=>"put","id"=>"productCreate"]) !!}
    <div class="tab-content">
        <div id="productInfo" class="tab-pane fade in active">
            <div class="row">
                <div class="col-md-12">

                    <div class="portlet box blue-hoki">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-building"></i>ویرایش محصول
                            </div>
                            <div class="tools">
                                <!--<a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                                <a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title=""> </a>
                                <a href="javascript:;" class="reload" data-original-title="" title=""> </a>
                                <a href="javascript:;" class="remove" data-original-title="" title=""> </a>-->
                            </div>
                        </div>
                        <div class="portlet-body">
                            @include("errors.list")
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->


                            <div class="form-body">

                                <div class="form-group form-md-line-input">
                                    {!! Form::label("name","نام محصول",["class"=>"col-md-2 control-label"]) !!}
                                    <div class="col-md-4">
                                        {!! Form::input("text","name",$product->name,["class"=>"form-control","placeholder"=>"نام محصول"]) !!}
                                        <div class="form-control-focus"></div>
                                    </div>
                                </div>

                                <div class="form-group form-md-line-input">
                                    {!! Form::label("en_name","نام انگلیسی محصول",["class"=>"col-md-2 control-label"]) !!}
                                    <div class="col-md-4">
                                        {!! Form::input("text","en_name",$product->en_name,["class"=>"form-control","placeholder"=>"نام انگلیسی محصول"]) !!}
                                        <div class="form-control-focus"></div>
                                    </div>
                                </div>


                                <div class="form-group form-md-line-input">

                                    <div class="col-md-2 col-md-offset-2">
                                        <button type="button" class="galleryPickeraBtn btn btn-info" style="width:100%">
                                            افزودن تصویر در متن محصول
                                        </button>
                                    </div>
                                </div>


                                <div class="form-group form-md-line-input">
                                    {!! Form::label("description","توضیحات",["class"=>"col-md-2 control-label"]) !!}
                                    <div class="col-md-8">
                                        {!! Form::textarea("description",$product->description,['class'=>'form-group','id'=>'ckeditor']) !!}
                                        <div class="form-control-focus"></div>
                                    </div>
                                </div>


                                <div class="form-group form-md-line-input">
                                    {!! Form::label("short_description","توضیحات کوتاه",["class"=>"col-md-2 control-label"]) !!}
                                    <div class="col-md-8">
                                        {!! Form::textarea("short_description",$product->short_description,['class'=>'form-control','rows'=>1]) !!}
                                        <div class="form-control-focus"></div>
                                    </div>
                                </div>


                                <div class="form-group form-md-line-input">
                                    {!! Form::label("meta","توضیحات seo",["class"=>"col-md-2 control-label"]) !!}
                                    <div class="col-md-8">
                                        {!! Form::textarea("meta",$product->meta,['class'=>'form-control','rows'=>1]) !!}
                                        <div class="form-control-focus"></div>
                                    </div>
                                </div>


                                <div class="form-group form-md-line-input">
                                    {!! Form::label("price","قیمت",["class"=>"col-md-2 control-label"]) !!}
                                    <div class="col-md-4">
                                        {!! Form::input("text","price",$product->price,["class"=>"form-control","placeholder"=>"قیمت"]) !!}
                                        <div class="form-control-focus"></div>
                                    </div>
                                </div>


                                <div class="form-group form-md-line-input">
                                    {!! Form::label("weight","وزن",["class"=>"col-md-2 control-label"]) !!}
                                    <div class="col-md-4">
                                        {!! Form::input("text","weight",$product->weight,["class"=>"form-control","placeholder"=>"وزن"]) !!}
                                        <div class="form-control-focus"></div>
                                    </div>
                                </div>


                                <div class="form-group form-md-line-input">
                                    {!! Form::label("length","طول",["class"=>"col-md-2 control-label"]) !!}
                                    <div class="col-md-4">
                                        {!! Form::input("text","length",$product->length,["class"=>"form-control","placeholder"=>"طول"]) !!}
                                        <div class="form-control-focus"></div>
                                    </div>
                                </div>

                                <div class="form-group form-md-line-input">
                                    {!! Form::label("width","عرض",["class"=>"col-md-2 control-label"]) !!}
                                    <div class="col-md-4">
                                        {!! Form::input("text","width",$product->width,["class"=>"form-control","placeholder"=>"عرض"]) !!}
                                        <div class="form-control-focus"></div>
                                    </div>
                                </div>


                                <div class="form-group form-md-line-input">
                                    {!! Form::label("height","ارتفاع",["class"=>"col-md-2 control-label"]) !!}
                                    <div class="col-md-4">
                                        {!! Form::input("text","height",$product->height,["class"=>"form-control","placeholder"=>"ارتفاع"]) !!}
                                        <div class="form-control-focus"></div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    {!! Form::label("stock","موجودی انبار",["class"=>"col-md-2 control-label"]) !!}
                                    <div class="col-md-4">
                                        {!! Form::input("text","stock",$product->stock,["class"=>"form-control","placeholder"=>"موجودی انبار"]) !!}
                                        <div class="form-control-focus"></div>
                                    </div>
                                </div>


                                <div class="form-group form-md-line-input">
                                    <label class="col-md-2 control-label">دسته بندی</label>
                                    <div class="col-md-8">


                                        <select class="js-example-basic-multiple" name="categories[]"
                                                style="width:100%;" multiple="multiple">
                                            @foreach($categories as $category)

                                                @php $olds = old("categories"); @endphp

                                                <option value="{{$category->id}}" {{((!is_null($currentCategories) && in_array($category->id,$currentCategories)) || (!is_null($olds) && in_array($category->id,$olds)))?"selected":""}}>
                                                    {{$category->name}}
                                                </option>

                                                @if(isset($category->childrens) && $category->childrens->count() > 0)

                                                    @include("panel.partials.getSubCats",['categories'=>$category->childrens,"dash"=>'','currentCategories'=>$currentCategories,'olds'=>$olds])

                                                @endif

                                            @endforeach
                                        </select>


                                    </div>
                                </div>

                                <div class="form-group form-md-line-input">
                                    {!! Form::label("brand_id","برند",["class"=>"col-md-2 control-label"]) !!}
                                    <div class="col-md-4">
                                        {!! Form::select('brand_id',$brands, $product->brand->id,["class"=>"form-control selectRtl"]) !!}

                                    </div>
                                </div>


                                <div class="form-group form-md-line-input">
                                    {!! Form::label("off_id","تخفیف محصول",["class"=>"col-md-2 control-label"]) !!}
                                    <div class="col-md-4">
                                        {!! Form::select('off_id',[] , 0,["class"=>"form-control selectRtl"]) !!}

                                    </div>
                                </div>


                                <hr>
                                <div class="form-group form-md-line-input">
                                    <div class="col-md-2">
                                        <button class="btn btn-primary btn-block" id="addOption" type="button">افزودن
                                            آپشن
                                        </button>
                                    </div>
                                    <div class="col-md-10">


                                        <figure id="optionsResult">
                                            <legend>آپشن ها</legend>

                                            @foreach($product_option_product as $item)
                                                <div class="row optionsList">

                                                    <div class="col-md-6">
                                                        <div class="form-group form-md-line-input">
                                                            {!! Form::label("option1[]","آپشن اول",["class"=>"col-md-2 control-label","style"=>"text-align:right"]) !!}
                                                            <div class="col-md-7">
                                                                {!! Form::select('option1[]',$productOptions, $item->product_option_id_1,["class"=>"form-control selectRtl"]) !!}

                                                            </div>
                                                        </div>
                                                        <div class="form-group form-md-line-input">
                                                            {!! Form::label("priceOption[]","قیمت",["class"=>"col-md-2 control-label"]) !!}
                                                            <div class="col-md-4">
                                                                {!! Form::input("text","priceOption[]",$item->price,["class"=>"form-control","placeholder"=>"قیمت"]) !!}
                                                                <div class="form-control-focus"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group form-md-line-input">
                                                            {!! Form::label("option2[]","آپشن دوم",["class"=>"col-md-2 control-label","style"=>"text-align:right"]) !!}
                                                            <div class="col-md-7">
                                                                {!! Form::select('option2[]',$productOptions, $item->product_option_id_2,["class"=>"form-control selectRtl"]) !!}

                                                            </div>
                                                        </div>

                                                        <div class="form-group form-md-line-input">
                                                            {!! Form::label("countOption[]","تعداد",["class"=>"col-md-2 control-label"]) !!}
                                                            <div class="col-md-4">
                                                                {!! Form::input("text","countOption[]",$item->count,["class"=>"form-control","placeholder"=>"تعداد"]) !!}
                                                                <div class="form-control-focus"></div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <hr>

                                                </div>
                                            @endforeach


                                        </figure>


                                    </div>
                                </div>
                                <hr>


                                <div class="form-group form-md-line-input">
                                    {!! Form::label("status","وضعیت نمایش محصول",["class"=>"col-md-2 control-label"]) !!}
                                    <div class="col-md-4">
                                        <div class="md-checkbox">
                                            <input id="status" value="1" class="md-check" name="status"
                                                   type="checkbox" {{$product->status == 1 ? 'checked="checked"' : ""}}>
                                            <label for="status">
                                                <span></span>
                                                <span class="check"></span>
                                                <span class="box"></span> فعال </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group form-md-line-input">
                                    {!! Form::label("featured","محصول ویژه",["class"=>"col-md-2 control-label"]) !!}
                                    <div class="col-md-4">
                                        <div class="md-checkbox">
                                            <input id="featured" value="1" class="md-check" name="status"
                                                   type="checkbox" {{$product->featured == 1 ? 'checked="checked"' : ""}}>
                                            <label for="featured">
                                                <span></span>
                                                <span class="check"></span>
                                                <span class="box"></span> ویژه </label>
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group form-md-line-input">
                                    <div class="col-md-8 col-md-offset-2">


                                        <div class="row">
                                            <div class="col-md-3">

                                                <button type="button" data-action="thumb"
                                                        class="galleryPickeraBtn btn btn-info"
                                                        style="width:100%">انتخاب تصویر دسته بندی
                                                </button>

                                            </div>

                                            <div class="col-md-3">

                                                {!! Form::text("image_name",isset($product->media->name)?$product->media->name:null,['class'=>'form-control galleryInput','readonly'=>'readonly']) !!}
                                                <input class="imageHiddenInput" type="hidden" name="media_id"
                                                       value="{{ isset($product->media->id)?$product->media->id:null }}">

                                            </div>

                                            <div class="col-md-3">
                                                <figure class="preview" style="max-width:100%">
                                                    @if( isset($product->media)) {!! thumb($product->media) !!} @endif
                                                </figure>
                                            </div>
                                        </div>


                                    </div>
                                </div>

                                <div class="form-group form-md-line-input">
                                    <div class="col-md-8 col-md-offset-2">


                                        <div class="row">
                                            <div class="main">
                                                <div class="col-md-3">

                                                    <button type="button" data-type="multiple" data-action="thumb"
                                                            class="galleryPickeraBtn btn btn-info" style="width:100%">
                                                        انتخاب سایر تصاویر محصول
                                                    </button>

                                                </div>


                                                <div class="col-md-12">

                                                    @if($product->medias->count() > 0)

                                                        {!! Form::text("image_name",$product->medias->first()->name,['class'=>'form-control galleryInput','readonly'=>'readonly']) !!}
                                                        <input class="imageHiddenInput col-md-6" type="hidden"
                                                               name="galleryImages[]"
                                                               value="{{$product->medias->first()->id}}">

                                                    @else

                                                        {!! Form::text("image_name",null,['class'=>'form-control galleryInput','readonly'=>'readonly']) !!}
                                                        <input class="imageHiddenInput col-md-6" type="hidden"
                                                               name="galleryImages[]" value="">

                                                    @endif

                                                </div>


                                            </div>

                                            <div class="moreThumbs col-md-12" style="width:100%;">


                                                @if($product->medias->count() > 1)

                                                    @php $mdcount=1; @endphp

                                                    @foreach($product->medias as $med)

                                                        @if($mdcount>1)
                                                            {!! Form::text("image_name",$med->name,['class'=>'form-control galleryInput','readonly'=>'readonly']) !!}
                                                            <input class="imageHiddenInput col-md-6" type="hidden"
                                                                   name="galleryImages[]" value="{{$med->id}}">
                                                        @endif

                                                        @php $mdcount++; @endphp

                                                    @endforeach

                                                @endif


                                            </div>

                                        </div>


                                    </div>
                                </div>


                            </div>


                            <div class="form-actions fluid">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        {!! Form::button("مرحله بعد",["class"=>"btn green","id"=>"next"]) !!}
                                        <a href="{{ route("products.create") }}">
                                            <button type="button" class="btn default">بازگشت</button>
                                        </a>
                                    </div>
                                </div>
                            </div>


                            <!-- END FORM-->
                        </div>
                    </div>


                </div>
            </div>
        </div>
        <div id="productPropertiesInfo" class="tab-pane fade">
            <div class="row">
                <div class="col-md-12">

                    <div class="portlet box blue-hoki">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-building"></i>ویرایش خصوصیات محصول
                            </div>
                            <div class="tools">
                                <!--<a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                                <a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title=""> </a>
                                <a href="javascript:;" class="reload" data-original-title="" title=""> </a>
                                <a href="javascript:;" class="remove" data-original-title="" title=""> </a>-->
                            </div>
                        </div>
                        <div class="portlet-body">
                            @include("errors.list")
                        </div>
                        <div class="portlet-body form">


                            <div class="form-body">


                                <div id="showFeatures">

                                    @foreach($features as $feature)

                                        <div class="form-group form-md-line-input"><label for="name"
                                                                                          class="col-md-2 control-label">{{$feature->name}}</label>
                                            <div class="col-md-4"><input
                                                        value="{{(isset($feature->feature_value->first()->value) ? $feature->feature_value->first()->value : "")}}"
                                                        class="form-control" placeholder="{{$feature->name}}"
                                                        name="feature_value{{$feature->id}}" type="text">
                                                <div class="form-control-focus"></div>
                                            </div>
                                        </div>

                                        {{--<input type="hidden" name="feature[]" value="{{$feature->id}}">--}}

                                    @endforeach

                                </div>


                                <div class="form-actions fluid">
                                    <div class="row">
                                        <div class="col-md-offset-3 col-md-9">
                                            {!! Form::submit("ذخیره",["class"=>"btn green"]) !!}
                                            <a href="{{ route("products.create") }}">
                                                <button type="button" class="btn default">بازگشت</button>
                                            </a>
                                        </div>
                                    </div>
                                </div>


                                <!-- END FORM-->
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}


@endsection


@section("extra_script")

    {!! JsValidator::formRequest('App\Http\Requests\ProductRequest', '#productCreate'); !!}

    <script>


        $("#next").click(function () {
            $('[href="#productPropertiesInfo"]').tab('show');
        });


        $(document).ready(function () {
            $('.js-example-basic-multiple').select2({
                dir: "rtl"
            });
        });


        var optionsList = ' <hr> <div class="row optionsList"> <div class="col-md-6"> <div class="form-group form-md-line-input"> <label for="option1[]" class="col-md-2 control-label" style="text-align:right">آپشن اول</label> <div class="col-md-7">';

        optionsList += '<select class="form-control selectRtl" id="option1[]" name="option1[]">';


        @foreach($productOptions as $k=>$v)

            optionsList += '<option value="{{$k}}">{{$v}}</option>';

        @endforeach


            optionsList += '</select> ';

        optionsList += '</div> </div> <div class="form-group form-md-line-input"> <label for="priceOption[]" class="col-md-2 control-label">قیمت</label> <div class="col-md-4"> <input class="form-control" placeholder="قیمت" name="priceOption[]" id="priceOption[]" type="text"> <div class="form-control-focus"></div> </div> </div> </div> <div class="col-md-6"> <div class="form-group form-md-line-input"> <label for="option2[]" class="col-md-2 control-label" style="text-align:right">آپشن دوم</label> <div class="col-md-7"> <select class="form-control selectRtl" id="option2[]" name="option2[]">';


        @foreach($productOptions as $k=>$v)

            optionsList += '<option value="{{$k}}">{{$v}}</option>';

        @endforeach

            optionsList += '</select>\n</div> </div> <div class="form-group form-md-line-input"> <label for="countOption[]" class="col-md-2 control-label">تعداد</label> <div class="col-md-4"> <input class="form-control" placeholder="تعداد" name="countOption[]" id="countOption[]" type="text"> <div class="form-control-focus"></div> </div> </div> </div> <hr> </div>';


        $("#addOption").click(function () {

            $('#optionsResult .selectRtl').select2('destroy');

            $("#optionsResult").append(optionsList);

            $('#optionsResult .selectRtl').select2({
                dir: "rtl",
                placeholder: "انتخاب کنید ...",
                allowClear: !0,
            });

        });


    </script>

@endsection