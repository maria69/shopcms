@extends('panel.layout')

@section('content')

    <div class="row">
        <div class="col-md-12">

            <div class="portlet box blue-hoki">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-building"></i>لیست محصولات
                    </div>
                    <div class="pull-left">
                        <a class="btn green" href="{{route('products.create')}}"> <i class="fa fa-plus"></i> افزودن محصول جدید
                        </a>
                    </div>
                </div>
                <div class="portlet-body">
                    @include("errors.list")
                </div>
                <div class="portlet-body">
                    <!-- BEGIN TABLE-->
                    <table class="table table-striped table-bordered table-hover datatable" id="sample_1">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>نام محصول</th>
                            <th>قیمت (تومان)</th>
                            <th>دسته بندی</th>
                            <th>وضعیت</th>
                            <th>تاریخ</th>
                            <th>عملیات</th>
                        </tr>
                        </thead>


                        <tbody>
                        @foreach($products as $product)

                            <tr>
                                <td>{{ $product->id }}</td>
                                <td>{{ $product->name }} <sup>({{$product->en_name}})</sup></td>
                                <td>{{ number_format($product->price) }}</td>
                                <td>
                                    @foreach($product->categories as $cat)
                                        {{$cat->name}}
                                        @endforeach
                                </td>
                                {{--<td>{{ isset($product->off->offer) ? $product->off->offer : "تخفیف موجود نیست" }}</td>
                                <td>{{ $product->brand->title}}</td>--}}
                                {{-- <td>{{ $product->warranty->title}}</td>--}}
                                <td>{!! status($product->status) !!}</td>
                                <td><span class='label label-primary'>ویرایش {{ jDate::forge($product->updated_at)->format('Y/m/d H:i:s') }}</span> <span class='label label-primary'>ساخت : {{ jDate::forge($product->created_at)->format('Y/m/d H:i:s') }}</span></td>
                                <td><div class="btn-group btn-group-lg" role="group"><a href="{{route('products.edit', $product->id)}}" class="btn green">ویرایش</a></div><div class="btn-group btn-group-lg" role="group">

                                        {!! Form::open(['url'=>route('products.destroy', $product->id),'method'=>'delete','class'=>'delete']) !!}
                                        <button type="submit" class="btn red"> حذف</button> {!! Form::close() !!} </div></td>
                            </tr>

                            @endforeach
                            </tbody>

                        </table>
<!-- END TABLE-->
</div>
</div>


</div>
</div>


@endsection