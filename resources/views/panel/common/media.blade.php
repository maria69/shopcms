@extends('panel.layout')
@section("title") پنل مدیریت | فایل های چند رسانه ای @endsection
@section('content')
            <div class="row">
                <div class="col-md-12">

                    <div class="portlet box blue-hoki">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-picture-o"></i> مدیریت فایل های چند رسانه ای </div>
                            <div class="tools">
                                <!-- <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                                 <a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title=""> </a>
                                 <a href="javascript:;" class="reload" data-original-title="" title=""> </a>
                                 <a href="javascript:;" class="remove" data-original-title="" title=""> </a>-->
                            </div>
                        </div>
                        <div class="portlet-body">
                        @include("errors.list")
                        <!-- BEGIN FORM-->

                            <div class="row">


                                <div class="col-md-6">




                                    {!! Form::open(['url'=>route("media.search"),'method'=>'post','id'=>'searchMedia']) !!}


                                    <fieldset>
                                        <!-- Name input-->
                                        <div class="form-group row form_inline_inputs_bot">
                                            <div class="col-lg-6">
                                                <div class="input-group" style="width:100%">
                                                    <input class="form-control" name="search" placeholder="جستجو در عنوان تصویر" type="text">
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <button class="btn btn-primary layout_btn_prevent btn-responsive form_inline_btn_margin-top">جستجو</button>
                                            </div>
                                        </div>
                                    </fieldset>


                                    {!! Form::close() !!}
                                </div>

                                <div class="col-md-6">

                                    <div class="pull-left">


                                        <button class="btn btn-success" data-toggle="modal" data-target="#uploader"> <i class="fa fa-plus-circle"></i>  افزودن فایل جدید </button>


                                    </div>

                                </div>


                            </div>

                            <hr>



                            <!-- Modal -->
                            <div class="modal bs-example-modal-lg fade" id="uploader" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel">اپلود فایل جدید</h4>
                                        </div>
                                        <form action ="{{ route("media.store") }}" method="POST" enctype="multipart/form-data" name="formData" id="ajaxUpload">
                                            <!--File Upload-->

                                            {{ csrf_field() }}
                                            <div class="modal-body">







                                                <div class="row">
                                                    <div class="form-group col-sm-10" id="uploadInput">
                                                        <div class="row">
                                                            <label class="control-label col-sm-3" for="file">آپلود فایل </label>
                                                            <div class="col-sm-9">
                                                                <input type="file" name="uploads[]" id="image_file"  class="form-control" accept="image/*">
                                                            </div>
                                                        </div>
                                                    </div>





                                                    <div class="form-group col-sm-2">

                                                        <button class="btn btn-primary" id="addMoreFile" type="button">افزودن فایل بیشتر</button>

                                                    </div>
                                                </div>

                                                <br/>

                                                <hr>
                                                <p> توجه داشته باشید آپلود فایل نیاز به زمان دارد و چندبار کلیک روی دکمه اپلود فایل ها باعث اپلود چندین تصویر تکراری میشود !</p>
                                                <hr>

                                                <br/>
                                                <div class="progress" style="display:none;">
                                                    <div class="progress-bar progress-bar-striped bg-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%;">

                                                    </div>
                                                </div>
                                                <div id = "result"></div>




                                            </div>
                                            <div class="modal-footer">
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">انصراف</button>

                                                    <input type="submit" name="submit" class="btn btn-primary" value="آپلود فایل ها" id="file_upload">
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>



                            <div class="row m-t-10" id="uploadRow">

                                <!--include("errors.list")-->

                                @foreach($media as $item)

                                    @php $createdTime = strtotime($item->created_at); @endphp

                                    <div class="col-md-2">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h3 class="panel-title">{{ $item->name }}</h3>
                                            </div>
                                            <div class="panel-body">

                                                @if(in_array($item->extension,["png","jpg","jpeg","gif"]))
                                                    <img src="{{ url("/media/".date("Y",$createdTime)."/".date("m",$createdTime)."/".date("d",$createdTime)."/".$item->hash.".".$item->extension) }}" class="img-responsive fullwidth" alt="{{ $item->name }}">
                                                @elseif(in_array($item->extension,["mp4"]))
                                                    <a href="{{ url("/media/".date("Y",$createdTime)."/".date("m",$createdTime)."/".date("d",$createdTime)."/".$item->hash.".".$item->extension) }}" download="{{ url("/media/".date("Y",$createdTime)."/".date("m",$createdTime)."/".date("d",$createdTime)."/".$item->hash.".".$item->extension) }}"><img src="/assets/img/video.png" class="img-responsive fullwidth" alt="{{ $item->name }}"></a>
                                                <!--<video controls class="fullwidth">
                                                        <source src="{{ url("/media/".date("Y",$createdTime)."/".date("m",$createdTime)."/".date("d",$createdTime)."/".$item->hash.".".$item->extension) }}" type="video/mp4">
                                                    </video>-->
                                                @elseif(in_array($item->extension,["mp3"]))
                                                    <a href="{{ url("/media/".date("Y",$createdTime)."/".date("m",$createdTime)."/".date("d",$createdTime)."/".$item->hash.".".$item->extension) }}" download="{{ url("/media/".date("Y",$createdTime)."/".date("m",$createdTime)."/".date("d",$createdTime)."/".$item->hash.".".$item->extension) }}"><img src="/assets/img/music.png" class="img-responsive fullwidth" alt="{{ $item->name }}"></a>
                                                <!--<video controls class="fullwidth">
                                                        <source src="{{ url("/media/".date("Y",$createdTime)."/".date("m",$createdTime)."/".date("d",$createdTime)."/".$item->hash.".".$item->extension) }}" type="video/mp4">
                                                    </video>-->
                                                @elseif(in_array($item->extension,["pdf"]))
                                                    <a href="{{ url("/media/".date("Y",$createdTime)."/".date("m",$createdTime)."/".date("d",$createdTime)."/".$item->hash.".".$item->extension) }}" download="{{ url("/media/".date("Y",$createdTime)."/".date("m",$createdTime)."/".date("d",$createdTime)."/".$item->hash.".".$item->extension) }}"><img src="/assets/img/pdf.png" class="img-responsive fullwidth" alt="{{ $item->name }}"></a>
                                                @elseif(in_array($item->extension,["doc","docx"]))
                                                    <a href="{{ url("/media/".date("Y",$createdTime)."/".date("m",$createdTime)."/".date("d",$createdTime)."/".$item->hash.".".$item->extension) }}" download="{{ url("/media/".date("Y",$createdTime)."/".date("m",$createdTime)."/".date("d",$createdTime)."/".$item->hash.".".$item->extension) }}"><img src="/assets/img/word.png" class="img-responsive fullwidth" alt="{{ $item->name }}"></a>
                                                @endif

                                            </div>
                                            <div class="panel-footer">
                                                <div class="row" style="display: block; margin:0 auto;">

                                                    <button class="btn btn-primary btn-xs editImg"  data-toggle="modal" data-target="#edit{{ $item->id }}" data-id="{{ $item->id }}"><span class="fa fa-pencil"></span></button>

                                                    <button class="btn btn-danger btn-xs deleteImg" data-id="{{ $item->id }}" data-name="{{ $item->name }}"><span class="fa fa-trash-o"></span></button>



                                                    <a href="{{ url("/media/".date("Y",$createdTime)."/".date("m",$createdTime)."/".date("d",$createdTime)."/".$item->hash.".".$item->extension) }}" download="{{ url("/media/".date("Y",$createdTime)."/".date("m",$createdTime)."/".date("d",$createdTime)."/".$item->hash.".".$item->extension) }}"><button class="btn btn-info btn-xs"><span class="fa fa-download"></span></button></a>



                                                </div>
                                            </div>
                                        </div>
                                    </div>




                                    <!-- Modal -->
                                    <div class="modal bs-example-modal-lg fade" id="edit{{ $item->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                    <h4 class="modal-title" id="myModalLabel">ویرایش فایل</h4>
                                                </div>


                                                {!! Form::open(['url'=>route('media.edit',$item->id),'method'=>'POST','class'=>'ajaxEdit','data-id'=>$item->id]) !!}



                                                <div class="modal-body">







                                                    <div class="row">



                                                        <div class="col-md-12">
                                                            <!-- last name-->
                                                            <div class="form-group row">
                                                                <div class="col-lg-12 col-md-12 col-sm-12">
                                                                    <div class="input-group">
                                                                <span class="input-group-addon">
                                                                <i class="fa fa-bars"></i>
                                                            </span>
                                                                        {!! Form::text("name",$item->name,['class'=>'form-control','placeholder'=>'نام تصویر','required'=>'required']) !!}

                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- mail name-->
                                                        </div>



                                                    </div>


                                                    <input type="hidden" value="{{ $item->id }}" name="imageId">



                                                </div>
                                                <div class="modal-footer">
                                                    <div class="btn-group">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">انصراف</button>

                                                        <input type="submit" name="submit" class="btn btn-primary" value="ویرایش" id="edit">
                                                    </div>
                                                </div>
                                                {!! Form::close() !!}
                                            </div>
                                        </div>
                                    </div>



                                @endforeach



                            </div>


                            <div id="galleryPaginate" class="row">
                                @include('panel.partials.paginate')
                            </div>


                            <!-- END FORM-->
                        </div>
                    </div>


                </div>
            </div>

@endsection

@section("extra_script")
    <script>
        $('.panel').matchHeight('remove').matchHeight();
        $('.fullwidth').matchHeight('remove').matchHeight();
    </script>
@endsection

