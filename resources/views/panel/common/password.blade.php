@extends("panel.layout")
@section("title") پنل مدیریت | تغییر گذرواژه @endsection
@section("content")


            <div class="row">
                <div class="col-md-12">

                    <div class="portlet box blue-hoki">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-key"></i>تغییر گذرواژه حساب کاربری </div>
                            <div class="tools">
                                <!--<a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                                <a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title=""> </a>
                                <a href="javascript:;" class="reload" data-original-title="" title=""> </a>
                                <a href="javascript:;" class="remove" data-original-title="" title=""> </a>-->
                            </div>
                        </div>
                        <div class="portlet-body">
                            @include("errors.list")
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            {!! Form::open(["url"=>"dashboard/password","method"=>"post","class"=>"form-horizontal"]) !!}

                                <div class="form-body">
                                    <div class="form-group form-md-line-input">
                                        {!! Form::label("oldpass","گذرواژه فعلی",["class"=>"col-md-2 control-label"]) !!}
                                        <div class="col-md-4">
                                            {!! Form::input("password","oldpass",null,["class"=>"form-control","placeholder"=>"گذرواژه فعلی"]) !!}
                                            <div class="form-control-focus"> </div>
                                        </div>
                                    </div>

                                    <div class="form-group form-md-line-input">
                                        {!! Form::label("password","گذرواژه جدید",["class"=>"col-md-2 control-label"]) !!}
                                        <div class="col-md-4">
                                            {!! Form::input("password","password",null,["class"=>"form-control","placeholder"=>"گذرواژه جدید"]) !!}
                                            <div class="form-control-focus"> </div>
                                        </div>
                                    </div>


                                    <div class="form-group form-md-line-input">
                                        {!! Form::label("password_confirmation","تکرار گذرواژه جدید",["class"=>"col-md-2 control-label"]) !!}
                                        <div class="col-md-4">
                                            {!! Form::input("password","password_confirmation",null,["class"=>"form-control","placeholder"=>"تکرار گذرواژه جدید"]) !!}
                                            <div class="form-control-focus"> </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="form-actions fluid">
                                    <div class="row">
                                        <div class="col-md-offset-3 col-md-9">
                                            {!! Form::submit("ذخیره گذرواژه",["class"=>"btn green"]) !!}
                                            <a href="/dashboard/"><button type="button" class="btn default">بازگشت</button></a>
                                        </div>
                                    </div>
                                </div>
                            {!! Form::close() !!}
                            <!-- END FORM-->
                        </div>
                    </div>


                </div>
            </div>



@endsection