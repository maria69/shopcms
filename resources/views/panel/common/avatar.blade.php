@extends("panel.layout")

@section("title") پنل مدیریت | تصویر حساب کاربری @endsection

@section("content")


    <div class="row">
        <div class="col-md-12">

            <div class="portlet box blue-hoki">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-picture-o"></i> تغییر تصویر حساب کاربری </div>
                    <div class="tools">
                        <!--<a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                        <a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title=""> </a>
                        <a href="javascript:;" class="reload" data-original-title="" title=""> </a>
                        <a href="javascript:;" class="remove" data-original-title="" title=""> </a>-->
                    </div>
                </div>

                <div class="portlet-body">
                    @include("errors.list")
                </div>
                <div class="portlet-body form">

                    <!-- BEGIN FORM-->

                    {!! Form::open(["url"=>"dashboard909/avatar","method"=>"post","enctype"=>"multipart/form-data","class"=>"form-horizontal"]) !!}

                    <div class="form-body">

                        <div class="form-group form-md-line-input">
                            <label class="col-md-2 control-label">تصویر فعلی</label>
                            <div class="col-md-4">
                                <img class="img-responsive" src="/avatars/{{ $user->avatar }}" alt="تصویر فعلی">
                            </div>
                        </div>


                        <div class="form-group form-md-line-input">
                            {!! Form::label("name","انتخاب تصویر جدید پروفایل",["class"=>"col-md-2 control-label"]) !!}
                            <div class="col-md-4">
                                {!! Form::input("file","avatar",null,["class"=>"form-control"]) !!}
                                <div class="form-control-focus"> </div>
                            </div>
                        </div>

                    </div>
                    <div class="form-actions fluid">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                {!! Form::submit("آپلود تصویر جدید",["class"=>"btn green"]) !!}
                                <a href="/dashboard909/"><button type="button" class="btn default">بازگشت</button></a>
                            </div>
                        </div>
                    </div>
                {!! Form::close() !!}
                <!-- END FORM-->
                </div>
            </div>


        </div>
    </div>


@endsection