@extends("panel.layout")
@section("title") پنل مدیریت | حساب کاربری @endsection
@section("content")
            <div class="row">
                <div class="col-md-12">

                    <div class="portlet box blue-hoki">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-users"></i>ویرایش حساب کاربری </div>
                            <div class="tools">
                                <!--<a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                                <a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title=""> </a>
                                <a href="javascript:;" class="reload" data-original-title="" title=""> </a>
                                <a href="javascript:;" class="remove" data-original-title="" title=""> </a>-->
                            </div>
                        </div>
                        <div class="portlet-body">
                            @include("errors.list")
                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            {!! Form::open(["class"=>"form-horizontal","url"=>"/dashboard/profile","method"=>"post"]) !!}

                            <div class="form-body">
                                <div class="form-group form-md-line-input">
                                    {!! Form::label("name","نام",["class"=>"col-md-2 control-label"]) !!}
                                    <div class="col-md-4">
                                        {!! Form::input("text","name",$user->name,["class"=>"form-control","placeholder"=>"نام کوچک شما"]) !!}
                                        <div class="form-control-focus"> </div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    {!! Form::label("last_name","نام خانوادگی",["class"=>"col-md-2 control-label"]) !!}
                                    <div class="col-md-4">
                                        {!! Form::input("text","last_name",$user->last_name,["class"=>"form-control","placeholder"=>"نام خانوادگی"]) !!}
                                        <div class="form-control-focus"> </div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    {!! Form::label("email","پست الکترونیک",["class"=>"col-md-2 control-label"]) !!}
                                    <div class="col-md-4">
                                        {!! Form::input("text","email",$user->email,["class"=>"form-control","placeholder"=>"پست الکترونیک"]) !!}
                                        <div class="form-control-focus"> </div>
                                    </div>
                                </div>

                            </div>
                            <div class="form-actions fluid">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        {!! Form::submit("ذخیره",["class"=>"btn green"]) !!}
                                        <a href="/dashboard/"><button type="button" class="btn default">بازگشت</button></a>
                                    </div>
                                </div>
                            </div>
                        {!! Form::close() !!}
                        <!-- END FORM-->
                        </div>
                    </div>


                </div>
            </div>


@endsection