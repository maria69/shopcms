<div id="galleryPicker" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">انتخاب یا اپلود تصویر</h4>
            </div>
            <div class="modal-body" id="galleryPickerContent">


                <form action ="{{ route("media.store") }}" method="POST" enctype="multipart/form-data" name="formData" id="ajaxUpload">
                    <!--File Upload-->

                    {{ csrf_field() }}


                    <div class="row">
                        <div class="form-group col-sm-10" id="uploadInput">
                            <div class="row">
                                <label class="control-label col-sm-3" for="file">آپلود فایل </label>
                                <div class="col-sm-9">
                                    <input type="file" name="uploads[]" id="image_file"  class="form-control" accept="image/*">
                                </div>
                            </div>
                        </div>





                        <div class="form-group col-sm-2">

                            <button class="btn btn-primary" id="addMoreFile" type="button">افزودن فایل بیشتر</button>

                        </div>
                    </div>

                    <br/>
                    <div class="progress" style="display:none;">
                        <div class="progress-bar progress-bar-striped bg-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%;">

                        </div>
                    </div>
                    <div id = "result"></div>



                    <div class="form-group">
                        <input type="submit" name="submit" class="btn btn-primary btn-block" value="آپلود فایل ها" id="file_upload">
                    </div>
                </form>


                <hr>


                <div class="row">
                    <div class="col-md-12">




                        {!! Form::open(['url'=>route("media.search"),'method'=>'post','id'=>'searchMedia']) !!}


                        <fieldset>
                            <!-- Name input-->
                            <div class="form-group row form_inline_inputs_bot">
                                <div class="col-lg-10">
                                    <div class="input-group" style="width:100%">
                                        <input class="form-control" name="search" placeholder="جستجو در عنوان تصویر" type="text">
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <button class="btn btn-primary btn-block layout_btn_prevent btn-responsive form_inline_btn_margin-top">جستجو</button>
                                </div>
                            </div>
                        </fieldset>


                        {!! Form::close() !!}
                    </div>
                </div>


                <hr>
                <div class="row" id="uploadRow">





                </div>





            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">انصراف</button>
            </div>
        </div>

    </div>
</div>