@extends('panel.layout')

@section('content')

    <div class="row">
        <div class="col-md-12">

            <div class="portlet box blue-hoki">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-building"></i>لیست اسلایدر ها
                    </div>
                    <div class="pull-left">
                        <a class="btn green" href="{{route('sliders.create')}}"> <i class="fa fa-plus"></i>اسلایدر جدید</a>
                    </div>
                </div>
                <div class="portlet-body">
                    @include("errors.list")
                </div>
                <div class="portlet-body">
                    <!-- BEGIN TABLE-->
                    <table class="table table-striped table-bordered table-hover datatable" id="sample_1">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>عنوان اسلایدر</th>
                            <th>تصویر</th>
                            <th>تاریخ</th>
                            <th>لینک اسلایدر</th>
                            <th>عملیات</th>
                        </tr>
                        </thead>


                        <tbody>
                        @foreach($sliders as $slider)

                            <tr>
                                <td>{{ $slider->id }}</td>
                                <td>{{ $slider->title }}</td>
                                <td> {!!  thumb($slider->media,1) !!}  </td>
                                <td>
                                    <span class='label label-primary'>ویرایش {{ jDate::forge($slider->updated_at)->format('Y/m/d H:i:s') }}</span>
                                    <span class='label label-primary'>ساخت : {{ jDate::forge($slider->created_at)->format('Y/m/d H:i:s') }}</span>
                                </td>
                                <td>
                                    <a target="_blank" href="{{ $slider->link }}"><span class='label label-primary'> کلیک کنید </span></a>
                                </td>
                                <td>
                                    <div class="btn-group btn-group-lg" role="group"><a
                                                href="{{route('sliders.edit', $slider->id)}}" class="btn green">ویرایش</a>
                                    </div>
                                    <div class="btn-group btn-group-lg" role="group">

                                        {!! Form::open(['url'=>route('sliders.destroy', $slider->id),'method'=>'delete','class'=>'delete']) !!}
                                        <button type="submit" class="btn red"> حذف</button> {!! Form::close() !!} </div>
                                </td>
                            </tr>

                        @endforeach
                        </tbody>

                    </table>
                    <!-- END TABLE-->
                </div>
            </div>


        </div>
    </div>


@endsection