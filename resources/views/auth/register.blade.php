<!DOCTYPE html>
<html lang="fa">
<head>
    <meta charset="utf-8">
    <title>ثبت نام | مجتمع فنی قیطریه</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <link href="/panelassets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/panelassets/global/css/components-md.min.css" rel="stylesheet" id="style_components" type="text/css" />
    <link href="/panelassets/global/css/plugins-md.min.css" rel="stylesheet" type="text/css" />
    <link href="/panelassets/pages/css/login-2.min.css" rel="stylesheet" type="text/css" />
    <link href="/css/rtl.css" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="http://mftght.com/favicon.jpg" />
    <!-- END HEAD -->
</head>
<body class="login">
<style>
    .content {
        margin-top:15px !important;
        width:50% !important;
    }

    .login .logo img {
        width: 90%;
    }
</style>
<!-- BEGIN LOGIN -->
<div class="content">
@include("errors.list")
    @if(Session::has("registerMessage"))
        <div class="alert alert-info">
            {!! Session::get("registerMessage")[0] !!}
        </div>
    @endif
    <!-- BEGIN REGISTRATION FORM -->
    {!! Form::open(["url"=>"auth/register","method"=>"post","id"=>"register"]) !!}


    <div class="row">
        <div class="col-md-6">
            <!-- BEGIN LOGO -->
            <div class="logo">
                <img src="/img/logo.png" title="مجتمع فنی تهران" alt="مجتمع فنی تهران" />
            </div>
            <!-- END LOGO -->



        </div>

        <div class="col-md-6">



            <div class="form-title">
                <span class="form-title">{{ $lang[25] }}</span>
            </div>
            <p class="hint"> {{ $lang[27] }}  </p>

            <p class="hint">{{ $lang[28] }}  </p>

            <p class="hint"> {{ $lang[29] }}  </p> <br>
            <div class="form-group">
                <label class="control-label visible-ie8 visible-ie9">نام *</label>
                {!! Form::input("text","name",null,["class"=>"form-control placeholder-no-fix","placeholder"=>"نام *"]) !!}
            </div>


            <div class="form-group">
                <label class="control-label visible-ie8 visible-ie9">نام خانوادگی *</label>
                {!! Form::input("text","last_name",null,["class"=>"form-control placeholder-no-fix","placeholder"=>"نام خانوادگی *"]) !!}
            </div>
            <div class="form-group">
                <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                <label class="control-label visible-ie8 visible-ie9">پست الکترونیک *</label>
                {!! Form::input("email","email",null,["class"=>"form-control placeholder-no-fix","placeholder"=>"پست الکترونیک *"]) !!}
            </div>


            <div class="form-group">
                <label class="control-label visible-ie8 visible-ie9">موبایل *</label>
                {!! Form::input("text","mobile",null,["class"=>"form-control placeholder-no-fix","placeholder"=>"موبایل *"]) !!}
            </div>



            <p class="hint"> اطلاعات ورود را وارد نمایید </p>
            <div class="form-group">
                <label class="control-label visible-ie8 visible-ie9">گذرواٰژه *</label>
                {!! Form::input("text","password",null,["class"=>"form-control placeholder-no-fix","placeholder"=>"گذرواٰژه *"]) !!}
            </div>
            <div class="form-group">
                <label class="control-label visible-ie8 visible-ie9">تکرار گذرواٰژه *</label>
                {!! Form::input("text","password_confirmation",null,["class"=>"form-control placeholder-no-fix","placeholder"=>"تکرار گذرواٰژه *"]) !!}
            </div>

            

            <div class="form-actions">
                <a href="/auth/login"><button type="button" id="back-btn" class="btn btn-default pull-left">بازگشت</button></a>
                {!! Form::submit("ثبت نام",["class"=>"btn red uppercase"]) !!}
            </div>


        </div>
    </div>




{!! Form::close() !!}
<!-- END REGISTRATION FORM -->
</div>
@include("auth.footer")