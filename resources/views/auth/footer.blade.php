<div class="copyright hide"> <?php echo date("Y"); ?> © MFT</div>
<!-- END LOGIN -->
<!--[if lt IE 9]>
<script src="/panelassets/global/plugins/respond.min.js"></script>
<script src="/panelassets/global/plugins/ie8.fix.min.js"></script>
<![endif]-->
<script src="/panelassets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="/panelassets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="/vendor/jsvalidation/js/jsvalidation.js"></script>
<style>.form-group.form-md-line-input { margin: 0 0 0px; }  .has-success .md-checkbox label > .box, .has-success .md-checkbox label > .check, .has-success.md-checkbox label > .box, .has-success.md-checkbox label > .check {  border-color: #edf4f8;  }.md-radio label > .check {background: #fff;}.md-radio label > .box {border: 2px solid #333;}.md-radio {margin-left: 25px;}</style>
@if(Request::is("auth/login"))
{!! JsValidator::formRequest('App\Http\Requests\LoginRequest', '#login') !!}
<script>
    $(document).ready(function() {
        $('.md-radio').click(function(){
            var email = $("#email").val();
            $("#email").val(Math.random()+"@gmail.com");
            $("#login").valid();
            $("#email").val(email);
            $("#login").valid();
        });})
</script>
@elseif(Request::is("auth/register"))
{!! JsValidator::formRequest('App\Http\Requests\RegisterRequest', '#register') !!}
@elseif(Request::is("exam/*"))
    {!! JsValidator::formRequest('App\Http\Requests\ExamRequest', '#exam') !!}

    <script>
        $(function(){
            $("#prevExamNum").fadeOut();

            var type = $("#type option:selected").val();

            if(type == "compensation_price" || type == "compensation_verbally"){
                $("#prevExamNum").fadeIn();
            }else{
                $("#prevExamNum").fadeOut();
            }

            $("#type").change(function(){

                var val = $(this).val();

                switch(val){

                    case "compensation_price":
                    case "compensation_verbally":
                        $("#prevExamNum").fadeIn();
                        break;
                    case "full_exam_price":
                        $("#prevExamNum").fadeOut();

                }

            });
        });
    </script>


    <script>
        $("input[lang='en'],textarea[lang='en']").keypress(function(event){
            var ew = event.which;
            console.log(event.which);
            if(ew == 32 || ew == 127 || ew == 8 || ew == 0 || ew == 64 || ew == 95 || ew == 46 | ew == 44)
                return true;
            if(48 <= ew && ew <= 57)
                return true;
            if(65 <= ew && ew <= 90)
                return true;
            if(97 <= ew && ew <= 122)
                return true;
            alert("لطفا به انگلیسی تایپ نمایید")
            return false;
        });
    </script>
@else
    <script>
        $(document).ready(function()
        {

            $('.md-radio').click(function(){
                var email = $("#forgot-email").val();
                $("#forgot-email").val(Math.random()+"@gmail.com");
                $("#forgot").valid();
                $("#forgot-email").val(email);
                $("#forgot").valid();
            });
        })
    </script>
{!! JsValidator::formRequest('App\Http\Requests\ForgotPasswordRequest', '#forgot') !!}
@endif
</body>
</html>