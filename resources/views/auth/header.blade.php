<!DOCTYPE html>
<html lang="fa">
<head>
    <meta charset="utf-8">
    <title>ورود به حساب کاربری | مجتمع فنی قیطریه</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta name="description" content="ثبت نام انلاین مجتمع فنی تهران"/>
    <meta name="author" content="مجتمع فنی تهران" />
    <link href="/panelassets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/panelassets/global/css/components-md.min.css" rel="stylesheet" id="style_components" type="text/css" />
    <link href="/panelassets/global/css/plugins-md.min.css" rel="stylesheet" type="text/css" />
    <link href="/panelassets/pages/css/login-2.min.css" rel="stylesheet" type="text/css" />
    <link href="/css/rtl.css" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="http://mftght.com/favicon.jpg" />
    <!-- END HEAD -->
</head>
<body class="login">
<!-- BEGIN LOGO -->
<div class="logo">
        <img src="/img/logo.png" title="مجتمع فنی تهران" alt="مجتمع فنی تهران" width="500" />
</div>
<!-- END LOGO -->