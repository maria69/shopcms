@include("auth.header")
<!-- BEGIN LOGIN -->
<div class="content">

    <!-- BEGIN FORGOT PASSWORD FORM -->

    @include('errors.list')

    {!! Form::open(["url"=>url('/auth/password/reset'),"method"=>"post"]) !!}

        <input type="hidden" name="token" value="{{ $token }}">
    <div class="form-title">
        <span class="form-title">تغییر رمز عبور</span>
    </div>
        <div class="form-group">
            {!! Form::input("email","email",null,["class"=>"form-control placeholder-no-fix","id"=>"forgot-email","autocomplete"=>"off","placeholder"=>"پست الکترونیک"]) !!}
        </div>


        <div class="form-group">
            {!! Form::input("password","password",null,["class"=>"form-control placeholder-no-fix","id"=>"forgot-email","autocomplete"=>"off","placeholder"=>"گذرواژه"]) !!}
        </div>



        <div class="form-group">
            {!! Form::input("password","password_confirmation",null,["class"=>"form-control placeholder-no-fix","id"=>"forgot-email","autocomplete"=>"off","placeholder"=>"تکرار گذر واژه"]) !!}
        </div>

        <div class="form-actions">
            <a href="/auth/login"><button type="button" id="back-btn" class="btn btn-default pull-left">انصراف</button></a>
            {!! Form::submit("ذخیره گذرواژه",["class"=>"btn btn-primary uppercase pull-right red"]) !!}
        </div>

</div>


{!! Form::close() !!}
<!-- END FORGOT PASSWORD FORM -->

    <!-- END REGISTRATION FORM -->
</div>
@include("auth.footer")

