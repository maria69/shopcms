@include("auth.header")
<!-- BEGIN LOGIN -->
<div class="content">

<!-- BEGIN FORGOT PASSWORD FORM -->

    @include('errors.list')

    {!! Form::open(["url"=>"auth/password/email","method"=>"post"]) !!}
    <div class="form-title">
        <span class="form-title">رمز عبور خود را فراموش کرده اید‌؟</span>
        <br> <br>
        <span class="form-subtitle">پست الکترونیک خود را وارد نمایید</span>
    </div>
    <div class="form-group">
        {!! Form::input("email","email",null,["class"=>"form-control placeholder-no-fix","id"=>"forgot-email","autocomplete"=>"off","placeholder"=>"پست الکترونیک"]) !!}
    </div>


    <div class="form-actions">
        <a href="/auth/login"><button type="button" id="back-btn" class="btn btn-default pull-left">بازگشت</button></a>
        {!! Form::submit("بازیابی گذرواژه",["class"=>"btn btn-primary uppercase pull-right red"]) !!}
    </div>
{!! Form::close() !!}
<!-- END FORGOT PASSWORD FORM -->

    <!-- END REGISTRATION FORM -->
</div>
@include("auth.footer")