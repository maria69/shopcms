<?php
Auth::routes();
/* User Route Group */
Route::group(['prefix' => 'user', 'middleware' => ["checkUserAuth"]], function () {

    Route::post("addComment",'Site\ProductController@addComment')->name("comment.store");

    Route::get('/profile', 'Site\ProfileController@show')->name("userProfile");
    Route::put('/update', 'Site\ProfileController@updateUserInfo')->name("updateUserInfo");

    Route::resource('addresses', 'Site\UserAddressController');
    Route::resource('wish_list', 'Site\WishListController');
    Route::get('order/items/{id}','Site\OrderController@getItems')->name("orderItem");




    Route::resource('orders', 'Site\OrderController');
});

Route::post('myData','Admin\OrderController@getAll');
Route::post('orderItem','Admin\OrderController@getorderItems');

Route::post('like', 'Site\ProductController@like')->name("like");

Route::post('/user/addWishList', 'Site\WishListController@addWishList')->name('addWishList');

Route::group(['prefix' => 'cart'], function () {
    Route::post('/addToCart', 'Site\CartController@addToCart')->name('cart.addToCart');
    Route::post('/deleteFromCart', 'Site\CartController@delete')->name('cart.deleteFromCart');
    Route::post('/counter/{dir}', 'Site\CartController@counter')->name('cart.counter');
});


Route::get('orders/track', 'Site\OrderController@track')->name("orderTrack");



Route::get('compaire', 'Site\CompaireController@index')->name("compaire");

Route::post('addCompaire', 'Site\CompaireController@store')->name("addCompaireToList");

Route::post('removeFromCompaire', 'Site\CompaireController@destroy')->name("removeFromCompaire");

Route::post('getProductsByType', 'Site\CompaireController@getProductsByType')->name("getProductsByType");

Route::post('addCompaireToListFromProduct', 'Site\CompaireController@addCompaireToListFromProduct')->name("addCompaireToListFromProduct");



Route::get('search', 'Site\HomeController@search')->name("search");

$this->post('login', 'Auth\LoginController@myLogin')->name('phoneLogin');

/* Public Route Group */

Route::get('/', 'Site\HomeController@index')->name("home");
Route::get('/product/{id}', 'Site\ProductController@show')->name('singleProduct');
Route::get('/category/{id}', 'Site\CategoryController@show')->name('categoryProducts');
Route::get('/cart', 'Site\CartController@show')->name("cart");
Route::get('/payment', 'Site\PaymentController@pay')->name("pay");

/* Admin Route Group */

Route::group(['prefix' => 'dashboard909', 'middleware' => ['checkAdminAuth']], function () {


    Route::post('getTypes', 'Admin\ProductTypeController@getTypes')->name("getTypes");
    Route::post('getTypeFromOption', 'Admin\ProductOptionController@getTypeFromOption')->name("getTypeFromOption");

    /* Setting Routes */
    Route::group(['prefix' => 'settings'], function () {

        Route::resource('options', 'Admin\OptionController');
        Route::resource('cities', 'Admin\CityController');
        Route::resource('states', 'Admin\StateController');
        Route::resource('navs', 'Admin\NavController');
        Route::resource('payments', 'Admin\PaymentController');
        Route::resource('shoppings', 'Admin\ShoppingController');
        Route::resource('socials', 'Admin\SocialController');
    });
    Route::resource('productOptionCategories', 'Admin\ProductOptionCategoryController');
    Route::resource('productOptions', 'Admin\productOptionController');
    Route::resource('products', 'Admin\ProductController');
    Route::resource('brands', 'Admin\BrandController');
    Route::resource('offers', 'Admin\OfferController');
    Route::resource('shippings', 'Admin\ShippingController');
    Route::resource('categories', 'Admin\CategoryController');
    Route::resource('product_types', 'Admin\ProductTypeController');
    Route::resource('sliders', 'Admin\SliderController');
    Route::resource('features', 'Admin\FeatureController');
    Route::resource('feature_categories', 'Admin\FeatureCategoryController');
    Route::resource('banners', 'Admin\BannerController');
    Route::resource('comments', 'Admin\CommentController');
    Route::resource('companies', 'Admin\CompanyController');
    Route::resource('warranties', 'Admin\WarrantyController');
    Route::resource('users', 'Admin\UserController');
    Route::resource('admins', 'Admin\AdminController');
    Route::get('/', 'Admin\DashboardController@index')->name("dashboard");
    /* Media Routes */
    /* Admin Profile Routes */
    Route::get('logout/0', 'Auth\AdminAuthController@logout')->name("adminLogout");
    Route::get('/password', 'Admin\DashboardController@password')->name("password");
    Route::post('/password', 'Admin\DashboardController@passwordStore');
    Route::get('/avatar', 'Admin\DashboardController@avatar')->name("avatar");
    Route::post('/avatar', 'Admin\DashboardController@avatarStore');
    Route::get('/profile', 'Admin\DashboardController@profile')->name("profile");
    Route::post('/profile', 'Admin\DashboardController@profileStore');

    Route::resource('order', 'Admin\OrderController');
//    Route::resource('order', 'Admin\OrderController');
});

Route::prefix("dashboard909/media")->group(function () {
    Route::delete('{id}', 'Admin\MediaController@destroy')->name("media.destroy");
    Route::post('ajaxEdit/{id}', 'Admin\MediaController@update')->name("media.edit");
    Route::post('store', 'Admin\MediaController@store')->name("media.store");
    Route::get('/', 'Admin\MediaController@index')->name("media.index");
    Route::get('picker', 'Admin\MediaController@picker')->name("media.picker");
    Route::post('/ajaxSearch', 'Admin\MediaController@ajaxSearch')->name("media.search");
});

Route::group(['prefix' => 'dashboard909'], function () {
    /* Admin Auth */
    // Authentication Routes...
    Route::get('login', 'Auth\AdminAuthController@showLoginForm');
    Route::post('login', 'Auth\AdminAuthController@login')->name('adminLogin');
    Route::get('logout', 'Auth\AdminAuthController@logout')->name('adminLogout');
    // Password Reset Routes...
    Route::get('password/reset/{token?}', 'Auth\AdminPasswordController@showResetForm');
    Route::post('password/email', 'Auth\AdminPasswordController@sendResetLinkEmail');
    Route::post('password/reset', 'Auth\AdminResetPasswordController@reset');
});





