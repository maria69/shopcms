<?php

namespace App\Providers;

use App\Model\Category;
use App\Model\ProductType;
use Illuminate\Support\ServiceProvider;
//use App\Model\Cart;

class ViewComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        if (function_exists('header_remove')) {
            header_remove('x-powered-by');
        }

        $categories = Category::where("parent_id",null)->with(["childrens" =>function($query){$query->with("media");$query->with("childrens");},"media"])
//            ->with(["childrens" =>function($query){$query->with("media");},"media"])
            ->get();





//        dd($categories);
//        $cart = getBasket();


        $productTypeSearch=[0=>"همه محصولات"];
        $productTypeSearch+=ProductType::pluck("name","id")->toArray();

        view()->composer('client.layout',function($view) use ($categories,$productTypeSearch){
            $view->with(['categoriesInNav'=>$categories,'productTypeSearch'=>$productTypeSearch]);
        });
        view()->composer('newClient.layout',function($view) use ($categories,$productTypeSearch){
            $view->with(['categoriesInNav'=>$categories,'productTypeSearch'=>$productTypeSearch]);
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
