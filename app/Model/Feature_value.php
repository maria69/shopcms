<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Feature_value extends Model
{
    protected $table = "feature_value";

    protected $fillable = ["value","product_id","feature_id"];

    protected $appends = ['feature_pro'];


    public function getFeatureProAttribute()
    {

        return  $this->feature['name'] ;
    }

    public function product(){

        return $this->belongsTo(Product::class);
    }

    public function feature(){

        return $this->belongsTo(Feature::class);
    }
}
