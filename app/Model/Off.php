<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Off extends Model
{

    //I LOVE MAGHSOODI
    protected $fillable = ['name','code','offer_type','offer','start_date','end_date'];


    public function products(){
        return $this->hasMany(Product::class);
    }
}
