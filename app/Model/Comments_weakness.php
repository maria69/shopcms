<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Comments_weakness extends Model
{
    public $table = "comments_weakness";

    protected $fillable = ["comment_id","description","type"];


    public $timestamps = false;

}
