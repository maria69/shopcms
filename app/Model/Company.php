<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $fillable = ['name'];

    public function warranties()
    {
        return $this->hasMany(Warranty::class);
    }

    public function media()
    {
        return $this->belongsTo(Media::class);
    }
}
