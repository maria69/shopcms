<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Comment_Star extends Model
{
    protected $table = "comments_stars";

    protected $fillable = ["star_id","comment_id","count"];

    public $timestamps = false;
}
