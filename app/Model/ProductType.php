<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProductType extends Model
{
    protected $guarded = [];

    protected $fillable= ['name','status'];


    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function userAddress()
    {
        return $this->belongsToMany(Category::class);
    }

    public function feature()
    {
        return $this->hasMany(Feature::class);
    }

}
