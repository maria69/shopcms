<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Shopping extends Model
{
    protected $fillable = ['title', 'desc', 'status', 'media_id'];

    protected $table = 'shopping_types';

    public function media()
    {
        return $this->belongsTo(Media::class);
    }
}
