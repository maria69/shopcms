<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    protected $fillable = ["key", "value"];

    public function userRole()
    {
        return $this->belongsToMany(UserRole::class, 'user_role_permissions');
    }

}
