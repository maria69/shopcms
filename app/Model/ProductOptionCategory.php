<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProductOptionCategory extends Model
{
    protected $fillable=['name','type'];

    public function productoptions()
    {
        return $this->hasMany(ProductOption::class);
    }
}
