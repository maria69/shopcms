<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Product_option_product extends Model
{
    protected $table = "product_option_product";

    protected $fillable = ["product_id","product_option_id_1","product_option_id_2","price","count"];


    public $timestamps = false;

}
