<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    public function userAddress()
    {
        return $this->hasMany(UserAddress::class);
    }
}
