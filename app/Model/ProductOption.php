<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProductOption extends Model
{
    protected $fillable= ['name','value','product_option_category_id','media_id'];

    public function media()
    {
        return $this->belongsTo(Media::class);
    }

    public function productoptioncategory()
    {
        return $this->belongsTo(ProductOptionCategory::class,'product_option_category_id');
    }
}
