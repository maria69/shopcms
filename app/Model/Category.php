<?php

namespace App\Model;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use Sluggable;



    protected $fillable = ['name', 'description', 'parent_id', 'media_id', 'meta', 'keywords', 'status'];
    protected $appends = ['parent_info'];
//
//    protected $appends = ['child'];


    public function getParentInfoAttribute(){
        return $this->name;
    }
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }



    public function parent()
    {
        return $this->belongsTo($this, 'id','parent_id');
    }

    public function childrens(){

        return $this->hasMany($this,'parent_id','id');
    }

    public function products()
    {
        return $this->belongsToMany(Product::class, 'product_category');
    }

    public function media()
    {
        return $this->belongsTo(Media::class);
    }


//    public function childrens()
//    {
//        return $this->hasMany($this, "parent_id");
//    }




}
