<?php

namespace App\Model;

use App\Model\Comment;
use App\Model\UserAddress;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'code_melli', 'phone', 'mobile', 'status', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function addresses()
    {
        return $this->hasMany(UserAddress::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }


    public function wishlist()
    {
        return $this->hasMany(Wishlist::class);
    }


    public function orders(){
        return $this->hasMany(Order::class);
    }
}
