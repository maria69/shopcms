<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    protected $table = "media";

    public $fillable = ['name','hash','size','extension','type','usage','creator_id','creator_type','user_id'];

    public function admin(){
        $this->belongsTo('App\Admin');
    }
    public function slider(){
        $this->belongsTo(Slider::class);
    }
    public function product(){
        $this->belongsTo(Product::class);
    }
    public function category(){
        $this->belongsTo(Category::class);
    }
    public function page(){
        $this->belongsTo(Page::class);
    }
}