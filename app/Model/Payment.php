<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    public function media()
    {
        return $this->belongsTo(Media::class);
    }
}
