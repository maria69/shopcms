<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Comment_like extends Model
{
    public $table = "comment_likes";

    public $timestamps = false;

    protected $fillable = ["user_id","type","comment_id"];
}
