<?php

namespace App\Model;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ["stock", "featured", "serialnumber", "sales", "name", "media_id", "en_name", "price", "weight", "length", "width", "height", "brand_id", "creator_id", "off_id", "short_description", "description", "meta", "keywords", "review", "comment_count", "score", "status", "type"];
    use Sluggable;

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

//    protected $table = 'feature_value';
    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class, 'product_category');
    }

    public function warranties()
    {
        return $this->belongsToMany(Warranty::class, 'product_warranty');
    }

    public function media()
    {
        return $this->belongsTo(Media::class, 'media_id');
    }

    public function medias()
    {
        return $this->belongsToMany(Media::class, 'product_media');
    }

    public function wishlist()
    {
        return $this->hasMany(Wishlist::class);
    }

    public function orderItem()
    {

        return $this->hasOne(OrderItem::class);
    }

    public function features()
    {
        return $this->belongsToMany(Feature::class, 'feature_value');
    }

    public function feature_values()
    {

        return $this->hasMany(Feature_value::class);

    }

    public function off()
    {

        return $this->belongsTo(Off::class, 'off_id');
    }
    public function options()
    {

        return $this->hasMany(ProductOption::class, 'off_id');
    }
}
