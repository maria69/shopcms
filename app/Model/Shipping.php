<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Shipping extends Model
{
    protected $fillable = ['title', 'desc', 'status', 'media_id'];

    protected $table = 'shipping_types';

    public function media()
    {
        return $this->belongsTo(Media::class);
    }
}
