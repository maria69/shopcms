<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    protected $fillable = ['title', 'body', 'link', 'media_id', 'status'];

    public function media()
    {
        return $this->belongsTo(Media::class);
    }

}
