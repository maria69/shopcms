<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Feature extends Model
{
    protected $fillable= ['name','product_type_id','feature_category_id'];

//    public function feature_value(){
//        return $this->hasMany('App\Model\Feature_value','feature_id');
//    }

    public function productType()
    {
        return $this->belongsTo(ProductType::class);
    }
    public function featureCategory()
    {
        return $this->belongsTo(FeatureCategory::class);
    }

    public function products(){
        return $this->belongsToMany(Product::class,'feature_value');
    }

    public function feature_value(){

        return $this->hasMany(Feature_value::class);

    }
}
