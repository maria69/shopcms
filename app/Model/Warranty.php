<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Warranty extends Model
{

    protected $fillable = ["company_id", "expire_at"];

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function products()
    {
        return $this->belongsToMany(Product::class, 'product_warranty');
    }

}
