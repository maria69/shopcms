<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table = "comments";

    protected $fillable = ["user_id","product_id","score","body","status","notification","like","unlike","parent_id"];

    public function stars(){
        return $this->belongsTo("App\Model\Star","star_id");
    }

    public function user(){
        return $this->belongsTo("App\Model\User","user_id");
    }

    public function weaknesses(){
        return $this->hasMany("App\Model\Comments_weakness","comment_id")->where("type",1);
    }

    public function strengths(){
        return $this->hasMany("App\Model\Comments_weakness","comment_id")->where("type",0);;
    }
}
