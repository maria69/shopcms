<?php

namespace App\Model;

use App\Model\Media;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    use Sluggable;

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }


    protected $fillable = ["name","media_id"];

    public function products()
    {
        return $this->belongsToMany(Product::class);
    }

    public function media()
    {
        return $this->belongsTo(Media::class);
    }



}
