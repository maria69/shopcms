<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class checkUserAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            if (auth()->user()->status == 0) {
                return redirect("/")->withErrors(['msg', 'حساب کاربری فعال نمی باشد']);
            }
            return $next($request);
        }
        return redirect("/login");
    }
}
