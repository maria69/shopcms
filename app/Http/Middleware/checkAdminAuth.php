<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

class checkAdminAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

//        $route = Route::current();
//
//        $name = $route->getName();




        if( (Auth::guard("admins")->check() && Auth::guard("admins")->user()->status == 1 ) || Auth::guard("superAdmin")->check()){

            return $next($request);
        }
//        return 1;
        return redirect('/dashboard909/login');
    }
}
