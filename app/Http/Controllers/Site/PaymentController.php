<?php

namespace App\Http\Controllers\Site;

use App\Price;
use App\User;
use App\Factor;
use Illuminate\Cookie\CookieJar;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Morilog\Jalali\jDate;
use App\Http\Controllers\Site\GatewayController;

class PaymentController extends GatewayController
{
    public function charge()
    {
        $price = Price::all();
        $prices = ['0'=>'انتخاب نمایید'];
        foreach ($price as $record){
            $prices += array($record['id']=>$record['day']." روزه ".$record['price']. "  ریال  ");
        }
        return view('userarea.charge',['prices'=>$prices]);
    }

    public function makeXMLTree($data)
    {
        $ret = array();
        $parser = xml_parser_create();
        xml_parser_set_option($parser,XML_OPTION_CASE_FOLDING,0);
        xml_parser_set_option($parser,XML_OPTION_SKIP_WHITE,1);
        xml_parse_into_struct($parser,$data,$values,$tags);
        xml_parser_free($parser);
        $hash_stack = array();
        foreach ($values as $key => $val)
        {
            switch ($val['type'])
            {
                case 'open':
                    array_push($hash_stack, $val['tag']);
                    break;
                case 'close':
                    array_pop($hash_stack);
                    break;
                case 'complete':
                    array_push($hash_stack, $val['tag']);
                    // uncomment to see what this function is doing
                    //echo("\$ret['" . implode($hash_stack, "']['") . "'] = '{$val['value']}';\n");
                    eval("\$ret['" . implode($hash_stack, "']['") . "'] = '{$val['value']}';");
                    array_pop($hash_stack);
                    break;
            }
        }
        return $ret;
    }


    /* ------------------------------------- CURL POST TO HTTPS --------------------------------- */
    public function post2https($fields_arr, $url)
    {

        //url-ify the data for the POST
        $fields_string="";
        foreach($fields_arr as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
        $fields_string = substr($fields_string, 0, -1);

        //open connection
        $ch = curl_init();

        //set the url, number of POST vars, POST data
        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt($ch,CURLOPT_POST,count($fields_arr));
        curl_setopt($ch,CURLOPT_POSTFIELDS,$fields_string);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);


        //execute post
        $res = curl_exec($ch);

        //close connection
        curl_close($ch);
        return $res;
    }



    public function resultShow(Request $request){
        $array=null;
        if($request->has(["tref","iN","iD"])){
            $data=$request->only(["tref","iN","iD"]);
            $fields = array('invoiceUID' => $data['tref'] );
            $result = $this->post2https($fields,'https://pep.shaparak.ir/CheckTransactionResult.aspx');
            $array = $this->makeXMLTree($result);
            $factor=Factor::find($array['resultObj']['invoiceNumber']);
            if(!$factor){
                Session::flash("error","شماره فاکتور ارسالی از بانک با شماره فاکتور سایت همخوانی ندارد !");
                return view("resultShow",['data'=>$array]);
            }
            if($factor['status'] != 0){
                Session::flash("error","عملیات پرداخت این فاکتور قبلا انجام شده است");
                return view("resultShow",['data'=>$array]);
            }
            if(isset($array['resultObj']['result']) && $array['resultObj']['result']=="True"){
                //verify payment
                $fields = array(
                    'MerchantCode' => '4278044',			 // شماره پذیرنده
                    'TerminalCode' => '1446751',			 // شماره ترمینال
                    'InvoiceNumber' => $array['resultObj']['invoiceNumber'],			// شماره فاکتور 		(شماره فاکتور باید با شماره فاکتوری که در فایل ایندکس هست، یکسان باشد )
                    'InvoiceDate' => $array['resultObj']['invoiceDate'],  		   // تاریخ تراکنش      		با  $invoiceDate که در ایندکس هست یکسان باشد
                    'amount' => $array['resultObj']['amount'], 					  // مبلغ تراکنش		(مبلغ باید با مبلغی که در فایل ایندکس هست، یکسان باشد )
                    'TimeStamp' => date("Y/m/d H:i:s"), 	  // زمان جاری سیستم
                    'sign' => '' 						    // یک رشته سریالی که کد شده و اتوماتیک پر میشود و شما باید در این خط آن را خالی بگذارید
                );

                $data = "#". $fields['MerchantCode'] ."#". $fields['TerminalCode'] ."#". $fields['InvoiceNumber'] ."#". $fields['InvoiceDate'] ."#". $fields['amount'] ."#". $fields['TimeStamp'] ."#";
                $data = sha1($data,true);
                $data =  GatewayController::sign($data); // امضاي ديجيتال
                $fields['sign'] =  base64_encode($data); // base64_encode

                $verifyresult = $this->post2https($fields,'https://pep.shaparak.ir/VerifyPayment.aspx');
                $arrayVerify = $this->makeXMLTree($verifyresult);


                if(isset($arrayVerify['actionResult']['result']) && $arrayVerify['actionResult']['result']=="True"){
                    //charge User Account
                    Session::flash("ok","حساب کاربری شما با موفقیت شارژ شد - ".$arrayVerify['actionResult']['resultMessage']);
                    $day = (int)$factor['day'];
                    $user=User::find(Auth::user()->id);
                    if(Auth::user()->account_expire==null or strtotime(Auth::user()->account_expire) <= time()){
                        $expire_time = $day*24*60*60+time();
                    }else{
                        $expire_time = $day*24*60*60+(int)strtotime(Auth::user()->account_expire);
                    }
                    $user->update(['account_expire'=>date("Y-m-d H:i:s",$expire_time)]);

                    $factor->update(['status'=>1,'transactionReferenceID'=>$array['resultObj']['transactionReferenceID']]);
                }else{
                    Session::flash("error","خطا : ".$arrayVerify['actionResult']['resultMessage']." - در صورت کسر مبلغ از حساب شما , مبلغ برداشتی به زودی برگشت میخورد");
                    $factor->update(['status'=>2,'transactionReferenceID'=>$array['resultObj']['transactionReferenceID']]);
                    return view("userarea.resultShow",['factor'=>$array]);
                }
            }else{
                Session::flash("error","پرداخت نا موفق بود");
                $factor->update(['status'=>2,'transactionReferenceID'=>$array['resultObj']['transactionReferenceID']]);
            }
        }else{
            Session::flash("error","اطلاعات دریافتی ناقص است , لطفا با پشتیبانی سایت تماس بگیرید");
        }

        return view("userarea.resultShow",['factor'=>$array]);
    }

    public function dashboardPayments(){
        $factors = Factor::orderBy("created_at","DESC")->get();
        return view("dashboard.payments",['payments'=>$factors]);
    }
//
//    public function cart(CookieJar $cookieJar,Requests\PayRequest $request){
//        $cookieJar->queue($cookieJar->forget('cart'));
//        $price = Price::where('id','=',(int)$request->only('prices')['prices'])->first();
//        $factor_id = Factor::create(['day'=>$price['day'],'product'=>"اعتبار".$price['day']." روزه مشاوره ",'price'=>$price['price'],'name'=>Auth::user()->name,'user_id'=>Auth::user()->id]);
//
//        $request->request->add(['factor'=>$factor_id->id]);
//        $cookieJar->queue(cookie('cart', $request->only('prices','factor') , 45000));
//
//        $info = $request->only('prices','factor');
//        if(isset($info['prices']) && !empty($info['prices']))
//        {
//            $buy = Price::find($info['prices']);
//            return view("userarea.cart",['info'=>$info,'buy'=>$buy]);
//
//        }else{
//            Session::flash("error","هیچ موردی در سبد خرید یافت نشد");
//            return view("userarea.cart");
//        }
//    }
//
//
//    public function cartShow(Request $request){
//        $info = $request->cookie("cart");
//        if(isset($info['prices']) && !empty($info['prices']))
//        {
//            $buy = Price::find($info['prices']);
//            return view("userarea.cart",['info'=>$info,'buy'=>$buy]);
//
//        }else{
//            Session::flash("error","هیچ موردی در سبد خرید یافت نشد");
//            return view("userarea.cart");
//        }
//
//
//    }

    public function pay(Request $request){

        if(!is_null($price) && isset($price['price'])){
            $merchantCode = 4278044; // كد پذيرنده
            $terminalCode = 1446751; // كد ترمينال
            $amount = $price['price']; // مبلغ فاكتور
            $redirectAddress = url('userarea/tresult');
            $invoiceNumber = $input['factor']; //شماره فاكتور
            $timeStamp = date("Y/m/d h:m:i");
            $invoiceDate = date("Y/m/d h:m:i"); //تاريخ فاكتور
            $action = "1003"; 	// 1003 : براي درخواست خريد
            $data = "#". $merchantCode ."#". $terminalCode ."#". $invoiceNumber ."#". $invoiceDate ."#". $amount ."#". $redirectAddress ."#". $action ."#". $timeStamp ."#";
            $data = sha1($data,true);
            $data =  GatewayController::sign($data); // امضاي ديجيتال
            $result =  base64_encode($data); // base64_encode
            return view("userarea.gotobank",['result'=>$result,'invoiceDate'=>$invoiceDate,'invoiceNumber'=>$invoiceNumber,'amount'=>$amount,'terminalCode'=>$terminalCode,'action'=>$action,'timeStamp'=>$timeStamp,'redirectAddress'=>$redirectAddress,'merchantCode'=>$merchantCode]);

        }else{
            abort(403);
        }

    }


    public function pay_status(){
        return view("userarea.pay_status");
    }


    public function payments(){
        $factors = Factor::where("user_id","=",Auth::user()->id)->orderBy("created_at","DESC")->get();
        return view("userarea.payments",['factors'=>$factors]);
    }

}
