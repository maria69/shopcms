<?php

namespace App\Http\Controllers\Site;

use App\Model\OrderUserInfo;
use App\Model\OrderItem;
use App\Model\Payment;
use App\Model\Cart;
use App\Model\Order;
use App\Model\Off;
use App\Model\UserAddress;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Order::where('user_id', auth()->user()->id)->get();
//        return $orders;

//        foreach ($orders as $orderItem){
//            $orderItem = OrderItem::where('order_id',$orderItem->id)->get();
//        }


//return $orderItem;


        return view('client.order.index', compact('orders'));
    }

    public function track()
    {
        return view('client.order.track');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
////        $cart = getBasket();
//        if (isset($cart)) {
////            if (isset($request->offerCode)) {
////                $off = Off::where('code', $request->offerCode)->first();
////                $offerPrice = getDiscount($cart->final_price);
////                $offerCode = $off->code;
////
////            } else {
////                $offerPrice = $cart->final_price;
////                $offerCode = '0';
////            }
//            $userAddres = UserAddress::where('id', $request->address)->first();
//            $user = auth()->user();
//            $orderUserInfo = new OrderUserInfo();
//            $orderUserInfo->firstName = $user->first_name;
//            $orderUserInfo->lastName = $user->last_name;
//            $orderUserInfo->address = $userAddres->address;
//            $orderUserInfo->codeMelli = $user->code_melli;
//            $orderUserInfo->postCode = $userAddres->post_code;
//            $orderUserInfo->mobile = $userAddres->phone_number;
//            $orderUserInfo->phone = $user->phone;
//            $orderUserInfo->save();
//
//            $number = mt_rand(10000, 99999); // better than rand()
//            $order = new Order();
//            $order->user_id = $user->id;
//            $order->admin_id = 1;
//            $order->order_user_info_id = $orderUserInfo->id;
//            $order->offerPrice = $offerPrice;
//            $order->offerCode = $offerCode;
//            $order->tax = 0;
//            $order->totalPrice = $cart->final_price;
//            $order->status = 0;
//            $order->finalPrice = $cart->final_price;
//            $order->payment_id = $request->payment_id;
//            $order->shipping_id = $request->shipping_id;
//            $order->description = $request->description;
//            $order->trackingCode = $number;
////        return $order;
//            $order->save();
//
//            foreach ($cart->products as $product) {
//                $orderItem = new OrderItem();
//                $orderItem->product_id = $product->product_id;
//                $orderItem->unitPrice = $product->unit_price;
//                $orderItem->offer = $product->offer;
//                $orderItem->order_id = $order->id;
//                $orderItem->quantity = $product->count;
//                $orderItem->totalPrice = $product->total_price;
//                $orderItem->finalPrice = $product->final_price;
//                $orderItem->save();
//            }
//            $payment = Payment::where('id', $request->payment_id)->first();
//            Cart::find($cart->id)->delete();
//
//            if ($payment->type == 1) {
//                session()->flash("ok", "سفارش شما با موفقیت ایجاد شد - پرداخت آنلاین");
////            return redirect(route('pay',$order->finalPrice));
//            } else {
//                $orderInfo = $order;
//                return view('client.order.orderComplete', compact('orderInfo', 'payment'));
//            }
//
//            return redirect(route('cart'));
//        } else
//            return redirect(route('cart'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getItems($id)
    {

//        $order = Order::find($id);
        return (auth()->user()->id);

        $order = Order::where('id', $id)->where('user_id', auth()->user()->id);

        if ($order->exists()) {
            $order = $order->first();
        } else {
            $order = '';
        }
//        return($order) ;
//        return $order->order_item;

        return view('client.order.orderItems', compact('order'));


    }
}
