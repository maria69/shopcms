<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;

use App\Model\Brand;
use App\Model\Category;
use App\Model\Off;
use App\Model\Product;
use App\Model\Slider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{
    public function index()
    {

        $sliders = Slider::with('media')->get();
        $lastProducts = Product::with(['media'])->orderBy('sales')->limit(15)->get();
        $featuredProducts = Product::with(['media'])->where("featured",1)->limit(20)->get();
        $salesProducts = Product::with(['media'])->orderBy("sales","ASC")->limit(15)->get();
        $brands= Brand::with('media')->get();
        $categoriesAndProducts = Category::with(["products"=>function($query){
            $query->with("media");
        }])->where("parent_id",null)->get();

        $offer = Off::orderBy("offer","DESC")->pluck('id');

        $dailyPro = Product::whereIn('off_id',$offer)->take(5)->get();
//        return $dailyPro;
//        return $categoriesAndProducts;
        return view("newClient.index", compact('sliders','lastProducts','featuredProducts','salesProducts','categoriesAndProducts','brands','dailyPro'));
    }

    public function search(Request $request){

        if(!$request->has("s") || empty($request->s)){
            Session::flash("error","لطفا چیزی برای جستجو وارد کنید");
            return redirect()->back();
        }

        if($request->has("type") && $request->type > 0){

            $products = Product::where("type",$request->type)->where(function($q) use ($request){
                $q->where("name","LIKE","%".$request->s."%")->orWhere("en_name","LIKE","%".$request->s."%");
            })->select("name","en_name","price","id","media_id")->get();

        }else{
            $products = Product::with(["media"])->where("name","LIKE","%".$request->s."%")->orWhere("en_name","LIKE","%".$request->s."%")->select("name","en_name","price","id","media_id")->get();
        }

        return view('client.search',compact('products'));

    }
}
