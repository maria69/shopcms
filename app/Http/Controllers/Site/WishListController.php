<?php

namespace App\Http\Controllers\Site;

use App\Model\Wishlist;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;


class WishListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $wishlists = Wishlist::where('user_id', auth()->user()->id)->with(['product'])->get();
//        return $wishlists;
        return view('newClient.product.wishlist', compact('wishlists'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $wishlist = Wishlist::findOrFail($id)->delete();;
        Session::flash("ok", "محصول با موفقیت از لیست علاقه مندی ها حذف شد");
        return redirect()->back();
    }

    public function addWishList(Request $request)
    {
        if (Auth::guest()) {
            return response(['msg' => 'وارد شو', 'status' => 'error']);
        } else {
            $wishList = Wishlist::where('user_id', auth()->user()->id)->where('product_id', $request->id)->get();
            if (isset($wishList) && $wishList->count() > 0) {
                return response(['msg' => 'قبلا اضافه شده', 'status' => 'error']);
            } else {
                $wishList = Wishlist::create([
                    'user_id' => auth()->user()->id,
                    'product_id' => $request->id,
                ]);
            }
        }
        return response()->json(['msg' => 'اضافه شد', 'status' => 'success']);
    }
}
