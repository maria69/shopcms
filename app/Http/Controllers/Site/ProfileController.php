<?php
namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;

use App\Model\City;
use App\Model\UserAddress;
use App\Model\User;
use App\Model\Order;
use Illuminate\Http\Request;

class ProfileController extends Controller
{

    public function show()
    {
//        return auth()->user();
        $user = auth()->user();
        $cities= City::all()->pluck('name','id');
        $orders= Order::where('user_id',auth()->user()->id)->get();
        $userAddresses= UserAddress::where('user_id',$user->id)->with('city')->with('country')->get();
//        return $cities;
        return view('client.profile', compact('user','cities','userAddresses','orders'));
    }

    public function edit()
    {

    }

    public function updateUserInfo(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required|max:250',
            'last_name' => 'required|max:250',
            'code_melli' => 'required|max:10',
            'email' => 'required|email',
            'mobile' => 'required|max:12|min:10',
            'phone' => 'required|max:12|min:10',
            'password' => 'nullable|confirmed|min:8',
        ]);
//        return $request;
        if (isset($request->password)) {
            $user = User::find(auth()->user()->id)->update($request->all());
        } else {
            $user=User::find(auth()->user()->id)->update([
                'first_name' => $request->last_name,
                'last_name' => $request->last_name,
                'code_melli' => $request->code_melli,
                'email' => $request->email,
                'mobile' => $request->mobile,
                'phone' => $request->phone,
            ]);
        }

        session()->flash("ok", "اطلاعات حساب کاربری با موفقیت بروز رسانی شد");
        return redirect(route('userProfile'));


    }
}
