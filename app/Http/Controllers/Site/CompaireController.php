<?php

namespace App\Http\Controllers\Site;

use App\Model\ProductType;
use App\Model\Product;
use App\Model\FeatureCategory;
use App\Model\Feature_value;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class CompaireController extends Controller
{


    public function getProductsByType(Request $request){
        if(!$request->has("type")){
            abort(404);
        }

        return response()->json(Product::select("name","id")->where("type",$request->type)->get()->toArray());
    }



    public function index(Request $request){

        $oldCompaire = [];

        if(Session::has("compaire")){
            $data=Session::get("compaire")[0];
        }

        if(!$request->has("c1") && !$request->has("c2") && !$request->has("c3") && !$request->has("c4")){
            if(Session::has("compaire")){
                return redirect("/compaire".(isset($data['c1']) ? "?c1=".$data['c1'] : "").(isset($data['c2']) ? "&c2=".$data['c2'] : "").(isset($data['c3']) ? "&c3=".$data['c3'] : "").(isset($data['c4']) ? "&c4=".$data['c4'] : ""));
            }
        }

        $request->has("c1") ? $oldCompaire[] = $request->c1 : "";
        $request->has("c2") ? $oldCompaire[] = $request->c2 : "";
        $request->has("c3") ? $oldCompaire[] = $request->c3 : "";;
        $request->has("c4") ? $oldCompaire[] = $request->c4 : "";

        //$products = [];
        $first_product=Product::where("id",$request->c1)->first();

        if($first_product){
            $type=$first_product->type;
        }else{
            $type=ProductType::first()->id;
        }


        $allProducts=Product::where("type",$type)->pluck("name","id")->toArray();

        $compaire = [];

        foreach ($oldCompaire as $each_number) {
            $compaire[] = (int) $each_number;
        }

        if(count($compaire) > 0 ){
            $ids_ordered = implode(',', $compaire);
            $products=Product::with("media")->whereIn("id",$compaire)->where("type",$type)->orderByRaw(DB::raw("FIELD(id, $ids_ordered)"))->get();
        }


        $featuresCategory=FeatureCategory::with(["features"=>function($q) use ($type){
            $q->where("product_type_id",$type);
        }])->get();


        $featureValues = [];

        $request->has("c1") ? $featureValues[] = Feature_value::where("product_id",$request->c1)->pluck("value","feature_id")->toArray() : "";
        $request->has("c2") ? $featureValues[] = Feature_value::where("product_id",$request->c2)->pluck("value","feature_id")->toArray() : "";
        $request->has("c3") ? $featureValues[] = Feature_value::where("product_id",$request->c3)->pluck("value","feature_id")->toArray() : "";
        $request->has("c4") ? $featureValues[] = Feature_value::where("product_id",$request->c4)->pluck("value","feature_id")->toArray() : "";


        $types = ProductType::pluck("name","id");


        return view("client.product.compair",compact('products','types','type','allProducts','featuresCategory','featureValues'));


    }


    public function store(Request $request)
    {

        $sessionStatus = Session::has("compaire");
        $data = null;

        /* Redirect If Already Added To Compaire */
        if($sessionStatus){
            $data=Session::get("compaire")[0];
            for($i=1;$i<=4;$i++){
                if((isset($data["c".$i]) && $data["c".$i] == $request->c1) || (isset($data["c".$i]) && $data["c".$i] == $request->c2) || (isset($data["c".$i]) && $data["c".$i] == $request->c3) || (isset($data["c".$i]) && $data["c".$i] == $request->c4)){
                    return redirect()->back();
                }
            }
        }


        if($request->has(["type","c1"])){
            $type=ProductType::find($request->type);
            if(!$type){
                return redirect()->back();
            }
        }

        if($request->has(["c1","type"])){

            if($sessionStatus){
                Session::forget("compaire");
            }

            $data = ["c1"=>$request->c1,'type'=>$request->type];

            Session::push("compaire",$data);

            return redirect("/compaire?c1=".$request->c1);

        }else if($request->has(["c2"])){



            if(!$sessionStatus || is_null($data) || !isset($data["c1"])){
                return redirect("/compaire");
            }


            $product=Product::find($request->c2);


            if($product->type != $data['type']){
                Session::forget("compaire");
                Session::push("compaire",["c1"=>$request->c1,'type'=>$request->type]);
                return redirect("/compaire?c1=".$request->c1);
            }

            Session::forget("compaire");
            $data=["c2"=>$request->c2,'type'=>$data['type'],'c1'=>$data['c1']];
            Session::push("compaire",$data);
            return redirect("/compaire?c1=".$data['c1']."&c2=".$request->c2);

        }else if($request->has(["c3"])){


            if(!$sessionStatus || is_null($data) || !isset($data["c1"]) || !isset($data["c2"])){
                return redirect("/compaire");
            }


            $product=Product::find($request->c3);


            if($product->type != $data['type']){
                Session::forget("compaire");
                $data = ["c1"=>$request->c1,'type'=>$request->type];
                Session::push("compaire",$data);
                return redirect("/compaire?c1=".$request->c1);
            }

            Session::forget("compaire");
            $data = ["c3"=>$request->c3,'type'=>$data['type'],'c1'=>$data['c1'],'c2'=>$data['c2']];
            Session::push("compaire",$data);
            return redirect("/compaire?c1=".$data['c1']."&c2=".$data['c2']."&c3=".$request->c3);


        }else if($request->has(["c4"])){

            if(!$sessionStatus || is_null($data) || !isset($data["c1"]) || !isset($data["c2"]) || !isset($data["c3"])){
                return redirect("/compaire");
            }

            $product=Product::find($request->c4);

            if($product->type != $data['type']){
                Session::forget("compaire");
                $data = ["c1"=>$request->c1,'type'=>$request->type];
                Session::push("compaire",$data);
                return redirect("/compaire?c1=".$request->c1);
            }

            Session::forget("compaire");
            $data=["c4"=>$request->c4,'type'=>$data['type'],'c1'=>$data['c1'],'c2'=>$data['c2'],'c3'=>$data['c3']];
            //dd($data);
            Session::push("compaire",$data);
            return redirect("/compaire?c1=".$data['c1']."&c2=".$data['c2']."&c3=".$data['c3']."&c4=".$request->c4);


        }else{
            return redirect("/compaire");
        }


    }


    public function addCompaireToListFromProduct(Request $request){
        if(!$request->has("product_id")){
            return redirect()->back();
        }
        $product=Product::select("id","name","type")->findorfail($request->product_id);
        $count = 0;
        if(Session::has("compaire")){
            $data=Session::get("compaire")[0];
            for($i=1;$i<=4;$i++){
                if((isset($data["c".$i]) && $data["c".$i] == $request->c1) || (isset($data["c".$i]) && $data["c".$i] == $request->c2) || (isset($data["c".$i]) && $data["c".$i] == $request->c3) || (isset($data["c".$i]) && $data["c".$i] == $request->c4)){
                    return redirect()->back();
                }
                if(isset($data["c".$i])){
                    $count++;
                }
            }
        }
        if($count == 4){
            $data["c4"] = $product->id;
        }else{
            if($count == 0){
                $data["type"] = $product->type;
            }
            $data["c".($count+1)] = $product->id;
        }
        //dd($data);
        Session::forget("compaire");
        Session::push("compaire",$data);
        return redirect("/compaire".(isset($data['c1']) ? "?c1=".$data['c1'] : "").(isset($data['c2']) ? "&c2=".$data['c2'] : "").(isset($data['c3']) ? "&c3=".$data['c3'] : "").(isset($data['c4']) ? "&c4=".$data['c4'] : ""));
    }


    public function destroy(Request $request){

        if(!$request->has("cId")){
            return redirect()->back();
        }

        if(!Session::has("compaire")){
            return redirect()->back();
        }

        $data=Session::get("compaire")[0];

        if(!isset($data["c".$request->cId])){
            return redirect()->back();
        }


        $deletedNum = $request->cId;

        if(count($data) == 2){
            Session::forget("compaire");
            return redirect("/compaire");
        }

        if(isset($data["c".$deletedNum])){
            unset($data["c".$deletedNum]);
        }

        $newData = ['type'=>$data["type"]];

        $count=1;

        for($i=1;$i<=4;$i++){
            if(isset($data["c".$i])){
                $newData["c".$count]=$data["c".$i];
                $count++;
            }
        }

        unset($data);

//dd($newData);

        Session::forget("compaire");
        Session::push("compaire",$newData);
       // dd($data);

        return redirect("/compaire".(isset($newData['c1']) ? "?c1=".$newData['c1'] : "").(isset($newData['c2']) ? "&c2=".$newData['c2'] : "").(isset($newData['c3']) ? "&c3=".$newData['c3'] : "").(isset($newData['c4']) ? "&c4=".$newData['c4'] : ""));

    }

}
