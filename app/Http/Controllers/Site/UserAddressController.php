<?php

namespace App\Http\Controllers\Site;

use App\Model\City;
use App\Model\Country;
use App\Model\UserAddress;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserAddressRequest;
use Illuminate\Support\Facades\Auth;

class UserAddressController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserAddressRequest $request)
    {
//        $validData = $this->validate($request, [
//            'city_id' => 'required|exists:cities,id',
//            'country_id' => 'required|exists:countries,id',
//            'address' => 'required',
//            'post_code ' => 'required',
//            'phone_number' => 'required|max:12|min:10',
//        ]);
        $userAddress = UserAddress::create([
            'user_id' => auth()->user()->id,
            'city_id' => $request->city_id,
            'country_id' => $request->country_id,
            'address' => $request->address,
            'post_code' => $request->post_code,
            'phone_number' => $request->phone_number,
        ]);

        $info = $request->only([
            'user_id',
            'address',
            'post_code',
            'phone_number',
        ]);

        $info += ["city"=>City::find($request->city_id)->name,"country"=>Country::find($request->country_id)->name,'id'=>$userAddress->id];

        return response(['msg' => 'اضافه شد ', 'status' => 'success','info'=>$info]);
//        return response(['msg' => 'آدرس وارد شد', 'status' => 'succes']);
//        return redirect(route('userProfile'));


    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        UserAddress::where("user_id",Auth::user()->id)->findorfail($id)->delete();
        return response(['status'=>'success']);
    }
}
