<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Model\City;
use App\Model\Product;
use App\Model\Cart;
use App\Model\Shipping;
use App\Model\Payment;
use App\Model\Cart_product;
use App\Model\UserAddress;
use Illuminate\Cookie\CookieJar;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;

class CartController extends Controller
{
    /* User Login Status Will Stored Here */
    private $userLoginStatus = false;
    /* Cart Token Will Stored Here */
    private $cartToken = null;
    private $userCartByTokenStatus = false;

    public function show()
    {
        $cart = $this->getCurrentCartForUser();

        if (Auth::check()) {
            $user = auth()->user();
            $userAddresses = UserAddress::where('user_id', $user->id)->with('city')->with('country')->get();
        } else {
            $userAddresses = [];
        }
        $cities = City::all()->pluck('name', 'id');
        $shippments = Shipping::where('status', 1)->get();
        $payments = Payment::with('media')->get();


        $cart = ($cart);
//        dd($cart);
        return view("newClient.cart", compact('cart', 'userAddresses', 'cities', 'shippments', 'payments'));
    }

    /**
     * @param $price
     * @param $offer
     * @param int $type - 1 : Offer - 2 : Price Mines Offer
     * @return float|int - 1 : Offer - 2 : Price Mines Offer
     */
    private function getDiscount($price, $offer, $type = 1)
    {
        if ($type == 1) {
            return ($price / 100) * $offer;
        }
        return $price - (($price / 100) * $offer);
    }


    /* Get Cart From Cookie Or User Id */
    private function getCurrentCartForUser()
    {

        if (Auth::check()) {
            $this->userLoginStatus = true;
        }

        if ($this->userLoginStatus === true) {

            /* Get Cookie By User Id */
            $cart = Cart::with(["products" => function ($query) {
                $query->with(["product" => function ($query) {
                    $query->with("media");
                }]);
            }])->where("user_id", Auth::user()->id)->first();

        } else {

            /* Get Cookie By Cookie Token */
            if (Cookie::has("btk")) {
                $this->cartToken = Cookie::get("btk");
            }

            if ($this->cartToken) {
                $this->userCartByTokenStatus = true;
            }
            $cart = Cart::with(["products" => function ($query) {
                $query->with(["product" => function ($query) {
                    $query->with("media");
                }]);
            }])->where("cart_token", $this->cartToken)->first();
        }

        return $cart;

    }


    /* Create Cart With Cookie And User Id */
    private function createCartForUser()
    {
        $this->cartToken = md5(time() . microtime() . rand(rand(500, 1000), rand(500000, 100000)));
        if (Auth::check()) {
            $this->userLoginStatus = true;
        }
        $cartDataArray = [];
        if ($this->userLoginStatus) {
            $cartDataArray += ['user_id' => Auth::user()->id];
        }
        if (Cookie::has("btk")) {
            Cookie::forget("btk");
        }
        $cartDataArray += ['cart_token' => $this->cartToken];
        $cart = Cart::create($cartDataArray);
        return $cart;
    }


    public function addToCart(Request $request, CookieJar $cookieJar)
    {

//        return 1;
        if (!$request->has("id")) {
            abort(404);
        }

        $product = Product::findorfail($request->id);

        $cart = $this->getCurrentCartForUser();
        if (!$cart) {
            $cart = $this->createCartForUser();
            $cookieJar->queue(cookie('btk', $this->cartToken, 518400));
        }

        /* Add Item To Cart */

        if (is_null($product->offer_id)) {
            $offerPrecent = 0;
        } else {
            /* Get Offer From Offers Table */
        }

        $cart_after = $cart->update([
            'count' => $cart->count + 1,
            'total_price' => $cart->total_price + $product->price,
            'offer_price' => $cart->offer_price + $this->getDiscount($product->price, $offerPrecent),
            'final_price' => $cart->final_price + $this->getDiscount($product->price, $offerPrecent, 2),
        ]);

        $cartInProduct = Cart_product::where("cart_id", $cart->id)->where("product_id", $product->id)->first();

        if ($cartInProduct) {

            $cartInProduct->update([
                'total_price' => $cartInProduct->total_price + $product->price,
                'offer_price' => $cartInProduct->offer_price + $this->getDiscount($product->price, $offerPrecent),
                'final_price' => $cartInProduct->final_price + $this->getDiscount($product->price, $offerPrecent, 2),
                'count' => $cartInProduct->count + 1,
            ]);

        } else {

            $cartInProduct = Cart_product::create([
                'cart_id' => $cart->id,
                'product_id' => $product->id, 'unit_price' => $product->price,
                'total_price' => $product->price,
                'offer_price' => $this->getDiscount($product->price, $offerPrecent),
                'final_price' => $this->getDiscount($product->price, $offerPrecent, 2),
                'count' => 1,
            ]);

        }



        $cart = $this->getCurrentCartForUser();
//        $cart->final_price = number_format($cart->final_price);


//
        foreach ($cart['products'] as $pro) {

        $box =  $pro['product']['media'];

        if($box !== null) $pro['address'] = thumb($box, 5);
       else $pro['address'] = null;



//            return    $pro['product']['media']['name'];
        $pro['final_price'] = number_format($pro['final_price']);
        $pro['unit_price'] = number_format($pro['unit_price']);
        $pro['total_price'] = number_format($pro['total_price']);



//        return $pro;
    }

    $cart->final_price =  number_format($cart->final_price);

//return  $pro['total_price'];
        return response(['status' => 'success', 'msg' => ' محصول ' . " " . $product->name . " " . 'به سبد خرید اضافه شد', 'data' => $cart]);

    }


    public function counter(Request $request, $dir)
    {

        if (!$request->has("id") || ($dir != "+" && $dir != "-")) {
            return response(['status' => 'error', 'msg' => 'اطلاعات ارسالی به سیستم نادرست است']);
        }
        $cart = $this->getCurrentCartForUser();

        if (!$cart) {
            return response(['status' => 'error', 'msg' => 'سبد خرید ثبت شده با سیستم مطابقت ندارد']);
        }

        $cartInProduct = Cart_product::where("cart_id", $cart->id)->where("id", $request->id)->first();

        if (!$cartInProduct) {
            return response(['status' => 'error', 'msg' => 'محصول مورد نظر در سیستم ثبت نشده است , لطفا صفحه را دوباره باز کنید']);
        }

        $product = Product::findorfail($cartInProduct->product_id);

        if (is_null($product->offer_id)) {
            $offerPrecent = 0;
        } else {
            /* Get Offer From Offers Table */
        }

        $total_price = $cart->total_price;
        $offer_price = $cart->offer_price;
        $final_price = $cart->final_price;
        $count = $cart->count;

        if ($dir == "+") {

            $cartProductsData = [
                'total_price' => $cartInProduct->total_price + ($product->price),
                'offer_price' => $cartInProduct->offer_price + ($this->getDiscount($product->price, $offerPrecent)),
                'final_price' => $cartInProduct->final_price + ($this->getDiscount($product->price, $offerPrecent, 2)),
                'count' => $cartInProduct->count + 1,
            ];

            $cart->update([
                'count' => ($count + 1),
                'total_price' => $total_price + ($product->price),
                'offer_price' => $offer_price + ($this->getDiscount($product->price, $offerPrecent)),
                'final_price' => $final_price + ($this->getDiscount($product->price, $offerPrecent, 2)),
            ]);

            $cartInProduct->update($cartProductsData);

            return response(array_merge($cartProductsData, ['status' => 'success', 'count' => $cartInProduct->count]));
        } else {
            $cartProductsData = [
                'total_price' => $cartInProduct->total_price - ($product->price),
                'offer_price' => $cartInProduct->offer_price - ($this->getDiscount($product->price, $offerPrecent)),
                'final_price' => $cartInProduct->final_price - ($this->getDiscount($product->price, $offerPrecent, 2)),
                'count' => $cartInProduct->count - 1,
            ];


            if (($cartInProduct->count - 1) <= 0) {
                return $this->deleteById($cartInProduct->id);
                //$this->delete($product->id);
            } else {
                $cart->update([
                    'count' => ($count - 1),
                    'total_price' => $total_price - ($product->price),
                    'offer_price' => $offer_price - ($this->getDiscount($product->price, $offerPrecent)),
                    'final_price' => $final_price - ($this->getDiscount($product->price, $offerPrecent, 2)),
                ]);

                $cartInProduct->update($cartProductsData);

            }

            /*if($cartProductsData['count'] == 0){
                $cartProductsData['delete'] = 1;
            }*/

            return response(array_merge($cartProductsData, ['status' => 'success', 'count' => $cartInProduct->count]));
        }

    }


    public function delete(Request $request)
    {


        if (!$request->has("id")) {
            return response(['status' => 'error', 'msg' => 'اطلاعات ارسالی به سیستم نادرست است']);
        }

        return $this->deleteById($request->id);


        return response(['cart' => $cart]);

    }


    private function deleteById($id = null)
    {
        if (is_null($id)) {
            return response(['status' => 'error', 'msg' => 'اطلاعات ارسالی به سیستم نادرست است']);
        }

        $cart = $this->getCurrentCartForUser();
        if (!$cart) {
            return response(['status' => 'error', 'msg' => 'سبد خرید ثبت شده با سیستم مطابقت ندارد']);
        }

        $cartInProduct = Cart_product::where("cart_id", $cart->id)->where("id", $id)->first();

        if (!$cartInProduct) {
            return response(['status' => 'error', 'msg' => 'محصول مورد نظر در سیستم ثبت نشده است , لطفا صفحه را دوباره باز کنید']);
        }

        $product = Product::findorfail($cartInProduct->product->id);
        if (is_null($product->offer_id)) {
            $offerPrecent = 0;
        } else {
            /* Get Offer From Offers Table */
        }

        $cart->update([
            'count' => zeroHandler($cart->count - $cartInProduct->count),
            'total_price' => $cart->total_price - ($product->price * $cartInProduct->count),
            'offer_price' => $cart->offer_price - ($this->getDiscount($product->price, $offerPrecent) * $cartInProduct->count),
            'final_price' => $cart->final_price - ($this->getDiscount($product->price, $offerPrecent, 2) * $cartInProduct->count),
        ]);

        $cartInProduct->delete();

        $cart = $this->getCurrentCartForUser();

        $cart->final_price = number_format($cart->final_price);
        $cart->unit_price = number_format($cart->unit_price);

        return response(['status' => 'success', 'delete' => 1, 'cart' => $cart]);
    }

}
