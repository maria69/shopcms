<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;

use App\Model\Comment;
use App\Model\Comment_like;
use App\Model\Product;
use App\Model\Star;
use App\Model\Comments_weakness;
use App\Model\Comment_Star;
use App\Model\Category;
use App\Model\FeatureCategory;
use App\Model\Feature_value;
use App\Http\Requests\CommentRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class ProductController extends Controller
{
    public function show($id){

        $product=Product::with(["categories",'medias','OrderItem'])->whereSlug($id)->first();
        $featuresCategory=FeatureCategory::with(["features"=>function($q) use ($product){
            $q->where("product_type_id",$product->type);
        }])->get();
//        return $product;
        $featureValues = Feature_value::where("product_id",$product->id)->pluck("value","feature_id")->toArray();
        $comments = Comment::with(["stars","user","strengths","weaknesses"])->where("product_id",$product->id)->get();
        $stars = Star::all();
        $float        = $product->score;
        $parts        = explode('.', (string)$float);
        $star_int = $parts[0];
        if(!isset($parts[1])){
            $star_float = 0;
        }else{
            $star_float    = trim($parts[1], '0');
        }
        $catIds = $product->categories->pluck("id")->toArray();
        $similarProductsCat = Category::whereIn("id",$catIds)->with(["products"=>function($q){
            $q->with("media")->orderByRaw("RAND()")->limit(15);
        }])->get();
        return view("newClient.single_product",compact('product','featuresCategory','featureValues','comments','stars','star_int','star_float','similarProductsCat'));

    }


    public function addComment(CommentRequest $request){
        $product=Product::findorfail($request->product_id);
        $stars = Star::select("id")->get();
        $score = 0;
        foreach($stars as $star){
            $p="stars".$star->id;
            if($request->has($p)){
                $score+=$request->{$p};
            }
        }
        $score = number_format((float)$score/$stars->count(),2,".","");
        $request->request->add([
            "score"=>$score,
            "user_id"=>Auth::user()->id,
            'product_id'=>$product->id,
        ]);
        $comment = Comment::create($request->all());
        $commentStarInsertArray = [];
        foreach($stars as $star){
            $p="stars".$star->id;
            if($request->has($p)){
                $count = $request->{$p};
            }else{
                $count = 0;
            }
            $commentStarInsertArray[] = ["star_id"=>$star->id,"comment_id"=>$comment->id,"count"=>$count];
        }

        (is_array($commentStarInsertArray) && count($commentStarInsertArray) > 0) ? Comment_Star::insert($commentStarInsertArray) : "";

        unset($commentStarInsertArray);
        $commentStrengthsInsertArray = [];

        if($request->has("strengths") && is_array($request->strengths)){
            foreach ($request->strengths as $strength) {
                $commentStrengthsInsertArray[] = ["description"=>$strength,"comment_id"=>$comment->id,"type"=>1];
            }
        }


        if($request->has("weaknesses") && is_array($request->weaknesses)){
            foreach ($request->weaknesses as $weaknesses) {
                $commentStrengthsInsertArray[] = ["description"=>$weaknesses,"comment_id"=>$comment->id,"type"=>0];
            }
        }

        (is_array($commentStrengthsInsertArray) && count($commentStrengthsInsertArray) > 0) ? Comments_weakness::insert($commentStrengthsInsertArray) : "";

        Session::flash("comment","ok");
        Session::flash("ok","دیدگاه با موفقیت ارسال شد و بعد از تایید کارشناس سایت نمایش داده میشود");
        return redirect()->back();
    }



    public function like(Request $request){

        if(!Auth::check()){
            return response(["msg"=>"برای رای دهی باید وارد سایت شوید","status"=>"error"]);
        }

        if(!$request->has("id") || !$request->has("dir") ){
            return response(["msg"=>"اطلاعات ارسالی به سیستم نادرست است","status"=>"error"]);
        }

        $comment = Comment::findorfail($request->id);

        ($request->dir == "up") ? $type = 1 : $type = 0;

        $like = Comment_like::where("user_id",Auth::user()->id)->where("comment_id",$request->id)->first();

        if($request->dir == "up"){
            if($like && $type == $like->type){
                $like->delete();
                $comment->update(["like"=>$comment->like-1]);
            }else{
                if($like && $type != $like->type){
                    $like->delete();
                    $comment->update(["unlike"=>$comment->unlike-1]);
                }
                Comment_like::create(["user_id"=>Auth::user()->id,"comment_id"=>$request->id,"type"=>$type]);
                $comment->update(["like"=>$comment->like+1]);
            }
        }else if($request->dir == "down"){
            if($like && $type == $like->type){
                $like->delete();
                $comment->update(["unlike"=>$comment->unlike-1]);
            }else{
                if($like && $type != $like->type){
                    $like->delete();
                    $comment->update(["like"=>$comment->like-1]);
                }
                Comment_like::create(["user_id"=>Auth::user()->id,"comment_id"=>$request->id,"type"=>$type]);
                $comment->update(["unlike"=>$comment->unlike+1]);
            }
        }


        return response(["like"=>$comment->like,"unlike"=>$comment->unlike,"status"=>"success"]);

    }
}
