<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;

use App\Model\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index()
    {
    }

    public function show($id)
    {
//        $this->validate($id, [
//            'id' => 'exists:categories'
//        ]);
        $menu = Category::get();
        $category = Category::with(['products' => function ($query) {
            $query->with(["media"])->paginate(1);
        }, 'media'])->whereSlug($id)->first();
        //$products = $category->products->pluck('media')->flatten();
//        return $category;
//        $paginate = Category::find($id)->products()->paginate(1);
//        return $produc;
        return view('newClient.category', compact('category', 'menu'));
    }

    public
    function edit()
    {

    }

    public
    function update()
    {

    }

    public
    function destroy()
    {

    }
}
