<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/user/profile';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }


    protected function basket($id){
        $cart = getBasketWithCookie();
        if($cart){
            deleteUserCart();
            $cart->update(['user_id'=>$id]);
        }
    }

    public function myLogin(Request $request)
    {
        $redirect = "/";

        if($request->has("redirect")){
            $this->redirectTo = '/cart#two';
            $redirect = "/cart#two";
        }

        $mobile = $request->email; //the input field has name='username' in form
        $password = $request->password;
        if (Auth::attempt(['email' => $mobile, 'password' => $password])) {
            //user sent their email
            $this->basket(Auth::user()->id);
            return redirect($redirect);
        } elseif (Auth::attempt(['mobile' => $mobile, 'password' => $password])) {
            //they sent their username instead
            $this->basket(Auth::user()->id);
            return redirect($redirect);
        }

        //Nope, something wrong during authentication
        return redirect()->back()->withErrors([
            'credentials' => 'نام کاربری یا رمز عبور اشتباه است'
        ]);
    }

    /**
     * Validate the user login request.
     *
     * @param  \Illuminate\Http\Request $request
     * @return void
     */

    protected function validateLogin(LoginRequest $request)
    {
        $req = new LoginRequest();
        $this->validate($request, $req->rules());
    }


    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     */
    public function login(LoginRequest $request)
    {
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {
            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }
}
