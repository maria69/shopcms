<?php

namespace App\Http\Controllers\Auth;

//use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;

class AdminAuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard909';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }


    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        return view('panel.auth.login');
    }

    public function login(Request $request)
    {

        if (Auth::guard('admins')->attempt(['email' => $request->email, 'password' => $request->password])) {
            //Authentication passed...
//            $user = Auth::guard('admins')->user();
//            return $user;

            return redirect()
                ->intended(route('dashboard'))
                ->with('status', 'You are Logged in as Admin!');

        } elseif (Auth::guard('superAdmin')->attempt(['email' => $request->email, 'password' => $request->password])) {
//            $user = Auth::guard('superAdmin')->user();
//            return $user;
            return redirect()
                ->intended(route('dashboard'))
                ->with('status', 'You are Logged in as superAdmin!');

        } else {
            return 2;
        }
        //Authentication failed...
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard("admins");
    }
}
