<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Model\ProductOptionCategory;
use App\Http\Controllers\Controller;

class ProductOptionCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ProductOptionCategories= ProductOptionCategory::all();
        return view('panel.productOptionCategories.index',compact('ProductOptionCategories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('panel.productOptionCategories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'type' => 'required'
        ]);
        ProductOptionCategory::create($request->all());
        session()->flash("ok", "دشته بندی آپشن های محصولات با موفقیت ایجاد شد");
        return redirect(route("productOptionCategories.index"));

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ProductOptionCategory= ProductOptionCategory::findOrFail($id);
        return view('panel.productOptionCategories.edit',compact('ProductOptionCategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        ProductOptionCategory::find($id)->update($request->all());
        session()->flash('ok', "با موفقیت به روز رسانی شد");
        return redirect(route('productOptionCategories.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ProductOptionCategory::find($id)->delete();
        session()->flash('ok', "با موفقیت به حذف شد");
        return redirect(route('productOptionCategories.index'));
    }
}
