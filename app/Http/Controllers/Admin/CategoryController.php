<?php

namespace App\Http\Controllers\Admin;

use App\Model\Category;
use App\Model\CategoryType;
use Illuminate\Contracts\Session\Session;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class CategoryController extends Controller
{
    public function index()
    {
        $categories = Category::with('parent')->get();
        //dd($categories);
        return view('panel.categories.index', compact('categories'));
    }

    public function create()
    {
        $categories = [null => "بدون دسته بندی پدر"];
        $categories += Category::all()->pluck('name', 'id')->toArray();
        return view('panel.categories.create', compact('categories'));
    }

    public function store(Request $request)
    {
        if (!isset($request->status))
            $request->request->add(['status' => 0]);
        $result = Category::create($request->all());
        session()->flash('ok', 'دسته بندی با موفقیت آپلود شد');
        return redirect(route('categories.index'));
    }

    public function edit(Request $request, $id)
    {
        $validData = $this->validate($request, [
            'id' => 'exists:categories',
        ]);
        $category = Category::with('parent')->with('media')->find($id);
        $categories = [null => "بدون دسته بندی پدر"];
        $categories += Category::all()->pluck('name', 'id')->toArray();
        return view('panel.categories.edit', compact('category', 'categories'));
    }

    public function update(Request $request, $id)
    {
//        return $request;
        if (isset($request->status))
            $request->request->add(['status' => 1]);
        else
            $request->request->add(['status' => 0]);
//        return $request;
        $resualt = Category::find($id)->update($request->all());
        session()->flash('ok', 'دسته بندی با موفقیت بروزرسانی گردید');
        return redirect(route('categories.index'));
    }

    public function destroy($id)
    {
        $resualt = Category::find($id)->destroy($id);
        session()->flash('ok', 'دسته بندی با موفقیت حذف گردید');
        return redirect(route('categories.index'));


    }
}
