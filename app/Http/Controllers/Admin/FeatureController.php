<?php

namespace App\Http\Controllers\Admin;

use App\Model\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Feature;
use App\Model\FeatureCategory;
use App\Model\ProductType;
use Illuminate\Contracts\Session\Session;


class FeatureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $features = Feature::with(['productType','featureCategory'])->orderBy('created_at', 'desc')->get();
        return view('panel.features.index', compact('features'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $productTypes = ProductType::all()->pluck('name', 'id');
        $featureCategories = FeatureCategory::all()->pluck('name', 'id');
        return view("panel.features.create", compact('productTypes', 'featureCategories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:2|max:120',
            'product_type_id' => 'exists:product_types,id',
            'feature_category_id' => 'exists:feature_categories,id',
        ]);
        Feature::create($request->all());
        session()->flash("ok", "ویژگی محصول با موفقیت ایجاد شد");
        return redirect(route("features.index"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $productTypes = ProductType::all()->pluck('name', 'id');
        $featureCategories = FeatureCategory::all()->pluck('name', 'id');
        $feature = Feature::find($id);
        return view("panel.features.edit", compact('feature', 'productTypes','featureCategories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|min:2|max:120',
            'product_type_id' => 'exists:product_types,id'
        ]);
        Feature::find($id)->update($request->all());
        session()->flash("ok", "ویژگی محصول با موفقیت ویرایش شد");
        return redirect(route('features.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Feature::find($id)->delete();
        session()->flash("ok", "ویژگی محصول با موفقیت حذف شد");

        return redirect(route('features.index'));
    }
}
