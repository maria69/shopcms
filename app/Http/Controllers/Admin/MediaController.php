<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\MediaRequest;
use App\Http\Requests\SearchMediaRequest;
use App\Model\Media;
use Carbon\Carbon;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Image;
use File;
use App\Http\Controllers\Controller;

class MediaController extends Controller
{


    public function ajaxSearch(SearchMediaRequest $request)
    {

        $searchQuery = $request->only("search");

        $result = Media::where('name', 'LIKE', "%$searchQuery[search]%")->get();

        if ($result->isEmpty()) {
            return response()->json([
                'errorStatus' => 1
            ]);
        }

        return response()->json([
            'errorStatus' => 0,
            'searchResult' => $result
        ]);

    }


    public function index()
    {
        if (Auth::guard("admins")->check()) {
            $media = Media::where('creator_id', Auth::guard("admins")->user()->id)->orderBy("id", "desc")->orderBy("id", "desc")->paginate(20);
        } elseif (Auth::guard("superAdmin")->check()) {
            $media = Media::orderBy("id", "desc")->paginate(20);
        }

//        $media = Media::orderBy("id", "desc")->paginate(20);
        return view("panel.common.media", ['paginate' => $media, 'media' => $media]);
    }

    public function update(Request $request, $id)
    {

        $media = Media::find($id);

        $data = $request->only(['name']);

        if (!$media) {
            return json_encode(['errorStatus' => 1]);
        }

        $data['name'] = current(explode(".", $data['name']));

        $media->update(['name' => $data['name'] . "." . $media['extension']]);

        return json_encode(['errorStatus' => 0, 'name' => $data['name'] . "." . $media['extension']]);

    }


    public function store(MediaRequest $request)
    {
        $files = $request->only('uploads');
        $filesDate = [];
        if (Auth::guard("admins")->check()) {
            $userId = Auth::guard("admins")->user()->id;
            $userType = 'admins';
        } elseif (Auth::guard("superAdmin")->check()) {
            $userId = Auth::guard("superAdmin")->user()->id;
            $userType = 'superAdmin';
        }
        if (isset($files['uploads']) && count($files['uploads']) > 0) {

            $date = Carbon::now()->toDateTimeString();
            $count = 0;
            foreach ($files['uploads'] as $file) {
                $filesDate[$count]['size'] = $file->getClientSize();
                $filesDate[$count]['extension'] = $file->getClientOriginalExtension();
                $filesDate[$count]['name'] = $file->getClientOriginalName();
                $filesDate[$count]['creator_id'] = $userId;
                $filesDate[$count]['creator_type'] = $userType;
                $filesDate[$count]['hash'] = md5(rand(1231, 5654765) . str_random(10) . microtime());
                $filesDate[$count]['created_at'] = $date;
                $filesDate[$count]['updated_at'] = $date;
                $media = Media::create($filesDate[$count]);
                $filesDate[$count]['id'] = $media->id;
                $createdTime = strtotime($media->created_at);
                defined('DS') ? null : define('DS', DIRECTORY_SEPARATOR);
                if (!file_exists(public_path(DS . "media" . DS . date("Y", $createdTime) . DS))) {
                    mkdir(public_path(DS . "media" . DS . date("Y", $createdTime) . DS), 777);
                }
                if (!file_exists(public_path(DS . "media" . DS . date("Y", $createdTime) . DS . date("m", $createdTime)))) {
                    mkdir(public_path(DS . "media" . DS . date("Y", $createdTime) . DS . date("m", $createdTime)), 777);
                }
                if (!file_exists(public_path(DS . "media" . DS . date("Y", $createdTime) . DS . date("m", $createdTime) . DS . date("d", $createdTime) . DS))) {
                    mkdir(public_path(DS . "media" . DS . date("Y", $createdTime) . DS . date("m", $createdTime) . DS . date("d", $createdTime) . DS), 777);
                }
                if (!$file->move(public_path(DS . "media" . DS . date("Y", $createdTime) . DS . date("m", $createdTime) . DS . date("d", $createdTime) . DS), $filesDate[$count]['hash'] . "." . $filesDate[$count]['extension'])) {
                    return json_encode(['errorStatus' => 1, 'errorText' => 'مشکلی در آپلود یکی از فایل ها بروز داده است']);
                }
                $count++;
            }

        }

        return json_encode(['errorStatus' => 0, 'files' => $filesDate]);
    }


    public function destroy($id)
    {


        $media = Media::find($id);

        if (!$media) {
            abort(404);
        }

        if ($media['usage'] == 0) {

            $createdTime = strtotime($media->created_at);

            File::delete(public_path() . "/media/" . date("Y", $createdTime) . "/" . date("m", $createdTime) . "/" . date("d", $createdTime) . "/" . $media['hash'] . "." . $media['extension']);
            if ($media['extension'] == "png" || $media['extension'] == "jpg" || $media['extension'] == "jpeg") {
                File::delete(public_path() . "/media/" . date("Y", $createdTime) . "/" . date("m", $createdTime) . "/" . date("d", $createdTime) . "/" . $media['hash'] . "-thumb." . $media['extension']);
            }
            $media->delete();
            return response()->json(['errorStatus' => 0]);

        } else {

            return response()->json(['errorStatus' => 1]);

        }

    }


    public function picker()
    {


        if (Auth::guard("admins")->check()) {
            $media = Media::where('creator_id',auth()->guard('admins')->user()->id)->orderBy("id", "desc")->limit(12)->get();
        } elseif (Auth::guard("superAdmin")->check()) {
            $media = Media::where('creator_id',auth()->guard('superAdmin')->user()->id)->orderBy("id", "desc")->limit(12)->get();
        }



        return response()->json(['searchResult' => $media]);
    }
}