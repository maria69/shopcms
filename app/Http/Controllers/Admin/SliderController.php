<?php

namespace App\Http\Controllers\Admin;

use App\Model\Slider;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class SliderController extends Controller
{
    public function index()
    {
        $sliders = Slider::all();
        return view('panel.sliders.index', compact('sliders'));
    }

    public function create()
    {
        return view("panel.sliders.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'media_id' => 'nullable|exists:media,id'
        ]);
        if (!isset($request->status))
            $request->request->add(['status' => 0]);

        Slider::create($request->all());
        Session::flash("ok", "اسلایدر با موفقیت ایجاد شد");
        return redirect(route("sliders.index"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $slider = Slider::with("media")->find($id);
        return view("panel.sliders.edit", compact('slider'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'media_id' => 'nullable|exists:media,id'
        ]);
        Slider::find($id)->update($request->all());
        Session::flash("ok", "اسلایدر با موفقیت بروزسانی شد");
        return redirect(route('sliders.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Slider::find($id)->delete();
    }

}
