<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Model\Admin;
//use App\Model\Role;
use App\Model\Permission;
//use App\Model\UserRole;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $admins = Admin::all();
        return view("panel.admins.index", compact("admins"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::pluck('name', 'id');
        $permissions = Permission::pluck('key', 'id');
        return view("panel.admins.create", compact('roles', 'permissions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        return $request->permissions[0];
        $admin = Admin::create($request->all());
        foreach ($request->roles as $key => $role) {
            $userrole = new UserRole();
            $userrole->user_id = $admin->id;
            $userrole->role_id = $role;
            $userrole->save();
            foreach ($request->permissions[$key] as $permission) {
                $userrole->permissions()->attach($permission);
            }
        }
        return $admin;
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $admin = Admin::findorfail($id);
        return view("panel.admins.edit", compact('admin'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Admin::findorfail($id)->update($request->all());
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Admin::findorfail($id)->delete();
        return redirect()->back();
    }

    public function login(Request $request)
    {


//        $this->validator($request);

        if(Auth::guard('admin')->attempt($request->only('email','password'),0)){
            //Authentication passed...
            return 1;

        }

        //Authentication failed...
        return 2;
    }
}
