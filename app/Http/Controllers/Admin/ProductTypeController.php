<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\ProductType;
use Illuminate\Http\Request;
use App\Model\Feature;
use Illuminate\Http\Response;

class ProductTypeController extends Controller
{
    public function index()
    {
        $productTypes= ProductType::all();
        return view('panel.productType.index',compact('productTypes'));
        
    }
    public function create()
    {
        return view('panel.productType.create');
    }

    public function store(Request $request)
    {
        $productType= new ProductType();
        $productType->name =  $request->name;
        $productType->status =  $request->status;
        $productType->save();
        session()->flash('ok', 'نوع دسته بندی با موفقیت آپلود شد');
        return redirect(route('product_types.index'));

    }
    public function edit(Request $request,$id)
    {
        $validData = $this->validate($request, [
            'id' => 'exists:categories',
        ]);
        $productType= ProductType::find($id);
        return view('panel.productType.edit',compact('productType'));
    }

    public function update(Request $request,$id)
    {
//        return $request;
        $resualt= ProductType::find($id)->update($request->all());
        session()->flash('ok','نوع دسته بندی با موفقیت بروزرسانی گردید');
        return redirect(route('product_types.index'));
    }

    public function destroy($id)
    {
        $resualt= ProductType::find($id)->destroy($id);
        session()->flash('ok','نوع دسته بندی با موفقیت حذف گردید');
        return redirect(route('product_types.index'));


    }

    public function getTypes(Request $request){
        if(isset($request->type) && !empty($request->type)){
            return response()->json(Feature::where("product_type_id",$request->type)->get());
        }

        return abort(404);
    }


    public function getTypesValues(Request $request){
        if(isset($request->type) && !empty($request->type)){
            return response()->json(Feature::with("feature_values")->where("product_type_id",$request->type)->get());
        }

        return abort(404);
    }

}
