<?php

namespace App\Http\Controllers\Admin;

use App\Model\Off;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class OfferController extends Controller
{
    public function index()
    {
        $offers = Off::all();
        return view('panel.offers.index', compact('offers'));
    }

    public function create()
    {
        return view('panel.offers.create');
    }

    public function store(Request $request)
    {
//        $start_time = \Morilog\Jalali\jDateTime::toGregorian($request->start_date); // [2016, 5, 7]
//        $end_time = \Morilog\Jalali\jDateTime::toGregorian($request->end_date); // [2016, 5, 7]
//        return $request;

        $validData = $this->validate($request, [
            'name' => 'required',
            'offer' => 'required',
            'offer_type' => 'required',
            'start_year' => 'required',
            'start_month' => 'required',
            'start_day' => 'required',
            'end_day' => 'required',
            'end_month' => 'required',
            'end_year' => 'required',
        ]);

        $request->request->add(['start_date' => covertShamsiToMiladi($request->start_date)]);
        $request->request->add(['end_date' => covertShamsiToMiladi($request->end_date)]);
        $offer = Off::create($request->all());
        session()->flash('ok', 'کد تخفیف با موفقیت ایجاد شد');
        return redirect(route('offers.index'));
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $offer = Off::findOrFail($id);
        return view('panel.offers.edit', compact('offer'));
    }

    public function update(Request $request, $id)
    {
        $validData = $this->validate($request, [
            'name' => 'required',
            'offer' => 'required',
            'offer_type' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
        ]);
        $offer = Off::findOrFail($id);
        $request->request->add(['start_date' => covertShamsiToMiladi($request->start_date)]);
        $request->request->add(['end_date' => covertShamsiToMiladi($request->end_date)]);
        $offer->update([$request->all()]);
        session()->flash('ok', 'کد تخفیف با موفقیت ویرایش شد');
        return redirect(route('offers.index'));

    }

    public function destroy($id)
    {
        Off::find($id)->delete();
        return redirect(route('offers.index'));

    }
}
