<?php

namespace App\Http\Controllers\Admin;

use App\Model\Brand;
use App\Model\ProductOption;
use App\Model\Category;
use App\Model\Feature;
use App\Model\Feature_value;
use App\Model\Product_option_product;
use App\Model\Warranty;
use Illuminate\Support\Facades\Validator;
use App\Model\Product;
use App\Http\Requests\ProductRequest;
use App\Model\ProductType;
use Illuminate\Contracts\Session\Session;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::with(["media", "categories"])->get();

//        return $products;
        return view('panel.products.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $productOptions = ProductOption::pluck("name", "id");
//        $warranties= Warranty::with('company')->get();
        $categories = Category::with('childrens')->where("parent_id", null)->get();
        $types = ProductType::pluck("name", "id");
        $brands = Brand::pluck("name", "id");
        $features = Feature::where("product_type_id", 1)->get();
        return view("panel.products.create", compact('categories', 'brands', 'types', 'features', 'productOptions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {

        $request->request->add(["keywords" => $request->meta]);

        $product = Product::create($request->all());

        //dd($request->galleryImages);

        if (isset($request->galleryImages) && !empty($request->galleryImages) && is_array($request->galleryImages) && isset($request->galleryImages[0]) && $request->galleryImages[0] != null) {
            $product->medias()->attach($request->galleryImages);
        }

        $product->categories()->attach($request->categories);
//        $product->warranties()->attach($request->warranties);

        $features = Feature::where("product_type_id", $request->type)->get();
        $featureInsertArray = [];

        foreach ($features as $feature) {
            $featureInput = "feature_value" . $feature->id;
            $request->has($featureInput) ? ($featureInsertArray[] = ['product_id' => $product->id, 'value' => $request->{$featureInput}, 'feature_id' => $feature->id]) : "";
        }

        Feature_value::insert($featureInsertArray);

        $optionsArray = [];

        if ($request->has("countOption") && $request->has("priceOption") && $request->has("option1") && $request->has("option2")) {
            for ($i = 0; $i < count($request->priceOption); $i++) {
                if (!is_null($request->option1[$i]) && !is_null($request->priceOption[$i]) && !is_null($request->countOption[$i]) && !is_null($request->option2[$i])) {
                    $optionsArray[] = ["product_id" => $product->id, "product_option_id_1" => $request->option1[$i], "product_option_id_2" => $request->option2[$i], "price" => $request->priceOption[$i], "count" => $request->countOption[$i]];
                }
            }
            Product_option_product::insert($optionsArray);
        }

        session()->flash("ok", "محصول با موفقیت ثبت شد");
        return redirect(route("products.index"));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::firstOrFail($id);
        return view('panel.product.index', compact($product));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $productOptions = ProductOption::pluck("name", "id");
//        $warranties= Warranty::with('company')->get();
        $product = Product::with(["media", "medias"])->findorfail($id);
        $categories = Category::with('childrens')->where("parent_id", null)->get();
        $brands = Brand::pluck("name", "id");
        $types = ProductType::pluck("name", "id");

        $currentCategories = $product->categories->pluck("id")->toArray();

        $features = Feature::with(["feature_value" => function ($query) use ($id) {
            $query->where("product_id", $id);
        }])->where("product_type_id", $product->type)->get();

        $product_option_product = Product_option_product::where('product_id', $product->id)->get();

        return view("panel.products.edit", compact('categories', 'brands', 'product', 'types', 'features', 'currentCategories', 'productOptions', 'product_option_product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductRequest $request, $id)
    {
//        dd($request);
        $request->request->add(["keywords" => $request->meta]);
        if (isset($request->status))
            $request->request->add(['status' => 1]);
        else
            $request->request->add(['status' => 0]);
        if (isset($request->featured))
            $request->request->add(['featured' => 1]);
        else
            $request->request->add(['featured' => 0]);

        $product = Product::findorfail($id);
        $product->update($request->all());

        if (isset($request->galleryImages) && !empty($request->galleryImages) && is_array($request->galleryImages) && isset($request->galleryImages[0]) && $request->galleryImages[0] != null) {
            $product->medias()->detach();
            $product->medias()->attach($request->galleryImages);
        }

        $product->categories()->detach();
        $product->categories()->attach($request->categories);
//        $product->warranties()->detach();
//        $product->warranties()->attach($request->warranties);

        $features = Feature::where("product_type_id", $product->type)->get();

        foreach ($features as $feature) {
            $featureInput = "feature_value" . $feature->id;
            $featureValue = Feature_value::where('feature_id', $feature->id)->where('product_id', $id);
            if ($featureValue->count() > 0) {
                $featureValue->update(['value' => $request->{$featureInput}]);
            } else {
                Feature_value::create(['product_id' => $product->id, 'value' => $request->{$featureInput}, 'feature_id' => $feature->id]);
            }
        }


//        dd($request->priceOption);

        if ($request->has("countOption") && $request->has("priceOption") && $request->has("option1") && $request->has("option2")) {
            for ($i = 0; $i < count($request->priceOption); $i++) {
                $Product_option_product = Product_option_product::where('product_id', $product->id)->where('product_option_id_2', $request->option2[$i])->where('product_option_id_1', $request->option1[$i])->where('price', $request->priceOption[$i])->where('count', $request->countOption[$i]);
                if ($Product_option_product->count() > 0) {
                    $Product_option_product->update(["product_option_id_1" => $request->option1[$i], "product_option_id_2" => $request->option2[$i], "price" => $request->priceOption[$i], "count" => $request->countOption[$i]]);
                } else {
                    if (!is_null($request->option1[$i]) && !is_null($request->priceOption[$i]) && !is_null($request->countOption[$i]) && !is_null($request->option2[$i])) {
                        Product_option_product::create(["product_id" => $product->id, "product_option_id_1" => $request->option1[$i], "product_option_id_2" => $request->option2[$i], "price" => $request->priceOption[$i], "count" => $request->countOption[$i]]);
                    }
                }
            }
        }

        session()->flash("ok", "محصول با موفقیت ویرایش شد");
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Product::find($id)->delete();
        session()->flash("ok", "محصول با موفقیت حذف شد");
        return redirect()->back();
    }
}
