<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Model\ProductOption;
use App\Model\ProductOptionCategory;
use App\Http\Controllers\Controller;

class ProductOptionController extends Controller
{

    public function getTypeFromOption(Request $request){

        if(!$request->has("id")){
            abort(404);
        }

        $productOptionCategory = ProductOptionCategory::findorfail($request->id);

        return $productOptionCategory;

        if(!$productOptionCategory){
            abort(404);
        }


        return response(["type"=>$productOptionCategory->type]);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        dd("1");
        $productOptions = ProductOption::with('media')->with('productoptioncategory')->get();
//        dd($productOptions);
        return view('panel.productOptions.index', compact('productOptions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories=ProductOptionCategory::all()->pluck('name','id');
        return view('panel.ProductOptions.create',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);
        return $request;
        ProductOption::create($request->all());
        session()->flash("ok", "دشته بندی آپشن های محصولات با موفقیت ایجاد شد");
        return redirect(route("productOptions.index"));

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $ProductOptionCategories = ProductOptionCategory::pluck('name','id');

        $ProductOption = ProductOption::with('category')->findOrFail($id);

        $ProOpCat = $ProductOption->category;
//        return $ProOpCat->id ;
        return view('panel.productOptions.edit', compact('ProductOptionCategories','ProductOption','ProOpCat'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
//        return $request;
        ProductOption::find($id)->update($request->all());
        session()->flash('ok', "با موفقیت به روز رسانی شد");
        return redirect(route('productOptions.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ProductOption::find($id)->delete();
        session()->flash('ok', "با موفقیت به حذف شد");
        return redirect(route('productOptions.index'));
    }
}
