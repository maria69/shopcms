<?php

namespace App\Http\Controllers\Admin;

use App\Model\Company;
use App\Model\Warranty;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class WarrantyController extends Controller
{
    public function index()
    {
        $warranties = Warranty::with('company')->get();
        return view('panel.warranties.index', compact('warranties'));
    }

    public function create()
    {
        $companies = Company::all()->pluck('name','id');
        return view('panel.warranties.create', compact('companies'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'expire_at' => 'required',
            'company_id' => 'required|exists:companies,id'
        ]);
        Warranty::create($request->all());
        session()->flash("ok", "گارانتی با موفقیت ایجاد شد");
        return redirect(route("warranties.index"));

    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $company = Warranty::with("company")->find($id);
        $companies = Company::all()->pluck('name','id');
        return view("panel.warranties.edit", compact('company', 'companies'));

    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'expire_at' => 'required',
            'company_id' => 'exists:companies,id'
        ]);
        Warranty::find($id)->update($request->all());
        session()->flash("ok", "گارانتی با موفقیت به روز رسانی شد");
        return redirect(route('warranties.index'));
    }

    public function destroy($id)
    {
        Warranty::find($id)->delete();
        session()->flash("ok", "گارانتی با موفقیت حذف شد");
        return redirect(route('warranties.index'));

    }
}