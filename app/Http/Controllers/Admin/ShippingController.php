<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\Model\Shipping;


class ShippingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $shippings = Shipping::all();
        return view('panel.shippings.index', compact('shippings'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("panel.shippings.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:3|max:120',
            'media_id' => 'nullable|exists:media,id',
            'price' => 'required|integer',
        ]);
        shipping::create($request->all());
        session()->flash("ok", "شیوه حمل و نقل با موفقیت ایجاد شد");
        return redirect(route('shippings.index'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $shipping = shipping::with("media")->find($id);
        return view("panel.shippings.edit", compact('shipping'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|min:3|max:40',
            'media_id' => 'nullable|exists:media,id'
        ]);
        Shipping::find($id)->update($request->all());
        session()->flash("ok", "شیوه حمل و نقل با موفقیت بروز رسانی شد");
        return redirect(route('shippings.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Shipping::find($id)->delete();
        session()->flash("ok", "شیوه حمل و نقل با موفقیت حذف شد");
        return redirect(route('shippings.index'));

    }
}
