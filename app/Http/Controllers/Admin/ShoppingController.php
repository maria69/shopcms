<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\Model\Shopping;


class ShoppingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $shoppings = Shopping::all();
        return view('panel.shoppings.index', compact('shoppings'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("panel.shoppings.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|min:3|max:120',
            'media_id' => 'nullable|exists:media,id',
        ]);
        Shopping::create($request->all());
        session()->flash("ok", "شیوه حمل و نقل با موفقیت ایجاد شد");
        return redirect(route('shoppings.index'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $shopping = Shopping::with("media")->find($id);
        return view("panel.shoppings.edit", compact('shopping'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|min:3|max:40',
            'media_id' => 'nullable|exists:media,id'
        ]);
        Shopping::find($id)->update($request->all());
        session()->flash("ok", "شیوه حمل و نقل با موفقیت بروز رسانی شد");
        return redirect(route('shoppings.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Shopping::find($id)->delete();
        session()->flash("ok", "شیوه حمل و نقل با موفقیت حذف شد");
        return redirect(route('shoppings.index'));

    }
}
