<?php

namespace App\Http\Controllers\Admin;

use App\Model\Order;
use App\Model\OrderItem;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index()
    {
        //

        $orders = Order::with(['orderItems'])->first();
//        return $orders;

//        return $orders->shipping_info->name;
//        $orders = OrderItem::with(['product'])->get();
//        $orders = OrderItem::all();
//        return $orders;
        return view('panel.orders.index',compact('orders'));


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

        $orders = Order::with(['orderItems'])->where('id',$id)->first();
//        $orders = OrderItem::where('order_id',$id)->get();

//        return $orders;

//        return $orders->product->categories;
        return view('panel.orders.orderitem',compact('orders'));


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getAll(Request $request){


        $columns = array(
            0 => 'id',
            1 => 'number',
            2 => 'username',
//            3 => 'count',
            4 => 'payment',
            5 => 'shipping',
            6 => 'offer',
            7 => 'price',
            8 => 'date',
//           9 => 'address',
            10 => 'options',
        );


        $i = 1;

        $totalData = Order::count();
        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');


        $search = $request->input('search.value');

        if ($search) {

            $order = Order::where('number', 'LIKE', "%{$search}%")->pluck('id');


            $orders = Order::where('number', 'LIKE', "%{$search}%")
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();

            $totalFiltered = Order::where('number', 'LIKE', "%{$search}%")->count();

        } else {

            $orders = Order::offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();

            $totalFiltered = Order::count();
        }


        $data = array();


        if (!empty($orders)) {
            foreach ($orders as $order) {


                $nestedData['id'] = $order->id;
                $nestedData['number'] = $order->number;
                $nestedData['username'] = $order->user_info->first_name . $order->user_info->last_name;
//                $nestedData['count'] = $order->count;
                $nestedData['payment'] = $order->payment_info ? $order->payment_info->name : '';
                $nestedData['shipping'] = $order->shipping_info ? $order->shipping_info->name: '';
//                $nestedData['count'] = $order->count;
                $nestedData['offer'] = $order->offerPrice;
                $nestedData['price'] = $order->finalPrice;
                $nestedData['date'] = jdate()->forge($order->created_at)->format('%A, %d %B %y');
//                $nestedData['address'] = $order->order_info->address;

//                $nestedData['options'] = "";

                $nestedData['options'] = "<div class='btn-group'> 

              <a data=$order->id class='operation-icon fa fa-eye show_product btn btn-success' href='./order/$order->id' aria-hidden='true'   title='مشاهده جزئیات'></a>

             <a  data=$order->id type='submit' class='operation-icon fa fa-trash-o delete_product btn btn-warning' title='حذف' onclick='del_flight(this)'></a>

           </div>";


                $data[] = $nestedData;

            }
        }


//        $data = Airline::all();
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );


//        $data =  array($airline);

//        return response()->json(['data' =>array($data)]);
        echo json_encode($json_data);
//        echo json_encode();
    }
    public function getorderItems(Request $request){


        $columns = array(
            0 => 'id',
            1 => 'number',
            2 => 'username',
//            3 => 'count',
            4 => 'payment',
            5 => 'shipping',
            6 => 'offer',
            7 => 'price',
            8 => 'date',
           9 => 'address',
            10 => 'options',
        );


        $i = 1;

        $totalData = Order::count();
        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');


        $search = $request->input('search.value');

        if ($search) {

            $order = Order::where('number', 'LIKE', "%{$search}%")->pluck('id');


            $orders = Order::where('number', 'LIKE', "%{$search}%")
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();

            $totalFiltered = Order::where('number', 'LIKE', "%{$search}%")->count();

        } else {

            $orders = Order::offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();

            $totalFiltered = Order::count();
        }


        $data = array();


        if (!empty($orders)) {
            foreach ($orders as $order) {


                $nestedData['id'] = $order->id;
                $nestedData['number'] = $order->number;
                $nestedData['username'] = $order->user_info->first_name . $order->user_info->last_name;
                $nestedData['count'] = $order->count;
                $nestedData['payment'] = $order->payment_info ? $order->payment_info->name : '';
                $nestedData['shipping'] = $order->shipping_info ? $order->shipping_info->name: '';
                $nestedData['count'] = $order->count;
                $nestedData['offer'] = $order->offerPrice;
                $nestedData['price'] = $order->totalPrice;
                $nestedData['date'] = $order->date_order_create;
                $nestedData['address'] = $order->date_order_create;

                $nestedData['options'] = "";

                $nestedData['options'] = "<div class='btn-group'> 

              <a data=$order->id class='operation-icon fa fa-eye show_product btn btn-success' href='./order/$order->id' aria-hidden='true'   title='مشاهده جزئیات'></a>

             <a  data=$order->id type='submit' class='operation-icon fa fa-trash-o delete_product btn btn-warning' title='حذف' onclick='del_flight(this)'></a>

           </div>";


                $data[] = $nestedData;

            }
        }


//        $data = Airline::all();
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );


//        $data =  array($airline);

//        return response()->json(['data' =>array($data)]);
        echo json_encode($json_data);
//        echo json_encode();
    }


}
