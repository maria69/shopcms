<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Model\FeatureCategory;
use App\Http\Controllers\Controller;

class FeatureCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $fCategories= FeatureCategory::all();
        return view('panel.featuresCategories.index',compact('fCategories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('panel.featuresCategories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:2|max:120',

        ]);
        $fCategory= FeatureCategory::create($request->all());
        session()->flash('ok','دسته بندی با موفقیت ایجاد شد');
        return redirect(route('feature_categories.index'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $fCategory= FeatureCategory::findOrFail($id);
        return view('panel.featuresCategories.edit',compact('fCategory'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|min:2|max:120',

        ]);
        FeatureCategory::find($id)->update($request->all());
        session()->flash("ok", " دسته بندی ویژگی محصول با موفقیت ویرایش شد");
        return redirect(route('feature_categories.index'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        FeatureCategory::find($id)->delete();
        session()->flash("ok", " دسته بندی ویژگی محصول با موفقیت حذف شد");
        return redirect(route('feature_categories.index'));

    }
}
