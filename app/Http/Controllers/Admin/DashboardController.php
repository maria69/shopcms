<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Category;
use App\Model\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
//use Intervention\Image;
use Intervention\Image\Facades\Image as Image;
use Illuminate\Support\Facades\File as File;

class DashboardController extends Controller
{
    public function index(){
        $productCount= Product::all()->count();
        $categoryCount= Category::all()->count();
        return view("panel.dashboard_admin",compact('productCount','categoryCount'));
    }

    protected function profileValidator(array $data)
    {
        return Validator::make($data, [
            "name"=>"required|min:3|max:200",
            "last_name"=>"required|min:3|max:200",
            "email"=>"required|email|unique:admins,email,".Auth::guard("admins")->user()->id,
        ]);
    }

    public function profile(){
        $user=Auth::guard("admins")->user();
        return view("panel.common.profile",compact("user"));
    }

    public function profileStore(Request $request){
        $this->profileValidator($request->all())->validate();
        Session::flash("ok","حساب کاربری با موفقیت ویرایش شد");
        return redirect()->back();
    }


    public function password(){
        return view("panel.common.password");
    }


    public function passwordStore(PasswordRequest $request){
        $input = $request->only(['password','oldpass']);
        $user=$this->getUser();
        if(Hash::check($input['oldpass'], $user->password)){
            $user->update(['password'=>$input['password']]);
            Session::flash('ok','گذرواژه با موفقیت تغییر کرد');
        }else{
            Session::flash('error','رمز عبور فعلی به درستی وارد نشده است');
        }
        return redirect('/dashboard/password');
    }




    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            "name"=>"required|min:2|max:200",
            "last_name"=>"required|min:2|max:200",
            "father_name"=>"required|min:2|max:200",
            "address"=>"required|min:10|max:6000",
            "email"=>"required|email|unique:users,email,".Auth::guard()->user()->id,
            "mobile"=>"required|numeric|unique:users,mobile,".Auth::guard()->user()->id."|digits:11",
            "phone"=>"required|numeric",
            "melli_code"=>"required|numeric|digits:10",
            "birth_certificate_code"=>"required|numeric|digits_between:1,10",
            "birth_certificate_issuance"=>"required|min:3|max:200",
            "day"=>"required|integer|min:1|max:31",
            "month"=>"required|integer|min:1|max:12",
            "year"=>"required|integer|min:1300|max:1420",
            "tnc"=>"required|in:1",
        ]);
    }


    public function avatar(){

        $user = Auth::user();
        return view("panel.common.avatar",compact('user'));
    }

    public function avatarStore(Request $request){

//        $user=$this->getUser();
        $user = Auth::user();
//return $user;
        if($user->avatar != "default.png"){
            File::delete(public_path()."/avatars/".$user->avatar);
        }
        $name="default.png";
        if($request->hasFile("avatar")){
            $file = $request->file("avatar");
            $img = Image::make($file->getRealPath());
            $img->resize(150, 150);
            $name = md5("iik".microtime()."nj".rand(999,rand(367,4945))).".".$file->getClientOriginalExtension();
            if(!$img->save(public_path()."/avatars/".$name)){
                \Session::flash("error","مشکلی در آپلود اواتار پیش آمده است لطفا دوباره تلاش نمایید");
                return redirect()->back();
            }
            //chmod(public_path()."/avatars/".$name,777);
        }

        $user->update(["avatar"=>$name]);

        \Session::flash("ok","تصویر پروفایل با موفقیت تغییر کرد");
        return redirect()->back();
    }
}
