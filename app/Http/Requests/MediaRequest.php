<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MediaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];
        $files=$this->only('uploads');
        $count=0;
        if(isset($files['uploads'])){
            $count=count($files['uploads']);
        }
        if($count>=0){
            for ($i=0;$i<$count;$i++){
                $rules['uploads.' . $i] = 'max:30000|mimes:png,jpg,jpeg,gif,mpga,mp3,mp4,pdf,docx,doc';
            }
        }
        return $rules;
    }

}