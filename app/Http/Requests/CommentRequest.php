<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Model\Star;

class CommentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules =  [
            'body'=>'required|min:3|max:2000',
            'weaknesses'=>'nullable|array',
            'strengths'=>'nullable|array',
        ];

        $stars = Star::select("id")->get();

        foreach($stars as $star) {
            $p = "stars" . $star->id;
            $rules[$p] = "numeric|max:5|min:0";
        }

        return $rules;
    }
}
