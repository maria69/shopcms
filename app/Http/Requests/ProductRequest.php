<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3|max:100',
            'en_name' => 'required|min:3|max:100',
            'price' => 'required|integer|min:0',
            'media_id' => 'required|exists:media,id',
            'brand_id' => 'required|exists:brands,id',
            'categories' => 'required|exists:categories,id',
            'weight' => 'nullable|numeric|min:0',
            'width' => 'nullable|numeric|min:0',
            'height' => 'nullable|numeric|min:0',
            'off_id' => 'nullable',
            'short_description' => 'required|max:65000',
            'description' => 'required|max:65000',
            'meta' => 'required|max:255',
        ];
    }
}
