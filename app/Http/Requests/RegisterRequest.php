<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name'=>'required|max:250',
            'last_name'=>'required|max:250',
            'code_melli'=>'required|max:10',
            'email'=>'required|email',
            'mobile'=>'required|max:12|min:10',
            'phone'=>'required|max:12|min:10',
            'password'=>'required|confirmed|min:8',
        ];
    }
}
